<?php get_header(); ?>


	<div class="singleDetail-wrap">
		<div class="singleDetail-wrap">
			
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			
			<div class="wrapper">
				<div class="postROw">
					<div class="postInfoRight">
						<div class="w-blogpost-preview">
							<?php echo the_post_thumbnail(); ?>
						</div>
					</div>
				</div>
				<div class="postDetail">
					<h2><?php the_title();?></h2>
					<?php the_content(); ?>
				</div>
			</div>
			
		</div><?php endwhile;  endif; ?>
	</div>

<?php get_footer(); ?>