<?php
/**
 * The template for displaying the about-us page
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="hero-section">
				<div class="hero-section__img" style="background-position: right center; background-image: url(<?php the_field('hero_image', 15) ?>)"></div>
				<div class="hero-section__content hero-section__content--inverse">
					<h1>Courses</h1>
				</div>
			</section>
			<?php if ( have_posts() ) : ?>
				<section class="courses">
					<?php while ( have_posts() ) : the_post(); ?>
						<div class="course-list-item">
				      <div class="course-list-item__img" style="background-image: url(<?php the_post_thumbnail_url() ?>)"></div>
							<div class="course-list-item__content">
								<h3><?php the_title() ?></h3>
								<p>Coach: <a href="<?php echo site_url('/coaches/'.get_field('coach')['ID']); ?>"><?php echo get_field('coach')['display_name']; ?></a></p>
					      <p>Price: $<?php the_field('_pmproap_price') ?></p>
					      <time><?php the_field('date_time') ?> PST</time>
								<a href="<?php the_permalink($course->ID) ?>" class="btn btn--primary btn--small">View Details</a>
							</div>
						</div>
					<?php endwhile ?>
				</section>
			<?php endif ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
