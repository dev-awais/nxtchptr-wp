<?php
/**
 * NxtChptr functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package NxtChptr
 */

remove_action( 'shutdown', 'wp_ob_end_flush_all', 1 );

if ( ! function_exists( 'nxtchptr_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function nxtchptr_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on NxtChptr, use a find and replace
		 * to change 'nxtchptr' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'nxtchptr', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'nxtchptr' ),
			'menu-2' => esc_html__( 'Footer', 'nxtchptr' )	
		) );

		// Customize Buddypress Nav menu
		function nxtchptr_customize_bp_nav() {
	    buddypress()->members->nav->add_nav( array(
        'name' => __( 'Test Results', 'nxtchptr' ), 
        'slug' => 'personal-area/test-results', 
        'css_id' => 'test-results', 
        'link' => site_url('personal-area/test-results'), 
        'show_for_displayed_user' => 1,
        'position' => 10,
	    ));

	    buddypress()->members->nav->add_nav( array(
        'name' => __( 'Community Board', 'nxtchptr' ), 
        'slug' => 'community-board', 
        'css_id' => 'community-board', 
        'link' => site_url( 'community-board' ), 
        'show_for_displayed_user' => 1,
        'position' => 50,
      ));

      buddypress()->members->nav->edit_nav( array(
        'position' => 20,
        'name' => __('Peer Matching', 'nxtchptr'),
        'show_for_displayed_user' => 1,
      ), 'groups' );


      buddypress()->members->nav->edit_nav( array(
        'position' => 30,
        'name' => __('My Account', 'nxtchptr'),
        'show_for_displayed_user' => 1,
      ), 'profile' );

      buddypress()->members->nav->edit_nav( array(
        'position' => 40,
        'name' => __('Messages', 'nxtchptr'),
        'show_for_displayed_user' => 1,
	    ), 'messages' );

		  /*
		  buddypress()->members->nav->add_nav( array(
        'name' => __('My Account', 'nxtchptr'),
        'slug' => 'members/'.bp_core_get_username( get_current_user_id() ),
        'css_id' => 'my-account', 
        'link' => profile_link(), 
        'show_for_displayed_user' => 1,
		  	'position' => 30
	    ));

		  buddypress()->members->nav->delete_nav( 'profile' );*/


		}
		add_action( 'bp_setup_nav', 'nxtchptr_customize_bp_nav', 100 );


		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'nxtchptr_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		// Add Google fonts
		function load_fonts() {
   		wp_register_style('googleFonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700,800');
   		wp_enqueue_style('googleFonts');
		}
		add_action('wp_enqueue_scripts', 'load_fonts');

		if( !is_super_admin() ) {
			add_filter( 'show_admin_bar', '__return_false' );
		}
	}
endif;
add_action( 'after_setup_theme', 'nxtchptr_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function nxtchptr_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'nxtchptr_content_width', 640 );
}
add_action( 'after_setup_theme', 'nxtchptr_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function nxtchptr_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'nxtchptr' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'nxtchptr' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'nxtchptr_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function nxtchptr_scripts() {
	wp_enqueue_style( 'slick-style', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css');

	wp_enqueue_style( 'chosen', get_stylesheet_directory_uri() . '/lib/chosen/chosen.min.css');

	wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
	
  wp_enqueue_style( 'flatpickr', 'https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css');

	wp_enqueue_style( 'nxtchptr-style', get_stylesheet_uri(), [], filemtime(get_stylesheet_directory() . '/style.css'));

	wp_enqueue_script( 'nxtchptr-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'nxtchptr-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'slick', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', array('jquery'));

	wp_enqueue_script( 'chosen', get_stylesheet_directory_uri() . '/lib/chosen/chosen.jquery.min.js', array('jquery'));
	
  wp_enqueue_script( 'sortable', get_stylesheet_directory_uri() . '/lib/sortable.js');

  wp_enqueue_script( 'jstz', get_stylesheet_directory_uri() . '/lib/jstz.min.js');
	
  wp_enqueue_script( 'flatpickr', 'https://cdn.jsdelivr.net/npm/flatpickr');
	
	wp_enqueue_script( 'nxtchptr-main', get_template_directory_uri() . '/js/main.js', array('jquery', 'slick'));

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'nxtchptr_scripts', '999' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

// function has_membership() {
// 	return pmpro_hasMembershipLevel(array(2,3,4,5));
// }

function has_membership() {
  //bail if PMPRo is not active
  if(function_exists('pmpro_has_membership_access')){
    return pmpro_hasMembershipLevel(array(2,3,4,5));
  }
  else{
    echo 'asdas';
  }
  
}

if(function_exists('pmpro_hasMembershipLevel') && pmpro_hasMembershipLevel()) {
  //this is a member, do something cool
}

function include_page($slug, $member = false) {
	include "pages/$slug-" . ($member ? 'member' : 'guest') . '.php';
}

function get_link_by_slug($slug, $type = 'page'){
  $post = get_page_by_path($slug, OBJECT, $type);
  return get_permalink($post->ID);
}

function subscription_plans($heading = '') {
	$subscriptions_heading = $heading; 
	include 'pages/includes/subscription-plans.php' ;
}

// Add page slug to body class
function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );


function wsl_use_fontawesome_icons( $provider_id, $provider_name, $authenticate_url ) {
   ?>
   <a 
      rel           = "nofollow"
      href          = "<?php echo $authenticate_url; ?>"
      data-provider = "<?php echo $provider_id ?>"
      class         = "btn btn--primary" 
    >
      <span>
         <i class="fa fa-<?php echo strtolower( $provider_id ); ?>"></i>
         <?php echo $provider_name; ?>
      </span>
   </a>
<?php
}
 
add_filter( 'wsl_render_auth_widget_alter_provider_icon_markup', 'wsl_use_fontawesome_icons', 10, 3 );

/**
	* When registering, add the member to a specific membership level 
	* @param integer $user_id
**/
//Disables the pmpro redirect to levels page when user tries to register
add_filter("pmpro_login_redirect", "__return_false");
function my_pmpro_default_registration_level($user_id) {
	//Give all members who register membership level 1
	pmpro_changeMembershipLevel(1, $user_id);
}
add_action('user_register', 'my_pmpro_default_registration_level');


/*function modify_social_scopes($scope, $provider) {
	if($provider == 'Facebook'){
		//$scope .= ', user_photos';
	}
	return $scope;
}
add_filter( 'wsl_hook_alter_provider_scope', 'modify_social_scopes', 10, 2 );*/

function validate_social_account($provider_name) {
  include_once( WORDPRESS_SOCIAL_LOGIN_ABS_PATH . '/hybridauth/Hybrid/Auth.php' );
  try {
    $provider = Hybrid_Auth::getAdapter( $provider_name ); 
		if($provider_name == 'Facebook'){
    	$response = $provider->api()->get( '/me/friends', $provider->token('access_token'));
    	$connections_count = $response->getDecodedBody()['summary']['total_count'];
		}elseif($provider_name == 'LinkedIn'){
      $response = $provider->adapter->api->profile('~:(num-connections)');
      $connections_count = $response['linkedin'];
		}elseif($provider_name == 'Instagram'){
      $response = $provider->adapter->api->api('users/self');
      $connections_count = $response->data->counts->followed_by;
		}elseif($provider_name == 'Goodreads'){
      $provider = Hybrid_Auth::getAdapter( $provider ); 
      $id = $provider->adapter->getUserProfile()->identifier;
      $response = $provider->adapter->api->get( 'http://www.goodreads.com/friend/user/'.$id.'?format=xml' );
			$response = @ new SimpleXMLElement( $response );
      $connections_count = $response->friends->attributes()->total;
   	}
  }
  catch( Exception $e )
  {
    echo "Ooophs, we got an error: " . $e->getMessage();
  }
  
  // Remove 'social_login_insufficient_activity' from query string, in case the request is successful
  if(isset( $_REQUEST[ 'redirect_to' ] ) ){
  	$string = parse_url($_REQUEST['redirect_to'], PHP_URL_QUERY);
  	if($string){
  		$query = parse_str($string, $parts);
  		unset($parts['social_login_insufficient_activity']);
  		$_REQUEST['redirect_to'] = strtok($_REQUEST['redirect_to'], '?') . http_build_query($parts);
  	}
	}

  if(isset($connections_count) && $connections_count < 50){
		$redirect_to = isset( $_REQUEST[ 'redirect_to' ] ) ? $_REQUEST[ 'redirect_to' ] : home_url();
		$redirect_to .= '?social_login_insufficient_activity=true';
		wp_redirect($redirect_to);
		exit;
  }

  return $provider;
}

//add_filter('wsl_hook_process_login_after_hybridauth_authenticate', 'validate_social_account');


function check_for_failed_registration() {
	if(isset($_GET['social_login_insufficient_activity'])){
		?>
		<script type="text/javascript">
			(function($){
				show_modal($('#login'));
			})(jQuery);
		</script>
		<?php
	}
}
add_action('wp_footer', 'check_for_failed_registration', 50);


function profile_link() {
	return site_url("members/".bp_core_get_username( get_current_user_id() ));
}

function nxtchptr_filter_wpjb_fulltext_min_chars() {
	return 3;
}

add_filter( 'wpjb_fulltext_min_chars', 'nxtchptr_filter_wpjb_fulltext_min_chars');

function nxtchptr_bp_get_nav_menu_items($menus) {
	$permalink = rtrim(get_permalink(), '/');
	foreach($menus as $menu){
		if(strpos($menu->link, 'http') === false ){
			$menu->link = bp_loggedin_user_domain() . $menu->link;
		}
		if(strpos($permalink, rtrim($menu->link, '/')) !== false) {
			$menu->class[] = 'current-menu-parent';	
		}
	}
	return $menus;
}
add_filter( 'bp_get_nav_menu_items', 'nxtchptr_bp_get_nav_menu_items' );

function nxtchptr_wpjb_breadcrumbs() {
	return '';
}
add_filter( 'wpjb_breadcrumbs', 'nxtchptr_wpjb_breadcrumbs' );

function nxtchptr_pmprorh_init()
{
  //don't break if Register Helper is not loaded
  if(!function_exists("pmprorh_add_registration_field"))
  {
    return false;
  }

  // Only show extra fields the first time someone checks out
  if(has_membership()) {
    return false;
  }

  //define the fields
  $fields = array();
  $fields[] = new PMProRH_Field(
    "referral",                     // input name, will also be used as meta key
    "text",                         // type of field
    array(
      "label" => "Referral Code",   // custom field label
      "profile" => "admins"         // only show in profile for admins
    )
  );

  $fields[] = new PMProRH_Field(
    "terms_agreement",                     // input name, will also be used as meta key
    "checkbox",                         // type of field
    array(
      "label" => "Terms and Conditions Agreement",   // custom field label
      "profile" => "admins",
      "required" => true,
      "text" => 'By checking this box, you agree to the <a href="'.site_url('terms-and-conditions').'">Nxt Chptr Terms and Conditions</a>'         // only show in profile for admins
    )
  );

  foreach($fields as $field) {
    pmprorh_add_registration_field(
      "checkout_boxes",
      $field           
    );
  }

}
add_action("init", "nxtchptr_pmprorh_init");

//checkout boxes
function nxtchptr_pmprorh_pmpro_checkout_boxes()
{
  global $pmprorh_registration_fields, $pmprorh_checkout_boxes; 
  
  foreach($pmprorh_checkout_boxes as $cb)
  {
    //how many fields to show at checkout?
    $n = 0;   
    if(!empty($pmprorh_registration_fields[$cb->name]))
      foreach($pmprorh_registration_fields[$cb->name] as $field)        
        if(pmprorh_checkFieldForLevel($field) && (!isset($field->profile) || (isset($field->profile) && $field->profile !== "only" && $field->profile !== "only_admin")))   $n++;

    if($n > 0)
    {
      ?>
      <div id="pmpro_checkout_box-<?php echo $cb->name; ?>" class="pmpro_checkout">
        <h2>  
          <span class="pmpro_thead-name"><?php echo $cb->label;?></span>
        </h2>
        <div class="pmpro_checkout-fields">
        <?php if(!empty($cb->description)) {  ?><div class="pmpro_checkout_decription"><?php echo $cb->description; ?></div><?php } ?>
        <?php
        foreach($pmprorh_registration_fields[$cb->name] as $field)
        {     
          if(pmprorh_checkFieldForLevel($field) && (!isset($field->profile) || (isset($field->profile) && $field->profile !== "only" && $field->profile !== "only_admin")))
            echo "<div>";
              $field->displayAtCheckout();    
            echo "</div>";
        }
        ?>
        </div> <!-- end pmpro_checkout-fields -->
      </div> <!-- end pmpro_checkout_box-name -->
      <?php
    }
  }
}
remove_action("pmpro_checkout_boxes", "pmprorh_pmpro_checkout_boxes");
add_action("pmpro_checkout_boxes", "nxtchptr_pmprorh_pmpro_checkout_boxes");

if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page(array(
    'page_title'  => 'Theme General Settings',
    'menu_title'  => 'Theme Settings',
    'menu_slug'   => 'theme-general-settings',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));

}

function nxtchptr_bp_get_users_by_xprofile( $field_id, $value ) {
  global $wpdb;

  $user_ids = $wpdb->get_col(
    $wpdb->prepare(
      "SELECT `user_id`
        FROM `{$wpdb->prefix}bp_xprofile_data`
        WHERE `field_id` = %d AND `value` = %s", 
      $field_id,
      $value
    )
  );

  return $user_ids;
}

function nxtchptr_unique_id () {
  $try = 0;
  do{
    $id = 'NXT';
    for ($i=0; $i < 8; $i++) { 
      $id .= rand(0,9);
    }
    $try++;
  }while($try < 10 && count(nxtchptr_bp_get_users_by_xprofile('2', $id)) > 0);
  return $id;
}

// Add Unique ID to new signups
function nxtchptr_add_profile_unique_id ( $user_id) {
  xprofile_set_field_data('2', $user_id, nxtchptr_unique_id());
}
add_filter( 'bp_core_signup_user', 'nxtchptr_add_profile_unique_id' );
add_filter( 'wsl_hook_process_login_after_wp_insert_user', 'nxtchptr_add_profile_unique_id' );

// Prevent Unique ID from being edited
function nxtchptr_bp_hide_profile_edit( $retval ) {  
  // remove field from edit tab
  if( bp_is_user_profile_edit() ) {   
    $retval['exclude_fields'] = '2'; // field ID's separated by comma
  } 
  
  return $retval; 
}
add_filter( 'bp_after_has_profile_parse_args', 'nxtchptr_bp_hide_profile_edit' );

function nxtchptr_give_test_results_pmpro_after_checkout($user_id) {
  if(pmpro_hasMembershipLevel(4, $user_id)){
    $test_results_id = 48;
    pmproap_addMemberToPost($user_id, $test_results_id);
  }
}
add_action( 'pmpro_after_checkout', 'nxtchptr_give_test_results_pmpro_after_checkout' );

function pmproap_add_careerleader_authorization($user_id, $post_id)
{
  if ($post_id == 48) {
    careerleader_authorize_user($user_id);
  }
}

add_action('pmproap_action_add_to_package', 'pmproap_add_careerleader_authorization', 10, 2);

function ec_mail_name( $email ){
  return 'Nxt Chptr'; // new email name from sender.
}
add_filter( 'wp_mail_from_name', 'ec_mail_name' );
function ec_mail_from ($email ){
  return 'fjain@nxt-chptr.com'; // new email address from sender.
}
add_filter( 'wp_mail_from', 'ec_mail_from' );

function nxtchptr_fb_title($title){
  return is_front_page() ? 'NxtChptr' : $title;
}
add_filter( 'fb_og_title', 'nxtchptr_fb_title' );


//add social login to the checkout page
function nxtchptr_pmprosl_pmpro_user_fields() {
  global $pmpro_level, $pmpro_error_fields;
  $hide_social_login = get_option("level_" . $pmpro_level->id . "_hide_social_login");
  if(empty($hide_social_login) && !is_user_logged_in())
  {
    ?>
    <style>#pmpro_user_fields {display: none; }</style>
    <div id="pmpro_social_login" class="pmpro_checkout">
      <?php if(!empty($pmpro_error_fields)): ?>
        <div id="pmpro_message" class="pmpro_message pmpro_error">Please sign up below in order to continue.</div>
      <?php endif; ?>
      <p>Sign up with Facebook to create an account</p>
      <?php echo do_shortcode( '[wordpress_social_login]' ); ?>
    </div>
    <?php
  }
}
remove_action('pmpro_checkout_after_pricing_fields', 'pmprosl_pmpro_user_fields');
add_action('pmpro_checkout_after_pricing_fields','nxtchptr_pmprosl_pmpro_user_fields');

add_filter( 'dynamic_sidebar_params', 'nxtchptr_add_widget_classes' );

/**
 * Add one or more classes to the WordPress search form's 'Search' button
 * @author Alain Schlesser (alain.schlesser@gmail.com)
 * @link http://www.brightnucleus.com/add-class-wordpress-search-button/
 *
 * @param  string  $form   the search form HTML output
 * @return string          modified version of the search form HTML output
 *
 * @see http://codex.wordpress.org/Function_Reference/get_search_form
 * @see http://developer.wordpress.org/reference/hooks/get_search_form/
 */
function nxtchptr_adapt_search_form( $form ) {
  $form = str_replace(
      'search-submit',
      'search-submit btn btn--primary',
      $form
  );
  // return the modified string
  return $form;
}
// run the search form HTML output through the newly defined filter
add_filter( 'get_search_form', 'nxtchptr_adapt_search_form' );


/**
 * Extend WordPress search to include custom fields
 *
 * Join posts and postmeta tables
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */

function cf_search_join( $join ) {
    global $wpdb;

    if ( is_search() ) {    
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }

    return $join;
}
add_filter('posts_join', 'cf_search_join' );

/**
 * Modify the search query with posts_where
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
function cf_search_where( $where ) {
    global $pagenow, $wpdb;

    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }

    return $where;
}
add_filter( 'posts_where', 'cf_search_where' );

/**
 * Prevent duplicates
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
function cf_search_distinct( $where ) {
    global $wpdb;

    if ( is_search() ) {
        return "DISTINCT";
    }

    return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );

function nxt_chptr_course_addons($types) {
  $types[] = 'courses';
  return $types;
}
add_filter( 'pmproap_supported_post_types', 'nxt_chptr_course_addons' );

function nxt_chptr_meta_wrapper()
{
  //duplicate this row for each CPT
  add_meta_box('pmpro_page_meta', 'Require Membership', 'pmpro_page_meta', 'courses', 'side');  
}
function nxtchptr_cpt_init()
{
  if (is_admin())
  {
    add_action('admin_menu', 'nxt_chptr_meta_wrapper');
  }
}
add_action("init", "nxtchptr_cpt_init", 20);

/**
 * Add the wp-editor back into WordPress after it was removed in 4.2.2.
 *
 * @see https://wordpress.org/support/topic/you-are-currently-editing-the-page-that-shows-your-latest-posts?replies=3#post-7130021
 * @param $post
 * @return void
 */
function fix_no_editor_on_posts_page($post)
{

  if ($post->ID != get_option('page_for_posts')) {
    return;
  }

  remove_action('edit_form_after_title', '_wp_posts_page_notice');
  add_post_type_support('page', 'editor');

}

add_action('edit_form_after_title','fix_no_editor_on_posts_page', 0);
/*=============================================================================
				 		Custom Post Type for Blogs
===============================================================================*/
function blog_post_type() {

	$labels = array(
		'name'                  => _x( 'Blog', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Blog', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Blog', 'text_domain' ),
		'name_admin_bar'        => __( 'Blog', 'text_domain' ),
		'archives'              => __( 'Blog Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Blog:', 'text_domain' ),
		'all_items'             => __( 'All Blogs', 'text_domain' ),
		'add_new_item'          => __( 'Add New Blog', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$rewrite = array(
		'slug'                  => 'blog',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Blogs', 'text_domain' ),
		'description'           => __( 'Our Blogs', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'page-attributes', 'post-formats', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
		'menu_icon'				=> 'dashicons-clipboard'
		
	);
	register_post_type( 'blog', $args );

}
add_action( 'init', 'blog_post_type', 0 );