var gulp = require('gulp'),
	devBuild = (process.env.NODE_ENV !== 'production'),
	sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  sourcemaps = require('gulp-sourcemaps');


gulp.task('styles', function() {
  gulp.src('sass/**/*.scss')
  	.pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./'));
});

gulp.task('default',function() {
  gulp.watch('sass/**/*.scss',['styles']);
});