<?php
	// There's no page for this url, so redirect to my-account
	wp_redirect(get_link_by_slug('personal-area/my-account'));