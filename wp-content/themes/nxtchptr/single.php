<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package NxtChptr
 */

get_header(); ?>
	<div class="singleDetail-wrap">
		<div class="singleDetail-wrap">
			<div class="wrapper">
				<div class="postROw">
					<div class="postInfoLeft">
						<h1><?php the_field('interviewee_name') ?></h1>
						<h5><?php the_field('introduction') ?></h5>
						<h3 class="w-blogpost-title entry-title" itemprop="headline"><?php the_title() ?></h3>
						<p><i class="fa fa-clock"></i> <?php the_field('publication_date') ?></p>				
					</div>
					<div class="postInfoRight">
						<div class="w-blogpost-preview">
							<img src="<?php the_post_thumbnail_url() ?>" >
						</div>
					</div>
				</div>
				<div class="postDetail">
					<?php the_post();	?>
					<?php the_content() ?>
				</div
			</div>
		</div>
	</div>
<?php
get_footer();
