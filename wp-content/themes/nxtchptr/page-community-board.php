<?php
/**
 * The template for displaying a job posting
 */

$manager = Wpjb_Project::getInstance()->env("user_manager");
/* @var $manager Wpjb_User_Manager */

$dashboard = $manager->buildDashboard("employer", get_the_ID());

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="personal-area-nav">
		 		<?php bp_nav_menu(); ?> 
			</section>
			<section class="blocks">
				<div class="blocks__block blocks__block--dark blocks__block--title blocks__block--sm-content">
					<div class="block__content">
						<?php the_title( '<h1>', '</h1>' ); ?>
					</div>
				</div>
				<div class="blocks__block blocks__block--image blocks__block--sm-bg" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/images/career_logged.jpg); background-position: right center;"></div>
			</section>
			<section class="personal-area-nav personal-area-nav--sub-nav">
	 			<ul class="menu">
	 				<?php $permalink = rtrim(home_url( $wp->request ), '/'); ?>
	 				<?php foreach($dashboard as $gname => $group): ?>
		        <?php foreach($group["links"] as $lname => $link): ?>
	 						<li id="<?= $lname ?>-li" <?php if(strpos($permalink, rtrim($link['url'], '/')) !== false): ?> class="current-menu-parent" <?php endif; ?>>
	 							<a href="<?php echo esc_attr($link["url"]) ?>"><?php echo esc_html($link["title"]) ?></a>
	 						</li>
		        <?php endforeach; ?>
			    <?php endforeach; ?>
				</ul> 
			</section>
			<section class="employer-panel">
				<?php the_post(); the_content(); ?>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
