<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package NxtChptr
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php the_post();	?>
		<section class="blocks">
			<div class="blocks__block blocks__block--title blocks__block--sm-content">
				<div class="block__content">
					<h1><?php the_title() ?></h1>
				</div>
			</div>
			<div class="blocks__block blocks__block--image blocks__block--sm-bg" style="background-image: url(<?php the_post_thumbnail_url() ?>)"></div>
		</section>
		<section class="course">
			<p><b>Coach:</b> <a href="<?php echo site_url('/coaches/'.get_field('coach')['ID']); ?>"><?php echo get_field('coach')['display_name']; ?></a></p>
			<time><b>Date and time:</b> <?php the_field('date_time') ?></time>
			<p><b>Price:</b> $<?php the_field('_pmproap_price') ?></p>
			<?php the_field('description') ?>
			<?php the_content() ?>
		</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
