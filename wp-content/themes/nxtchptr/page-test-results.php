<?php
/**
 * The template for displaying the about-us page
 */
$reports = get_career_leader_reports();
if(count($reports) == 0){
	download_career_leader_report();
	$reports = get_career_leader_reports();
}
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="personal-area-nav">
		 		<?php bp_nav_menu(); ?> 
			</section>
			<section class="blocks">
				<div class="blocks__block blocks__block--dark">
					<div class="block__content">
						<h1>Test Results</h1>
					</div>
				</div>
				<div class="blocks__block blocks__block--image" style="background-image: url(<?php echo bp_attachments_get_attachment() ?>)"></div>
			</section>
			<section class="blocks">
				<?php if(pmproap_isPostLocked($post->ID) && !pmproap_hasAccess($current_user->ID, $post->ID)): ?>
					<div class="blocks__block">
						<div class="block__content">
							<?php the_content() ?>
						</div>
					</div>
				<?php else: ?>
					<div class="blocks__block blocks__block--dark">
						<div class="block__content">
							<p>To take your CareerLeader Test, click the button below and use the following details to register:</p>
							<dl>
								<?php $current_user = wp_get_current_user() ?>
								<div>
									<dt>Email</dt>
									<dd><?php echo $current_user->user_email ?></dd>
								</div>
								<div>
									<dt>Registration Key</dt>
									<dd>nxt-chptr</dd>
								</div>
							</dl>
							<a href="https://www.careerleader.com/sign-in.html" target="_blank" class="btn btn--primary">Start Your Test</a>
						</div>
					</div>
					<div class="blocks__block blocks__block--dark">
						<div class="block__content">
							<div class="test-results">
								<?php 
								foreach($reports as $report) :?>
									<div class="test-result">
										<p>Your CareerLeader Results from </p>
										<time><?php echo $report->creation_date ?></time>
										<a href="<?php echo $report->filename ?>" target="_blank" class="btn btn--primary btn--small">View</a>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				<?php endif ?>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
