<?php
/**
 * The template for displaying the about-us page
 */

get_header(); ?>
<div class="about-wrap">
  <div class="about-sect01">
      <div class="wrapper">
          <div class="row align-items-center">
              <div class="col-lg-6 col-md-12">
                  <div class="content-sect01">
                      <h2><?php the_field('hero_heading') ?></h2>
                      <?php the_field('hero_introduction') ?>
                  </div>
              </div>
              <div class="col-6 about-pageimg">
                  <img src="<?php the_field('hero_image') ?>">
              </div>
          </div>
      </div>
  </div>
  <div class="about-sect02">
    <div class="wrapper">
      <h4 class="title">You can count on us for</h4>
      <div class="row">
        <div class="col-md-4">
          <div class="sect02-box">
            <div class="sect02-boxInner">
              <img src="<?php echo site_url() ?>/wp-content/themes/nxtchptr/images/new/about/icon-1.png">
              <p>Inspiring transformation</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="sect02-box">
            <div class="sect02-boxInner">
              <img src="<?php echo site_url() ?>/wp-content/themes/nxtchptr/images/new/about/icon-2.png">
              <p>Valuable connection</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="sect02-box">
            <div class="sect02-boxInner">
              <img src="<?php echo site_url() ?>/wp-content/themes/nxtchptr/images/new/about/icon-3.png">
              <p>Thoughtful support</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="about-sect03">
    <div class="wrapper">
        <h2>For Coaches</h2>
        <p>For coaches and growth leaders, we provide access to a global client base. We want to serve as a trusted partner in addition to helping you grow your practice. We offer best-in-class marketplace technology to fuel your business growth and support coaches looking for additional marketing opportunities. Specialists on Nxt Chptr also have access to an extensive coaching community to inspire their own growth and improve their performance. Because who doesn’t want to learn and get better at what they do? </p>
        <div class="spacer"></div>
        <h2>For Students</h2>
        <p>For students looking for inspiration, growth and connection, Nxt Chptr offers a safe and affordable way to take the leap. We invite you to explore a range of coaching sessions offered by vetted, best-in-class and user-rated professionals. And if you don’t see what you want, let us know! We may just go out a find a great coach for you. </p>
    </div>
  </div>    
  <div class="about-sect04">
    <div class="wrapper">
      <div class="about-founder">
          <img src="<?php the_field('founder_image') ?>">
          <h2><?php the_field('founder_heading') ?></h2>
          <?php the_field('founder_description') ?>
      </div>
    </div>
  </div>      
</div>

<?php
get_footer();
