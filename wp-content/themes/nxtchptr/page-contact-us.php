<?php
/**
 * The template for displaying the about-us page
 */
 
  $response = "";
  function my_contact_form_generate_response($type, $message){
 
    global $response;
 
    $response = "<div class='alert alert--{$type}'>{$message}</div>";
  }
  

	if(isset($_POST['submitted']) && $_POST['submitted']){
	  //response messages
		//$not_human       = "Human verification incorrect.";
		$missing_content = "Please supply all information.";
		$email_invalid   = "Email Address Invalid.";
		$message_unsent  = "Message was not sent. Try Again.";
		$message_sent    = "Thanks! Your message has been sent.";
		 
		//user posted variables
		$name = $_POST['contact_name'];
		$email = $_POST['contact_email'];
		$message = $_POST['contact_comments'];
		$message .= "\r\n" . "From $name - $email";
		
		//$human = $_POST['contact_human'];
		 
		//php mailer variables
		$to = get_option('admin_email');
		$subject = "Someone sent a message from ".get_bloginfo('name');
		$headers = 'From: '. $email . "\r\n" .
		  'Reply-To: ' . $email . "\r\n";

		if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			my_contact_form_generate_response("error", $email_invalid);
		} else {
		  if(empty($name) || empty($message)){
			  my_contact_form_generate_response("error", $missing_content);
			} else {
				$sent = wp_mail($to, $subject, strip_tags($message), $headers);
				if($sent) {
					my_contact_form_generate_response("success", $message_sent); 
				} else {
					my_contact_form_generate_response("error", $message_unsent); 
				} 
			}
		}
	}


get_header(); ?>
	<div class="contactPage-main">
		<div class="contactPage-title">
			<div class="wrapper">
				<h1><?php the_field('heading') ?></h1>
			</div>
		</div>
		<div class="contactPage-content">
			<div class="wrapper">
				<div class="row">
					<div class="col-md-6">
						<div class="contactPage-sect">
							<?php the_field('introduction') ?>
						</div>
					</div>
					<div class="col-md-6">
						<form method="post" action="<?php the_permalink(); ?>" class="contact-us__form">
								<input name="contact_name" type="text" placeholder="Name" value="<?php echo isset($_POST['contact_name']) ? esc_attr($_POST['contact_name']) : ''; ?>">
								<input name="contact_email" type="email" placeholder="Email" value="<?php echo isset($_POST['contact_email']) ? esc_attr($_POST['contact_email']) : ''; ?>">
								<textarea name="contact_comments" placeholder="Comments and general inquiry"><?php echo isset($_POST['contact_comments']) ? esc_textarea($_POST['contact_comments']) : ''; ?></textarea>
								<input type="hidden" name="submitted" value="1">
								<input class="btn btn-cstm" type="submit" value="Send">
							<?php echo $response; ?>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<!-- <section class="contact-us" style="background-image: url(<?php the_field('background_image') ?>)">
				<div class="contact-us__content">
					
					<div class="contact-us__form-container">
						<div class="contact-us__form-meta">
							
							<?php //include 'social-links.php' ?>
						</div>
						
					</div>
				</div>
			</section> -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
