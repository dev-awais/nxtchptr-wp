<!-- <ul class="social-links">
	<li><a href="https://www.facebook.com/nxt.chptr.moms/" class="social-links__facebook"></a></li>
	<li><a href="https://twitter.com/nxt_chptr" class="social-links__twitter"></a></li>
	<li><a href="https://www.instagram.com/nxt_chptr/" class="social-links__instagram"></a></li>
</ul> -->
<ul class="social-links">
	<li>
		<a href="https://www.facebook.com/nxt.chptr.grow/" target="_blank">
		<img height="33px" src="<?php echo site_url() ?>/wp-content/themes/nxtchptr/images/new/socail-icons/fb-icon.svg">
	</a>
</li>
<li>
	<a href="https://www.instagram.com/nxt_chptr/" target="_blank">
	<img height="33px" src="<?php echo site_url() ?>/wp-content/themes/nxtchptr/images/new/socail-icons/insta-icon.svg">
</a>
</li>
<li>
	<a href="https://twitter.com/nxt_chptr" target="_blank">
	<img height="33px" src="<?php echo site_url() ?>/wp-content/themes/nxtchptr/images/new/socail-icons/twtr-icon.svg">
</a>
</li>
<li>
	<a href="https://www.youtube.com/channel/UCY2GkgkYtJT1AdULC6To03A?view_as=subscriber" target="_blank">
	<img height="33px" src="<?php echo site_url() ?>/wp-content/themes/nxtchptr/images/new/socail-icons/youtube-icon.svg">
</a>
</li>
<li>
	<a href="https://www.linkedin.com/company/nxtchptr/" target="_blank">
	<img height="33px" src="<?php echo site_url() ?>/wp-content/themes/nxtchptr/images/new/socail-icons/linkedin-icon.svg">
</a>
</li>
</ul>