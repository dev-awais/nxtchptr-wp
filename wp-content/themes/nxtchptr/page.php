<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package NxtChptr
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php
			while ( have_posts() ) : the_post(); ?>

			<section class="blocks">
				<div class="blocks__block blocks__block--dark blocks__block--title blocks__block--sm-content">
					<div class="block__content">
						<?php the_title( '<h1>', '</h1>' ); ?>
					</div>
				</div>
				<div class="blocks__block blocks__block--image blocks__block--sm-bg" style="background-image: url(<?php the_field('hero_image', get_page_by_path('membership', OBJECT, 'page')) ?>"></div>
			</section>
			<section class="blocks">
				<div class="blocks__block">
					<div class="block__content">
						<?php the_content(); ?>
					</div>
				</div>
			</section>

			<?php
			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
