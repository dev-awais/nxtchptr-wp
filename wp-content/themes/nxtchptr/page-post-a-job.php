<?php
/**
 * The template for displaying a job posting
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="blocks">
				<div class="blocks__block blocks__block--dark blocks__block--title">
					<div class="block__content">
						<?php the_title( '<h1>', '</h1>' ); ?>
					</div>
				</div>
				<div class="blocks__block"  style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/images/career_logged.jpg); background-position: right center;"></div>
			</section>
			<section class="job-posting">
				<?php the_post(); the_content(); ?>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
