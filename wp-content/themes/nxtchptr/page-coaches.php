<?php
/**
 * The template for displaying the about-us page
 */

$coach = get_user_by('id', get_query_var('coach_id'));
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="coaches">
				<div class="coach">
				  <div class="coach__img" style="background-image: url(<?php the_field('photo', 'user_'.$coach->ID) ?>)"></div>
				  <div class="coach__content">
						<h3><?php echo $coach->display_name ?></h3>
						<p><?php the_field('bio', 'user_'.$coach->ID) ?></p>
						<?php 
							$courses = get_posts(array(
								'post_type'		=> 'courses',
								'meta_key' => 'coach',
								'meta_value' => get_query_var('coach_id')
							));
							if($courses):
						?>
							<h4>Courses Offered</h4>
							<ul>
								<?php foreach($courses as $course): ?>
									<div class="course-list-item">
							      <div class="course-list-item__img" style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id($course->ID), 'thumbnail' ) ?>)"></div>
										<div class="course-list-item__content">
											<h3><?php echo get_the_title($course->ID) ?></h3>
								      <p>Price: $<?php the_field('_pmproap_price', $course->ID) ?></p>
								      <time><?php the_field('date_time', $course->ID) ?> PST</time>
											<a href="<?php the_permalink($course->ID) ?>" class="btn btn--primary btn--small">View Details</a>
										</div>
									</div>
								<?php endforeach; ?>
							</ul>
						<?php endif; ?>
				  </div>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
