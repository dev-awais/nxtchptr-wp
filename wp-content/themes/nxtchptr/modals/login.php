<div id="login" class="modal">
	<div class="modal__content">
		<span data-dismiss="modal" class="modal__close">&times;</span>
		<?php theme_my_login([
			"show_title" => false]) ?>
	</div>
</div>