<?php
/**
 * The template for displaying the self discovery page
 */

get_header(); ?>

	<div id="primary" class="content-area content-area--no-image">
		<main id="main" class="site-main">
			<div class="container">
				<div class="row">
					<div class="col-12 col-md-8">
						<header class="page-header">
							<h1 class="page-title">Group Video Coaching</h1>
						</header>
						<p>Ever feel like you want an actual person to help you? We are working on affordable career and life coaching via group video. Please take a look at our upcoming test classes.</p>
						<p><a class="btn btn--small btn--primary" href="https://nxtchptrgroupvideoclasses.as.me">Group Classes</a></p>
						<p>If you would like to be informed about the progress of group video coaching and when group video coaching is live, please add your email address to our distribution list.</p>
						<form class="row align-items-center" action="https://nxt-chptr.us15.list-manage.com/subscribe/post?u=c0e06a0414ea9b9e1fc566b90&amp;id=37f80c4a2c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank">
							<div class="col-12 col-sm-6">
								<input class="form-control mb-sm-0" type="email" name="EMAIL" placeholder="Email">
							</div>
							<div class="col-12 col-sm-6">
								<input class="btn btn--small btn--primary" type="submit" name="subscribe" value="Subscribe">
							</div>
							<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
							<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_c0e06a0414ea9b9e1fc566b90_37f80c4a2c" tabindex="-1" value=""></div>
						</form>
					</div>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
