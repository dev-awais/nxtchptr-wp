<?php
/**
 * The template for displaying the career discovery page
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php include_page('career-discovery') ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
