<?php
/*
If you would like to edit this file, copy it to your current theme's directory and edit it there.
Theme My Login will always look in your theme's directory first, before using this default template.
*/
?>
<div class="modal__form" id="theme-my-login<?php $template->the_instance(); ?>">
	<small class="modal__change">Don't have an account? <a data-modal="signup" data-dismiss="modal" href="<?php echo site_url('register') ?>">Sign up</a></small>
	<?php $template->the_errors(); ?>
	<form name="loginform" id="loginform<?php $template->the_instance(); ?>" action="<?php $template->the_action_url( 'login', 'login_post' ); ?>" method="post">
		<?php if (isset($_GET['social_login_insufficient_activity'])): ?>
			<h2>Oops</h2>
			<p>Your registration has failed as your social media account doesn't have enough activity.</p>
			<p>You can try another account instead:</p>
		<?php else: ?>
			<h2>Welcome Back!</h2>
			<p>Please log in to your account</p>
		<?php endif; ?>
		<?php do_action( 'login_form' ); ?>

		<span class="hr-span">or</span>
		<input type="text" name="log" id="user_login<?php $template->the_instance(); ?>" value="<?php $template->the_posted_value( 'log' ); ?>" size="20" placeholder="Email or Username"/>
		<input type="password" name="pwd" id="user_pass<?php $template->the_instance(); ?>" value="" size="20" autocomplete="off" placeholder="Password"/>


 		<div>
			<input name="rememberme" type="checkbox" id="rememberme<?php $template->the_instance(); ?>" value="forever" />
			<label for="rememberme<?php $template->the_instance(); ?>">Remember Me</label>
			<p>
				<input class="btn btn--primary" type="submit" name="wp-submit" id="wp-submit<?php $template->the_instance(); ?>" value="Log in" />
				<input type="hidden" name="redirect_to" value="<?php $template->the_redirect_url( 'login' ); ?>" />
				<input type="hidden" name="instance" value="<?php $template->the_instance(); ?>" />
				<input type="hidden" name="action" value="login" />
			</p>
		</div>
	</form>
	<?php $template->the_action_links( array( 'login' => false ) ); ?>
</div>
