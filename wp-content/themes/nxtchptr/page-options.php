<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section>
				<div class="container">
					<div class="blocks">
						<div class="blocks__block">
							<div class="block__content">
								<h2>Self Discovery</h2>
								<p>Are you looking for change, but unsure where to begin? Let us present you with some options.</p>
								<a href="/self-discovery">Learn More</a>
							</div>
						</div>
						<div class="blocks__block">
							<div class="block__content">
								<h2>Peer Discovery</h2>
								<p>Are you having trouble because of isolation or a lack of support? Let us pitch you on Peer Groups.</p>
								<a href="/peer-discovery">Learn More</a>
							</div>
						</div>
						<div class="blocks__block">
							<div class="block__content">
								<h2>Mom Profiles</h2>
								<p>Looking for inspiration about career change? Our Mom Profiles could be for you. We interview interesting women entrepreneurs who founded organizations and companies after they were mothers.</p>
								<a href="/mom-profiles">Learn More</a>
							</div>
						</div>
					</div>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
