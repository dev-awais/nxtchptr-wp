<?php
/**
 * The template for displaying the peer group 
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="blocks">
				<div class="blocks__block blocks__block--dark blocks__block--title">
					<div class="block__content">
						<h1>Find a peer group</h1>
					</div>
				</div>
				<div class="blocks__block" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/images/feature.png"></div>
			</section>
			<section class="blocks">
				<div class="blocks__block">
					<div class="block__content">
						<form class="long-form" method="POST" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>">
						<input type="hidden" name="action" value="peer_matching_form">
							<h2>Matching Questionnaire</h2>
							<div class="long-form__field">
								<label for="gender">My Gender</label>
								<select name="gender" id="gender" required>
									<option value="">Select one</option>
									<option>Female</option>
									<option>Male</option>
								</select>
							</div>
							<div class="long-form__field">
								<label for="status">I am</label>
								<select name="status" id="status" required>
									<option value="">Select one</option>
									<option value="1">Mom looking to return to the workforce after not working for several years</option>
									<option value="2">Mom looking to change careers</option>
								</select>
							</div>
							<div class="long-form__field">
								<label for="timezone">My Timezone</label>
								<select name="timezone" id="timezone" required>
									<option value="">Select one</option>
									<option value="1">Eastern</option>
									<option value="2">Central</option>
									<option value="3">Mountain</option>
									<option value="4">Pacific</option>
								</select>
							</div>
							<div class="long-form__field">
								<label for="zipcode">Zip code where I live</label>
								<input type="text" name="zipcode" id="zipcode" required>
							</div>
							<div class="long-form__field">
								<label for="school_name">Name of last school I attended</label>
								<input type="text" name="school_name" id="school_name" required>
							</div>
							<div class="long-form__field">
								<label for="education_level">Highest level of education</label>
								<select name="education_level" id="education_level" required>
									<option value="">Select one</option>
									<option value="1">Attended high school but didn't graduate</option>
									<option value="2">High School Diploma</option>
									<option value="3">Attended College but didn't graduate</option>
									<option value="4">College Degree</option>
									<option value="5">Graduate School Degree</option>
								</select>
							</div>
							<div class="long-form__field">
								<label for="books_mags">What is your preference:</label>
								<select name="books_mags" id="books_mags" required>
									<option value="">Select one</option>
									<option value="books">Books</option>
									<option value="magazines">Magazines</option>
									<option value="neither">Neither: Social Media</option>
								</select>
							</div>
							<div class="long-form__field">
								<label for="crossword_sudoku">What is your preference:</label>
								<select name="crossword_sudoku" id="crossword_sudoku" required>
									<option value="">Select one</option>
									<option value="crossword">Crossword</option>
									<option value="sudoku">Sudoku</option>
									<option value="neither">Neither: I don't like games</option>
								</select>
							</div>
							<div class="long-form__field">
								<label for="feel_look">I prefer to feel good than look good</label>
								<select name="feel_look" id="feel_look" required>
									<option value="feel">Agree</option>
									<option value="look">Disagree</option>
								</select>
							</div>
							<div class="long-form__field">
								<label for="humor_important">A sense of humor is important to me</label>
								<select name="humor_important" id="humor_important" required>
									<option value="1">Agree</option>
									<option value="0">Disagree</option>
								</select>
							</div>
							<div class="long-form__field">
								<input type="hidden" name="most_important_ABCD[A]" id="most_important_A">
								<input type="hidden" name="most_important_ABCD[B]" id="most_important_B">
								<input type="hidden" name="most_important_ABCD[C]" id="most_important_C">
								<input type="hidden" name="most_important_ABCD[D]" id="most_important_D">
								<label>What is most important to you in a peer group? Drag and drop to rank in order of most (1) to least important (4) to you. </label><br>
								<ol id="most_important_ABCD_list">
									<li data-input="A">Emotional Support as I search</li>
									<li data-input="B">Feedback on my ideas and holding me accountable for making a change</li>
									<li data-input="C">Help with brainstorming, connections, and leads</li>
									<li data-input="D">Making new friends</li>
								</ol>
							</div>
							<div class="long-form__field">
								<label for="why_joined">2-4 Sentences on why you joined Nxt Chptr. (This response will be seen when other members receive matches.)</label>
								<textarea name="why_joined" required></textarea>
							</div>
							<div class="long-form__field">
								<label for="fav_websites">2 favorite websites. (This response will be seen when other members receive matches.)</label>
								<textarea name="fav_websites" required></textarea>
							</div>
							<div class="long-form__field">
								<label for="fav_tv_shows">2 favorite current tv shows . (This response will be seen when other members receive matches.)</label>
								<textarea name="fav_tv_shows" required></textarea>
							</div>
							<div class="long-form__field">
								<label for="fav_movie">All-time favorite movie. (This response will be seen when other members receive matches.)</label>
								<textarea name="fav_movie" required></textarea>
							</div>
							<div class="long-form__field">
								<label for="child_career_dream">When you were a child, you wanted to be X when you grew up.  (This response will be seen when other members receive matches.)</label>
								<input type="text" name="child_career_dream" required>
							</div>
							<div class="long-form__field">
								<label for="prev_career">Your career before you becamse a full-time caregiver. (This response will be seen when other members receive matches.)</label>
								<input type="text" name="prev_career" required>
							</div>
							<div class="long-form__field">
								<label>Select top five descriptors that make me good at roles I take on</label>
								<br>
								<?php for($i=0; $i<5; $i++): ?>
									<select name="descriptors[]" required>
										<option value="">Select one</option>
										<?php echo nxtchptr_descriptive_skills() ?>
									</select>
								<?php endfor; ?>
							</div>
							<div class="long-form__field">
								<label for="gender_preference">I want gender of peer group members to be:</label>
								<select name="gender_preference" id="gender_preference" required>
									<option value="">Select one</option>
									<option>Female</option>
									<option>Male</option>
								</select>
							</div>
							<div class="long-form__field">
								<label for="peer_member_residence_preference">Preference of where my peer group members reside</label>
								<select name="peer_member_residence_preference" id="peer_member_residence_preference" required>
									<option value="">Select one</option>
									<option value="local">Same Time Zone</option>
									<option value="anywhere">Anywhere in US is fine with me</option>
								</select>
							</div>
							<div class="long-form__field">
								<label for="language">Language used in my group</label>
								<select name="language" id="language" required>
									<option value="">Select one</option>
									<option value="1">English</option>
								</select>
							</div>
							<div class="long-form__field">
								<label for="friends">Include my friend in this group (Friend has to be a NXT CHTPR member)</label><br>
								<select name="friends[]" id="friends" multiple>
									<?php $user_query = new BP_User_Query( array( 'user_ids' => array( bp_loggedin_user_id() ) ) );
									foreach ($user_query->results as $member){
										echo "<option value='{$member->id}'>{$member->display_name}</option>";
									} ?>
								</select>
							</div>
							<script type="text/javascript">
								(function($){
									$('#friends').width('100%').chosen();
								})(jQuery);
							</script>
							<div class="long-form__field">
								<label for="duration">Peer Group Duration </label>
								<select name="duration" id="duration" required>
									<option value="">Select one</option>
									<option value="6 months">6 months</option>
									<option value="1 year">1 year</option>
									<option value="indefinite">No opinion/indefinitely</option>
								</select>
							</div>
							<div class="long-form__field">
								<label for="auto_match">I want to be automatically matched with highest % match.  (If not, you will receive leads of top matches in MY GROUPS.)</label>
								<input type="hidden" name="auto_match" value="0">
								<input type="checkbox" name="auto_match" value="1" id="auto_match">
							</div>
							<div class="long-form__field">
								<label for="agreement">I promise that all of my interactions with members of this community will be supportive and respectful.  I acknowledge that inappropriate behavior could lead to my expulsion from Nxt Chptr.  In addition, I affirm that all information that I have given about myself above is true.  I acknowledge that intentionally false information listed on my peer matching questionnaire could lead to expulsion from Nxt Chptr.  I further acknowledge that any expulsion would be a forfeiture of my membership dues.</label>
								<?php echo wp_nonce_field('register-user', '_mynonce'); ?>
								<input type="checkbox" name="agreement" value="1" id="agreement" required>
								<br>
								<br>
								<input class="btn btn--primary" type="submit" value="Find Peers">
							</div>
						</form>
					</div>
				</div>
			</section>
		</main>
	</div>

<div id="women-only-modal" class="modal">
	<div class="modal__content">
		<span data-dismiss="modal" class="modal__close">&times;</span>
		<p>Nxt Chptr Peer groups are only supporting Moms right now.</p>
		<p>If you would like to be notified when we offer these services to men, please enter your email address here:</p>
		<input type="email" name="notify_men_email">
		<input type="submit" class="btn btn--small btn--primary" value="Submit">
	</div>
</div>

<?php
get_footer();