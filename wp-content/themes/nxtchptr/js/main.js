(function($){
  $('.testimonial-container').slick({
    dots: true
  });

  $('.faqs__questions').on('click', '.faqs__question', function(){
    show_faq($(this).parent('li').index());
  });

  function show_faq(index) {
    $('.faqs__question').removeClass('open');
    $('.faqs__question').eq(index).addClass('open');
    $('.faqs__answers .faqs__answer').fadeOut(200).promise().done(function(){
      $('.faqs__answers .faqs__answer').eq(index).fadeIn(200);
    });
  }

  show_faq(0);

  window.show_modal = function($modal) {
    $modal.hide();
    $('.modal-overlay').show();
    $('body').addClass('noscroll');
    $modal.fadeIn(300);
  }

  $('[data-dismiss="modal"]').click(function(){
  	$(this).parents('.modal').fadeOut(300,function(){
  		if(!$('.modal:visible').length){
	  		$('.modal-overlay').hide();
        $('body').removeClass('noscroll');
      }
    });
  });

  $('[data-modal]').click(function(e){
    e.preventDefault();
    $modal = $('#' + $(this).data('modal'));
    show_modal($modal);
  });

  var ad_shown = false;
  $(window).scroll(function(e){
    if($(this).scrollTop() >= 500
      && $(document).outerHeight() > $(this).outerHeight() + $(this).scrollTop() + 200){
      if(!ad_shown) {
        ad_shown = true;
        $('.urbansitter-ad').stop(true).fadeIn(700);
      }
    }else{
      if(ad_shown){
        ad_shown = false;
        $('.urbansitter-ad').stop(true).fadeOut(700);
      }
    }
  });

  $mi_list = $('#most_important_ABCD_list'); 
  if($mi_list.length){
    Sortable.create($mi_list.get(0), {
      onUpdate: function (evt){
        $mi_list.find('li').each(function(){
          $('#most_important_' + $(this).data('input')).val($(this).index() + 1);
        });   
      }
    });
  }

  $('#gender, #gender_preference').change(function(){
    if($(this).val() == 'Male') {
      show_modal($('#women-only-modal'));
      $(this).find(':selected').prop("selected", false);
    }
  })

  window.wpjb_ls_jobs = function(e) {
        
    var $ = jQuery;
        
    if(WpjbXHR) {
        WpjbXHR.abort();
    }
                
    var page = null;
                
    if(typeof e == 'undefined') {
        page = 1;
    } else {
        page = parseInt($(".wpjb-ls-load-more a.btn").data("wpjb-page"));
    }
      
    var data = $.extend({}, WPJB_SEARCH_CRITERIA);
    data.action = "wpjb_jobs_search";
    data.page = page;
    data.type = [];
    
    WPJB_SEARCH_CRITERIA.filter = "active";
                
    if($(".wpjb-ls-query").val().length > 0) {
        data.query = $(".wpjb-ls-query").val();
        WPJB_SEARCH_CRITERIA.query = data.query;
    }else{
        WPJB_SEARCH_CRITERIA.query = "";
    }                
    if($(".wpjb-ls-location").length && $(".wpjb-ls-location").val().length >= 0) {
        data.location = $(".wpjb-ls-location").val();
        WPJB_SEARCH_CRITERIA.location = data.location;
    }else{
        WPJB_SEARCH_CRITERIA.location = "";
    }                
              
    $(".wpjb-ls-type:checked").each(function() {
        data.type.push($(this).val());
    });
    WPJB_SEARCH_CRITERIA.type = data.type;
                
    $(".wpjb-job-list").css("opacity", "0.4");
                
    WpjbXHR = $.ajax({
        type: "POST",
        data: data,
        url: ajaxurl,
        dataType: "json",
        success: function(response) {
                        
            var total = parseInt(response.total);
            var nextPage = parseInt(response.page)+1;
            var perPage = parseInt(response.perPage);
            var loaded = 0;
                                
            $(".wpjb-subscribe-rss input[name=feed]").val(response.url.feed);
            $(".wpjb-subscribe-rss a.wpjb-button.btn").attr("href", response.url.feed);

            if(total == 0) {
                $(".wpjb-job-list").css("opacity", "1");
                $(".wpjb-job-list").html('<div>'+WpjbData.no_jobs_found+'</div>');
                return;
            }
                                
            var more = perPage;
                                
            if(nextPage == 2) {
                $(".wpjb-job-list").empty();
            }
                        
            $(".wpjb-job-list .wpjb-ls-load-more").remove();
            $(".wpjb-job-list").css("opacity", "1");
            $(".wpjb-job-list").append(response.html);
                                
            loaded = $(".wpjb-job-list").children().length;
                                
            var delta = total-loaded;
                                
            if(delta > perPage) {
                more = perPage;
            } else if(delta > 0) {
                more = delta;
            } else {
                more = 0;
            }
                                
            if(more) {
                var txt = WpjbData.load_x_more.replace("%d", more);
                var loadMoreHtml = "";
                
                if($(".wpjb-job-list").prop("tagName") == "TBODY") {
                    loadMoreHtml = '<tr class="wpjb-ls-load-more"><td colspan="3"><a href="#" data-wpjb-page="'+(nextPage)+'" class="btn">'+txt+'</a></td></tr>';
                } else {
                    loadMoreHtml = '<div class="wpjb-ls-load-more"><a href="#" data-wpjb-page="'+(nextPage)+'" class="wpjb-button btn">'+txt+'</a></div>';
                }
                
                $(".wpjb-job-list").append(loadMoreHtml);
                $(".wpjb-job-list .wpjb-ls-load-more a").click(wpjb_ls_jobs);
            }
                                
        }
    });
                
    return false;
  }

  $('.meeting-date-time').flatpickr({
    enableTime: true,
    defaultDate: 'today',
    dateFormat: "Y-m-d\TH:i:S",
    altInput: true,
    altFormat: "j M Y h:iK"
  });

})(jQuery);
		
if(!document.cookie.match(/^(?:.*;)?\s*NxtchptrUserTimeZone\s*=\s*([^;]+)(?:.*)?$/)||[,null][1]){
  var timezone = jstz.determine();
  var timezoneName = timezone.name(); 
  document.cookie="NxtchptrUserTimeZone=" + timezoneName + ";path=/";
}