<?php
/**
 * The template for displaying search results pages
 *
 *
 *
 * @package NxtChptr
 */

get_header(); ?>

	
	<div class="postsListings-main">
		<div class="wrapper">
			<?php if ( have_posts() ){?>
        <h2>Search Result for : <?php echo "$s"; ?> </h2>
        <?php }else { ?>     
        <h2>No Result Found for : <?php echo "$s"; ?> </h2>
          <?php } ?>  
			<div class="postsMainflex">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>   

					<div class="postBlock">
						<img class="postThumbnail" src="<?php the_post_thumbnail_url() ?>">
						<div class="postBlock-content">
							<h4><?php the_title(); ?></h4>
							<p><?php $intro = strip_tags(get_field('introduction')); echo substr($intro, 0, 339); if(strlen($intro) > 339): ?>&hellip;<?php endif; ?></p>
							<a class="postBlock-btn" href="<?php the_permalink(); ?>" title="Read">Read More</a>
						</div>
					</div>
				<?php endwhile; ?>
				
				<?php 
					the_posts_pagination( array(
						'mid_size'  => 2,
						'prev_text' => __( 'Prev', 'textdomain' ),
						'next_text' => __( 'Next', 'textdomain' ),
					) );
				?>					
				<?php wp_reset_postdata(); ?>	
				
				<?php endif; ?>
			</div>
		</div>
	</div>

<?php
get_footer();