<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package NxtChptr
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<div class="mom-profile-archive row">
      <div class="mom-profile-archive__img col-12 col-sm-5" style="background-image: url(<?php the_post_thumbnail_url() ?>)"></div>
      <div class="mom-profile-archive__content col-12 col-sm-7">
	      <time><?php the_field('publication_date') ?></time>
	      <h6><?php the_title() ?></h6>
	      <p><?php $intro = strip_tags(get_field('introduction')); echo substr($intro, 0, 339); if(strlen($intro) > 339): ?>&hellip;<?php endif; ?></p>
	      <a href="<?php the_permalink() ?>">Read More</a>
      </div>
    </div>
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
