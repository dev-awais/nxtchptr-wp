<?php

/**
 * Job details container
 * 
 * Inside this template job details page is generated (using function 
 * wpjb_job_template)
 * 
 * @author Greg Winiarski
 * @package Templates
 * @subpackage JobBoard
 * 
 * @var $application_url string
 * @var $job Wpjb_Model_Job
 * @var $related array List of related jobs
 * @var $show_related boolean
 * @var $show stdClass
 */

?>
<div class="job-posting__content">
    <?php wpjb_flash(); ?>
    <?php include $this->getTemplate("job-board", "job"); ?> 
</div>

<form action="<?php esc_attr_e(wpjb_link_to("job", $job, array("form"=>"apply"))) ?>#wpjb-scroll" method="post" enctype="multipart/form-data" class="wpjb job-posting__form">
    <h4>Apply now</h4>

    <?php echo $form->renderHidden() ?>
    <?php foreach($form->getReordered() as $group): ?>
        <?php foreach($group->getReordered() as $name => $field): ?>
            <div class="<?php wpjb_form_input_features($field) ?>">
                <label class="wpjb-label">
                    <?php esc_html_e($field->getLabel()) ?>
                    <?php if($field->isRequired()): ?><span class="wpjb-required">*</span><?php endif; ?>
                </label>
                <div class="wpjb-field">
                    <?php wpjb_form_render_input($form, $field) ?>
                    <?php wpjb_form_input_hint($field) ?>
                    <?php wpjb_form_input_errors($field) ?>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endforeach; ?>
    
    <div class="buttons">
        <input type="submit" class="btn btn--white" value="Send">
    </div>
</form>