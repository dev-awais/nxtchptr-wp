<?php 

/**
 * Job list item
 * 
 * This template is responsible for displaying job list item on job list page
 * (template index.php) it is alos used in live search
 * 
 * @author Greg Winiarski
 * @package Templates
 * @subpackage JobBoard
 */

 /* @var $job Wpjb_Model_Job */

?>

    <tr>
        <td><h5><?php echo esc_html($job->job_title) ?></h5></td>
        <td><?php echo esc_html(substr($job->job_description, 0, 100)) . '&hellip;'; ?></td>
        <td><?php echo esc_html($job->locationToString()) ?></td>
        <td><?php echo wpjb_date_display("M d, Y", $job->job_created_at, false); ?></td>
        <td><a href="<?php echo wpjb_link_to("job", $job) ?>"><i class="fa fa-chevron-right"></i></a></td>
    </tr>