<?php 

/**
 * Job details
 * 
 * This template is responsible for displaying job details on job details page
 * (template single.php) and job preview page (template preview.php)
 * 
 * @author Greg Winiarski
 * @package Templates
 * @subpackage JobBoard
 */

/* @var $job Wpjb_Model_Job */
/* @var $company Wpjb_Model_Employer */

?>
<h1><?php esc_html_e($job->job_title) ?></h1>
<h6>Summary of Responsibilities:</h6>
<?php wpjb_rich_text($job->job_description, $job->meta->job_description_format->value());
if($job->meta->job_duties_responsibilities->value() != ""): ?>
  <h6>Essential Duties and Responsibilities:</h6>
  <ul>
    <?php foreach(preg_split("/\r\n|\n|\r/", $job->meta->job_duties_responsibilities->value()) as $line_item): ?>
      <li><?php echo $line_item; ?></li>
    <?php endforeach; ?>
  </ul>
<?php endif;
if($job->meta->job_skills_abilities->value() != ""): ?>
  <h6>Required Skills and Abilities:</h6>
  <ul>
    <?php foreach(preg_split("/\r\n|\n|\r/", $job->meta->job_skills_abilities->value()) as $line_item): ?>
      <li><?php echo $line_item; ?></li>
    <?php endforeach; ?>
  </ul>
<?php endif ?>
<?php if($job->meta->company_description->value() != ""){
  wpjb_rich_text($job->meta->company_description->value(), $job->meta->job_description_format->value());
} ?>
