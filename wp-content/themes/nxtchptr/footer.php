<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NxtChptr
 */

?>

	</div><!-- #content -->
	<div class="newsLetter-section">
		<div class="wrapper">
			<h3>Subscribe to Nxt-Chptr Newsletter</h3>
			<?php echo do_shortcode( '[mc4wp_form id="1112"]' ); ?>
		</div>
	</div>	
	<div class="footer-main">
		<div class="footer-top">
			<div class="wrapper">
				<div class="footer-box footer-box01">
					<div class="footer-logo">
						<a href="<?php echo site_url() ?>"><img src="<?php the_field('site_logo', 'option') ?>"></a>
					</div>
					<p>Online Classes to Change The Trajectory of Your Life.</p>
					<p>At Nxt Chptr we connect those approaching any transition in their lives with resources and qualified coaches. We believe in joyful change.
What about you?</p>
					<?php include 'social-links.php'; ?>
				</div>
				<div class="footer-box footer-box02">
					<h4>Quick Links</h4>
					<nav class="footer-navigation my-3">
						<?php
							wp_nav_menu( array( 'theme_location' => 'menu-2' ) );
						?>
					</nav>
				</div>
				<div class="footer-box footer-box03">
					<h4>Contact</h4>
					<p><a href="mailto:admin@nxt-chptr.com">admin@nxt-chptr.com</a></p>
				</div>
			</div>
		</div>
		<div class="footer-btm">
			<div class="wrapper">
				<span>&copy; My Game Changer, Inc All Rights Reserved</span>
			</div>
		</div>
	</div>
</div><!-- #page -->



<div class="modal-overlay"></div>
<?php include 'modals/login.php' ?>
<?php include 'modals/signup.php' ?>
<?php wp_footer(); ?>	

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-124891779-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-124891779-1');
</script>

</body>
</html>
