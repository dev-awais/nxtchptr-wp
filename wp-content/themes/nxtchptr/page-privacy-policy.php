<?php
/**
 * The template for displaying the privacy-policy page
 */

get_header(); ?>

	<div class="about-wrap">
  <div class="about-sect01 pp-sect-01">
      <div class="wrapper">
          <div class="row align-items-center">
              <div class="col-lg-6 col-md-12">
                  <div class="content-sect01">
                    <h4>MY GAME CHANGER, INC. </h4>
                      <h2>PRIVACY POLICY</h2>
                      <p><small>- Last Updated: October 2, 2017</small></p>
                      <p>This privacy policy ("<b>Policy</b>") describes how My Game Changer, Inc. and its related companies ("<b>Company</b>") collect, use and share personal information of consumer users of this website, www.nxt-chptr.com (the "<b>Site</b>"). This Policy also applies to any of our other websites that post this Policy. This Policy does not apply to websites that post different statements.</p>
                  </div>
              </div>
              <div class="col-6 about-pageimg">
                  <img src="<?php echo site_url() ?>/wp-content/themes/nxtchptr/images/new/about/privacy.png">
              </div>
          </div>
      </div>
  </div>
  <div class="pp-content">
  <div class="wrapper">
      <h4>WHAT WE COLLECT</h4>
      <p>We get information about you in a range of ways. </p>
      <p>Information You Give Us. We collect your‎ name, postal address, email address, username, password, demographic information (such as your gender and occupation), personal information relevant to career change and life experience as well as other information you directly give us on our Site. For example:</p>
      <ul class="ordered_list">
        <li>We will collect your name, email address and any other information you provide when you register for an account on our Site.</li>
        <li>If you contact us for support via email, we will collect your email address and the content of your email in order to send you a reply.</li>
        <li>If you purchase any products or services on our Site, we may collect your shipping information in order to send you your products, and our third party payment processor may collect your billing information in order to process your payments.</li>
        <li>If you participate in a sweepstakes, contest or giveaway on our Site, we may ask you for your email address and/or phone number (to notify you if you win or not). We may also ask for first and last names, and sometimes post office addresses to verify your identify. In some situations we may need additional information as part of the entry process, such as a prize selection choice. These sweepstakes are voluntary. We recommend that you read the rules for each sweepstakes and contest that you enter.</li>
      </ul>
      <p><b>Information We Get From Others.</b> We may get information about you from other sources. We may add this to information we get from this Site.</p>
      <p><b>Information Automatically Collected.</b> We automatically log information about you and your computer. For example, when visiting our Site, we log the website you visited before browsing to our Site, pages you viewed, how long you spent on a page and information about your use of and actions on our Site. </p>
      <p><b>Cookies.</b> We may log information using "cookies." Cookies are small data files stored on your hard drive by a website. We may use both session Cookies (which expire once you close your web browser) and persistent Cookies (which stay on your computer until you delete them) to provide you with a more personal and interactive experience on our Site.    This type of information is collected to make the Site more useful to you and to tailor the experience with us to meet your special interests and needs.</p>
      <h4>USE OF PERSONAL INFORMATION</h4>
      <p>We use your personal information as follows:</p>
      <ul class="ordered_list">
        <li>We use your personal information to operate, maintain, and improve our sites, products, and services.</li>
        <li>We use your personal information to respond to comments and questions and provide customer service.</li>
        <li>We use your personal information to send information including confirmations, invoices, technical notices, updates, security alerts, and support and administrative messages.</li>
        <li>We use your personal information to communicate about promotions, upcoming events, and other news about products and services offered by us and our selected partners.</li>
        <li>We use your personal information to protect, investigate, and deter against fraudulent, unauthorized, or illegal activity.</li>
        <li>We use personal information to match matches intended for peer groups.</li>
      </ul>
      <h4>SHARING OF PERSONAL INFORMATION</h4>
      <p>We may share personal information as follows:</p>
      <ul class="ordered_list">
        <li>We may share personal information with your consent. For example, you may let us share personal information with others for their own marketing uses. Those uses will be subject to their privacy policies.</li>
        <li>We may share some or all of your personal information in connection with or during negotiation of any merger, financing, acquisition or dissolution transaction or proceeding involving sale, transfer, divestiture, or disclosure of all or a portion of our business or assets.  In the event of an insolvency, bankruptcy, or receivership, personal information may also be transferred as a business asset. If another company acquires our company, business, or assets, that company will possess the personal information collected by us and will assume the rights and obligations regarding your personal information as described in this Privacy Policy. We may share personal information for legal, protection, and safety purposes.
          <ul>
            <li>We may share information to comply with laws.</li>
            <li>We may share information to respond to lawful requests and legal processes.</li>
            <li>We may share information to protect the rights and property of My Game Changer, Inc., our agents, customers, and others. This includes enforcing our agreements, policies, and terms of use.</li>
            <li>We may share information in an emergency. This includes protecting the safety of our employees and agents, our customers, or any person.</li>
          </ul>
        </li>
        <li>We may share information with those who need it to do work for us.</li>
      </ul>
      <p>We may also share aggregated and/or anonymized data with others for their own uses.</p>
      <h4>INFORMATION CHOICES AND CHANGES</h4>
      <p>Our marketing emails tell you how to “opt-out.” If you opt out, we may still send you non-marketing emails. Non-marketing emails include emails about your accounts and our business dealings with you.</p>
      <p>You may send requests about personal information to our Contact Information below. You can request to change contact choices, opt-out of our sharing with others, and update your personal information.</p>
      <p>You can typically remove and reject cookies from our Site with your browser settings. Many browsers are set to accept cookies until you change your settings. If you remove or reject our cookies, it could affect how our Site works for you. </p>
      <p><b>CONTACT INFORMATION.</b> We welcome your comments or questions about this privacy policy. You may also contact us at our address:</p>
      <p>My Game Changer, Inc. <br>329 Primrose Road<br>Burlingame, California 94011-292</p>
      <p><b>CHANGES TO THIS PRIVACY POLICY.</b> We may change this privacy policy. If we make any changes, we will change the Last Updated date above.</p>
  </div>
</div>
</div>  

<?php
get_footer();
