<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package NxtChptr
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function nxtchptr_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class' , 'nxtchptr_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function nxtchptr_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head' , 'nxtchptr_pingback_header' );


function nxtchptr_descriptive_skills() {
	return  '<optgroup label="Communication">
		<option>writes clearly and concisely</option> 
		<option>speaks effectively</option> 
		<option>listens attentively</option> 
		<option>openly expresses ideas</option> 
		<option>negotiates/resolves differences</option> 
		<option>leads group discussions</option> 
		<option>provides feedback</option> 
		<option>persuades others</option> 
		<option>provides well-thought out solutions</option> 
		<option>gathers appropriate information</option> 
		<option>confidently speaks in public</option> 
	</optgroup>
	<optgroup label="Interpersonal Skills">
		<option>works well with others</option> 
		<option>sensitive</option> 
		<option>supportive</option> 
		<option>motivates others</option> 
		<option>shares credit</option> 
		<option>counsels</option> 
		<option>cooperates</option> 
		<option>delegates effectively</option> 
		<option>represents others</option> 
		<option>understands feelings</option> 
		<option>self-confident</option> 
		<option>accepts responsibility</option> 
		</optgroup>
	<optgroup label="Research and Planning">
		<option>forecasts/predicts</option> 
		<option>creates ideas</option> 
		<option>identifies problems</option> 
		<option>meets goals</option> 
		<option>identifies resources</option> 
		<option>gathers information</option> 
		<option>solves problems</option> 
		<option>defines needs</option> 
		<option>analyzes issues</option> 
		<option>develops strategies</option> 
		<option>assesses situations</option> 
		</optgroup>
	<optgroup label="Organizational Skills">
		<option>handles details</option>
		<option>coordinates tasks</option>
		<option>punctual</option>
		<option>manages projects effectively</option>
		<option>meets deadlines</option>
		<option>sets goals</option>
		<option>keeps control over budget</option>
		<option>plans and arranges activities</option>
		<option>multi-tasks</option>
		</optgroup>
	<optgroup label="Management Skills">
		<option>leads groups</option>
		<option>teaches/trains/instructs</option>
		<option>counsels/coaches</option>
		<option>manages conflict</option>
		<option>delegates responsibility</option>
		<option>makes decisions</option>
		<option>directs others</option>
		<option>implements decisions</option>
		<option>enforces policies</option>
		<option>takes charge</option>
	</optgroup>';
}