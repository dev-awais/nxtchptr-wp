<?php
/**
 * The template for displaying buddypress pages
 *
 * @package NxtChptr
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php
			if ( have_posts() ) : the_post(); 
 				the_content();
			endif; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
