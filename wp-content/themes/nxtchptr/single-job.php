<?php
/**
 * The template for displaying a job posting
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="job-posting">
				<?php the_post(); the_content(); ?>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
