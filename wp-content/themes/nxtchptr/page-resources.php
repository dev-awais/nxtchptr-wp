<?php
/**
 * The template for displaying the self discovery page
 */

get_header(); ?>

	<div class="resources-wrap">
		<?php include 'pages/includes/book-suggestions.php' ?>			
	</div>

<?php
get_footer();
