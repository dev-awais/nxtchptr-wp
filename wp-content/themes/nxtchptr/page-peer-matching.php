<?php
/**
 * The template for displaying the peer matching page
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="personal-area-nav">
				<?php bp_nav_menu() ?>
			</section>
			<section class="blocks">
				<div class="blocks__block blocks__block--dark blocks__block--title">
					<div class="block__content">
						<h1>Your Peer Groups</h1>
					</div>
				</div>
				<div class="blocks__block" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/images/feature.png"></div>
			</section>
			<?php if( !bp_get_total_group_count_for_user( bp_loggedin_user_id() ) || !bp_has_groups() ) : ?>
				<section class="blocks">
					<div class="blocks__block">
						<div class="block__content">
							<p>You don't have a peer group yet.</p>
							<a href="/peer-matching-form" class="btn btn--primary">Create one here</a>
						</div>
					</div>
				</section>
			<?php else: ?>
				<section class="personal-area-nav personal-area-nav--light">
					<ul id="groups-list">
					<?php while ( bp_groups() ) : bp_the_group(); 
						$classes = $groups_template->current_group == 0 ? ['current-menu-item'] : []; 
					?>
						<li <?php bp_group_class($classes); ?>>
							<?php if ( ! bp_disable_group_avatar_uploads() ) : ?>
								<div class="item-avatar">
									<a href="<?php bp_group_permalink(); ?>"><?php bp_group_avatar( 'type=thumb&width=50&height=50' ); ?></a>
								</div>
							<?php endif; ?>
							<?php bp_group_link(); ?>
						</li>
					<?php endwhile; ?>
					</ul>
					<!-- <a href="/peer-matching-form" class="btn btn--primary btn--small">Create another peer group</a> -->
				</section>
				<?php /*<section class="peer-group">
					<table class="peer-group__list">
						<tr>
							<th></th>
							<th>Peer name</th>
							<th>Email</th>
							<th>Skype</th>
							<th>Remove</th>
						</tr>
						<?php if ( bp_has_groups() ) : 
							bp_the_group();
			    		if ( bp_group_has_members(['group_id' => bp_get_group_id()]) ) : 
			    			$skype_ids = [];
			    			while ( bp_group_members() ) : bp_group_the_member(); 
			    				$skype_ids[] = xprofile_get_field_data( 'Skype name', bp_get_member_user_id() );
			    				?>
									<tr>
										<td><?php bp_group_member_avatar_thumb() ?></td>
										<td><?php bp_group_member_name() ?></td>
										<td><?php bp_member_user_email() ?></td>
										<td><?php echo xprofile_get_field_data( 'Skype name', bp_get_member_user_id() ); ?></td>
										<td><i class="fa fa-times-circle"></i></td>
									</tr>
							  <?php endwhile; ?>
			    		<?php endif; ?>
			    	<?php endif; ?>
					</table>
					<div class="peer-group__content">
						<a href="skype:<?php echo implode(';', $skype_ids) ?>?chat" class="btn btn--primary">Start Chatting</a>
						<h1>Peer Suggestions</h1>
					</div>
					<table class="peer-group__list">
						<tr>
							<th>Peer name</th>
							<th>Percentage Match</th>
							<th></th>
							<th>Add to group</th>
						</tr>
						<tr>
							<td>DR</td>
							<td><span class="match-percent">65%</span> <span class="match-bar"><span class="match-bar__inner" style="width: 65%"></span></span></td>
							<td><a href="#" class="btn btn--outline btn--outline-dark btn--small">Send Message</a></td>
							<td><i class="fa fa-plus-circle"></i></td>
						</tr>
						<tr>
							<td>EG</td>
							<td><span class="match-percent">40%</span> <span class="match-bar"><span class="match-bar__inner" style="width: 40%"></span></span></td>
							<td><a href="#" class="btn btn--outline btn--outline-dark btn--small">Send Message</a></td>
							<td><i class="fa fa-plus-circle"></i></td>
						</tr>
						<tr>
							<td>MT</td>
							<td><span class="match-percent">35%</span> <span class="match-bar"><span class="match-bar__inner" style="width: 35%"></span></span></td>
							<td><a href="#" class="btn btn--outline btn--outline-dark btn--small">Send Message</a></td>
							<td><i class="fa fa-plus-circle"></i></td>
						</tr>
						<tr>
							<td>CD</td>
							<td><span class="match-percent">20%</span> <span class="match-bar"><span class="match-bar__inner" style="width: 20%"></span></span></td>
							<td><a href="#" class="btn btn--outline btn--outline-dark btn--small">Send Message</a></td>
							<td><i class="fa fa-plus-circle"></i></td>
						</tr>
					</table>
				</section> */?>
			<?php endif; ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
