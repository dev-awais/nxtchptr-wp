<?php
/**
 * The template for displaying the terms-and-conditions page
 */

get_header(); ?>

	<div class="faq-wrap">
  <div class="wrapper">
    <h2 class="mb-4">FAQ</h2>   
    <h3 class="mb-3">Nxt Chptr for Students</h3>

    <div class="accordion" id="accordionStudents">
      <div class="card">
        <div class="card-header" id="headingOne">
          <h5 class="mb-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <b>How can Nxt Chptr help me?</b>
          </h5>
        </div>
        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionStudents">
          <div class="card-body">
            <p>Nxt Chptr aims to make coaching easier and more affordable. We take the hard and costly part out of coaching, and we leave in what makes coaching so helpful—making meaningful change.</p>
            <p>Specifically, we host live, group video classes with talented and vetted career & life coaches. What this means is that we find great coaches, and we help them create impactful group classes for people everywhere.</p>
            <p>For students, our goal is to make it easier to hire a coach and to be a trusted matchmaker.</p>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" id="headingTwo">
          <h5 class="mb-0" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            <b>Why group classes? </b>
          </h5>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionStudents">
          <div class="card-body">
              Because they are more affordable than one-on-one sessions. Group classes means that you can work with some of the best coaches out there. Plus, it means that if you are considering one-on-one coaching, you can test out the coaches first.  
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" id="headingThree">
          <h5 class="mb-0" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
            <b>Why online classes?</b>
          </h5>
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionStudents">
          <div class="card-body">
              Because you don’t have to leave the comfort of your own home. And, because you can connect to excellent coaches who don’t live near you.
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" id="headingFour">
          <h5 class="mb-0" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
            <b>Why live classes?</b>
          </h5>
        </div>
        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionStudents">
          <div class="card-body">
              So that you can ask questions, and so that everyone can learn from each other in real time. It’s possible that you may even make a friend or two. 
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" id="headingFive">
          <h5 class="mb-0" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
            <b>Why coaching?</b>
          </h5>
        </div>
        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionStudents">
          <div class="card-body">
              <p>At some point everyone needs a guide to create real change. </p>
              <ul>
                <li>Maybe you are looking to switch careers. Coaching can help with that.</li>
                <li>Maybe you are looking to get a big promotion and jet out of a sleepy career track. Coaching can help with that.</li>
                <li>Maybe you aren’t working after an extended absence. Further, you have low confidence in how to tell your story and which of your abilities are marketable. Coaching can help with that. </li>
                <li>Maybe you are experiencing a life change such as divorce or death, and you need help with moving forward or finding a new path. Coaching can help with that.</li>
              </ul>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" id="headingSix">
          <h5 class="mb-0" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
            <b>Why should I think about taking group classes via Nxt Chptr instead of working with a coach on my own?</b>
          </h5>
        </div>
        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionStudents">
          <div class="card-body">
            <p>For so many reasons!</p>
            <p>Nxt Chptr solicits user reviews from coaches so you aren’t hiring a coach without having a sense of his or her expertise and demeanor or what his or her classes are like. Our reviews help winnow down exactly what you want and give you confidence that the money you spend for guidance is going to be well spent. All reviews are our website are written by an actual person who took the class with that coach.</p>
            <p>Nxt Chptr makes coaching so much more affordable because group sessions are by definition more affordable than one-on-one coaching. If a coach is offering a session with twenty students, that will always be more affordable than a session with one student.</p>
            <p>Nxt Chptr makes it easy. We are a kind of one-stop shop for finding vetted, excellent coaches.</p>   
            <p>Plus, all we do is connect people looking for coaching. We don’t try to selling you anything else—not a job, no advertising, etc. We just hope to be the catalyst for your most desired life change.</p>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" id="headingSeven">
          <h5 class="mb-0" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
            <b>Why should I think about taking group classes via Nxt Chptr instead of working with a coach on my own?</b>
          </h5>
        </div>
        <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionStudents">
          <div class="card-body">
            <p>We have a 72 hour (3 day) cancellation policy before the class.  This is so that we can fill the spot should you decide to cancel.</p>
            <p>If you need to talk to us about a problem with a class or cancellation fee, contact <a href="mailto:admin@nxt-chptr.com">admin@nxt-chptr.com</a></p>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" id="headingEight">
          <h5 class="mb-0" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
            <b>What if I am looking for a class that doesn’t exist?</b>
          </h5>
        </div>
        <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionStudents">
          <div class="card-body">
            <p>Let us know! We are always looking to serve our students better. What better way than receiving suggestions for a type of class or coach? Email us: Fjain at Nxt-Chptr dot com.</p>
          </div>
        </div>
      </div>

    </div>
    <h3 class="mb-3 mt-5">Nxt Chptr for Coaches</h3>
    <div class="accordion" id="accordionCoaches">
      <div class="card">
        <div class="card-header" id="cheadingOne">
          <h5 class="mb-0" data-toggle="collapse" data-target="#collapseOnec" aria-expanded="true" aria-controls="collapseOnec">
            <b>Why is Nxt Chptr good for my business?</b>
          </h5>
        </div>
        <div id="collapseOnec" class="collapse show" aria-labelledby="cheadingOne" data-parent="#accordionCoaches">
          <div class="card-body">
            <p>Because we want you to have a great business and help people far and wide. Nxt Chptr seeks to be a matchmaker between coaches and students, helping to elevate everyone’s game.</p>
            <p>We want to bring more people to you for group classes. If a Nxt Chptr group class client wants to hire you for one-on-one coaching, go for it. Feel free to work directly with Nxt Chptr group clients off our website. We only care about group classes. We hope to be your resource for client lead generation and group class administration.</p>
            <p>In addition, we want to provide a virtual set up that mimics how you currently conduct your business. You decide the: content, class times, attendance maximum, cost per person, and length of session/series. We hope to be exactly what you would design for yourself.</p>
            <p>If you would like to add a feature, contact Francie Jain, Nxt Chptr’s founder, fjain at nxt-chpt dot com. Odds are, we will add it!</p>
          </div>
        </div>
      </div> 
      <div class="card">
        <div class="card-header" id="cheadingTwo">
          <h5 class="mb-0" data-toggle="collapse" data-target="#collapseTwoC" aria-expanded="false" aria-controls="collapseTwoC">
            <b>How does it work to host a class on Nxt Chptr?</b>
          </h5>
        </div>
        <div id="collapseTwoC" class="collapse" aria-labelledby="cheadingTwo" data-parent="#accordionCoaches">
          <div class="card-body">
            <p>It's pretty easy. Sign up as a coach on the home page. Once you are approved, you can propose classes.</p>  
            <p>We ask that coaches propose classes so that we can review them before they go live. This is for a variety of reasons related to keeping the class quality high and setting you up for success.</p>  
            <p>A portion of the gross value of the class goes to Nxt Chptr for administration and marketing costs, but you keep the rest. Once you register as a coach, we can go into greater detail on the costs and options.</p>  
          </div>
        </div>
      </div>  
    </div>
    <h3 class="mb-3 mt-5">Help</h3>
    <div class="accordion" id="accordionHelp">
      <div class="card">
        <div class="card-header" id="HheadingOne">
          <h5 class="mb-0" data-toggle="collapse" data-target="#collapseOneH" aria-expanded="true" aria-controls="collapseOneH">
            <b>How do I change my password?</b>
          </h5>
        </div>
        <div id="collapseOneH" class="collapse show" aria-labelledby="HheadingOne" data-parent="#accordionHelp">
          <div class="card-body">
            <p>Simply select “change my password” in the Settings section in your Portal. Once you do that, a temporary password will be generated and sent to the email address on file. </p>
          </div>
        </div>
      </div> 
      <div class="card">
        <div class="card-header" id="HheadingTwo">
          <h5 class="mb-0" data-toggle="collapse" data-target="#collapseTwoH" aria-expanded="false" aria-controls="collapseTwoH">
            <b>Who do I contact for help?</b>
          </h5>
        </div>
        <div id="collapseTwoH" class="collapse" aria-labelledby="HheadingTwo" data-parent="#accordionHelp">
          <div class="card-body">
              <ul>
                <li>If you are having trouble with the website, please contact <a href="mailto:admin@nxt-chptr.com">admin@nxt-chptr.com</a></li>
                <li>For any regular course of business questions and concerns, please contact: <a href="mailto:admin@nxt-chptr.com">admin@nxt-chptr.com</a>  </li>
                <li>If you would like to speak directly to our founder Francie Jain, please email fjain at nxt-chptr dot com.</li>
              </ul>
          </div>
        </div>
      </div>  
      <div class="card">
        <div class="card-header" id="HheadingThree">
          <h5 class="mb-0" data-toggle="collapse" data-target="#collapseThreeH" aria-expanded="false" aria-controls="collapseThreeH">
            <b>What happens if I encounter inappropriate behavior?</b>
          </h5>
        </div>
        <div id="collapseThreeH" class="collapse" aria-labelledby="HheadingThree" data-parent="#accordionHelp">
          <div class="card-body">
              Nxt Chptr is meant to be a safe and supportive environment. Inappropriate behavior may lead to expulsion from the website. If you experience behavior that is anything other than supportive, please write us a note at fjain at nxt-chptr dot com about your experience. We will address your concerns immediately.
          </div>
        </div>
      </div>  
    </div>
  </div>
</div>

<?php
get_footer();
?>
<script>
 jQuery(document).ready(function(){
  jQuery('.card > .card-header').click(function(){
    jQuery(".collapse").slideUp();    
    jQuery(this).next(".collapse").slideDown();
  })
}); 


</script>
