<?php
/**
 * The template for displaying the self discovery page
 */

get_header(); ?>
	<div class="def-title">
		<div class="wrapper">
			<h1>Self Discovery</h1>
		</div>
	</div>
	<div class="sd-wrap">
		<div class="wrapper">
			<div class="row">
				<div class="col-lg-4 col-md-6 mb-4">
					<div class="sd-box">
						<img src="<?php echo site_url() ?>/wp-content/themes/nxtchptr/images/new/self-discovery/group-video.png">
						<h3>Group Video Coaching</h3>
						<p>Ever feel like you want an actual person to help you? We are working on affordable career and life coaching via group video. Please take a look at our upcoming test classes.</p>
						<a class="btn-sd" href="https://nxtchptrgroupvideoclasses.as.me">Group Classes</a>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 mb-4">
					<div class="sd-box">
						<img src="<?php echo site_url() ?>/wp-content/themes/nxtchptr/images/new/self-discovery/resources.png">
						<h3>Resources</h3>
						<p>Sometimes a great read can make all of the difference. Buy these books or borrow them from the library, but definitely read them for ideas and strategies about change and growth.</p>
						<a class="btn-sd" href="/resources">Recommended Books</a>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 mb-4">
					<div class="sd-box">
						<img src="<?php echo site_url() ?>/wp-content/themes/nxtchptr/images/new/self-discovery/self.png">
						<h3>Self Inventory</h3>
						<p>This is one of our favorite tools: a guided questionnaire to look at your past decisions, your dreams, your needs, and your plans. The questionnaire can be something you refer to monthly, use as guided reflection for journal writing or just as food for thought.</p>
						<a class="btn-sd" href="/wp-content/themes/nxtchptr/documents/Nxt Chptr Self Inventory.pdf" target="_blank">Download PDF</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- <div id="primary" class="content-area content-area--no-image pt-4">
		<main id="main" class="site-main">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 mb-5 mb-lg-0 col-lg-4">
						
					</div>
					<div class="col-xs-12 mb-5 mb-lg-0 col-lg-4">
						
					</div>
					<div class="col-xs-12 mb-5 mb-lg-0 col-lg-4">
						
					</div>
					<div class="col-12 mt-3 mb-5 mt-lg-5">
						<p>If you would like to be informed about the progress of group video coaching and when group video coaching is live, please add your email address to our distribution list.</p>
						<form class="row align-items-center" action="https://nxt-chptr.us15.list-manage.com/subscribe/post?u=c0e06a0414ea9b9e1fc566b90&amp;id=37f80c4a2c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank">
							<div class="col-12 col-sm-6">
								<input class="form-control mb-sm-0" type="email" name="EMAIL" placeholder="Email">
							</div>
							<div class="col-12 col-sm-6">
								<input class="btn btn--small btn--primary" type="submit" name="subscribe" value="Subscribe">
							</div>
							<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_c0e06a0414ea9b9e1fc566b90_37f80c4a2c" tabindex="-1" value=""></div>
						</form>
					</div>
				</div>
			</div>			
		</main>
	</div> -->

<?php
get_footer();
