<section class="hero-section">
	<div class="hero-section__img hero-section__img--extra-large" style="background-image: url(<?php the_field('hero_image_guest') ?>)"></div>
	<div class="hero-section__content hero-section__content--inverse">
		<h1><?php the_field('hero_heading') ?></h1>
		<p><?php the_field('hero_introduction_guest') ?></p>
		<a href="<?php echo site_url('membership') ?>" class="btn btn--primary">Get Started</a>
	</div>
</section>