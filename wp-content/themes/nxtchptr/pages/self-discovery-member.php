<section class="blocks">
	<div class="blocks__block blocks__block--image blocks__block--sm-bg" style="background-image: url(<?php the_field('hero_image_member') ?>); background-position: center center;"></div>
	<div class="blocks__block blocks__block--sm-content">
		<div class="block__content">
			<h1><?php the_field('hero_heading') ?></h1>
			<?php the_field('hero_introduction') ?>
			<a class="btn btn--primary" href="<?php the_field('self_inventory_pdf') ?>" download target="_blank">Download Self Inventory PDF</a><br>
		</div>
	</div>
</section>
<?php include 'includes/book-suggestions.php' ?>
