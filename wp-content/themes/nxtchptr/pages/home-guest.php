<div class="landing-sect01">
  <div class="wrapper">
    <div class="row align-items-center">
      <div class="col-lg-6 col-md-12">
        <div class="content-sect01">
          <h4>Change can be scary and hard<br> But it doesn’t need to be expensive or lonely</h4>
          <h2>Live, Online Classes to Help You Embrace Your Next Chapter.</h2>
          <p>At Nxt Chptr we connect those approaching any transition in their lives with resources and qualified coaches. We believe in joyful change. What about you?</p>
          <a class="btn-cstm" href="https://marketplace.nxt-chptr.com/">Check Out Our Classes!</a>
        </div>
      </div>
      <div class="col-6 landing-pageimg">
        <img src="<?php echo site_url() ?>/wp-content/themes/nxtchptr/images/new/landing-page-01.png">
      </div>
    </div>
  </div>
</div>
<div class="landing-sect-featured">
  <div class="wrapper">
    <div class="row">
      <div class="col-md-4">
        <div class="landing-sectf-box lsb-01">
          <h6>Global reach</h6>
          <h2>The best teachers.</h2>
          <div>
            <a href="https://nxt-chptr.com/mom-profiles/"><span>Learn More</span></a>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="landing-sectf-box lsb-02">
          <h6>Your own pace</h6>
          <h2>Schedule classes and get notified.</h2>
          <div>
            <button href="https://marketplace.nxt-chptr.com/"><span>Learn More</span></button>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="landing-sectf-box lsb-03">
          <h6>Something for everyone</h6>
          <h2>Wide variety of courses.</h2>
          <div>
            <button href="https://marketplace.nxt-chptr.com/"><span>Learn More</span></button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="landing-sect02">
  <div class="wrapper">
    <h2 class="title">Featured this Month</h2>
    <div class="row">
		<?php
			$request = wp_remote_get( 'https://nxtchptr.azurewebsites.net/api/course/featured' );
			if( is_wp_error( $request ) ) {
				return false; // Bail early
			}
			$body = wp_remote_retrieve_body( $request );
			$data = json_decode( $body );
			if(empty( $data ) ) {
				echo'<p class="text-center">no results found...</p>';
			}
			if( ! empty( $data ) ) {
				
				foreach( $data as $courses ) {
					echo'<div class="col-lg-4 col-md-6">';
						echo'<div class="featured-courses">';
							echo'<div class="fc-title">';
								echo'<img src="'. $courses->DisplayPicture . '">';
								echo'<h4><a href="https://marketplace.nxt-chptr.com/courses/details?id='.$courses->TypeID.'">'. $courses->Title .'</a></h4>';
							echo'</div>';
							echo'<div class="fc-tdetails">';
								echo'<div class="fc-time">'. $courses->Duration .'</div>';
								echo'<div class="fc-students"><i class="fa fa-globe"></i> '.$courses->CourseLanguage.'</div>';
							echo'</div>';
							echo'<div class="fc-bdetails">';
								echo'<div class="fc-teacher"><a href="https://marketplace.nxt-chptr.com/coach/details?id='.$courses->CoachID.'">'. $courses->FullName.' </a></div>';
							echo'</div>';
						echo'</div>';	
					echo'</div>';
				}
			}
		?>	
    </div>
    <div class="btn-sect02" *ngIf="lstfeaturedcourses.length > 0">
      <a class="btn-cstm" href="https://marketplace.nxt-chptr.com/">See More Courses</a>
    </div>
  </div>
</div>
<div class="landing-sect03">
  <div class="wrapper">
    <div class="row justify-content-md-end">
      <div class="col-lg-7 col-md-8 text-center">
        <h2>Make The Life Change You've Been Looking For</h2>
        <p>Our students find new paths and develop new skills everyday - in work, their personal lives, and beyond. We host classes that help you turn over a new leaf, learn a skill or join a discussion. And if you don’t see what you want, let us know. We love to build classes based on customer requests.</p>
        <div class="sect03-btns">
          <a href="https://marketplace.nxt-chptr.com/" class="btn-cstm reverse">Search Our Courses</a>
          <a href="https://marketplace.nxt-chptr.com/?type=signup" class="btn-cstm reverse">Become a Student</a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="landing-sect04">
  <div class="wrapper">
    <div class="sect04-top">
      <div class="row justify-content-between align-items-center">
        <div class="col-lg-5 col-12">
          <h2 class="title">Trending Categories</h2>
        </div>
        <div class="col-lg-7 col-12">
		<?php
			$request = wp_remote_get( 'https://nxtchptr.azurewebsites.net/api/category/trending' );
			if( is_wp_error( $request ) ) {
				return false; // Bail early
			}
			$body = wp_remote_retrieve_body( $request );
			$data = json_decode( $body );
			echo'<ul class="nav nav-tabs" id="myTab" role="tablist">';
				if( ! empty( $data ) ) {
					if( ! empty($data->Result[0] ) ) {	
						$title = $data->Result[0]->Title;
						echo'<li class="nav-item">';
							echo'<a data-toggle="tab" href="#cate1" role="tab" aria-selected="true" class="active">'.$title.'</a>';
						echo'</li>';
					}
					if( ! empty($data->Result[1] ) ) {	
						$title = $data->Result[1]->Title;
						echo'<li class="nav-item">';
							echo'<a data-toggle="tab" href="#cate2" role="tab" aria-selected="false">'.$title.'</a>';
						echo'</li>';
					}
					if( ! empty($data->Result[1] ) ) {	
						$title = $data->Result[2]->Title;
						echo'<li class="nav-item">';
							echo'<a data-toggle="tab" href="#cate3" role="tab" aria-selected="false">'.$title.'</a>';
						echo'</li>';
					}
				}		
			echo'</ul>';
		?>			          
        </div>
      </div>
    </div>
    <div class="sect04-btm">
      <div class="tab-content" id="myTabContent">
	  <?php
			$request = wp_remote_get( 'https://nxtchptr.azurewebsites.net/api/category/trending' );
			if( is_wp_error( $request ) ) {
				return false; // Bail early
			}
			$body = wp_remote_retrieve_body( $request );
			$data = json_decode( $body );
			
			if( ! empty( $data ) ) {
				if(empty( $data->Result->Course ) ) {
					echo'<p class="text-center">no results found...</p>';
				}
				if(  !empty($data->Result[0]->Course ) ) {	
					$valAr = $data->Result[0]->Course;
						echo'<div id="cate1" class="tab-pane active">';
							echo'<div class="row justify-content-center">';
							foreach( $valAr as $cat ) {
								echo'<div class="col-lg-4 col-md-6">';
									echo'<div class="fc2-article" routerLink="/courses/details" [queryParams]="{ id : c.CourseID }">';
										echo'<img class="fc2-featuredimg" src="'.$cat->CoursePicture.'">';
										echo'<div class="fc2-content">';
											echo'<h4>'.$cat->CourseTitle.'</h4>';
											echo'<p>'.$cat->Description.'</p>';
											echo'<div class="btm-f2c">';
												echo'<span>'.$cat->CoursePrice.'</span>';
												echo'<button routerLink="/courses/details" [queryParams]="{ id : c.CourseID }">Buy Now</button>';
											echo'</div>';
										echo'</div>';
									echo'</div>';
								echo'</div>';
							}	
							echo'</div>';
						echo'</div>';
				}
				if(  !empty($data->Result[1]->Course ) ) {	
					$valAr = $data->Result[1]->Course;
					echo'<div id="cate2" class="tab-pane active">';
						echo'<div class="row justify-content-center">';
						foreach( $valAr as $cat ) {
							echo'<div class="col-lg-4 col-md-6">';
								echo'<div class="fc2-article" routerLink="/courses/details" [queryParams]="{ id : c.CourseID }">';
									echo'<img class="fc2-featuredimg" src="'.$cat->CoursePicture.'">';
									echo'<div class="fc2-content">';
										echo'<h4>'.$cat->CourseTitle.'</h4>';
										echo'<p>'.$cat->Description.'</p>';
										echo'<div class="btm-f2c">';
											echo'<span>'.$cat->CoursePrice.'</span>';
											echo'<button routerLink="/courses/details" [queryParams]="{ id : c.CourseID }">Buy Now</button>';
										echo'</div>';
									echo'</div>';
								echo'</div>';
							echo'</div>';
						}	
						echo'</div>';
					echo'</div>';
				}
				
				if(  !empty($data->Result[2]->Course ) ) {	
					$valAr = $data->Result[2]->Course;
					echo'<div id="cate2" class="tab-pane active">';
						echo'<div class="row justify-content-center">';
						foreach( $valAr as $cat ) {
							echo'<div class="col-lg-4 col-md-6">';
								echo'<div class="fc2-article" routerLink="/courses/details" [queryParams]="{ id : c.CourseID }">';
									echo'<img class="fc2-featuredimg" src="'.$cat->CoursePicture.'">';
									echo'<div class="fc2-content">';
										echo'<h4>'.$cat->CourseTitle.'</h4>';
										echo'<p>'.$cat->Description.'</p>';
										echo'<div class="btm-f2c">';
											echo'<span>'.$cat->CoursePrice.'</span>';
											echo'<button routerLink="/courses/details" [queryParams]="{ id : c.CourseID }">Buy Now</button>';
										echo'</div>';
									echo'</div>';
								echo'</div>';
							echo'</div>';
						}	
						echo'</div>';
					echo'</div>';
				}
			}
		?>  
        
      </div>
	</div>
  </div>
</div>
<div class="landing-sect05">
  <div class="wrapper">
    <div class="row align-items-center mb-5 grid-rev-md">
      <div class="col-lg-6 col-md-12">
        <div class="sect05-content">
          <h2>Calling All Coaches</h2>
          <p>Do you have great content, but need help with the marketing? Are you gaining traction, but want to to help your client while still retaining work-life balance? NXT CHPTR gives you an effortless and worldwide audience in the comfort of your own home! Group classes defined entirely by each coach to address his or her goals: Large/small classes--up to you; high rate/low rate--up to you; time of day--up to you. We are an extension of your existing coaching model, as personal and unique as you.</p>
          <a href="https://marketplace.nxt-chptr.com/?type=signup" class="btn-cstm" >Become a Coach</a>
        </div>
      </div>
      <div class="col-lg-6 col-md-12">
        <img class="sect05-img" src="<?php echo site_url() ?>/wp-content/themes/nxtchptr/images/new/calling-coaches.jpeg">
      </div>
    </div>
    <div class="row align-items-center">
      <div class="col-lg-6 col-md-12">
        <img class="sect05-img" src="<?php echo site_url() ?>/wp-content/themes/nxtchptr/images/new/new-start.jpg">
      </div>
      <div class="col-lg-6 col-md-12">
        <div class="sect05-content aligned-right">
          <h2>A New Start</h2>
          <p>We’ve partnered with Simple Steps, an organization that helps immigrant mothers looking to return to work. If you are looking for in-person support and you live in Northern California, Simple Steps is a great place to plot your triumphant return to career. https://www.simplestepscc.org/</p>
          <a target="_blank" href="https://www.simplestepscc.org/" class="btn-cstm">Learn More</a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="landing-sect06">
  <div class="wrapper">
    <div class="sect06-top">
      <h2>What Our Students Say</h2>
      <h4>Nxt Chptr helps career changers connect to great coaches. <br>Here's what our students have to say.</h4>
    </div>
    <div class="sect06-btm">
      <div class="row justify-content-center">
        <div class="col-lg-4 col-md-6">
          <div class="testiNbox">
            <h4>Bronia Hill</h4>
            <p>"Bronia's class on developing a personal narrative was very helpful.
              I really appreciated that she sent a slide deck before the class.
              After she talked through it during the session it became very actionable.
              Highly recommend her class."</p>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 testi-middle">
          <div class="testiNbox">
            <h4>Carly Janson</h4>
            <p>"I'd definitely recommend Carly to a friend. She was warm, friendly, personal, and also had great
              expertise and experience she brought into the session. I would love to work with Carly more!"</p>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 testi-last">
          <div class="testiNbox">
            <h4>Stephanie Freeth</h4>
            <p>"Stephanie delivered her points well. She set expectations by using an agenda, so everyone knew what was
              going to be covered."</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
