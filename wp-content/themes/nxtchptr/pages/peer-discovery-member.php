<section class="blocks">
	<div class="blocks__block blocks__block--dark blocks__block--title blocks__block--sm-content">
		<div class="block__content">
			<h1><?php the_field('hero_heading') ?></h1>
		</div>
	</div>
	<div class="blocks__block blocks__block--sm-bg"  style="background-image: url('<?php the_field('hero_image_member') ?>')"></div>
</section>
<section class="blocks blocks--stack-sm">
	<div class="blocks__block blocks__block--light">
		<div class="block__content">
			<?php the_field('introduction') ?>
			<a href="<?php echo site_url('peer-matching-form') ?>" class="btn btn--primary"><?php the_field('peer_group_link_text') ?></a>
		</div>
	</div>
	<div class="blocks__block blocks__block--light blocks__block--adjacent">
		<div class="block__content">
			<h6><?php the_field('peer_group_rules_heading') ?></h6>
			<?php if(have_rows('peer_group_rules')): ?>
				<?php while(have_rows('peer_group_rules')): the_row() ?>
					<p><?php the_sub_field('title') ?>: <?php the_sub_field('description') ?></p>
				<?php endwhile ?>
			<?php endif ?>
		</div>
	</div>
</section>
<?php if(have_rows('getting_started_sections')): ?>
	<section class="blocks">
		<div class="blocks__block">
			<div class="block__content">
				<h1><?php the_field('getting_started_header') ?></h1>
			</div>
		</div>
	</section>
	<?php 
		$count = 0;
		$total_rows = count(get_field('getting_started_sections'));
		while(have_rows('getting_started_sections')):
		the_row();
			if($count % 2 == 0): ?>
				<section class="blocks blocks--stack-sm">
			<?php endif ?>
			<div class="blocks__block">
				<div class="block__content">
					<div class="highlight-number"><?php echo ++$count ?></div>
					<h6><?php the_sub_field('heading') ?></h6>
					<?php if(have_rows('points')): ?>
						<ul>
						<?php while(have_rows('points')): the_row() ?>
							<li><?php the_sub_field('point') ?></li>
						<?php endwhile ?>
						</ul>
						<?php if($count == $total_rows): ?>
							<a href="<?php echo site_url('peer-matching-form'); ?>" class="btn btn--primary"><?php the_field('peer_group_link_text') ?></a>
						<?php endif ?>
					<?php endif ?>
				</div>
			</div>
		<?php if($count % 2 == 0): ?>
			</section>
		<?php endif ?>
	<?php endwhile ?>
<?php endif ?>
