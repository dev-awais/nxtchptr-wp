<section class="blocks">
	<div class="blocks__block blocks__block--dark blocks__block--title blocks__block--sm-content">
		<div class="block__content">
			<h1><?php the_field('hero_heading') ?></h1>
		</div>
	</div>
	<div class="blocks__block blocks__block--image blocks__block--image-right blocks__block--sm-bg" style="background-image: url(<?php the_field('hero_image_member') ?>); background-position: left center;">
		<div class="block__content">
			<div class="block__overlay">
				<h5><?php the_field('hero_introduction_member') ?></h5>
				<a href="<?php echo wpjb_link_to('step_add') ?>" class="btn btn--primary">Post to the Community</a>
			</div>
		</div>
	</div>
</section>
<section class="jobs-list__hire-mobile">
	<h5><?php the_field('hero_heading') ?></h5>
	<a href="<?php echo wpjb_link_to('step_add') ?>" class="btn btn--primary">Post to the Community</a>
</section>
<section class="jobs-list-container">
	<ul class="jobs-list-header">
		<li class="active"><a href="#">Jobs</a></li>
		<li><a href="#">Community</a></li>
	</ul>
</section>