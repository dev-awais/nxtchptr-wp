<?php global $current_user; ?>
<section class="welcome">
	<h1>Hello, <?php echo $current_user->first_name; ?></h1>
	<h5><?php the_field('introduction_member') ?></h5>
</section>
<?php include 'includes/feature-links.php' ?>
<?php include 'includes/mom-profiles.php' ?>