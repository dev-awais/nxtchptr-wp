<section class="feature-links">
	<?php $url = has_membership() ?  "/images/home peer logged.jpg" : "/images/home peer.jpg"; ?>
	<a href="<?php echo site_url('peer-discovery') ?>" class="feature-link" style="background-image: url('<?php echo get_stylesheet_directory_uri() . $url; ?>')">
		<div class="feature-link__content">
			<h3>Peer Discovery</h3>
			<p><?php the_field('peer_discovery_description') ?></p>
			<span class="btn btn--outline btn--small"><?php the_field('peer_discovery_link_text') ?></span>
		</div>
	</a>
	<?php $url = has_membership() ?  "/images/home self logged.jpg" : "/images/home self.jpg"; ?>
	<a href="<?php echo site_url('self-discovery') ?>" class="feature-link" style="background-image: url('<?php echo get_stylesheet_directory_uri() . $url; ?>')">
		<div class="feature-link__content">
			<h3>Self Discovery</h3>
			<p><?php the_field('self_discovery_description') ?></p>
			<span class="btn btn--outline btn--small"><?php the_field('self_discovery_link_text') ?></span>
		</div>
	</a>
</section>