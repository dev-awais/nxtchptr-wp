<section class="mom-profiles">
  <h2><a href="<?php echo site_url('mom-profiles') ?>">Mom Profiles</a></h2>
  <div class="mom-profiles__container">
    <?php $custom_query = new WP_Query(['suppress_filters' => true, 'posts_per_page' => 2]); 
      while($custom_query->have_posts()) : $custom_query->the_post(); ?>

        <div class="mom-profile">
          <div class="mom-profile__img" style="background-image: url(<?php the_post_thumbnail_url() ?>)"></div>
          <time><?php the_field('publication_date') ?></time>
          <h6><?php the_title() ?></h6>
          <p><?php $intro = strip_tags(get_field('introduction')); echo substr($intro, 0, 339); if(strlen($intro) > 339): ?>&hellip;<?php endif; ?></p>
          <a href="<?php the_permalink() ?>">Read More</a>
        </div>

      <?php endwhile; ?>
      <?php wp_reset_postdata(); // reset the query ?>
    <div class="mom-profile mom-profile--newsletter-signup">
      <?php include 'newsletter-signup.php' ?>
    </div>
  </div>
</section>