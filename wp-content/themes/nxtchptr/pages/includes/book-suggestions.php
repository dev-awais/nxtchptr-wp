



<?php $postID = get_page_by_path('self-discovery', OBJECT, 'page') ?>
<?php if(have_rows('books', $postID)): ?>
    <div class="def-title">
      <div class="wrapper">
        <h1><?php the_field('resources_heading', $postID) ?></h1>
      </div>
    </div>
    <p class="resources__intro"><?php the_field('resources_introduction', $postID) ?></p>
    <div class="wrapper">
      <div class="resources-row">
        <?php while(have_rows('books', $postID)): the_row() ?>
          <div class="resourceBox">
            <a href="<?php the_sub_field('amazon_link') ?>" target="_blank" class="resource__img">
              <img src="<?php the_sub_field('image') ?>">
            </a>
            <div class="resource__content">
              <h5 class="resource__title"><?php the_sub_field('title') ?></h5>
              <small class="resource__author"><?php the_sub_field('author') ?></small>
              <p class="resource__summary"><?php the_sub_field('summary') ?></p>
            </div>
            <?php 
              $freeLink = get_sub_field('free_link');
              if($freeLink): 
            ?>
              <a href="<?php $freeLink ?>" target="_blank" class="resource__link">Free Version</a>
            <?php endif; ?>
          </div>
        <?php endwhile ?>
      </div>
    </div>
<?php endif; ?>