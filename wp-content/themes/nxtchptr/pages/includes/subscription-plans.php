<?php 
global $current_user;
$pmpro_levels = pmpro_getAllLevels(false, true);
$pmpro_level_order = pmpro_getOption('level_order');

if(!empty($pmpro_level_order))
{
	$order = explode(',',$pmpro_level_order);

	//reorder array
	$reordered_levels = array();
	foreach($order as $level_id) {
		foreach($pmpro_levels as $key=>$level) {
			if($level_id == $level->id)
				$reordered_levels[] = $pmpro_levels[$key];
		}
	}

	$pmpro_levels = $reordered_levels;
}
?>
<section class="subscription-plans">
	<?php if($subscriptions_heading): ?>
		<h2><?php echo $subscriptions_heading ?></h2>
	<?php endif; ?>
	<div class="sp-table">
		<div class="sp-table__column sp-table__column--header">
			<div class="sp-table__cell"></div>
			<div class="sp-table__cell"></div>
			<div class="sp-table__cell">"Reflectionnaire"</div>
			<div class="sp-table__cell">Connecting Resources: Peer Discovery & Matching</div>
			<div class="sp-table__cell">Why choose this option?</div>
			<div class="sp-table__cell">Renewal Cost</div>
			<div class="sp-table__cell">Ability to "earn" suite of personality tests with 30 referral purchases</div>
			<div class="sp-table__cell"></div>
		</div>
		<?php 
		foreach($pmpro_levels as $level): 
		  if(isset($current_user->membership_level->ID)){
			  $is_current_level = ($current_user->membership_level->ID == $level->id);
				
		  }else{
	  		$is_current_level = false;
		  }
			if($level->id == 5){ continue; }
			/*if (!isset($current_user->membership_level->ID) || $current_user->membership_level->ID != 5) {
			} else{
				if($level->id != 5){ continue; }
			}*/
				
			if(pmpro_isLevelFree($level)){
				$initial_cost_text = __("Free", 'paid-memberships-pro' );
				$recurring_cost_text = '';
			}else{
				$initial_cost_text = pmpro_formatPrice($level->initial_payment);
				if($level->cycle_period){
					$initial_cost_text .= '<small>/' . $level->cycle_period . '</small>';
				}
				$recurring_cost_text = pmpro_formatPrice("4.99");
			}
			if($level->id == 6){
				$recurring_cost_text = '';
			}
			?>
			<div class="sp-table__column">
				<div class="sp-table__cell sp-table__cell--light sp-table__cell--mobile-dark">
					<h6 class="plan__heading"><?php echo $level->name ?></h6>
				</div>
				<div class="sp-table__cell sp-table__cell--light">
					<div class="plan__price"><?php echo $initial_cost_text ?></div>
				</div>
				<div class="sp-table__cell"><?php the_level_toggle_field('reflectionnaire_&_resources', $level) ?></div>
				<div class="sp-table__cell sp-table__cell--light"><?php the_level_toggle_field('peer_discovery_and_matching', $level) ?></div>
				<div class="sp-table__cell">
					<div class="plan__why"><?php the_level_field('why_choose_this_option', $level) ?></div>
				</div>
				<div class="sp-table__cell sp-table__cell--light"><?php echo $recurring_cost_text ?></div>
				<div class="sp-table__cell">
					<?php the_level_toggle_field('ability_to_earn_suite_of_personality_tests', $level) ?>
				</div>
				<div class="sp-table__cell">
					<?php 
						if($level->id == 6):?>
							<a class="<?php echo $btn_class ?>" href="<?php echo pmpro_url("checkout", "?level=2&ap=48", "https")?>"><?php _e('Select', 'paid-memberships-pro' );?></a>
						<?php else:

						$btn_class = 'btn btn--primary btn--small--large-screen';
						if(empty($current_user->membership_level->ID) || !$is_current_level): ?>
							<a class="<?php echo $btn_class ?>" href="<?php echo pmpro_url("checkout", "?level=" . $level->id, "https")?>"><?php _e('Select', 'paid-memberships-pro' );?></a>
						<?php elseif($is_current_level): 
							//if it's a one-time-payment level, offer a link to renew				
							if( pmpro_isLevelExpiringSoon( $current_user->membership_level) && $current_user->membership_level->allow_signups ) : ?>
									<a class="<?php echo $btn_class ?>" href="<?php echo pmpro_url("checkout", "?level=5", "https")?>"><?php _e('Renew', 'paid-memberships-pro' );?></a>
								<?php else: ?>
									<a class="btn btn--disabled btn--small--large-screen" href="<?php echo pmpro_url("account")?>"><?php _e('Your&nbsp;Level', 'paid-memberships-pro' );?></a>
								<?php endif;
				 		endif; ?>
					<?php endif;?>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</section>