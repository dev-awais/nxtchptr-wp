<section class="hero-section">
	<div class="hero-section__img" style="background-image: url(<?php the_field('hero_image_guest') ?>)"></div>
	<div class="hero-section__content">
		<h1><?php the_field('hero_heading') ?></h1>
		<p><?php the_field('hero_introduction') ?></p>
		<a href="<?php echo site_url('membership') ?>" class="btn btn--primary">Get Started</a>
	</div>
</section>
<section class="peer-group-rules">
	<h2><?php the_field('peer_group_rules_heading') ?></h2>
	<div class="rules">
		<?php if(have_rows('peer_group_rules')): ?>
			<?php while(have_rows('peer_group_rules')): the_row() ?>
				<div class="rule">
					<img src="<?php the_sub_field('image') ?>">
					<h5 class="rule__title"><?php the_sub_field('title') ?></h5>
					<p class="rule__description"><?php the_sub_field('description') ?></p>
				</div>
			<?php endwhile ?>
		<?php endif ?>
	</div>
</section>