<section class="hero-section">
	<div class="hero-section__img" style="background-image: url(<?php the_field('hero_image_guest') ?>)"></div>
	<div class="hero-section__content hero-section__content--inverse">
		<h1><?php the_field('hero_heading') ?></h1>
		<?php the_field('hero_introduction') ?>
		<a href="<?php echo site_url('membership') ?>" class="btn btn--primary">Get Started</a>
	</div>
</section>
<?php include 'includes/book-suggestions.php' ?>
<section class="cta-banner" style="background-image: url('<?php the_field('footer_background_image') ?>')">
	<h2><?php the_field('footer_heading') ?></h2>
	<a href="<?php echo site_url('membership') ?>" class="cta-banner__button"><?php the_field('footer_link_text') ?></a>	
</section>