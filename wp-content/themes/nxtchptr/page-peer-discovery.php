<?php
/**
 * The template for displaying the peer discovery page
 */

get_header(); ?>
	<div class="def-title">
		<div class="wrapper">
			<h1>Peer Discovery</h1>
		</div>
	</div>
	<div class="discovery_page">
		<div class="discovery_page-tp">
			<div class="row">
				<div class="col-md-6 discovery_page-lft">
					<div class="discovery_page-half alignrt">
						<h2>Guide for starting your peer group</h2>
						<p>Peer groups have been used the world over for support and encouragement. Here at Nxt Chptr, we use Peer Groups to connect members to like-minded moms as they explore a return to the workforce. A Peer Group can have as few as two or as many as six members, and for best results it should meet regularly (Weekly, bi-monthly, or monthly). Each group will have its own set of rules that members have agreed upon.</p>
						<a href="<?php echo site_url() ?>/peer-matching-form" class="discovery_page-btn">Start peer group</a>
					</div>
				</div>
				<div class="col-md-6 discovery_page-rgt">
					<div class="discovery_page-half">
						<h2>Three Key Rules for Peer Groups</h2>
						<p>Support & Respect: All members must commit to being encouraging and respectful towards fellow group members</p>
						<p>Different Viewpoints: Respect members' views that differ from yours. Remember that while it sometimes feels good to have people who see the world exactly as we do, those people may not provide an alternate perspective. Many times, a different perspective will be invaluable to you as you navigate your way back to work. A diversity of views is one of the key ingredients for successful groups.</p>
						<p>Living with Conflict: Conflict is an inevitable result of any group. Try to find ways to use conflict in your group to increase the lines of communication and increase understanding of each person's perspective. Agreeing to disagree is a fine solution.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="discovery_page-btm">
			<div class="wrapper">
				<h2>How to Get Started</h2>
				<div class="row">
					<div class="col-md-6">
						<div class="discovery_page-box dgb-upr">
							<span class="countRadius">1</span>
							<h4>Getting to know each other and determining fit</h4>
							<ul>
								<li>Tell me about yourself.</li>
								<li>Why do you want to join a peer group?</li>
								<li>Why did you decide to stop working?</li>
								<li>Why are you looking to go back to work?</li>
							</ul>
						</div>
					</div>
					<div class="col-md-6">
						<div class="discovery_page-box dgb-upr">
							<span class="countRadius">2</span>
							<h4>Defining parameters of peer group</h4>
							<ul>
								<li>What do you hope to get out of this Peer Group?</li>
								<li>How often do you want to meet?</li>
								<li>How long do you want the peer group to run?</li>
								<li>Is it important for the group to take the skills/strengths tests?</li>
								<li>What will be our group's policy on social media?</li>
								<li>What will be our group's policy on making introductions to friends?</li>
								<li>What are the stated values and goals of the Peer Group?</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="discovery_page-box">
							<span class="countRadius">3</span>
							<h4>Questions to ask each other at every meeting</h4>
							<ul>
								<li>How is your search going?</li>
								<li>Where do your interests lie?</li>
								<li>What are your strengths?</li>
								<li>What have been your successes this month?</li>
								<li>What have been your low points?</li>
								<li>What lessons did you learn this month?</li>
								<li>What questions do you have that you are working on?</li>
								<li>What new ideas have you had since our last session?</li>
								<li>How is your search affecting your family?</li>
								<li>Can you volunteer or work for a discount in order to get experience?</li>
								<li>How does this job look after one year at this role? Does any part of the role have to be re-negotiated after you have achieved expertise?</li>
								<li>How will your life change to accommodate your dream job?</li>
								<li>Or create your own set of questions.</li>
							</ul>
						</div>
					</div>
					<div class="col-md-6">
						<div class="discovery_page-box">
							<span class="countRadius">4</span>
							<h4>Some ways to start difficult conversations</h4>
							<ul>
								<li>I would like to discuss a concern of mine.</li>
								<li>I have an idea for a change to our discussion or group dynamic.</li>
								<li>I am uncomfortable when...</li>
								<li>I need to resign from our Peer Group.</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="primary" class="content-area">
		<!-- <main id="main" class="site-main">
			<?php include_page('peer-discovery', 1) ?>
		</main>  -->
		
		<!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
