<?php
/**
 * The template for displaying the recent-courses page
 */

get_header(); ?>

	<div id="primary" class="content-area">

	<?php
		$request = wp_remote_get( 'https://nxtchptr.azurewebsites.net/api/course/featured' );
		if( is_wp_error( $request ) ) {
			return false; // Bail early
		}
		$body = wp_remote_retrieve_body( $request );
		$data = json_decode( $body );
		if( ! empty( $data ) ) {
			
			echo '<ul>';
			foreach( $data as $data ) {
				echo '<li>';
					echo '<a href="' . esc_url( $data->Title ) . '">' . $data->Title . '</a>';
				echo '</li>';
			}
			echo '</ul>';
		}
	?>		

	</div><!-- #primary -->

<?php
get_footer();
