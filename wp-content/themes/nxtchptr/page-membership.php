<?php
/**
 * The template for displaying the membership page
 */
$pmpro_levels = pmpro_getAllLevels(false, true);
$pmpro_level_order = pmpro_getOption('level_order');

if(!empty($pmpro_level_order))
{
	$order = explode(',',$pmpro_level_order);

	//reorder array
	$reordered_levels = array();
	foreach($order as $level_id) {
		foreach($pmpro_levels as $key=>$level) {
			if($level_id == $level->id)
				$reordered_levels[] = $pmpro_levels[$key];
		}
	}

	$pmpro_levels = $reordered_levels;
}

$pmpro_levels = apply_filters("pmpro_levels_array", $pmpro_levels);

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="hero-section">
				<div class="hero-section__img" style="background-image: url(<?php the_field('hero_image') ?>)"></div>
				<div class="hero-section__content">
					<h1><?php the_field('hero_heading') ?></h1>
				</div>
			</section>
			<section class="membership-job-post-section">
				<a class="btn btn--logo-color btn--small" href="<?php wpjb_link_to('employer_new') ?>">Are you looking to advertise a job?</a>
			</section>
			<?php subscription_plans(); ?>
			<?php if(have_rows('question_sections')): ?>
				<section class="questions">
					<h2><?php the_field('questions_heading') ?></h2>
					<?php while ( have_rows('question_sections') ) : the_row(); ?>
						<?php if(have_rows('questions')): ?>
							<h3><?php the_sub_field('section_heading') ?></h3>
							<?php 
								$even = false;
								while ( have_rows('questions') ) : the_row();
								$questionClass = 'bg-circle--' . ($even ? 'small' : 'large'); 
								$answerClass = $even ? 'bg-circle--large bg-circle--large--light' : ''; 
							?>
								<div class="questions__row">
									<h3 class="questions__row__question <?php echo $questionClass ?>"><?php the_sub_field('question') ?></h3>
									<div class="questions__row__answer <?php echo $answerClass ?>"><?php the_sub_field('answer') ?></div>
								</div>
							<?php
								$even = !$even; 
							endwhile; 
							?>	
						<?php endif; ?>
					<?php endwhile ?>
					<div class="questions__cta">
						<h3>Questions?</h3>
						<p>Ask away!</p>
						<a href="mailto:questions@nxtchptr.com">questions@nxtchptr.com</a>
					</div>
				</section>
			<?php endif; ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
