<?php
/**
 * BuddyPress - Groups Home
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

global $current_user;
$zoom_id = xprofile_get_field_data( 'Zoom ID', $current_user->ID );
if($zoom_id && !xprofile_get_field_data( 'Zoom Verified', $current_user->ID )){
	$zoom_user = ZoomMeetings::getUser($zoom_id);
	if($zoom_user->verified){
    xprofile_set_field_data('Zoom Verified', $current_user->ID, 1);
	}
}else{
	$meetings = ZoomMeetings::getMeetings($zoom_id);
}
?>
<div id="buddypress">
	<?php if(isset($_GET['zoom_created'])): ?>
		<div class="container">
			<div class="alert alert--success">Zoom account creation successfully started. Please click the confirmation link in your email to complete the process.</div>
		</div>
	<?php endif; ?>
	<?php if(isset($_GET['zoom_found'])): ?>
		<div class="container">
			<div class="alert alert--success">Your existing Zoom account was successfully found and linked.</div>
		</div>
	<?php endif; ?>

	
	<section class="personal-area-nav">
		<?php bp_nav_menu() ?>
	</section>
	<section class="blocks">
		<div class="blocks__block blocks__block--dark blocks__block--title blocks__block--sm-content">
			<div class="block__content">
				<h1>Your Peer Group</h1>
			</div>
		</div>
		<div class="blocks__block blocks__block--image blocks__block--sm-bg" style="background-image: url(<?php echo bp_attachments_get_attachment(); ?>)">
		</div>
	</section>

	<?php if(bp_is_group_home()): ?>
		<?php if( !bp_get_total_group_count_for_user( bp_loggedin_user_id() ) || !bp_has_groups() ) : ?>
			<section class="blocks">
				<div class="blocks__block">
					<div class="block__content">
						<p>You don't have a peer group yet.</p>
						<a href="/peer-matching-form" class="btn btn--primary">Create one here</a>
					</div>
				</div>
			</section>
		<?php else: ?>
			<?php /*<section class="personal-area-nav personal-area-nav--light">
				<ul id="groups-list">
				<?php while ( bp_groups() ) : bp_the_group(); 
					$classes = $groups_template->current_group == 0 ? ['current-menu-item'] : []; 
				?>
					<li <?php bp_group_class($classes); ?>>
						<?php if ( ! bp_disable_group_avatar_uploads() ) : ?>
							<div class="item-avatar">
								<a href="<?php bp_group_permalink(); ?>"><?php bp_group_avatar( 'type=thumb&width=50&height=50' ); ?></a>
							</div>
						<?php endif; ?>
						<?php bp_group_link(); ?>
					</li>
				<?php endwhile; ?>
				</ul>
				<a href="/peer-matching-form" class="btn btn--primary btn--small">Create another peer group</a>
			</section> */ ?>
			<?php if ( bp_has_groups() ) : 
				bp_the_group();
						?>
				<section class="peer-group">
					<?php if ( bp_group_has_members('group_id='.bp_get_group_id() . '&exclude_admins_mods=1' ) ) : ?>
						<table class="peer-group__list">
							<tr>
								<th></th>
								<th>Peer name</th>
								<th>Email</th>
								<?php /*<th>Remove</th>*/ ?>
							</tr>
							<?php
			    			while ( bp_group_members() ) : bp_group_the_member(); ?>
									<tr class="peer-summary">
										<td><?php bp_group_member_avatar_thumb() ?></td>
										<td><?php bp_group_member_name() ?></td>
										<td><?php bp_member_user_email() ?></td>
										<?php /*<td><a href="<?php echo bp_get_group_member_remove_link(bp_get_member_user_id()) ?>"><i class="fa fa-times-circle"></i></a></td>*/ ?>
									</tr>
							  <?php endwhile; ?>
						</table>
						<?php if($zoom_id && xprofile_get_field_data( 'Zoom Verified', $current_user->ID )):?>
							<div class="peer-group__content">
								<button class="btn btn--primary" data-modal="schedule_meeting">Schedule Meeting</button>
							</div>
							<div id="schedule_meeting" class="modal">
								<div class="modal__content modal__content--wide-form">
									<span data-dismiss="modal" class="modal__close">&times;</span>
									<form method="POST" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>">
										<input type="hidden" name="action" value="zoom_create_meeting">
										<input type="hidden" name="zoomId" value="<?php echo $zoom_id ?>">
										<input type="hidden" name="userId" value="<?php echo $current_user->ID ?>">
										<input type="hidden" name="groupId" value="<?php echo  bp_get_group_id() ?>">
										<?php wp_nonce_field('schedule-meeting', '_schedule-meeting-nonce'); ?>
										<label for="meeting_name">Meeting Name</label>
										<input id="meeting_name" type="text" name="meeting_name" value="<?php bp_group_name() ?> Meeting">
										<label for="meeting_date_time">Start Date and Time</label>
										<input id="meeting_date_time" type="text" class="meeting-date-time" name="meeting_date_time">
										<button class="btn btn--primary">Schedule Meeting</button>
									</form>
								</div>
							</div>
						<?php else: ?>
							<div class="peer-group__content">
								<form method="POST" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>">
									<input type="hidden" name="action" value="zoom_create_user">
									<input type="hidden" name="userId" value="<?php echo $current_user->ID ?>">
									<?php wp_nonce_field('create-zoom-account', '_create-user-nonce'); ?>
									<?php if($zoom_id): ?>
										<label>Please click the confirmation link in your email to complete the Zoom signup process.</label>
										<label>To resend the invitation, click below.</label>
									<?php else: ?>
										<label>In order to schedule a meeting, click below to create a Zoom account.</label>
									<?php endif; ?>
									<br>
									<button class="btn btn--primary">Create Zoom Account</button>
								</form>
							</div>
	    			<?php endif; ?>
	    			<?php if(count($meetings->total_records > 0)): ?>
							<div class="peer-group__content">
	    					<h2>Your scheduled meetings</h2>
		    				<?php foreach($meetings->meetings as $meeting): ?>
									<div><?php echo $meeting->topic; ?> — <?php echo DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $meeting->start_time)->setTimezone(new DateTimeZone($_COOKIE['NxtchptrUserTimeZone']))->format('jS M Y g:ia T') ?> 
										<a target="_blank" href="<?php echo $meeting->join_url ?>">Join Meeting</a>
										<form class="inline-form" method="POST" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>">
											<input type="hidden" name="action" value="zoom_cancel_meeting">
											<?php wp_nonce_field('cancel-meeting', '_cancel-meeting-nonce'.$meeting->id); ?>
											<input type="hidden" name="meetingId" value="<?php echo $meeting->id ?>">
											<input type="hidden" name="userId" value="<?php echo $current_user->ID ?>">
											<input type="hidden" name="groupId" value="<?php echo  bp_get_group_id() ?>">
											<input class="plain-input" type="submit" value="Cancel Meeting">
										</form>
									</div>
		    				<?php endforeach; ?>
							</div>
    				<?php endif;?>
	    		<?php endif; ?>
					<?php if(bp_group_has_invites()): ?>
						<div class="peer-group__content">
							<h1>Invited Peers</h1>
						</div>
						<table class="peer-group__list">
							<tr>
								<th>Peer name</th>
								<th>Percentage Match</th>
								<th></th>
								<th>Uninvite to group</th>
							</tr>
								<?php 
								global $invites_template; 
								while(bp_group_invites()):
									bp_group_the_invite();
									$member = $invites_template->invite->user;
									?>
									<tr class="peer-summary">
										<td class="show-peer-details"><?php echo get_user_initials($member); ?></td>
										<td><span class="match-percent"><?php echo $member->match_percent ?>%</span> <span class="match-bar"><span class="match-bar__inner" style="width: <?php echo $member->match_percent ?>%"></span></span></td>
										<td><a href="<?php echo profile_link() . '/messages/compose/?r='.$member->ID; ?>" class="btn btn--outline btn--outline-dark btn--small">Send Message</a></td>
										<td><a href="<?php bp_group_invite_user_remove_invite_url() ?>"><i class="fa fa-minus-circle"></i></a></td>
									</tr>
									<tr class="peer-details"><td colspan="4">
										<?php $answers = $member->answers; ?>
										<table>
											<tr>
												<td>Why I joined</td>
												<td><?php echo $answers->why_joined ?></td>
											</tr>
											<tr>
												<td>Favorite Websites</td>
												<td><?php echo $answers->fav_websites ?></td>
											</tr>
											<tr>
												<td>Favorite TV shows</td>
												<td><?php echo $answers->fav_tv_shows ?></td>
											</tr>
											<tr>
												<td>Favorite Movie</td>
												<td><?php echo $answers->fav_movie ?></td>
											</tr>
											<tr>
												<td>Childhood Career Dream</td>
												<td><?php echo $answers->child_career_dream ?></td>
											</tr>
											<tr>
												<td>Previous Career</td>
												<td><?php echo $answers->prev_career ?></td>
											</tr>
										</table>
									</td></tr>
									<?php endwhile;  ?>
						</table>
					<?php endif;?>
					<?php if(bp_group_has_suggested_members()): ?>
						<div class="peer-group__content">
							<h1>Peer Suggestions</h1>
						</div>
						<form action="<?php bp_group_send_invite_form_action(); ?>" method="post" id="send-invite-form">
							<table class="peer-group__list">
								<tr>
									<th>Peer name</th>
									<th>Percentage Match</th>
									<th></th>
									<th>Add to group</th>
								</tr>
								<?php foreach(bp_group_get_suggested_members() as $member): ?>
								<tr class="peer-summary">
									<td class="show-peer-details"><?php echo get_user_initials($member); ?></td>
									<td><span class="match-percent"><?php echo $member->match_percent ?>%</span> <span class="match-bar"><span class="match-bar__inner" style="width: <?php echo $member->match_percent ?>%"></span></span></td>
									<td><a href="<?php echo profile_link() . '/messages/compose/?r='.$member->ID; ?>" class="btn btn--outline btn--outline-dark btn--small">Send Message</a></td>
									<td><i data-user-id="<?php echo $member->ID ?>" class="fa fa-plus-circle"></i></td>
								</tr>
								<tr class="peer-details"><td colspan="4">
									<?php $answers = $member->answers; ?>
									<table>
										<tr>
											<td>Why I joined</td>
											<td><?php echo $answers->why_joined ?></td>
										</tr>
										<tr>
											<td>Favorite Websites</td>
											<td><?php echo $answers->fav_websites ?></td>
										</tr>
										<tr>
											<td>Favorite TV shows</td>
											<td><?php echo $answers->fav_tv_shows ?></td>
										</tr>
										<tr>
											<td>Favorite Movie</td>
											<td><?php echo $answers->fav_movie ?></td>
										</tr>
										<tr>
											<td>Childhood Career Dream</td>
											<td><?php echo $answers->child_career_dream ?></td>
										</tr>
										<tr>
											<td>Previous Career</td>
											<td><?php echo $answers->prev_career ?></td>
										</tr>
									</table>
								</td></tr>
								<?php endforeach; ?>
							</table>
							<?php wp_nonce_field( 'groups_invite_uninvite_user', '_wpnonce_invite_uninvite_user' ); ?>
							<?php wp_nonce_field( 'groups_send_invites', '_wpnonce_send_invites' ); ?>
							<input type="hidden" name="group_id" id="group_id" value="<?php bp_group_id(); ?>" />
						</form>
	    		<?php endif; ?>

				</section>
    	<?php endif; ?>
		<?php endif; ?>
	<?php else :
		if     ( bp_is_group_admin_page() ) : bp_get_template_part( 'groups/single/admin'        );
		elseif ( bp_is_group_activity()   ) : bp_get_template_part( 'groups/single/activity'     );
		elseif ( bp_is_group_members()    ) : bp_groups_members_template_part();
		elseif ( bp_is_group_invites()    ) : bp_get_template_part( 'groups/single/send-invites' );
		elseif ( bp_is_group_membership_request() ) : bp_get_template_part( 'groups/single/request-membership' );
		endif;
	endif;?>

	<?php

	/**
	 * Fires after the display of the group home content.
	 *
	 * @since 1.2.0
	 */
	do_action( 'bp_after_group_home_content' ); ?>

</div><!-- #buddypress -->