<?php
/**
 * BuddyPress - Members Single Group Invites
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

/**
 * Fires before the display of member group invites content.
 *
 * @since 1.1.0
 */
do_action( 'bp_before_group_invites_content' ); ?>

<?php if ( bp_has_groups( 'type=invites&user_id=' . bp_loggedin_user_id() ) ) : ?>
	<div class="peer-group__content">
		<h1>Your Group Invitations</h1>
	</div>
	<table class="peer-group__list">
		<tr>
			<th>Group name</th>
			<th>Accept</th>
			<th>Reject</th>
		</tr>
			<?php while ( bp_groups() ) : bp_the_group(); ?>
				<?php global $groups_template; print_r($groups_template->group); echo bp_group_is_invited(); ?>
				<tr>
					<td><?php echo bp_group_name(); ?></td>
					<td><a href="<?php bp_group_accept_invite_link(); ?>"><i class="fa fa-plus-circle"></i></a></td>
					<td><a href="<?php bp_group_reject_invite_link(); ?>"><i class="fa fa-minus-circle"></i></a></td>
				</tr>
			<?php endwhile;  ?>
	</table>
<?php else: ?>

	<div id="message" class="info">
		<p><?php _e( 'You have no outstanding group invites.', 'buddypress' ); ?></p>
	</div>

<?php endif;?>

<?php

/**
 * Fires after the display of member group invites content.
 *
 * @since 1.1.0
 */
do_action( 'bp_after_group_invites_content' );
