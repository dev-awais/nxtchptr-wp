<?php
/**
 * BuddyPress - Members Home
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

?>

<div id="buddypress">

	<?php

	/**
	 * Fires before the display of member home content.
	 *
	 * @since 1.2.0
	 */
	do_action( 'bp_before_member_home_content' ); ?>

	<?php if(bp_is_my_profile()): ?> 
		<section class="personal-area-nav">
	 		<?php bp_nav_menu(); ?> 
		</section>
	<?php endif; ?>
	<section class="blocks <?php if(bp_is_my_profile()): ?>blocks--stack-sm<?php endif ?>">
		<div class="blocks__block blocks__block--dark blocks__block--title <?php if(!bp_is_my_profile()): ?>blocks__block--sm-content<?php endif ?>">
			<div class="block__content">
				<h1><?php if(bp_is_user_profile()){
					echo bp_is_my_profile() ? 'My Account' : bp_displayed_user_fullname();
				} elseif(bp_is_user_groups()){
					echo 'Your Peer Groups';
				} elseif(bp_is_user_messages()){
					echo 'Messages';
				}
				?></h1>
				<?php if(bp_is_user_groups()): ?>
					<a href="<?php echo bp_loggedin_user_domain() ?>profile/change-cover-image" class="upload-account-photo upload-account-photo--show-sm"><i class="fa fa-camera"></i> <span>Upload your own photo</span></a>
				<?php endif ?>
			</div>
		</div>
		<?php if(home_url( $wp->request ) == rtrim(bp_loggedin_user_domain(), '/')): ?>
			<div class="blocks__block">
				<div class="block__content block__content--padded">
					<dl>
						<?php $current_user = wp_get_current_user() ?>
						<div>
							<dt>Name</dt>
							<dd><?php echo $current_user->display_name ?></dd>
						</div>
						<div>
							<dt>Email</dt>
							<dd><?php echo $current_user->user_email ?></dd>
						</div>
						<div>
							<dt>Unique ID</dt>
							<dd><?php echo xprofile_get_field_data(2, $current_user->ID); ?></dd>
						</div>
					</dl>
					<a href="<?php echo site_url('lostpassword') ?>" class="btn btn--primary btn--small btn--outline-dark">Change Password</a>
					<br>
					<a href="<?php echo pmpro_url("cancel", "?levelstocancel=" . pmpro_getMembershipLevelsForUser()[0]->id)?>" class="btn btn--primary btn--small btn--outline-dark">Cancel Membership</a>

				</div>
			</div>
		<?php else: ?>
			<div class="blocks__block blocks__block--image blocks__block--sm-bg" style="background-image: url(<?php echo bp_attachments_get_attachment() ?>)">
				<?php if(bp_is_user_groups()): ?>
					<div class="block__content block__content--overlay-grey">
						<a href="<?php echo bp_loggedin_user_domain() ?>profile/change-cover-image" class="upload-account-photo"><i class="fa fa-camera"></i> <span>Upload your own photo</span></a>
					</div>
				<?php endif ?>
			</div>
		<?php endif ?>
	</section>


	<div id="item-body">

		<?php

		/**
		 * Fires before the display of member body content.
		 *
		 * @since 1.2.0
		 */
		do_action( 'bp_before_member_body' );

		if ( bp_is_user_front() ) :
			bp_displayed_user_front_template_part();

		elseif ( bp_is_user_activity() ) :
			bp_get_template_part( 'members/single/activity' );

		elseif ( bp_is_user_blogs() ) :
			bp_get_template_part( 'members/single/blogs'    );

		elseif ( bp_is_user_friends() ) :
			bp_get_template_part( 'members/single/friends'  );

		elseif ( bp_is_user_groups() ) :
			bp_get_template_part( 'members/single/groups'   );

		elseif ( bp_is_user_messages() ) :
			bp_get_template_part( 'members/single/messages' );

		elseif ( bp_is_user_profile() ) :
			bp_get_template_part( 'members/single/profile'  );

		elseif ( bp_is_user_forums() ) :
			bp_get_template_part( 'members/single/forums'   );

		elseif ( bp_is_user_notifications() ) :
			bp_get_template_part( 'members/single/notifications' );

		elseif ( bp_is_user_settings() ) :
			bp_get_template_part( 'members/single/settings' );

		// If nothing sticks, load a generic template
		else :
			bp_get_template_part( 'members/single/plugins'  );

		endif;

		/**
		 * Fires after the display of member body content.
		 *
		 * @since 1.2.0
		 */
		do_action( 'bp_after_member_body' ); ?>

	</div><!-- #item-body -->

	<?php

	/**
	 * Fires after the display of member home content.
	 *
	 * @since 1.2.0
	 */
	do_action( 'bp_after_member_home_content' ); ?>

</div><!-- #buddypress -->
