<?php
/* Template Name: blog */
$response = "";
function my_contact_form_generate_response($type, $message)
{

	global $response;

	$response = "<div class='alert alert--{$type}'>{$message}</div>";
}


if (isset($_POST['email']) && $_POST['email']) {
	  //response messages
		//$not_human       = "Human verification incorrect.";
	$missing_content = "Please supply all information.";
	$email_invalid = "Email Address Invalid.";
	$message_unsent = "Message was not sent. Try Again.";
	$message_sent = "Thanks! Your message has been sent.";
		 
		//user posted variables
	$email = $_POST['email'];
	$message = "Email address: $email";
		
  //$human = $_POST['contact_human'];
		 
  //php mailer variables
	$to = 'podcast@nxt-chptr.com';
	$subject = "Someone subscribed for podcast updates";
	$headers = 'From: ' . $email . "\r\n" .
		'Reply-To: ' . $email . "\r\n";

	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		my_contact_form_generate_response("error", $email_invalid);
	} else {
		$sent = wp_mail($to, $subject, strip_tags($message), $headers);
		if ($sent) {
			my_contact_form_generate_response("success", $message_sent);
		} else {
			my_contact_form_generate_response("error", $message_unsent);
		}
	}
}

get_header(); ?>
	<div class="def-title">
		<div class="wrapper">
			<h1 class="page-title">Blog</h1>
		</div>
	</div>
	<div class="postsListings-main">
		<div class="wrapper">
			<div class="postsMainflex">
				<?php
					$args = array(
		                'post_type' => 'blog',
		                'order' => 'ASC'
		            );
					$wp_query = new WP_Query($args);
					// $wp_query->query('showposts=10' . '&paged=' . $paged . '&category_name=' . $category_name);
					while ($wp_query->have_posts()) : $wp_query->the_post();
				?>
					<div class="postBlock">
						<img class="postThumbnail" src="<?php the_post_thumbnail_url() ?>">
						<div class="postBlock-content">
							<h4><?php the_title(); ?></h4>
							<div class="text-ellips">
							<?php the_excerpt(); ?></div>
							<p><?php $intro = strip_tags(get_field('introduction')); echo substr($intro, 0, 339); if(strlen($intro) > 339): ?>&hellip;<?php endif; ?></p>
							<a class="postBlock-btn" href="<?php the_permalink(); ?>" title="Read">Read More</a>
						</div>
					</div>
				<?php endwhile; ?>

				<?php 
					the_posts_pagination( array(
						'mid_size'  => 2,
						'prev_text' => __( 'Prev', 'textdomain' ),
						'next_text' => __( 'Next', 'textdomain' ),
					) );
				?>					
				<?php wp_reset_postdata(); ?>	
			</div>
		</div>
	</div>

<?php
get_footer(); ?>