# WordPress MySQL database migration
#
# Generated: Sunday 10. September 2017 03:53 UTC
# Hostname: localhost
# Database: `nxtchptr`
# URL: //nxtchptr.herokuapp.com
# Path: C:\\xampp\\htdocs\\nxtchptr
# Tables: wp_commentmeta, wp_comments, wp_links, wp_options, wp_pmpro_discount_codes, wp_pmpro_discount_codes_levels, wp_pmpro_discount_codes_uses, wp_pmpro_membership_levelmeta, wp_pmpro_membership_levels, wp_pmpro_membership_orders, wp_pmpro_memberships_categories, wp_pmpro_memberships_pages, wp_pmpro_memberships_users, wp_postmeta, wp_posts, wp_term_relationships, wp_term_taxonomy, wp_termmeta, wp_terms, wp_usermeta, wp_users
# Table Prefix: wp_
# Post Types: revision, nav_menu_item, page, post
# Protocol: http
# --------------------------------------------------------

/*!40101 SET NAMES utf8 */;

SET sql_mode='NO_AUTO_VALUE_ON_ZERO';



#
# Delete any existing table `wp_commentmeta`
#

DROP TABLE IF EXISTS `wp_commentmeta`;


#
# Table structure of table `wp_commentmeta`
#

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_commentmeta`
#

#
# End of data contents of table `wp_commentmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_comments`
#

DROP TABLE IF EXISTS `wp_comments`;


#
# Table structure of table `wp_comments`
#

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_comments`
#
INSERT INTO `wp_comments` ( `comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2017-09-07 01:15:44', '2017-09-07 01:15:44', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0) ;

#
# End of data contents of table `wp_comments`
# --------------------------------------------------------



#
# Delete any existing table `wp_links`
#

DROP TABLE IF EXISTS `wp_links`;


#
# Table structure of table `wp_links`
#

CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_links`
#

#
# End of data contents of table `wp_links`
# --------------------------------------------------------



#
# Delete any existing table `wp_options`
#

DROP TABLE IF EXISTS `wp_options`;


#
# Table structure of table `wp_options`
#

CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=345 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_options`
#
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://nxtchptr.herokuapp.com', 'yes'),
(2, 'home', 'http://nxtchptr.herokuapp.com', 'yes'),
(3, 'blogname', 'NxtChptr', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'laksmipati_dasa@hotmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:87:{s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:12:"robots\\.txt$";s:18:"index.php?robots=1";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"([^/]+)/embed/?$";s:37:"index.php?name=$matches[1]&embed=true";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:24:"([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:22:"[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:2:{i:0;s:45:"paid-memberships-pro/paid-memberships-pro.php";i:1;s:31:"wp-migrate-db/wp-migrate-db.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'nxtchptr', 'yes'),
(41, 'stylesheet', 'nxtchptr', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'wp_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:76:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:22:"pmpro_memberships_menu";b:1;s:22:"pmpro_membershiplevels";b:1;s:22:"pmpro_edit_memberships";b:1;s:18:"pmpro_pagesettings";b:1;s:21:"pmpro_paymentsettings";b:1;s:19:"pmpro_emailsettings";b:1;s:22:"pmpro_advancedsettings";b:1;s:12:"pmpro_addons";b:1;s:17:"pmpro_memberslist";b:1;s:20:"pmpro_memberslistcsv";b:1;s:13:"pmpro_reports";b:1;s:12:"pmpro_orders";b:1;s:15:"pmpro_orderscsv";b:1;s:19:"pmpro_discountcodes";b:1;s:13:"pmpro_updates";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:3:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes') ;
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(101, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'cron', 'a:8:{i:1504747321;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1504764241;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1504790152;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1504919583;a:1:{s:8:"do_pings";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:2:{s:8:"schedule";b:0;s:4:"args";a:0:{}}}}i:1504928126;a:1:{s:29:"pmpro_cron_expire_memberships";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1504928127;a:1:{s:30:"pmpro_cron_expiration_warnings";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1504928129;a:1:{s:40:"pmpro_cron_credit_card_expiring_warnings";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:7:"monthly";s:4:"args";a:0:{}s:8:"interval";i:2635200;}}}s:7:"version";i:2;}', 'yes'),
(110, 'nonce_salt', ')a*Ea~jB_g{PONA2JP1QM;=;k]b!LyAZ}ZW=_hQSD6;gAB<dDgCxe!`$w72L P&_', 'no'),
(111, 'theme_mods_twentyseventeen', 'a:2:{s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1504749676;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(124, 'can_compress_scripts', '0', 'no'),
(140, 'current_theme', 'NxtChptr', 'yes'),
(141, 'theme_mods_FoundationPress', 'a:3:{i:0;b:0;s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1504751909;s:4:"data";a:3:{s:19:"wp_inactive_widgets";a:0:{}s:15:"sidebar-widgets";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:14:"footer-widgets";a:0:{}}}}', 'yes'),
(142, 'theme_switched', '', 'yes'),
(145, 'theme_mods_nxtchptr', 'a:3:{i:0;b:0;s:18:"custom_css_post_id";i:-1;s:18:"nav_menu_locations";a:5:{s:6:"menu-1";i:2;s:6:"menu-2";i:3;s:6:"menu-3";i:4;s:6:"menu-4";i:5;s:6:"menu-5";i:6;}}', 'yes'),
(148, 'category_children', 'a:0:{}', 'yes'),
(167, 'recently_activated', 'a:0:{}', 'yes'),
(176, 'wpmdb_usage', 'a:2:{s:6:"action";s:8:"savefile";s:4:"time";i:1505015582;}', 'no'),
(193, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(200, 'pmpro_nonmembertext', 'This content is for !!levels!! members only.<br /><a href="http://nxtchptr.herokuapp.com/wp-login.php?action=register">Register</a>', 'yes'),
(201, 'pmpro_notloggedintext', 'This content is for !!levels!! members only.<br /><a href="http://nxtchptr.herokuapp.com/wp-login.php">Log In</a> <a href="http://nxtchptr.herokuapp.com/wp-login.php?action=register">Register</a>', 'yes'),
(202, 'pmpro_rsstext', 'This content is for !!levels!! members only. Visit the site and log in/register to read.', 'yes'),
(203, 'pmpro_gateway_environment', 'sandbox', 'yes'),
(204, 'pmpro_currency', 'USD', 'yes'),
(205, 'pmpro_accepted_credit_cards', 'Visa,Mastercard,American Express,Discover', 'yes'),
(206, 'pmpro_from_email', 'memberships@nxtchptr.herokuapp.com', 'yes'),
(207, 'pmpro_from_name', 'NxtChptr Memberships', 'yes'),
(208, 'pmpro_email_admin_checkout', '1', 'yes'),
(209, 'pmpro_email_admin_changes', '1', 'yes'),
(210, 'pmpro_email_admin_cancels', '1', 'yes'),
(211, 'pmpro_email_admin_billing', '1', 'yes'),
(212, 'pmpro_tospage', '', 'yes'),
(213, 'pmpro_nag_paused', '1505532916', 'no'),
(214, 'pmpro_db_version', '1.93', 'yes'),
(215, 'pmpro_filterqueries', '1', 'yes'),
(224, 'pmpro_account_page_id', '32', 'yes'),
(225, 'pmpro_billing_page_id', '33', 'yes'),
(226, 'pmpro_cancel_page_id', '34', 'yes'),
(227, 'pmpro_checkout_page_id', '35', 'yes'),
(228, 'pmpro_confirmation_page_id', '36', 'yes'),
(229, 'pmpro_invoice_page_id', '37', 'yes'),
(230, 'pmpro_levels_page_id', '38', 'yes'),
(233, 'pmpro_sslseal', '', 'yes'),
(234, 'pmpro_nuclear_HTTPS', '', 'yes'),
(235, 'pmpro_twocheckout_apiusername', '', 'yes'),
(236, 'pmpro_twocheckout_apipassword', '', 'yes'),
(237, 'pmpro_twocheckout_accountnumber', '', 'yes'),
(238, 'pmpro_twocheckout_secretword', '', 'yes'),
(239, 'pmpro_use_ssl', '0', 'yes'),
(240, 'pmpro_tax_state', '', 'yes'),
(241, 'pmpro_tax_rate', '', 'yes'),
(242, 'pmpro_stripe_secretkey', 'sk_test_K1n3NnMsMS5iw2vJ4c2cFPoP', 'yes'),
(243, 'pmpro_stripe_publishablekey', 'pk_test_URLN0Ihl6lxKXic5zRZTCaK5', 'yes'),
(244, 'pmpro_stripe_billingaddress', '0', 'yes'),
(245, 'pmpro_gateway_email', '', 'yes'),
(246, 'pmpro_apiusername', '', 'yes'),
(247, 'pmpro_apipassword', '', 'yes'),
(248, 'pmpro_apisignature', '', 'yes'),
(249, 'pmpro_paypalexpress_skip_confirmation', '0', 'yes'),
(250, 'pmpro_payflow_partner', '', 'yes'),
(251, 'pmpro_payflow_vendor', '', 'yes'),
(252, 'pmpro_payflow_user', 'admin', 'yes'),
(253, 'pmpro_payflow_pwd', 'wmnMpwrd', 'yes'),
(254, 'pmpro_cybersource_merchantid', '', 'yes'),
(255, 'pmpro_cybersource_securitykey', '', 'yes'),
(256, 'pmpro_instructions', '', 'yes'),
(257, 'pmpro_braintree_merchantid', '', 'yes'),
(258, 'pmpro_braintree_publickey', '', 'yes'),
(259, 'pmpro_braintree_privatekey', '', 'yes'),
(260, 'pmpro_braintree_encryptionkey', '', 'yes'),
(261, 'pmpro_loginname', '', 'yes'),
(262, 'pmpro_transactionkey', '', 'yes'),
(263, 'pmpro_gateway', 'stripe', 'yes'),
(266, 'pmpro_only_filter_pmpro_emails', '1', 'yes'),
(267, 'pmpro_email_member_notification', '', 'yes'),
(270, 'pmpro_visits', 'a:5:{s:5:"today";i:1;s:8:"thisdate";s:10:"2017-09-09";s:5:"month";i:1;s:9:"thismonth";s:1:"9";s:7:"alltime";i:1;}', 'yes'),
(271, 'pmpro_views', 'a:5:{s:5:"today";i:52;s:8:"thisdate";s:10:"2017-10-09";s:5:"month";i:174;s:9:"thismonth";s:1:"9";s:7:"alltime";i:174;}', 'yes') ;
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(314, 'pmpro_addons', 'a:71:{i:0;a:11:{s:4:"Name";s:30:"Hide Admin Bar From Non-admins";s:5:"Title";s:30:"Hide Admin Bar From Non-admins";s:9:"PluginURI";s:68:"http://www.paidmembershipspro.com/wp/hide-admin-bar-from-non-admins/";s:4:"Slug";s:30:"hide-admin-bar-from-non-admins";s:6:"plugin";s:65:"hide-admin-bar-from-non-admins/hide-admin-bar-from-non-admins.php";s:7:"Version";s:3:"1.0";s:11:"Description";s:71:"A tweak of the code by Yoast to hide the admin bar for non-admins only.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:73:"https://downloads.wordpress.org/plugin/hide-admin-bar-from-non-admins.zip";s:7:"License";s:13:"wordpress.org";}i:1;a:11:{s:4:"Name";s:21:"Import Users from CSV";s:5:"Title";s:21:"Import Users from CSV";s:9:"PluginURI";s:58:"http://wordpress.org/extend/plugins/import-users-from-csv/";s:4:"Slug";s:21:"import-users-from-csv";s:6:"plugin";s:47:"import-users-from-csv/import-users-from-csv.php";s:7:"Version";s:5:"1.0.0";s:11:"Description";s:47:"Import Users data and metadata from a csv file.";s:6:"Author";s:13:"Ulrich Sossou";s:9:"AuthorURI";s:24:"http://ulrichsossou.com/";s:8:"Download";s:64:"https://downloads.wordpress.org/plugin/import-users-from-csv.zip";s:7:"License";s:13:"wordpress.org";}i:2;a:11:{s:4:"Name";s:50:"Paid Memberships Pro - Add Name to Checkout Add On";s:5:"Title";s:50:"Paid Memberships Pro - Add Name to Checkout Add On";s:9:"PluginURI";s:64:"http://www.paidmembershipspro.com/wp/pmpro-add-name-to-checkout/";s:4:"Slug";s:26:"pmpro-add-name-to-checkout";s:6:"plugin";s:57:"pmpro-add-name-to-checkout/pmpro-add-name-to-checkout.php";s:7:"Version";s:4:".3.1";s:11:"Description";s:97:"Adds first and last name fields to the user account section at checkout for Paid Memberships Pro.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:83:"http://license.paidmembershipspro.com/downloads/plus/pmpro-add-name-to-checkout.zip";s:7:"License";s:4:"plus";}i:3;a:11:{s:4:"Name";s:48:"Paid Memberships Pro - Add PayPal Express Add On";s:5:"Title";s:48:"Paid Memberships Pro - Add PayPal Express Add On";s:9:"PluginURI";s:62:"http://www.paidmembershipspro.com/wp/pmpro-add-paypal-express/";s:4:"Slug";s:24:"pmpro-add-paypal-express";s:6:"plugin";s:53:"pmpro-add-paypal-express/pmpro-add-paypal-express.php";s:7:"Version";s:4:".5.1";s:11:"Description";s:49:"Add PayPal Express as a Second Option at Checkout";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:81:"http://license.paidmembershipspro.com/downloads/plus/pmpro-add-paypal-express.zip";s:7:"License";s:4:"plus";}i:4;a:11:{s:4:"Name";s:37:"Paid Memberships Pro - Addon Packages";s:5:"Title";s:37:"Paid Memberships Pro - Addon Packages";s:9:"PluginURI";s:55:"http://www.paidmembershipspro.com/pmpro-addon-packages/";s:4:"Slug";s:20:"pmpro-addon-packages";s:6:"plugin";s:45:"pmpro-addon-packages/pmpro-addon-packages.php";s:7:"Version";s:4:".7.3";s:11:"Description";s:169:"Allow PMPro members to purchase access to specific pages. This plugin is meant to be a temporary solution until support for multiple membership levels is added to PMPro.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:77:"http://license.paidmembershipspro.com/downloads/plus/pmpro-addon-packages.zip";s:7:"License";s:4:"plus";}i:5;a:11:{s:4:"Name";s:53:"Paid Memberships Pro - Address For Free Levels Add On";s:5:"Title";s:53:"Paid Memberships Pro - Address For Free Levels Add On";s:9:"PluginURI";s:67:"http://www.paidmembershipspro.com/wp/pmpro-address-for-free-levels/";s:4:"Slug";s:29:"pmpro-address-for-free-levels";s:6:"plugin";s:63:"pmpro-address-for-free-levels/pmpro-address-for-free-levels.php";s:7:"Version";s:4:".3.2";s:11:"Description";s:66:"Show address fields for free levels also with Paid Memberships Pro";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:86:"http://license.paidmembershipspro.com/downloads/plus/pmpro-address-for-free-levels.zip";s:7:"License";s:4:"plus";}i:6;a:11:{s:4:"Name";s:60:"Paid Memberships Pro - Advanced Levels Page Shortcode Add On";s:5:"Title";s:60:"Paid Memberships Pro - Advanced Levels Page Shortcode Add On";s:9:"PluginURI";s:59:"http://www.paidmembershipspro.com/wp/pmpro-advanced-levels/";s:4:"Slug";s:31:"pmpro-advanced-levels-shortcode";s:6:"plugin";s:67:"pmpro-advanced-levels-shortcode/pmpro-advanced-levels-shortcode.php";s:7:"Version";s:4:".2.4";s:11:"Description";s:105:"An enhanced shortcode for customizing the display of your Membership Levels Page for Paid Memberships Pro";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:88:"http://license.paidmembershipspro.com/downloads/plus/pmpro-advanced-levels-shortcode.zip";s:7:"License";s:4:"plus";}i:7;a:11:{s:4:"Name";s:40:"Paid Memberships Pro - Affiliates Add On";s:5:"Title";s:40:"Paid Memberships Pro - Affiliates Add On";s:9:"PluginURI";s:51:"http://www.paidmembershipspro.com/pmpro-affiliates/";s:4:"Slug";s:16:"pmpro-affiliates";s:6:"plugin";s:37:"pmpro-affiliates/pmpro-affiliates.php";s:7:"Version";s:2:".3";s:11:"Description";s:185:"Create affiliate accounts and codes. If a code is passed to a page as a parameter, a cookie is set. If a cookie is present after checkout, the order is awarded to the affiliate account.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:73:"http://license.paidmembershipspro.com/downloads/plus/pmpro-affiliates.zip";s:7:"License";s:4:"plus";}i:8;a:11:{s:4:"Name";s:39:"Paid Memberships Pro - Approvals Add On";s:5:"Title";s:39:"Paid Memberships Pro - Approvals Add On";s:9:"PluginURI";s:71:"https://www.paidmembershipspro.com/add-ons/approval-process-membership/";s:4:"Slug";s:15:"pmpro-approvals";s:6:"plugin";s:35:"pmpro-approvals/pmpro-approvals.php";s:7:"Version";s:5:"1.0.4";s:11:"Description";s:75:"Grants administrators the ability to approve/deny memberships after signup.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:34:"https://www.paidmembershipspro.com";s:8:"Download";s:72:"http://license.paidmembershipspro.com/downloads/plus/pmpro-approvals.zip";s:7:"License";s:4:"plus";}i:9;a:11:{s:4:"Name";s:44:"Paid Memberships Pro - Auto-Renewal Checkbox";s:5:"Title";s:44:"Paid Memberships Pro - Auto-Renewal Checkbox";s:9:"PluginURI";s:98:"https://www.paidmembershipspro.com/add-ons/plus-add-ons/auto-renewal-checkbox-membership-checkout/";s:4:"Slug";s:27:"pmpro-auto-renewal-checkbox";s:6:"plugin";s:59:"pmpro-auto-renewal-checkbox/pmpro-auto-renewal-checkbox.php";s:7:"Version";s:4:".2.2";s:11:"Description";s:55:"Make auto-renewal optional at checkout with a checkbox.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:84:"http://license.paidmembershipspro.com/downloads/plus/pmpro-auto-renewal-checkbox.zip";s:7:"License";s:4:"plus";}i:10;a:11:{s:4:"Name";s:36:"Paid Memberships Pro - AWeber Add On";s:5:"Title";s:36:"Paid Memberships Pro - AWeber Add On";s:9:"PluginURI";s:47:"http://www.paidmembershipspro.com/pmpro-aweber/";s:4:"Slug";s:12:"pmpro-aweber";s:6:"plugin";s:29:"pmpro-aweber/pmpro-aweber.php";s:7:"Version";s:3:"1.3";s:11:"Description";s:56:"Sync your WordPress users and members with AWeber lists.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:55:"https://downloads.wordpress.org/plugin/pmpro-aweber.zip";s:7:"License";s:13:"wordpress.org";}i:11;a:11:{s:4:"Name";s:37:"Paid Memberships Pro - bbPress Add On";s:5:"Title";s:37:"Paid Memberships Pro - bbPress Add On";s:9:"PluginURI";s:57:"https://www.paidmembershipspro.com/add-ons/pmpro-bbpress/";s:4:"Slug";s:13:"pmpro-bbpress";s:6:"plugin";s:31:"pmpro-bbpress/pmpro-bbpress.php";s:7:"Version";s:5:"1.5.4";s:11:"Description";s:54:"Allow individual forums to be locked down for members.";s:6:"Author";s:29:"Stranger Studios, Scott Sousa";s:9:"AuthorURI";s:34:"https://www.paidmembershipspro.com";s:8:"Download";s:56:"https://downloads.wordpress.org/plugin/pmpro-bbpress.zip";s:7:"License";s:13:"wordpress.org";}i:12;a:11:{s:4:"Name";s:50:"Paid Memberships Pro - Better Logins Report Add On";s:5:"Title";s:50:"Paid Memberships Pro - Better Logins Report Add On";s:9:"PluginURI";s:64:"http://www.paidmembershipspro.com/wp/pmpro-better-logins-report/";s:4:"Slug";s:26:"pmpro-better-logins-report";s:6:"plugin";s:57:"pmpro-better-logins-report/pmpro-better-logins-report.php";s:7:"Version";s:6:".2.3.2";s:11:"Description";s:66:"Adds login, view, and visit stats for "This Week" and "This Year".";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:83:"http://license.paidmembershipspro.com/downloads/plus/pmpro-better-logins-report.zip";s:7:"License";s:4:"plus";}i:13;a:11:{s:4:"Name";s:42:"Paid Memberships Pro - Check Levels Add On";s:5:"Title";s:42:"Paid Memberships Pro - Check Levels Add On";s:9:"PluginURI";s:56:"http://www.paidmembershipspro.com/wp/pmpro-check-levels/";s:4:"Slug";s:18:"pmpro-check-levels";s:6:"plugin";s:41:"pmpro-check-levels/pmpro-check-levels.php";s:7:"Version";s:2:".3";s:11:"Description";s:106:"A collection of customizations useful when allowing users to pay by check for Paid Memberships Pro levels.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:75:"http://license.paidmembershipspro.com/downloads/plus/pmpro-check-levels.zip";s:7:"License";s:4:"plus";}i:14;a:11:{s:4:"Name";s:46:"Paid Memberships Pro - Constant Contact Add On";s:5:"Title";s:46:"Paid Memberships Pro - Constant Contact Add On";s:9:"PluginURI";s:56:"http://www.paidmembershipspro.com/pmpro-constantcontact/";s:4:"Slug";s:22:"pmpro-constant-contact";s:6:"plugin";s:49:"pmpro-constant-contact/pmpro-constant-contact.php";s:7:"Version";s:5:"1.0.2";s:11:"Description";s:66:"Sync your WordPress users and members with Constant Contact lists.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:65:"https://downloads.wordpress.org/plugin/pmpro-constant-contact.zip";s:7:"License";s:13:"wordpress.org";}i:15;a:11:{s:4:"Name";s:52:"Paid Memberships Pro - Custom Level Cost Text Add On";s:5:"Title";s:52:"Paid Memberships Pro - Custom Level Cost Text Add On";s:9:"PluginURI";s:66:"http://www.paidmembershipspro.com/wp/pmpro-custom-level-cost-text/";s:4:"Slug";s:21:"pmpro-level-cost-text";s:6:"plugin";s:47:"pmpro-level-cost-text/pmpro-level-cost-text.php";s:7:"Version";s:4:".3.1";s:11:"Description";s:99:"Modify the default level cost text per level, per discount code, or globally via advanced settings.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:78:"http://license.paidmembershipspro.com/downloads/plus/pmpro-level-cost-text.zip";s:7:"License";s:4:"plus";}i:16;a:11:{s:4:"Name";s:46:"Paid Memberships Pro - Custom Post Type Add On";s:5:"Title";s:46:"Paid Memberships Pro - Custom Post Type Add On";s:9:"PluginURI";s:47:"http://www.paidmembershipspro.com/wp/pmpro-cpt/";s:4:"Slug";s:9:"pmpro-cpt";s:6:"plugin";s:23:"pmpro-cpt/pmpro-cpt.php";s:7:"Version";s:2:".2";s:11:"Description";s:76:"Add the PMPro meta box to CPTs and redirects non-members to a selected page.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:66:"http://license.paidmembershipspro.com/downloads/plus/pmpro-cpt.zip";s:7:"License";s:4:"plus";}i:17;a:11:{s:4:"Name";s:42:"Paid Memberships Pro - Developer\'s Toolkit";s:5:"Title";s:42:"Paid Memberships Pro - Developer\'s Toolkit";s:9:"PluginURI";s:0:"";s:4:"Slug";s:13:"pmpro-toolkit";s:6:"plugin";s:31:"pmpro-toolkit/pmpro-toolkit.php";s:7:"Version";s:2:".4";s:11:"Description";s:70:"Various tools to test and debug Paid Memberships Pro enabled websites.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:0:"";s:8:"Download";s:70:"http://license.paidmembershipspro.com/downloads/free/pmpro-toolkit.zip";s:7:"License";s:4:"plus";}i:18;a:11:{s:4:"Name";s:32:"Paid Memberships Pro - Donations";s:5:"Title";s:32:"Paid Memberships Pro - Donations";s:9:"PluginURI";s:58:"http://www.paidmembershipspro.com/add-ons/pmpro-donations/";s:4:"Slug";s:15:"pmpro-donations";s:6:"plugin";s:35:"pmpro-donations/pmpro-donations.php";s:7:"Version";s:2:".4";s:11:"Description";s:65:"Allow customers to set an additional donation amount at checkout.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:72:"http://license.paidmembershipspro.com/downloads/plus/pmpro-donations.zip";s:7:"License";s:4:"plus";}i:19;a:11:{s:4:"Name";s:58:"Paid Memberships Pro - Download Monitor Integration Add On";s:5:"Title";s:58:"Paid Memberships Pro - Download Monitor Integration Add On";s:9:"PluginURI";s:57:"http://www.paidmembershipspro.com/pmpro-download-monitor/";s:4:"Slug";s:22:"pmpro-download-monitor";s:6:"plugin";s:49:"pmpro-download-monitor/pmpro-download-monitor.php";s:7:"Version";s:2:".1";s:11:"Description";s:72:"Require membership for downloads when using the Download Monitor plugin.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:79:"http://license.paidmembershipspro.com/downloads/plus/pmpro-download-monitor.zip";s:7:"License";s:4:"plus";}i:20;a:11:{s:4:"Name";s:48:"Paid Memberships Pro - Email Confirmation Add On";s:5:"Title";s:48:"Paid Memberships Pro - Email Confirmation Add On";s:9:"PluginURI";s:66:"http://www.paidmembershipspro.com/addons/pmpro-email-confirmation/";s:4:"Slug";s:24:"pmpro-email-confirmation";s:6:"plugin";s:53:"pmpro-email-confirmation/pmpro-email-confirmation.php";s:7:"Version";s:2:".4";s:11:"Description";s:73:"Require email confirmation before certain levels are enabled for members.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:81:"http://license.paidmembershipspro.com/downloads/plus/pmpro-email-confirmation.zip";s:7:"License";s:4:"plus";}i:21;a:11:{s:4:"Name";s:45:"Paid Memberships Pro - Email Templates Add On";s:5:"Title";s:45:"Paid Memberships Pro - Email Templates Add On";s:9:"PluginURI";s:100:"http://www.paidmembershipspro.com/add-ons/plugins-wordpress-repository/email-templates-admin-editor/";s:4:"Slug";s:21:"pmpro-email-templates";s:6:"plugin";s:47:"pmpro-email-templates/pmpro-email-templates.php";s:7:"Version";s:5:"0.7.1";s:11:"Description";s:50:"Define your own custom PMPro HTML Email Templates.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:78:"http://license.paidmembershipspro.com/downloads/plus/pmpro-email-templates.zip";s:7:"License";s:4:"plus";}i:22;a:11:{s:4:"Name";s:61:"Paid Memberships Pro - Extra Expiration Warning Emails Add On";s:5:"Title";s:61:"Paid Memberships Pro - Extra Expiration Warning Emails Add On";s:9:"PluginURI";s:75:"http://www.paidmembershipspro.com/wp/pmpro-extra-expiration-warning-emails/";s:4:"Slug";s:37:"pmpro-extra-expiration-warning-emails";s:6:"plugin";s:79:"pmpro-extra-expiration-warning-emails/pmpro-extra-expiration-warning-emails.php";s:7:"Version";s:6:".3.7.1";s:11:"Description";s:81:"Send out more than one "membership expiration warning" email to users with PMPro.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:94:"http://license.paidmembershipspro.com/downloads/plus/pmpro-extra-expiration-warning-emails.zip";s:7:"License";s:4:"plus";}i:23;a:11:{s:4:"Name";s:41:"Paid Memberships Pro - GetResponse Add On";s:5:"Title";s:41:"Paid Memberships Pro - GetResponse Add On";s:9:"PluginURI";s:52:"http://www.paidmembershipspro.com/pmpro-getresponse/";s:4:"Slug";s:17:"pmpro-getresponse";s:6:"plugin";s:39:"pmpro-getresponse/pmpro-getresponse.php";s:7:"Version";s:4:".4.1";s:11:"Description";s:65:"Sync your WordPress users and members with GetResponse campaigns.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:74:"http://license.paidmembershipspro.com/downloads/plus/pmpro-getresponse.zip";s:7:"License";s:4:"plus";}i:24;a:11:{s:4:"Name";s:41:"Paid Memberships Pro - Gift Levels Add On";s:5:"Title";s:41:"Paid Memberships Pro - Gift Levels Add On";s:9:"PluginURI";s:60:"http://www.paidmembershipspro.com/add-ons/pmpro-gift-levels/";s:4:"Slug";s:17:"pmpro-gift-levels";s:6:"plugin";s:39:"pmpro-gift-levels/pmpro-gift-levels.php";s:7:"Version";s:4:".2.3";s:11:"Description";s:87:"Some levels will generate discount codes to give to others to use for gift memberships.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:74:"http://license.paidmembershipspro.com/downloads/plus/pmpro-gift-levels.zip";s:7:"License";s:4:"plus";}i:25;a:11:{s:4:"Name";s:50:"Paid Memberships Pro - Group Discount Codes Add On";s:5:"Title";s:50:"Paid Memberships Pro - Group Discount Codes Add On";s:9:"PluginURI";s:61:"http://www.paidmembershipspro.com/pmpro-group-discount-codes/";s:4:"Slug";s:26:"pmpro-group-discount-codes";s:6:"plugin";s:57:"pmpro-group-discount-codes/pmpro-group-discount-codes.php";s:7:"Version";s:4:".3.1";s:11:"Description";s:98:"Adds features to PMPro to better manage grouped discount codes or large numbers of discount codes.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:33:"http://www.paidmembershipspro.com";s:8:"Download";s:83:"http://license.paidmembershipspro.com/downloads/plus/pmpro-group-discount-codes.zip";s:7:"License";s:4:"plus";}i:26;a:11:{s:4:"Name";s:51:"Paid Memberships Pro - Import Users from CSV Add On";s:5:"Title";s:51:"Paid Memberships Pro - Import Users from CSV Add On";s:9:"PluginURI";s:62:"http://www.paidmembershipspro.com/pmpro-import-users-from-csv/";s:4:"Slug";s:27:"pmpro-import-users-from-csv";s:6:"plugin";s:59:"pmpro-import-users-from-csv/pmpro-import-users-from-csv.php";s:7:"Version";s:4:".3.3";s:11:"Description";s:90:"Add-on for the Import Users From CSV plugin to import PMPro and membership-related fields.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:84:"http://license.paidmembershipspro.com/downloads/plus/pmpro-import-users-from-csv.zip";s:7:"License";s:4:"plus";}i:27;a:11:{s:4:"Name";s:42:"Paid Memberships Pro - Infusionsoft Add On";s:5:"Title";s:42:"Paid Memberships Pro - Infusionsoft Add On";s:9:"PluginURI";s:53:"http://www.paidmembershipspro.com/pmpro-infusionsoft/";s:4:"Slug";s:18:"pmpro-infusionsoft";s:6:"plugin";s:41:"pmpro-infusionsoft/pmpro-infusionsoft.php";s:7:"Version";s:5:"1.3.2";s:11:"Description";s:65:"Sync your WordPress users and members with Infusionsoft contacts.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:61:"https://downloads.wordpress.org/plugin/pmpro-infusionsoft.zip";s:7:"License";s:13:"wordpress.org";}i:28;a:11:{s:4:"Name";s:41:"Paid Memberships Pro - Invite Only Add On";s:5:"Title";s:41:"Paid Memberships Pro - Invite Only Add On";s:9:"PluginURI";s:60:"http://www.paidmembershipspro.com/add-ons/pmpro-invite-only/";s:4:"Slug";s:17:"pmpro-invite-only";s:6:"plugin";s:39:"pmpro-invite-only/pmpro-invite-only.php";s:7:"Version";s:4:".3.2";s:11:"Description";s:102:"Users must have an invite code to sign up for certain levels. Users are given an invite code to share.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:74:"http://license.paidmembershipspro.com/downloads/plus/pmpro-invite-only.zip";s:7:"License";s:4:"plus";}i:29;a:11:{s:4:"Name";s:41:"Paid Memberships Pro - Kissmetrics Add On";s:5:"Title";s:41:"Paid Memberships Pro - Kissmetrics Add On";s:9:"PluginURI";s:0:"";s:4:"Slug";s:17:"pmpro-kissmetrics";s:6:"plugin";s:39:"pmpro-kissmetrics/pmpro-kissmetrics.php";s:7:"Version";s:4:".3.1";s:11:"Description";s:116:"Integrates your WordPress site with Kissmetrics to track meaningful user data, with or without Paid Memberships Pro.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:26:"http://strangerstudios.com";s:8:"Download";s:60:"https://downloads.wordpress.org/plugin/pmpro-kissmetrics.zip";s:7:"License";s:13:"wordpress.org";}i:30;a:11:{s:4:"Name";s:50:"Paid Memberships Pro - Levels as DIV Layout Add On";s:5:"Title";s:50:"Paid Memberships Pro - Levels as DIV Layout Add On";s:9:"PluginURI";s:55:"http://www.paidmembershipspro.com/wp/pmpro-divb-levels/";s:4:"Slug";s:16:"pmpro-div-levels";s:6:"plugin";s:37:"pmpro-div-levels/pmpro-div-levels.php";s:7:"Version";s:2:".2";s:11:"Description";s:76:"Display your Membership Levels Page in a DIV Layout for Paid Memberships Pro";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:73:"http://license.paidmembershipspro.com/downloads/plus/pmpro-div-levels.zip";s:7:"License";s:4:"plus";}i:31;a:11:{s:4:"Name";s:46:"Paid Memberships Pro - Limit Post Views Add On";s:5:"Title";s:46:"Paid Memberships Pro - Limit Post Views Add On";s:9:"PluginURI";s:60:"http://www.paidmembershipspro.com/wp/pmpro-limit-post-views/";s:4:"Slug";s:22:"pmpro-limit-post-views";s:6:"plugin";s:49:"pmpro-limit-post-views/pmpro-limit-post-views.php";s:7:"Version";s:2:".5";s:11:"Description";s:106:"Integrates with Paid Memberships Pro to limit the number of times non-members can view posts on your site.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:79:"http://license.paidmembershipspro.com/downloads/plus/pmpro-limit-post-views.zip";s:7:"License";s:4:"plus";}i:32;a:11:{s:4:"Name";s:44:"Paid Memberships Pro - Lock Membership Level";s:5:"Title";s:44:"Paid Memberships Pro - Lock Membership Level";s:9:"PluginURI";s:59:"http://www.paidmembershipspro.com/wp/lock-membership-level/";s:4:"Slug";s:27:"pmpro-lock-membership-level";s:6:"plugin";s:59:"pmpro-lock-membership-level/pmpro-lock-membership-level.php";s:7:"Version";s:2:".2";s:11:"Description";s:61:"Lock membership level changes for specific users or by level.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:84:"http://license.paidmembershipspro.com/downloads/plus/pmpro-lock-membership-level.zip";s:7:"License";s:4:"plus";}i:33;a:11:{s:4:"Name";s:39:"Paid Memberships Pro - MailChimp Add On";s:5:"Title";s:39:"Paid Memberships Pro - MailChimp Add On";s:9:"PluginURI";s:50:"http://www.paidmembershipspro.com/pmpro-mailchimp/";s:4:"Slug";s:15:"pmpro-mailchimp";s:6:"plugin";s:35:"pmpro-mailchimp/pmpro-mailchimp.php";s:7:"Version";s:3:"2.1";s:11:"Description";s:59:"Sync your WordPress users and members with MailChimp lists.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:58:"https://downloads.wordpress.org/plugin/pmpro-mailchimp.zip";s:7:"License";s:13:"wordpress.org";}i:34;a:11:{s:4:"Name";s:43:"Paid Memberships Pro - Member Badges Add On";s:5:"Title";s:43:"Paid Memberships Pro - Member Badges Add On";s:9:"PluginURI";s:54:"http://www.paidmembershipspro.com/pmpro-member-badges/";s:4:"Slug";s:19:"pmpro-member-badges";s:6:"plugin";s:43:"pmpro-member-badges/pmpro-member-badges.php";s:7:"Version";s:4:".3.1";s:11:"Description";s:115:"Assign unique member badges (images) to each membership level and display via a shortcode or template PHP function.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:76:"http://license.paidmembershipspro.com/downloads/plus/pmpro-member-badges.zip";s:7:"License";s:4:"plus";}i:35;a:11:{s:4:"Name";s:46:"Paid Memberships Pro - Member Directory Add On";s:5:"Title";s:46:"Paid Memberships Pro - Member Directory Add On";s:9:"PluginURI";s:60:"http://www.paidmembershipspro.com/wp/pmpro-member-directory/";s:4:"Slug";s:22:"pmpro-member-directory";s:6:"plugin";s:49:"pmpro-member-directory/pmpro-member-directory.php";s:7:"Version";s:2:".5";s:11:"Description";s:81:"Adds a customizable Member Directory and Member Profiles to your membership site.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:79:"http://license.paidmembershipspro.com/downloads/plus/pmpro-member-directory.zip";s:7:"License";s:4:"plus";}i:36;a:11:{s:4:"Name";s:44:"Paid Memberships Pro - Member History Add On";s:5:"Title";s:44:"Paid Memberships Pro - Member History Add On";s:9:"PluginURI";s:58:"http://www.paidmembershipspro.com/wp/pmpro-member-history/";s:4:"Slug";s:20:"pmpro-member-history";s:6:"plugin";s:45:"pmpro-member-history/pmpro-member-history.php";s:7:"Version";s:6:".2.4.2";s:11:"Description";s:77:"Display a history of a user\'s Membership on the User Profile for admins only.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:77:"http://license.paidmembershipspro.com/downloads/plus/pmpro-member-history.zip";s:7:"License";s:4:"plus";}i:37;a:11:{s:4:"Name";s:46:"Paid Memberships Pro - Member Homepages Add On";s:5:"Title";s:46:"Paid Memberships Pro - Member Homepages Add On";s:9:"PluginURI";s:57:"http://www.paidmembershipspro.com/pmpro-member-homepages/";s:4:"Slug";s:22:"pmpro-member-homepages";s:6:"plugin";s:49:"pmpro-member-homepages/pmpro-member-homepages.php";s:7:"Version";s:2:".1";s:11:"Description";s:72:"Redirect members to a unique homepage/landing page based on their level.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:79:"http://license.paidmembershipspro.com/downloads/plus/pmpro-member-homepages.zip";s:7:"License";s:4:"plus";}i:38;a:11:{s:4:"Name";s:50:"Paid Memberships Pro - Member Network Sites Add On";s:5:"Title";s:50:"Paid Memberships Pro - Member Network Sites Add On";s:9:"PluginURI";s:48:"http://www.paidmembershipspro.com/network-sites/";s:4:"Slug";s:13:"pmpro-network";s:6:"plugin";s:31:"pmpro-network/pmpro-network.php";s:7:"Version";s:2:".5";s:11:"Description";s:76:"Create a network site for the member as part of membership to the main site.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:70:"http://license.paidmembershipspro.com/downloads/plus/pmpro-network.zip";s:7:"License";s:4:"plus";}i:39;a:11:{s:4:"Name";s:40:"Paid Memberships Pro - Member RSS Add On";s:5:"Title";s:40:"Paid Memberships Pro - Member RSS Add On";s:9:"PluginURI";s:54:"http://www.paidmembershipspro.com/wp/pmpro-member-rss/";s:4:"Slug";s:16:"pmpro-member-rss";s:6:"plugin";s:37:"pmpro-member-rss/pmpro-member-rss.php";s:7:"Version";s:2:".2";s:11:"Description";s:57:"Create Member-Specific RSS Feeds for Paid Memberships Pro";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:73:"http://license.paidmembershipspro.com/downloads/plus/pmpro-member-rss.zip";s:7:"License";s:4:"plus";}i:40;a:11:{s:4:"Name";s:45:"Paid Memberships Pro - Membership Card Add On";s:5:"Title";s:45:"Paid Memberships Pro - Membership Card Add On";s:9:"PluginURI";s:59:"http://www.paidmembershipspro.com/wp/pmpro-membership-card/";s:4:"Slug";s:21:"pmpro-membership-card";s:6:"plugin";s:47:"pmpro-membership-card/pmpro-membership-card.php";s:7:"Version";s:2:".4";s:11:"Description";s:81:"Display a printable Membership Card for Paid Memberships Pro members or WP users.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:78:"http://license.paidmembershipspro.com/downloads/plus/pmpro-membership-card.zip";s:7:"License";s:4:"plus";}i:41;a:11:{s:4:"Name";s:53:"Paid Memberships Pro - Membership Manager Role Add On";s:5:"Title";s:53:"Paid Memberships Pro - Membership Manager Role Add On";s:9:"PluginURI";s:73:"https://www.paidmembershipspro.com/add-ons/pmpro-membership-manager-role/";s:4:"Slug";s:29:"pmpro-membership-manager-role";s:6:"plugin";s:63:"pmpro-membership-manager-role/pmpro-membership-manager-role.php";s:7:"Version";s:4:".3.1";s:11:"Description";s:86:"Adds a Membership Manager role to WordPress with access to PMPro settings and reports.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:34:"https://www.paidmembershipspro.com";s:8:"Download";s:86:"http://license.paidmembershipspro.com/downloads/plus/pmpro-membership-manager-role.zip";s:7:"License";s:4:"plus";}i:42;a:11:{s:4:"Name";s:50:"Paid Memberships Pro - Multisite Membership Add On";s:5:"Title";s:50:"Paid Memberships Pro - Multisite Membership Add On";s:9:"PluginURI";s:65:"http://www.paidmembershipspro.com/add-ons/pmpro-network-subsites/";s:4:"Slug";s:21:"pmpro-network-subsite";s:6:"plugin";s:47:"pmpro-network-subsite/pmpro-network-subsite.php";s:7:"Version";s:4:".4.1";s:11:"Description";s:139:"Manage memberships at the network’s main site (the primary domain of the network) and provide/restrict access on subsites in the network.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:78:"http://license.paidmembershipspro.com/downloads/plus/pmpro-network-subsite.zip";s:7:"License";s:4:"plus";}i:43;a:11:{s:4:"Name";s:39:"Paid Memberships Pro - Nav Menus Add On";s:5:"Title";s:39:"Paid Memberships Pro - Nav Menus Add On";s:9:"PluginURI";s:59:"https://www.paidmembershipspro.com/add-ons/pmpro-nav-menus/";s:4:"Slug";s:15:"pmpro-nav-menus";s:6:"plugin";s:35:"pmpro-nav-menus/pmpro-nav-menus.php";s:7:"Version";s:4:".3.2";s:11:"Description";s:100:"Creates member navigation menus and swaps your theme\'s navigation based on a user\'s Membership Level";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:34:"https://www.paidmembershipspro.com";s:8:"Download";s:72:"http://license.paidmembershipspro.com/downloads/plus/pmpro-nav-menus.zip";s:7:"License";s:4:"plus";}i:44;a:11:{s:4:"Name";s:42:"Paid Memberships Pro - Pay by Check Add On";s:5:"Title";s:42:"Paid Memberships Pro - Pay by Check Add On";s:9:"PluginURI";s:69:"https://www.paidmembershipspro.com/add-ons/pmpro-pay-by-check-add-on/";s:4:"Slug";s:18:"pmpro-pay-by-check";s:6:"plugin";s:41:"pmpro-pay-by-check/pmpro-pay-by-check.php";s:7:"Version";s:4:".7.8";s:11:"Description";s:106:"A collection of customizations useful when allowing users to pay by check for Paid Memberships Pro levels.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:34:"https://www.paidmembershipspro.com";s:8:"Download";s:75:"http://license.paidmembershipspro.com/downloads/plus/pmpro-pay-by-check.zip";s:7:"License";s:4:"plus";}i:45;a:11:{s:4:"Name";s:60:"Paid Memberships Pro - Post Affiliate Pro Integration Add On";s:5:"Title";s:60:"Paid Memberships Pro - Post Affiliate Pro Integration Add On";s:9:"PluginURI";s:62:"http://www.paidmembershipspro.com/wp/pmpro-post-affiliate-pro/";s:4:"Slug";s:24:"pmpro-post-affiliate-pro";s:6:"plugin";s:53:"pmpro-post-affiliate-pro/pmpro-post-affiliate-pro.php";s:7:"Version";s:6:".2.1.1";s:11:"Description";s:67:"Process an affiliate via Post Affiliate Pro after a PMPro checkout.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:81:"http://license.paidmembershipspro.com/downloads/plus/pmpro-post-affiliate-pro.zip";s:7:"License";s:4:"plus";}i:46;a:11:{s:4:"Name";s:39:"Paid Memberships Pro - Proration Add On";s:5:"Title";s:39:"Paid Memberships Pro - Proration Add On";s:9:"PluginURI";s:53:"http://www.paidmembershipspro.com/wp/pmpro-proration/";s:4:"Slug";s:15:"pmpro-proration";s:6:"plugin";s:35:"pmpro-proration/pmpro-proration.php";s:7:"Version";s:2:".2";s:11:"Description";s:46:"Custom Prorating Code for Paid Memberships Pro";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:72:"http://license.paidmembershipspro.com/downloads/plus/pmpro-proration.zip";s:7:"License";s:4:"plus";}i:47;a:11:{s:4:"Name";s:46:"Paid Memberships Pro - Recurring Emails Add On";s:5:"Title";s:46:"Paid Memberships Pro - Recurring Emails Add On";s:9:"PluginURI";s:60:"http://www.paidmembershipspro.com/wp/pmpro-recurring-emails/";s:4:"Slug";s:22:"pmpro-recurring-emails";s:6:"plugin";s:49:"pmpro-recurring-emails/pmpro-recurring-emails.php";s:7:"Version";s:2:".5";s:11:"Description";s:93:"Send email message(s) X days before a recurring payment is scheduled, to warn/remind members.";s:6:"Author";s:64:"Stranger Studios, Thomas Sjolshagen <thomas@eighty20results.com>";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:79:"http://license.paidmembershipspro.com/downloads/plus/pmpro-recurring-emails.zip";s:7:"License";s:4:"plus";}i:48;a:11:{s:4:"Name";s:45:"Paid Memberships Pro - Register Helper Add On";s:5:"Title";s:45:"Paid Memberships Pro - Register Helper Add On";s:9:"PluginURI";s:56:"http://www.paidmembershipspro.com/pmpro-register-helper/";s:4:"Slug";s:21:"pmpro-register-helper";s:6:"plugin";s:47:"pmpro-register-helper/pmpro-register-helper.php";s:7:"Version";s:5:"1.3.5";s:11:"Description";s:106:"Capture additional member information with custom fields at Membership Checkout with Paid Memberships Pro.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:78:"http://license.paidmembershipspro.com/downloads/free/pmpro-register-helper.zip";s:7:"License";s:13:"wordpress.org";}i:49;a:11:{s:4:"Name";s:47:"Paid Memberships Pro - Reports Dashboard Add On";s:5:"Title";s:47:"Paid Memberships Pro - Reports Dashboard Add On";s:9:"PluginURI";s:61:"http://www.paidmembershipspro.com/wp/pmpro-reports-dashboard/";s:4:"Slug";s:23:"pmpro-reports-dashboard";s:6:"plugin";s:51:"pmpro-reports-dashboard/pmpro-reports-dashboard.php";s:7:"Version";s:2:".2";s:11:"Description";s:86:"Responsive Membership Reports Dashboard for Administrator and Membership Manager Role.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:80:"http://license.paidmembershipspro.com/downloads/plus/pmpro-reports-dashboard.zip";s:7:"License";s:4:"plus";}i:50;a:11:{s:4:"Name";s:50:"Paid Memberships Pro - Set Expiration Dates Add On";s:5:"Title";s:50:"Paid Memberships Pro - Set Expiration Dates Add On";s:9:"PluginURI";s:64:"http://www.paidmembershipspro.com/wp/pmpro-set-expiration-dates/";s:4:"Slug";s:26:"pmpro-set-expiration-dates";s:6:"plugin";s:57:"pmpro-set-expiration-dates/pmpro-set-expiration-dates.php";s:7:"Version";s:2:".3";s:11:"Description";s:95:"Set a specific expiration date (e.g. 2013-12-31) for a PMPro membership level or discount code.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:83:"http://license.paidmembershipspro.com/downloads/plus/pmpro-set-expiration-dates.zip";s:7:"License";s:4:"plus";}i:51;a:11:{s:4:"Name";s:38:"Paid Memberships Pro - Shipping Add On";s:5:"Title";s:38:"Paid Memberships Pro - Shipping Add On";s:9:"PluginURI";s:80:"https://www.paidmembershipspro.com/add-ons/shipping-address-membership-checkout/";s:4:"Slug";s:14:"pmpro-shipping";s:6:"plugin";s:33:"pmpro-shipping/pmpro-shipping.php";s:7:"Version";s:2:".4";s:11:"Description";s:52:"Add shipping to the checkout page and other updates.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:34:"https://www.paidmembershipspro.com";s:8:"Download";s:71:"http://license.paidmembershipspro.com/downloads/plus/pmpro-shipping.zip";s:7:"License";s:4:"plus";}i:52;a:11:{s:4:"Name";s:39:"Paid Memberships Pro - Signup Shortcode";s:5:"Title";s:39:"Paid Memberships Pro - Signup Shortcode";s:9:"PluginURI";s:60:"http://www.paidmembershipspro.com/wp/pmpro-signup-shortcode/";s:4:"Slug";s:22:"pmpro-signup-shortcode";s:6:"plugin";s:49:"pmpro-signup-shortcode/pmpro-signup-shortcode.php";s:7:"Version";s:2:".1";s:11:"Description";s:94:"Shortcode for a simplified Membership Signup Form with options for email only signup and more.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:79:"http://license.paidmembershipspro.com/downloads/plus/pmpro-signup-shortcode.zip";s:7:"License";s:4:"plus";}i:53;a:11:{s:4:"Name";s:40:"Paid Memberships Pro - Slack Integration";s:5:"Title";s:40:"Paid Memberships Pro - Slack Integration";s:9:"PluginURI";s:0:"";s:4:"Slug";s:11:"pmpro-slack";s:6:"plugin";s:27:"pmpro-slack/pmpro-slack.php";s:7:"Version";s:3:"1.0";s:11:"Description";s:53:"Slack integration for the Paid Memberships Pro plugin";s:6:"Author";s:12:"Nikhil Vimal";s:9:"AuthorURI";s:24:"http://nik.techvoltz.com";s:8:"Download";s:68:"http://license.paidmembershipspro.com/downloads/plus/pmpro-slack.zip";s:7:"License";s:4:"plus";}i:54;a:11:{s:4:"Name";s:47:"Paid Memberships Pro - Sponsored Members Add On";s:5:"Title";s:47:"Paid Memberships Pro - Sponsored Members Add On";s:9:"PluginURI";s:66:"http://www.paidmembershipspro.com/add-ons/pmpro-sponsored-members/";s:4:"Slug";s:23:"pmpro-sponsored-members";s:6:"plugin";s:51:"pmpro-sponsored-members/pmpro-sponsored-members.php";s:7:"Version";s:4:".6.2";s:11:"Description";s:84:"Generate discount code for a main account holder to distribute to sponsored members.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:80:"http://license.paidmembershipspro.com/downloads/plus/pmpro-sponsored-members.zip";s:7:"License";s:4:"plus";}i:55;a:11:{s:4:"Name";s:45:"Paid Memberships Pro - State Dropdowns Add On";s:5:"Title";s:45:"Paid Memberships Pro - State Dropdowns Add On";s:9:"PluginURI";s:0:"";s:4:"Slug";s:21:"pmpro-state-dropdowns";s:6:"plugin";s:47:"pmpro-state-dropdowns/pmpro-state-dropdowns.php";s:7:"Version";s:3:"0.1";s:11:"Description";s:85:"Creates an autopopulated field for countries and states/provinces for billing fields.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:27:"https://strangerstuidos.com";s:8:"Download";s:78:"http://license.paidmembershipspro.com/downloads/plus/pmpro-state-dropdowns.zip";s:7:"License";s:4:"plus";}i:56;a:11:{s:4:"Name";s:48:"Paid Memberships Pro - Subscription Delays Addon";s:5:"Title";s:48:"Paid Memberships Pro - Subscription Delays Addon";s:9:"PluginURI";s:63:"http://www.paidmembershipspro.com/wp/pmpro-subscription-delays/";s:4:"Slug";s:25:"pmpro-subscription-delays";s:6:"plugin";s:55:"pmpro-subscription-delays/pmpro-subscription-delays.php";s:7:"Version";s:4:".4.3";s:11:"Description";s:138:"Add a field to levels and discount codes to delay the start of a subscription by X days. (Add variable-length free trials to your levels.)";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:82:"http://license.paidmembershipspro.com/downloads/plus/pmpro-subscription-delays.zip";s:7:"License";s:4:"plus";}i:57;a:11:{s:4:"Name";s:40:"Paid Memberships Pro - User Pages Add On";s:5:"Title";s:40:"Paid Memberships Pro - User Pages Add On";s:9:"PluginURI";s:51:"http://www.paidmembershipspro.com/pmpro-user-pages/";s:4:"Slug";s:16:"pmpro-user-pages";s:6:"plugin";s:37:"pmpro-user-pages/pmpro-user-pages.php";s:7:"Version";s:4:".5.3";s:11:"Description";s:88:"When a user signs up, create a page for them that only they (and admins) have access to.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:73:"http://license.paidmembershipspro.com/downloads/plus/pmpro-user-pages.zip";s:7:"License";s:4:"plus";}i:58;a:11:{s:4:"Name";s:30:"Paid Memberships Pro - VAT Tax";s:5:"Title";s:30:"Paid Memberships Pro - VAT Tax";s:9:"PluginURI";s:51:"http://www.paidmembershipspro.com/wp/pmpro-vat-tax/";s:4:"Slug";s:13:"pmpro-vat-tax";s:6:"plugin";s:31:"pmpro-vat-tax/pmpro-vat-tax.php";s:7:"Version";s:2:".5";s:11:"Description";s:114:"Calculate VAT tax at checkout and allow customers with a VAT Number lookup for VAT tax exemptions in EU countries.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:70:"http://license.paidmembershipspro.com/downloads/plus/pmpro-vat-tax.zip";s:7:"License";s:4:"plus";}i:59;a:11:{s:4:"Name";s:41:"Paid Memberships Pro - WooCommerce Add On";s:5:"Title";s:41:"Paid Memberships Pro - WooCommerce Add On";s:9:"PluginURI";s:52:"http://www.paidmembershipspro.com/pmpro-woocommerce/";s:4:"Slug";s:17:"pmpro-woocommerce";s:6:"plugin";s:39:"pmpro-woocommerce/pmpro-woocommerce.php";s:7:"Version";s:5:"1.4.5";s:11:"Description";s:48:"Integrate WooCommerce with Paid Memberships Pro.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:60:"https://downloads.wordpress.org/plugin/pmpro-woocommerce.zip";s:7:"License";s:13:"wordpress.org";}i:60;a:11:{s:4:"Name";s:52:"Paid Memberships Pro - WordPress Social Login Add On";s:5:"Title";s:52:"Paid Memberships Pro - WordPress Social Login Add On";s:9:"PluginURI";s:56:"http://www.paidmembershipspro.com/wp/pmpro-social-login/";s:4:"Slug";s:18:"pmpro-social-login";s:6:"plugin";s:41:"pmpro-social-login/pmpro-social-login.php";s:7:"Version";s:2:".2";s:11:"Description";s:111:"Allow users to create membership account via social networks as configured via WordPress Social Login by Miled.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:75:"http://license.paidmembershipspro.com/downloads/plus/pmpro-social-login.zip";s:7:"License";s:4:"plus";}i:61;a:11:{s:4:"Name";s:63:"Paid Memberships Pro - WP Affiliate Platform Integration Add On";s:5:"Title";s:63:"Paid Memberships Pro - WP Affiliate Platform Integration Add On";s:9:"PluginURI";s:47:"http://www.paidmembershipspro.com/wp/pmpro-dev/";s:4:"Slug";s:27:"pmpro-wp-affiliate-platform";s:6:"plugin";s:59:"pmpro-wp-affiliate-platform/pmpro-wp-affiliate-platform.php";s:7:"Version";s:5:"1.7.1";s:11:"Description";s:70:"Process an affiliate via WP Affiliate Platform after a PMPro checkout.";s:6:"Author";s:36:"Stranger Studios, Tips and Tricks HQ";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:84:"http://license.paidmembershipspro.com/downloads/plus/pmpro-wp-affiliate-platform.zip";s:7:"License";s:4:"plus";}i:62;a:11:{s:4:"Name";s:14:"PMPro Gift Aid";s:5:"Title";s:14:"PMPro Gift Aid";s:9:"PluginURI";s:52:"http://www.paidmembershipspro.com/wp/pmpro-gift-aid/";s:4:"Slug";s:14:"pmpro-gift-aid";s:6:"plugin";s:33:"pmpro-gift-aid/pmpro-gift-aid.php";s:7:"Version";s:4:".1.2";s:11:"Description";s:40:"Add checkbox to opt into the UK Gift Aid";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:71:"http://license.paidmembershipspro.com/downloads/free/pmpro-gift-aid.zip";s:7:"License";s:4:"plus";}i:63;a:11:{s:4:"Name";s:25:"PMPro Levels as UL Layout";s:5:"Title";s:25:"PMPro Levels as UL Layout";s:9:"PluginURI";s:53:"http://www.paidmembershipspro.com/wp/pmpro-ul-levels/";s:4:"Slug";s:15:"pmpro-ul-levels";s:6:"plugin";s:35:"pmpro-ul-levels/pmpro-ul-levels.php";s:7:"Version";s:2:".1";s:11:"Description";s:107:"Display your Membership Levels page in a UL Layout (includes classes for Foundation by ZURB Pricing table).";s:6:"Author";s:42:"Stranger Studios / Tweaked by Lasse Larsen";s:9:"AuthorURI";s:28:"http://www.seventysixnyc.com";s:8:"Download";s:72:"http://license.paidmembershipspro.com/downloads/plus/pmpro-ul-levels.zip";s:7:"License";s:4:"plus";}i:64;a:11:{s:4:"Name";s:30:"PMPro Payflow Recurring Orders";s:5:"Title";s:30:"PMPro Payflow Recurring Orders";s:9:"PluginURI";s:68:"http://www.paidmembershipspro.com/wp/pmpro-payflow-recurring-orders/";s:4:"Slug";s:30:"pmpro-payflow-recurring-orders";s:6:"plugin";s:65:"pmpro-payflow-recurring-orders/pmpro-payflow-recurring-orders.php";s:7:"Version";s:4:".1.1";s:11:"Description";s:72:"Check daily for new recurring orders in Payflow and add as PMPro orders.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:87:"http://license.paidmembershipspro.com/downloads/plus/pmpro-payflow-recurring-orders.zip";s:7:"License";s:4:"plus";}i:65;a:11:{s:4:"Name";s:27:"PMPro Reason For Cancelling";s:5:"Title";s:27:"PMPro Reason For Cancelling";s:9:"PluginURI";s:65:"http://www.paidmembershipspro.com/wp/pmpro-reason-for-cancelling/";s:4:"Slug";s:27:"pmpro-reason-for-cancelling";s:6:"plugin";s:59:"pmpro-reason-for-cancelling/pmpro-reason-for-cancelling.php";s:7:"Version";s:4:".1.1";s:11:"Description";s:67:"Adds a field to specify a reason for cancelling to the cancel page.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:84:"http://license.paidmembershipspro.com/downloads/plus/pmpro-reason-for-cancelling.zip";s:7:"License";s:4:"plus";}i:66;a:11:{s:4:"Name";s:11:"PMPro Roles";s:5:"Title";s:11:"PMPro Roles";s:9:"PluginURI";s:22:"http://joshlevinson.me";s:4:"Slug";s:11:"pmpro-roles";s:6:"plugin";s:27:"pmpro-roles/pmpro-roles.php";s:7:"Version";s:3:"1.0";s:11:"Description";s:160:"Adds a WordPress Role for each Membership Level with Display Name = Membership Level Name and Role Name = \'pmpro_role_X\' (where X is the Membership Level\'s ID).";s:6:"Author";s:13:"Josh Levinson";s:9:"AuthorURI";s:22:"http://joshlevinson.me";s:8:"Download";s:68:"http://license.paidmembershipspro.com/downloads/plus/pmpro-roles.zip";s:7:"License";s:4:"free";}i:67;a:11:{s:4:"Name";s:12:"PMPro Series";s:5:"Title";s:12:"PMPro Series";s:9:"PluginURI";s:47:"http://www.paidmembershipspro.com/pmpro-series/";s:4:"Slug";s:12:"pmpro-series";s:6:"plugin";s:29:"pmpro-series/pmpro-series.php";s:7:"Version";s:4:".3.7";s:11:"Description";s:59:"Offer serialized (drip feed) content to your PMPro members.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:69:"http://license.paidmembershipspro.com/downloads/plus/pmpro-series.zip";s:7:"License";s:4:"plus";}i:68;a:11:{s:4:"Name";s:19:"PMPro Social Locker";s:5:"Title";s:19:"PMPro Social Locker";s:9:"PluginURI";s:0:"";s:4:"Slug";s:19:"pmpro-social-locker";s:6:"plugin";s:43:"pmpro-social-locker/pmpro-social-locker.php";s:7:"Version";s:4:".1.1";s:11:"Description";s:194:"Integrate PMPro with the Social Locker plugin from OnePress (http://wordpress.org/support/plugin/social-locker). The goal is to give a user a free membership if they interact with Social Locker.";s:6:"Author";s:45:"Scott Sousa (Slocum Studio), Stranger Studios";s:9:"AuthorURI";s:28:"http://www.slocumstudio.com/";s:8:"Download";s:76:"http://license.paidmembershipspro.com/downloads/plus/pmpro-social-locker.zip";s:7:"License";s:4:"plus";}i:69;a:11:{s:4:"Name";s:22:"PMPro Strong Passwords";s:5:"Title";s:22:"PMPro Strong Passwords";s:9:"PluginURI";s:85:"http://www.paidmembershipspro.com/add-ons/plugins-on-github/require-strong-passwords/";s:4:"Slug";s:22:"pmpro-strong-passwords";s:6:"plugin";s:49:"pmpro-strong-passwords/pmpro-strong-passwords.php";s:7:"Version";s:3:"0.1";s:11:"Description";s:39:"Force users to submit strong passwords.";s:6:"Author";s:11:"Scott Sousa";s:9:"AuthorURI";s:23:"http://slocumstudio.com";s:8:"Download";s:79:"http://license.paidmembershipspro.com/downloads/plus/pmpro-strong-passwords.zip";s:7:"License";s:4:"plus";}i:70;a:11:{s:4:"Name";s:10:"WP Bouncer";s:5:"Title";s:10:"WP Bouncer";s:9:"PluginURI";s:45:"http://andrewnorcross.com/plugins/wp-bouncer/";s:4:"Slug";s:10:"wp-bouncer";s:6:"plugin";s:25:"wp-bouncer/wp-bouncer.php";s:7:"Version";s:5:"1.3.1";s:11:"Description";s:64:"Only allow one device to be logged into WordPress for each user.";s:6:"Author";s:32:"Andrew Norcross, strangerstudios";s:9:"AuthorURI";s:25:"http://andrewnorcross.com";s:8:"Download";s:53:"https://downloads.wordpress.org/plugin/wp-bouncer.zip";s:7:"License";s:13:"wordpress.org";}}', 'no'),
(315, 'pmpro_addons_timestamp', '1505005359', 'no'),
(342, 'pmpro_license_check', 'a:2:{s:7:"license";b:0;s:7:"enddate";i:0;}', 'no') ;

#
# End of data contents of table `wp_options`
# --------------------------------------------------------



#
# Delete any existing table `wp_pmpro_discount_codes`
#

DROP TABLE IF EXISTS `wp_pmpro_discount_codes`;


#
# Table structure of table `wp_pmpro_discount_codes`
#

CREATE TABLE `wp_pmpro_discount_codes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `starts` date NOT NULL,
  `expires` date NOT NULL,
  `uses` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `starts` (`starts`),
  KEY `expires` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_pmpro_discount_codes`
#

#
# End of data contents of table `wp_pmpro_discount_codes`
# --------------------------------------------------------



#
# Delete any existing table `wp_pmpro_discount_codes_levels`
#

DROP TABLE IF EXISTS `wp_pmpro_discount_codes_levels`;


#
# Table structure of table `wp_pmpro_discount_codes_levels`
#

CREATE TABLE `wp_pmpro_discount_codes_levels` (
  `code_id` int(11) unsigned NOT NULL,
  `level_id` int(11) unsigned NOT NULL,
  `initial_payment` decimal(10,2) NOT NULL DEFAULT '0.00',
  `billing_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `cycle_number` int(11) NOT NULL DEFAULT '0',
  `cycle_period` enum('Day','Week','Month','Year') DEFAULT 'Month',
  `billing_limit` int(11) NOT NULL COMMENT 'After how many cycles should billing stop?',
  `trial_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `trial_limit` int(11) NOT NULL DEFAULT '0',
  `expiration_number` int(10) unsigned NOT NULL,
  `expiration_period` enum('Day','Week','Month','Year') NOT NULL,
  PRIMARY KEY (`code_id`,`level_id`),
  KEY `initial_payment` (`initial_payment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_pmpro_discount_codes_levels`
#

#
# End of data contents of table `wp_pmpro_discount_codes_levels`
# --------------------------------------------------------



#
# Delete any existing table `wp_pmpro_discount_codes_uses`
#

DROP TABLE IF EXISTS `wp_pmpro_discount_codes_uses`;


#
# Table structure of table `wp_pmpro_discount_codes_uses`
#

CREATE TABLE `wp_pmpro_discount_codes_uses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_pmpro_discount_codes_uses`
#

#
# End of data contents of table `wp_pmpro_discount_codes_uses`
# --------------------------------------------------------



#
# Delete any existing table `wp_pmpro_membership_levelmeta`
#

DROP TABLE IF EXISTS `wp_pmpro_membership_levelmeta`;


#
# Table structure of table `wp_pmpro_membership_levelmeta`
#

CREATE TABLE `wp_pmpro_membership_levelmeta` (
  `meta_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pmpro_membership_level_id` int(10) unsigned NOT NULL,
  `meta_key` varchar(255) NOT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `pmpro_membership_level_id` (`pmpro_membership_level_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_pmpro_membership_levelmeta`
#

#
# End of data contents of table `wp_pmpro_membership_levelmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_pmpro_membership_levels`
#

DROP TABLE IF EXISTS `wp_pmpro_membership_levels`;


#
# Table structure of table `wp_pmpro_membership_levels`
#

CREATE TABLE `wp_pmpro_membership_levels` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `confirmation` longtext NOT NULL,
  `initial_payment` decimal(10,2) NOT NULL DEFAULT '0.00',
  `billing_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `cycle_number` int(11) NOT NULL DEFAULT '0',
  `cycle_period` enum('Day','Week','Month','Year') DEFAULT 'Month',
  `billing_limit` int(11) NOT NULL COMMENT 'After how many cycles should billing stop?',
  `trial_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `trial_limit` int(11) NOT NULL DEFAULT '0',
  `allow_signups` tinyint(4) NOT NULL DEFAULT '1',
  `expiration_number` int(10) unsigned NOT NULL,
  `expiration_period` enum('Day','Week','Month','Year') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `allow_signups` (`allow_signups`),
  KEY `initial_payment` (`initial_payment`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_pmpro_membership_levels`
#
INSERT INTO `wp_pmpro_membership_levels` ( `id`, `name`, `description`, `confirmation`, `initial_payment`, `billing_amount`, `cycle_number`, `cycle_period`, `billing_limit`, `trial_amount`, `trial_limit`, `allow_signups`, `expiration_number`, `expiration_period`) VALUES
(1, 'Explorer', '', '', '0.00', '0.00', 0, '', 0, '0.00', 0, 1, 0, ''),
(2, 'Community Member', '', '', '50.00', '4.99', 1, 'Year', 0, '0.00', 0, 1, 0, ''),
(3, 'Student', '', '', '130.00', '4.99', 1, 'Year', 0, '0.00', 0, 1, 0, ''),
(4, 'Sage', '', '', '250.00', '250.00', 1, 'Year', 0, '0.00', 0, 1, 0, '') ;

#
# End of data contents of table `wp_pmpro_membership_levels`
# --------------------------------------------------------



#
# Delete any existing table `wp_pmpro_membership_orders`
#

DROP TABLE IF EXISTS `wp_pmpro_membership_orders`;


#
# Table structure of table `wp_pmpro_membership_orders`
#

CREATE TABLE `wp_pmpro_membership_orders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `session_id` varchar(64) NOT NULL DEFAULT '',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `membership_id` int(11) unsigned NOT NULL DEFAULT '0',
  `paypal_token` varchar(64) NOT NULL DEFAULT '',
  `billing_name` varchar(128) NOT NULL DEFAULT '',
  `billing_street` varchar(128) NOT NULL DEFAULT '',
  `billing_city` varchar(128) NOT NULL DEFAULT '',
  `billing_state` varchar(32) NOT NULL DEFAULT '',
  `billing_zip` varchar(16) NOT NULL DEFAULT '',
  `billing_country` varchar(128) NOT NULL,
  `billing_phone` varchar(32) NOT NULL,
  `subtotal` varchar(16) NOT NULL DEFAULT '',
  `tax` varchar(16) NOT NULL DEFAULT '',
  `couponamount` varchar(16) NOT NULL DEFAULT '',
  `checkout_id` int(11) NOT NULL DEFAULT '0',
  `certificate_id` int(11) NOT NULL DEFAULT '0',
  `certificateamount` varchar(16) NOT NULL DEFAULT '',
  `total` varchar(16) NOT NULL DEFAULT '',
  `payment_type` varchar(64) NOT NULL DEFAULT '',
  `cardtype` varchar(32) NOT NULL DEFAULT '',
  `accountnumber` varchar(32) NOT NULL DEFAULT '',
  `expirationmonth` char(2) NOT NULL DEFAULT '',
  `expirationyear` varchar(4) NOT NULL DEFAULT '',
  `status` varchar(32) NOT NULL DEFAULT '',
  `gateway` varchar(64) NOT NULL,
  `gateway_environment` varchar(64) NOT NULL,
  `payment_transaction_id` varchar(64) NOT NULL,
  `subscription_transaction_id` varchar(32) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `affiliate_id` varchar(32) NOT NULL,
  `affiliate_subid` varchar(32) NOT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `session_id` (`session_id`),
  KEY `user_id` (`user_id`),
  KEY `membership_id` (`membership_id`),
  KEY `status` (`status`),
  KEY `timestamp` (`timestamp`),
  KEY `gateway` (`gateway`),
  KEY `gateway_environment` (`gateway_environment`),
  KEY `payment_transaction_id` (`payment_transaction_id`),
  KEY `subscription_transaction_id` (`subscription_transaction_id`),
  KEY `affiliate_id` (`affiliate_id`),
  KEY `affiliate_subid` (`affiliate_subid`),
  KEY `checkout_id` (`checkout_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_pmpro_membership_orders`
#
INSERT INTO `wp_pmpro_membership_orders` ( `id`, `code`, `session_id`, `user_id`, `membership_id`, `paypal_token`, `billing_name`, `billing_street`, `billing_city`, `billing_state`, `billing_zip`, `billing_country`, `billing_phone`, `subtotal`, `tax`, `couponamount`, `checkout_id`, `certificate_id`, `certificateamount`, `total`, `payment_type`, `cardtype`, `accountnumber`, `expirationmonth`, `expirationyear`, `status`, `gateway`, `gateway_environment`, `payment_transaction_id`, `subscription_transaction_id`, `timestamp`, `affiliate_id`, `affiliate_subid`, `notes`) VALUES
(1, '63280AB8F5', 'fo11sirb8n4ue8f6qtuigaa0l6', 1, 2, '', '', '', '', '', '', '', '', '50.00', '0', '', 1, 0, '', '50', '', 'Visa', 'XXXXXXXXXXXX4242', '04', '2022', 'success', 'stripe', 'sandbox', 'ch_1B01M1C1Fm4mFmRs53Lv7WGZ', 'sub_BMkrHHpjDGd6z3', '2017-09-09 05:18:03', '', '', '') ;

#
# End of data contents of table `wp_pmpro_membership_orders`
# --------------------------------------------------------



#
# Delete any existing table `wp_pmpro_memberships_categories`
#

DROP TABLE IF EXISTS `wp_pmpro_memberships_categories`;


#
# Table structure of table `wp_pmpro_memberships_categories`
#

CREATE TABLE `wp_pmpro_memberships_categories` (
  `membership_id` int(11) unsigned NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `membership_category` (`membership_id`,`category_id`),
  UNIQUE KEY `category_membership` (`category_id`,`membership_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_pmpro_memberships_categories`
#

#
# End of data contents of table `wp_pmpro_memberships_categories`
# --------------------------------------------------------



#
# Delete any existing table `wp_pmpro_memberships_pages`
#

DROP TABLE IF EXISTS `wp_pmpro_memberships_pages`;


#
# Table structure of table `wp_pmpro_memberships_pages`
#

CREATE TABLE `wp_pmpro_memberships_pages` (
  `membership_id` int(11) unsigned NOT NULL,
  `page_id` int(11) unsigned NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `category_membership` (`page_id`,`membership_id`),
  UNIQUE KEY `membership_page` (`membership_id`,`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_pmpro_memberships_pages`
#

#
# End of data contents of table `wp_pmpro_memberships_pages`
# --------------------------------------------------------



#
# Delete any existing table `wp_pmpro_memberships_users`
#

DROP TABLE IF EXISTS `wp_pmpro_memberships_users`;


#
# Table structure of table `wp_pmpro_memberships_users`
#

CREATE TABLE `wp_pmpro_memberships_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `membership_id` int(11) unsigned NOT NULL,
  `code_id` int(11) unsigned NOT NULL,
  `initial_payment` decimal(10,2) NOT NULL,
  `billing_amount` decimal(10,2) NOT NULL,
  `cycle_number` int(11) NOT NULL,
  `cycle_period` enum('Day','Week','Month','Year') NOT NULL DEFAULT 'Month',
  `billing_limit` int(11) NOT NULL,
  `trial_amount` decimal(10,2) NOT NULL,
  `trial_limit` int(11) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'active',
  `startdate` datetime NOT NULL,
  `enddate` datetime DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `membership_id` (`membership_id`),
  KEY `modified` (`modified`),
  KEY `code_id` (`code_id`),
  KEY `enddate` (`enddate`),
  KEY `user_id` (`user_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_pmpro_memberships_users`
#
INSERT INTO `wp_pmpro_memberships_users` ( `id`, `user_id`, `membership_id`, `code_id`, `initial_payment`, `billing_amount`, `cycle_number`, `cycle_period`, `billing_limit`, `trial_amount`, `trial_limit`, `status`, `startdate`, `enddate`, `modified`) VALUES
(1, 1, 2, 0, '50.00', '4.99', 1, 'Year', 0, '0.00', 0, 'active', '2017-09-09 05:18:10', '0000-00-00 00:00:00', '2017-09-09 06:18:10') ;

#
# End of data contents of table `wp_pmpro_memberships_users`
# --------------------------------------------------------



#
# Delete any existing table `wp_postmeta`
#

DROP TABLE IF EXISTS `wp_postmeta`;


#
# Table structure of table `wp_postmeta`
#

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=229 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_postmeta`
#
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 4, '_edit_last', '1'),
(3, 4, '_edit_lock', '1504764277:1'),
(4, 5, '_edit_last', '1'),
(5, 5, '_edit_lock', '1504914347:1'),
(6, 7, '_edit_last', '1'),
(7, 7, '_edit_lock', '1504917222:1'),
(8, 9, '_edit_last', '1'),
(9, 9, '_edit_lock', '1504917355:1'),
(10, 11, '_edit_last', '1'),
(11, 11, '_edit_lock', '1504918796:1'),
(19, 15, '_edit_last', '1'),
(20, 15, '_edit_lock', '1504920732:1'),
(21, 17, '_edit_last', '1'),
(22, 17, '_edit_lock', '1504923623:1'),
(23, 19, '_menu_item_type', 'custom'),
(24, 19, '_menu_item_menu_item_parent', '0'),
(25, 19, '_menu_item_object_id', '19'),
(26, 19, '_menu_item_object', 'custom'),
(27, 19, '_menu_item_target', ''),
(28, 19, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(29, 19, '_menu_item_xfn', ''),
(30, 19, '_menu_item_url', 'http://nxtchptr.herokuapp.com/'),
(31, 19, '_menu_item_orphaned', '1504926525'),
(32, 20, '_menu_item_type', 'post_type'),
(33, 20, '_menu_item_menu_item_parent', '0'),
(34, 20, '_menu_item_object_id', '15'),
(35, 20, '_menu_item_object', 'page'),
(36, 20, '_menu_item_target', ''),
(37, 20, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(38, 20, '_menu_item_xfn', ''),
(39, 20, '_menu_item_url', ''),
(41, 21, '_menu_item_type', 'post_type'),
(42, 21, '_menu_item_menu_item_parent', '0'),
(43, 21, '_menu_item_object_id', '11'),
(44, 21, '_menu_item_object', 'page'),
(45, 21, '_menu_item_target', ''),
(46, 21, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(47, 21, '_menu_item_xfn', ''),
(48, 21, '_menu_item_url', ''),
(50, 22, '_menu_item_type', 'post_type'),
(51, 22, '_menu_item_menu_item_parent', '0'),
(52, 22, '_menu_item_object_id', '17'),
(53, 22, '_menu_item_object', 'page'),
(54, 22, '_menu_item_target', ''),
(55, 22, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(56, 22, '_menu_item_xfn', ''),
(57, 22, '_menu_item_url', ''),
(59, 23, '_menu_item_type', 'post_type'),
(60, 23, '_menu_item_menu_item_parent', '0'),
(61, 23, '_menu_item_object_id', '5'),
(62, 23, '_menu_item_object', 'page'),
(63, 23, '_menu_item_target', ''),
(64, 23, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(65, 23, '_menu_item_xfn', ''),
(66, 23, '_menu_item_url', ''),
(68, 24, '_menu_item_type', 'post_type'),
(69, 24, '_menu_item_menu_item_parent', '0'),
(70, 24, '_menu_item_object_id', '9'),
(71, 24, '_menu_item_object', 'page'),
(72, 24, '_menu_item_target', ''),
(73, 24, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(74, 24, '_menu_item_xfn', ''),
(75, 24, '_menu_item_url', ''),
(77, 25, '_menu_item_type', 'post_type'),
(78, 25, '_menu_item_menu_item_parent', '0'),
(79, 25, '_menu_item_object_id', '2'),
(80, 25, '_menu_item_object', 'page'),
(81, 25, '_menu_item_target', ''),
(82, 25, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(83, 25, '_menu_item_xfn', ''),
(84, 25, '_menu_item_url', ''),
(85, 25, '_menu_item_orphaned', '1504926529'),
(86, 26, '_menu_item_type', 'post_type'),
(87, 26, '_menu_item_menu_item_parent', '0'),
(88, 26, '_menu_item_object_id', '7'),
(89, 26, '_menu_item_object', 'page'),
(90, 26, '_menu_item_target', ''),
(91, 26, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(92, 26, '_menu_item_xfn', ''),
(93, 26, '_menu_item_url', ''),
(95, 27, '_menu_item_type', 'post_type'),
(96, 27, '_menu_item_menu_item_parent', '0'),
(97, 27, '_menu_item_object_id', '17'),
(98, 27, '_menu_item_object', 'page'),
(99, 27, '_menu_item_target', ''),
(100, 27, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(101, 27, '_menu_item_xfn', ''),
(102, 27, '_menu_item_url', ''),
(104, 28, '_menu_item_type', 'post_type'),
(105, 28, '_menu_item_menu_item_parent', '0'),
(106, 28, '_menu_item_object_id', '15'),
(107, 28, '_menu_item_object', 'page'),
(108, 28, '_menu_item_target', ''),
(109, 28, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(110, 28, '_menu_item_xfn', ''),
(111, 28, '_menu_item_url', ''),
(113, 29, '_menu_item_type', 'post_type'),
(114, 29, '_menu_item_menu_item_parent', '0'),
(115, 29, '_menu_item_object_id', '9') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(116, 29, '_menu_item_object', 'page'),
(117, 29, '_menu_item_target', ''),
(118, 29, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(119, 29, '_menu_item_xfn', ''),
(120, 29, '_menu_item_url', ''),
(122, 30, '_menu_item_type', 'post_type'),
(123, 30, '_menu_item_menu_item_parent', '0'),
(124, 30, '_menu_item_object_id', '7'),
(125, 30, '_menu_item_object', 'page'),
(126, 30, '_menu_item_target', ''),
(127, 30, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(128, 30, '_menu_item_xfn', ''),
(129, 30, '_menu_item_url', ''),
(131, 31, '_menu_item_type', 'post_type'),
(132, 31, '_menu_item_menu_item_parent', '0'),
(133, 31, '_menu_item_object_id', '5'),
(134, 31, '_menu_item_object', 'page'),
(135, 31, '_menu_item_target', ''),
(136, 31, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(137, 31, '_menu_item_xfn', ''),
(138, 31, '_menu_item_url', ''),
(140, 32, '_edit_lock', '1505005728:1'),
(141, 39, '_menu_item_type', 'post_type'),
(142, 39, '_menu_item_menu_item_parent', '0'),
(143, 39, '_menu_item_object_id', '11'),
(144, 39, '_menu_item_object', 'page'),
(145, 39, '_menu_item_target', ''),
(146, 39, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(147, 39, '_menu_item_xfn', ''),
(148, 39, '_menu_item_url', ''),
(150, 40, '_menu_item_type', 'post_type'),
(151, 40, '_menu_item_menu_item_parent', '0'),
(152, 40, '_menu_item_object_id', '32'),
(153, 40, '_menu_item_object', 'page'),
(154, 40, '_menu_item_target', ''),
(155, 40, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(156, 40, '_menu_item_xfn', ''),
(157, 40, '_menu_item_url', ''),
(159, 41, '_menu_item_type', 'post_type'),
(160, 41, '_menu_item_menu_item_parent', '0'),
(161, 41, '_menu_item_object_id', '9'),
(162, 41, '_menu_item_object', 'page'),
(163, 41, '_menu_item_target', ''),
(164, 41, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(165, 41, '_menu_item_xfn', ''),
(166, 41, '_menu_item_url', ''),
(168, 42, '_menu_item_type', 'post_type'),
(169, 42, '_menu_item_menu_item_parent', '0'),
(170, 42, '_menu_item_object_id', '7'),
(171, 42, '_menu_item_object', 'page'),
(172, 42, '_menu_item_target', ''),
(173, 42, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(174, 42, '_menu_item_xfn', ''),
(175, 42, '_menu_item_url', ''),
(177, 43, '_menu_item_type', 'post_type'),
(178, 43, '_menu_item_menu_item_parent', '0'),
(179, 43, '_menu_item_object_id', '15'),
(180, 43, '_menu_item_object', 'page'),
(181, 43, '_menu_item_target', ''),
(182, 43, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(183, 43, '_menu_item_xfn', ''),
(184, 43, '_menu_item_url', ''),
(186, 44, '_menu_item_type', 'post_type'),
(187, 44, '_menu_item_menu_item_parent', '0'),
(188, 44, '_menu_item_object_id', '17'),
(189, 44, '_menu_item_object', 'page'),
(190, 44, '_menu_item_target', ''),
(191, 44, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(192, 44, '_menu_item_xfn', ''),
(193, 44, '_menu_item_url', ''),
(195, 32, '_edit_last', '1'),
(196, 46, '_edit_last', '1'),
(197, 46, '_edit_lock', '1505005752:1'),
(198, 48, '_edit_last', '1'),
(199, 48, '_edit_lock', '1505005758:1'),
(200, 50, '_menu_item_type', 'post_type'),
(201, 50, '_menu_item_menu_item_parent', '0'),
(202, 50, '_menu_item_object_id', '48'),
(203, 50, '_menu_item_object', 'page'),
(204, 50, '_menu_item_target', ''),
(205, 50, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(206, 50, '_menu_item_xfn', ''),
(207, 50, '_menu_item_url', ''),
(209, 51, '_menu_item_type', 'post_type'),
(210, 51, '_menu_item_menu_item_parent', '0'),
(211, 51, '_menu_item_object_id', '46'),
(212, 51, '_menu_item_object', 'page'),
(213, 51, '_menu_item_target', ''),
(214, 51, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(215, 51, '_menu_item_xfn', ''),
(216, 51, '_menu_item_url', ''),
(218, 52, '_menu_item_type', 'post_type'),
(219, 52, '_menu_item_menu_item_parent', '0'),
(220, 52, '_menu_item_object_id', '32'),
(221, 52, '_menu_item_object', 'page'),
(222, 52, '_menu_item_target', ''),
(223, 52, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(224, 52, '_menu_item_xfn', ''),
(225, 52, '_menu_item_url', ''),
(227, 54, '_edit_last', '1') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(228, 54, '_edit_lock', '1505005569:1') ;

#
# End of data contents of table `wp_postmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_posts`
#

DROP TABLE IF EXISTS `wp_posts`;


#
# Table structure of table `wp_posts`
#

CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_posts`
#
INSERT INTO `wp_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2017-09-07 01:15:44', '2017-09-07 01:15:44', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2017-09-07 01:15:44', '2017-09-07 01:15:44', '', 0, 'http://nxtchptr.herokuapp.com/?p=1', 0, 'post', '', 1),
(2, 1, '2017-09-07 01:15:44', '2017-09-07 01:15:44', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://nxtchptr.herokuapp.com/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2017-09-07 01:15:44', '2017-09-07 01:15:44', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=2', 0, 'page', '', 0),
(3, 1, '2017-09-07 01:22:02', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2017-09-07 01:22:02', '0000-00-00 00:00:00', '', 0, 'http://nxtchptr.herokuapp.com/?p=3', 0, 'post', '', 0),
(4, 1, '2017-09-07 06:04:37', '0000-00-00 00:00:00', '', 'Home', '', 'draft', 'closed', 'closed', '', '', '', '', '2017-09-07 06:04:37', '2017-09-07 06:04:37', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=4', 0, 'page', '', 0),
(5, 1, '2017-09-08 12:22:19', '2017-09-08 12:22:19', '', 'Membership', '', 'publish', 'closed', 'closed', '', 'membership', '', '', '2017-09-08 12:22:19', '2017-09-08 12:22:19', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=5', 0, 'page', '', 0),
(6, 1, '2017-09-08 12:22:19', '2017-09-08 12:22:19', '', 'Membership', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2017-09-08 12:22:19', '2017-09-08 12:22:19', '', 5, 'http://nxtchptr.herokuapp.com/5-revision-v1/', 0, 'revision', '', 0),
(7, 1, '2017-09-08 23:59:20', '2017-09-08 23:59:20', '', 'Self Discovery', '', 'publish', 'closed', 'closed', '', 'self-discovery', '', '', '2017-09-08 23:59:20', '2017-09-08 23:59:20', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=7', 0, 'page', '', 0),
(8, 1, '2017-09-08 23:59:20', '2017-09-08 23:59:20', '', 'Self Discovery', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2017-09-08 23:59:20', '2017-09-08 23:59:20', '', 7, 'http://nxtchptr.herokuapp.com/7-revision-v1/', 0, 'revision', '', 0),
(9, 1, '2017-09-09 00:36:32', '2017-09-09 00:36:32', '', 'Peer Discovery', '', 'publish', 'closed', 'closed', '', 'peer-discovery', '', '', '2017-09-09 00:36:32', '2017-09-09 00:36:32', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=9', 0, 'page', '', 0),
(10, 1, '2017-09-09 00:36:32', '2017-09-09 00:36:32', '', 'Peer Discovery', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2017-09-09 00:36:32', '2017-09-09 00:36:32', '', 9, 'http://nxtchptr.herokuapp.com/9-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2017-09-09 01:02:06', '2017-09-09 01:02:06', '', 'Career Discovery', '', 'publish', 'closed', 'closed', '', 'career-discovery', '', '', '2017-09-09 01:02:06', '2017-09-09 01:02:06', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=11', 0, 'page', '', 0),
(12, 1, '2017-09-09 01:02:06', '2017-09-09 01:02:06', '', 'Career Discovery', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2017-09-09 01:02:06', '2017-09-09 01:02:06', '', 11, 'http://nxtchptr.herokuapp.com/11-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2017-09-09 01:34:25', '2017-09-09 01:34:25', '', 'About Us', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2017-09-09 01:34:25', '2017-09-09 01:34:25', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=15', 0, 'page', '', 0),
(16, 1, '2017-09-09 01:34:25', '2017-09-09 01:34:25', '', 'About Us', '', 'inherit', 'closed', 'closed', '', '15-revision-v1', '', '', '2017-09-09 01:34:25', '2017-09-09 01:34:25', '', 15, 'http://nxtchptr.herokuapp.com/15-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2017-09-09 02:21:33', '2017-09-09 02:21:33', '', 'Contact Us', '', 'publish', 'closed', 'closed', '', 'contact-us', '', '', '2017-09-09 02:21:33', '2017-09-09 02:21:33', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=17', 0, 'page', '', 0),
(18, 1, '2017-09-09 02:21:33', '2017-09-09 02:21:33', '', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2017-09-09 02:21:33', '2017-09-09 02:21:33', '', 17, 'http://nxtchptr.herokuapp.com/17-revision-v1/', 0, 'revision', '', 0),
(19, 1, '2017-09-09 03:08:44', '0000-00-00 00:00:00', '', 'Home', '', 'draft', 'closed', 'closed', '', '', '', '', '2017-09-09 03:08:44', '0000-00-00 00:00:00', '', 0, 'http://nxtchptr.herokuapp.com/?p=19', 1, 'nav_menu_item', '', 0),
(20, 1, '2017-09-09 03:09:49', '2017-09-09 03:09:49', ' ', '', '', 'publish', 'closed', 'closed', '', '20', '', '', '2017-09-09 03:10:50', '2017-09-09 03:10:50', '', 0, 'http://nxtchptr.herokuapp.com/?p=20', 5, 'nav_menu_item', '', 0),
(21, 1, '2017-09-09 03:09:48', '2017-09-09 03:09:48', ' ', '', '', 'publish', 'closed', 'closed', '', '21', '', '', '2017-09-09 03:10:50', '2017-09-09 03:10:50', '', 0, 'http://nxtchptr.herokuapp.com/?p=21', 4, 'nav_menu_item', '', 0),
(22, 1, '2017-09-09 03:09:50', '2017-09-09 03:09:50', ' ', '', '', 'publish', 'closed', 'closed', '', '22', '', '', '2017-09-09 03:10:50', '2017-09-09 03:10:50', '', 0, 'http://nxtchptr.herokuapp.com/?p=22', 6, 'nav_menu_item', '', 0),
(23, 1, '2017-09-09 03:09:47', '2017-09-09 03:09:47', ' ', '', '', 'publish', 'closed', 'closed', '', '23', '', '', '2017-09-09 03:10:49', '2017-09-09 03:10:49', '', 0, 'http://nxtchptr.herokuapp.com/?p=23', 1, 'nav_menu_item', '', 0),
(24, 1, '2017-09-09 03:09:48', '2017-09-09 03:09:48', ' ', '', '', 'publish', 'closed', 'closed', '', '24', '', '', '2017-09-09 03:10:50', '2017-09-09 03:10:50', '', 0, 'http://nxtchptr.herokuapp.com/?p=24', 3, 'nav_menu_item', '', 0),
(25, 1, '2017-09-09 03:08:49', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2017-09-09 03:08:49', '0000-00-00 00:00:00', '', 0, 'http://nxtchptr.herokuapp.com/?p=25', 1, 'nav_menu_item', '', 0),
(26, 1, '2017-09-09 03:09:48', '2017-09-09 03:09:48', ' ', '', '', 'publish', 'closed', 'closed', '', '26', '', '', '2017-09-09 03:10:50', '2017-09-09 03:10:50', '', 0, 'http://nxtchptr.herokuapp.com/?p=26', 2, 'nav_menu_item', '', 0),
(27, 1, '2017-09-09 03:27:01', '2017-09-09 03:27:01', ' ', '', '', 'publish', 'closed', 'closed', '', '27', '', '', '2017-09-09 03:27:01', '2017-09-09 03:27:01', '', 0, 'http://nxtchptr.herokuapp.com/?p=27', 5, 'nav_menu_item', '', 0),
(28, 1, '2017-09-09 03:27:00', '2017-09-09 03:27:00', ' ', '', '', 'publish', 'closed', 'closed', '', '28', '', '', '2017-09-09 03:27:00', '2017-09-09 03:27:00', '', 0, 'http://nxtchptr.herokuapp.com/?p=28', 4, 'nav_menu_item', '', 0),
(29, 1, '2017-09-09 03:27:00', '2017-09-09 03:27:00', ' ', '', '', 'publish', 'closed', 'closed', '', '29', '', '', '2017-09-09 03:27:00', '2017-09-09 03:27:00', '', 0, 'http://nxtchptr.herokuapp.com/?p=29', 3, 'nav_menu_item', '', 0),
(30, 1, '2017-09-09 03:26:59', '2017-09-09 03:26:59', ' ', '', '', 'publish', 'closed', 'closed', '', '30', '', '', '2017-09-09 03:26:59', '2017-09-09 03:26:59', '', 0, 'http://nxtchptr.herokuapp.com/?p=30', 2, 'nav_menu_item', '', 0),
(31, 1, '2017-09-09 03:26:58', '2017-09-09 03:26:58', ' ', '', '', 'publish', 'closed', 'closed', '', '31', '', '', '2017-09-09 03:26:58', '2017-09-09 03:26:58', '', 0, 'http://nxtchptr.herokuapp.com/?p=31', 1, 'nav_menu_item', '', 0),
(32, 1, '2017-09-09 03:41:22', '2017-09-09 03:41:22', '[pmpro_account]', 'My Account', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2017-09-10 01:08:48', '2017-09-10 01:08:48', '', 54, 'http://nxtchptr.herokuapp.com/membership-account/', 0, 'page', '', 0),
(33, 1, '2017-09-09 03:41:22', '2017-09-09 03:41:22', '[pmpro_billing]', 'Membership Billing', '', 'publish', 'closed', 'closed', '', 'membership-billing', '', '', '2017-09-09 03:41:22', '2017-09-09 03:41:22', '', 32, 'http://nxtchptr.herokuapp.com/membership-account/membership-billing/', 0, 'page', '', 0),
(34, 1, '2017-09-09 03:41:22', '2017-09-09 03:41:22', '[pmpro_cancel]', 'Membership Cancel', '', 'publish', 'closed', 'closed', '', 'membership-cancel', '', '', '2017-09-09 03:41:22', '2017-09-09 03:41:22', '', 32, 'http://nxtchptr.herokuapp.com/membership-account/membership-cancel/', 0, 'page', '', 0),
(35, 1, '2017-09-09 03:41:23', '2017-09-09 03:41:23', '[pmpro_checkout]', 'Membership Checkout', '', 'publish', 'closed', 'closed', '', 'membership-checkout', '', '', '2017-09-09 03:41:23', '2017-09-09 03:41:23', '', 32, 'http://nxtchptr.herokuapp.com/membership-account/membership-checkout/', 0, 'page', '', 0),
(36, 1, '2017-09-09 03:41:23', '2017-09-09 03:41:23', '[pmpro_confirmation]', 'Membership Confirmation', '', 'publish', 'closed', 'closed', '', 'membership-confirmation', '', '', '2017-09-09 03:41:23', '2017-09-09 03:41:23', '', 32, 'http://nxtchptr.herokuapp.com/membership-account/membership-confirmation/', 0, 'page', '', 0),
(37, 1, '2017-09-09 03:41:23', '2017-09-09 03:41:23', '[pmpro_invoice]', 'Membership Invoice', '', 'publish', 'closed', 'closed', '', 'membership-invoice', '', '', '2017-09-09 03:41:23', '2017-09-09 03:41:23', '', 32, 'http://nxtchptr.herokuapp.com/membership-account/membership-invoice/', 0, 'page', '', 0),
(38, 1, '2017-09-09 03:41:24', '2017-09-09 03:41:24', '[pmpro_levels]', 'Membership Levels', '', 'publish', 'closed', 'closed', '', 'membership-levels', '', '', '2017-09-09 03:41:24', '2017-09-09 03:41:24', '', 32, 'http://nxtchptr.herokuapp.com/membership-account/membership-levels/', 0, 'page', '', 0),
(39, 1, '2017-09-09 05:51:41', '2017-09-09 05:51:41', ' ', '', '', 'publish', 'closed', 'closed', '', '39', '', '', '2017-09-09 10:28:46', '2017-09-09 10:28:46', '', 0, 'http://nxtchptr.herokuapp.com/?p=39', 3, 'nav_menu_item', '', 0),
(40, 1, '2017-09-09 05:51:41', '2017-09-09 05:51:41', '', 'Personal Area', '', 'publish', 'closed', 'closed', '', '40', '', '', '2017-09-09 10:28:46', '2017-09-09 10:28:46', '', 0, 'http://nxtchptr.herokuapp.com/?p=40', 4, 'nav_menu_item', '', 0),
(41, 1, '2017-09-09 05:51:41', '2017-09-09 05:51:41', ' ', '', '', 'publish', 'closed', 'closed', '', '41', '', '', '2017-09-09 10:28:46', '2017-09-09 10:28:46', '', 0, 'http://nxtchptr.herokuapp.com/?p=41', 2, 'nav_menu_item', '', 0),
(42, 1, '2017-09-09 05:51:40', '2017-09-09 05:51:40', ' ', '', '', 'publish', 'closed', 'closed', '', '42', '', '', '2017-09-09 10:28:46', '2017-09-09 10:28:46', '', 0, 'http://nxtchptr.herokuapp.com/?p=42', 1, 'nav_menu_item', '', 0),
(43, 1, '2017-09-09 05:52:36', '2017-09-09 05:52:36', ' ', '', '', 'publish', 'closed', 'closed', '', '43', '', '', '2017-09-09 05:52:36', '2017-09-09 05:52:36', '', 0, 'http://nxtchptr.herokuapp.com/?p=43', 1, 'nav_menu_item', '', 0),
(44, 1, '2017-09-09 05:52:37', '2017-09-09 05:52:37', ' ', '', '', 'publish', 'closed', 'closed', '', '44', '', '', '2017-09-09 05:52:37', '2017-09-09 05:52:37', '', 0, 'http://nxtchptr.herokuapp.com/?p=44', 2, 'nav_menu_item', '', 0),
(45, 1, '2017-09-09 06:39:57', '2017-09-09 06:39:57', '[pmpro_account]', 'Personal Area', '', 'inherit', 'closed', 'closed', '', '32-revision-v1', '', '', '2017-09-09 06:39:57', '2017-09-09 06:39:57', '', 32, 'http://nxtchptr.herokuapp.com/32-revision-v1/', 0, 'revision', '', 0),
(46, 1, '2017-09-09 10:25:57', '2017-09-09 10:25:57', '', 'Peer Matching', '', 'publish', 'closed', 'closed', '', 'peer-matching', '', '', '2017-09-10 01:09:12', '2017-09-10 01:09:12', '', 54, 'http://nxtchptr.herokuapp.com/?page_id=46', 0, 'page', '', 0),
(47, 1, '2017-09-09 10:25:57', '2017-09-09 10:25:57', '', 'Peer Matching', '', 'inherit', 'closed', 'closed', '', '46-revision-v1', '', '', '2017-09-09 10:25:57', '2017-09-09 10:25:57', '', 46, 'http://nxtchptr.herokuapp.com/46-revision-v1/', 0, 'revision', '', 0),
(48, 1, '2017-09-09 10:26:15', '2017-09-09 10:26:15', '', 'Test Results', '', 'publish', 'closed', 'closed', '', 'test-results', '', '', '2017-09-10 01:09:18', '2017-09-10 01:09:18', '', 54, 'http://nxtchptr.herokuapp.com/?page_id=48', 0, 'page', '', 0),
(49, 1, '2017-09-09 10:26:15', '2017-09-09 10:26:15', '', 'Test Results', '', 'inherit', 'closed', 'closed', '', '48-revision-v1', '', '', '2017-09-09 10:26:15', '2017-09-09 10:26:15', '', 48, 'http://nxtchptr.herokuapp.com/48-revision-v1/', 0, 'revision', '', 0),
(50, 1, '2017-09-09 10:27:42', '2017-09-09 10:27:42', ' ', '', '', 'publish', 'closed', 'closed', '', '50', '', '', '2017-09-09 10:27:42', '2017-09-09 10:27:42', '', 0, 'http://nxtchptr.herokuapp.com/?p=50', 1, 'nav_menu_item', '', 0),
(51, 1, '2017-09-09 10:27:43', '2017-09-09 10:27:43', ' ', '', '', 'publish', 'closed', 'closed', '', '51', '', '', '2017-09-09 10:27:43', '2017-09-09 10:27:43', '', 0, 'http://nxtchptr.herokuapp.com/?p=51', 2, 'nav_menu_item', '', 0),
(52, 1, '2017-09-09 10:27:43', '2017-09-09 10:27:43', ' ', '', '', 'publish', 'closed', 'closed', '', '52', '', '', '2017-09-09 10:27:43', '2017-09-09 10:27:43', '', 0, 'http://nxtchptr.herokuapp.com/?p=52', 3, 'nav_menu_item', '', 0),
(53, 1, '2017-09-09 10:28:11', '2017-09-09 10:28:11', '[pmpro_account]', 'My Account', '', 'inherit', 'closed', 'closed', '', '32-revision-v1', '', '', '2017-09-09 10:28:11', '2017-09-09 10:28:11', '', 32, 'http://nxtchptr.herokuapp.com/32-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2017-09-10 01:08:21', '2017-09-10 01:08:21', '', 'Personal Area', '', 'publish', 'closed', 'closed', '', 'personal-area', '', '', '2017-09-10 01:08:21', '2017-09-10 01:08:21', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=54', 0, 'page', '', 0),
(55, 1, '2017-09-10 01:08:21', '2017-09-10 01:08:21', '', 'Personal Area', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2017-09-10 01:08:21', '2017-09-10 01:08:21', '', 54, 'http://nxtchptr.herokuapp.com/54-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2017-09-10 02:19:27', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-10 02:19:27', '0000-00-00 00:00:00', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=56', 0, 'page', '', 0) ;

#
# End of data contents of table `wp_posts`
# --------------------------------------------------------



#
# Delete any existing table `wp_term_relationships`
#

DROP TABLE IF EXISTS `wp_term_relationships`;


#
# Table structure of table `wp_term_relationships`
#

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_term_relationships`
#
INSERT INTO `wp_term_relationships` ( `object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(20, 2, 0),
(21, 2, 0),
(22, 2, 0),
(23, 2, 0),
(24, 2, 0),
(26, 2, 0),
(27, 3, 0),
(28, 3, 0),
(29, 3, 0),
(30, 3, 0),
(31, 3, 0),
(39, 4, 0),
(40, 4, 0),
(41, 4, 0),
(42, 4, 0),
(43, 5, 0),
(44, 5, 0),
(50, 6, 0),
(51, 6, 0),
(52, 6, 0) ;

#
# End of data contents of table `wp_term_relationships`
# --------------------------------------------------------



#
# Delete any existing table `wp_term_taxonomy`
#

DROP TABLE IF EXISTS `wp_term_taxonomy`;


#
# Table structure of table `wp_term_taxonomy`
#

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_term_taxonomy`
#
INSERT INTO `wp_term_taxonomy` ( `term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 6),
(3, 3, 'nav_menu', '', 0, 5),
(4, 4, 'nav_menu', '', 0, 4),
(5, 5, 'nav_menu', '', 0, 2),
(6, 6, 'nav_menu', '', 0, 3) ;

#
# End of data contents of table `wp_term_taxonomy`
# --------------------------------------------------------



#
# Delete any existing table `wp_termmeta`
#

DROP TABLE IF EXISTS `wp_termmeta`;


#
# Table structure of table `wp_termmeta`
#

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_termmeta`
#

#
# End of data contents of table `wp_termmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_terms`
#

DROP TABLE IF EXISTS `wp_terms`;


#
# Table structure of table `wp_terms`
#

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_terms`
#
INSERT INTO `wp_terms` ( `term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Main Navigation', 'main-navigation', 0),
(3, 'Footer Navigation', 'footer-navigation', 0),
(4, 'Main Navigation - Member', 'main-navigation-member', 0),
(5, 'Footer - Members', 'footer-members', 0),
(6, 'Personal Area', 'personal-area', 0) ;

#
# End of data contents of table `wp_terms`
# --------------------------------------------------------



#
# Delete any existing table `wp_usermeta`
#

DROP TABLE IF EXISTS `wp_usermeta`;


#
# Table structure of table `wp_usermeta`
#

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_usermeta`
#
INSERT INTO `wp_usermeta` ( `umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'locale', ''),
(11, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(12, 1, 'wp_user_level', '10'),
(13, 1, 'dismissed_wp_pointers', ''),
(14, 1, 'show_welcome_panel', '1'),
(15, 1, 'session_tokens', 'a:1:{s:64:"2170c57eb781dd7864e5d108dda67ed199b88d39ea33d149481f4a1737778b1b";a:4:{s:10:"expiration";i:1506130225;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36";s:5:"login";i:1504920625;}}'),
(16, 1, 'wp_dashboard_quick_press_last_post_id', '3'),
(17, 1, 'community-events-location', 'a:1:{s:2:"ip";s:9:"127.0.0.0";}'),
(18, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";i:4;s:15:"title-attribute";}'),
(19, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:"add-post_tag";}'),
(20, 1, 'nav_menu_recently_edited', '4'),
(21, 1, 'pmpro_visits', 'a:5:{s:4:"last";s:17:"September 9, 2017";s:8:"thisdate";N;s:5:"month";i:1;s:9:"thismonth";s:1:"9";s:7:"alltime";i:1;}'),
(22, 1, 'pmpro_views', 'a:4:{s:4:"last";s:18:"September 10, 2017";s:5:"month";i:173;s:7:"alltime";i:173;s:9:"thismonth";s:1:"9";}'),
(23, 1, 'pmpro_stripe_customerid', 'cus_BMkrkKBbhp0Oz2'),
(24, 1, 'pmpro_stripe_updates', 'a:0:{}'),
(25, 1, 'pmpro_bfirstname', ''),
(26, 1, 'pmpro_blastname', ''),
(27, 1, 'pmpro_baddress1', ''),
(28, 1, 'pmpro_baddress2', ''),
(29, 1, 'pmpro_bcity', ''),
(30, 1, 'pmpro_bstate', ''),
(31, 1, 'pmpro_bzipcode', ''),
(32, 1, 'pmpro_bcountry', ''),
(33, 1, 'pmpro_bphone', ''),
(34, 1, 'pmpro_bemail', 'laksmipati_dasa@hotmail.com'),
(35, 1, 'pmpro_CardType', 'Visa'),
(36, 1, 'pmpro_AccountNumber', 'XXXX-XXXX-XXXX-4242'),
(37, 1, 'pmpro_ExpirationMonth', '04'),
(38, 1, 'pmpro_ExpirationYear', '2022') ;

#
# End of data contents of table `wp_usermeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_users`
#

DROP TABLE IF EXISTS `wp_users`;


#
# Table structure of table `wp_users`
#

CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_users`
#
INSERT INTO `wp_users` ( `ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$Bf6NwQKf/mctdJTEayk.pHgRglbiRL1', 'admin', 'laksmipati_dasa@hotmail.com', '', '2017-09-07 01:15:43', '', 0, 'admin') ;

#
# End of data contents of table `wp_users`
# --------------------------------------------------------

#
# Add constraints back in and apply any alter data queries.
#

