# WordPress MySQL database migration
#
# Generated: Wednesday 11. October 2017 14:15 UTC
# Hostname: localhost
# Database: `nxtchptr`
# URL: //nxtchptr.herokuapp.com
# Path: C:\\xampp\\htdocs\\nxtchptr
# Tables: wp_bp_activity, wp_bp_activity_meta, wp_bp_friends, wp_bp_groups, wp_bp_groups_groupmeta, wp_bp_groups_members, wp_bp_messages_messages, wp_bp_messages_meta, wp_bp_messages_notices, wp_bp_messages_recipients, wp_bp_notifications, wp_bp_notifications_meta, wp_bp_xprofile_data, wp_bp_xprofile_fields, wp_bp_xprofile_groups, wp_bp_xprofile_meta, wp_commentmeta, wp_comments, wp_links, wp_nxtchptr_peer_match_descriptors, wp_nxtchptr_peer_match_form_descriptors, wp_nxtchptr_peer_match_form_friends, wp_nxtchptr_peer_match_forms, wp_nxtchptr_peer_matches, wp_options, wp_pmpro_discount_codes, wp_pmpro_discount_codes_levels, wp_pmpro_discount_codes_uses, wp_pmpro_membership_levelmeta, wp_pmpro_membership_levels, wp_pmpro_membership_orders, wp_pmpro_memberships_categories, wp_pmpro_memberships_pages, wp_pmpro_memberships_users, wp_postmeta, wp_posts, wp_signups, wp_term_relationships, wp_term_taxonomy, wp_termmeta, wp_terms, wp_usermeta, wp_users, wp_wpjb_alert, wp_wpjb_application, wp_wpjb_company, wp_wpjb_discount, wp_wpjb_import, wp_wpjb_job, wp_wpjb_job_search, wp_wpjb_mail, wp_wpjb_membership, wp_wpjb_meta, wp_wpjb_meta_value, wp_wpjb_payment, wp_wpjb_pricing, wp_wpjb_resume, wp_wpjb_resume_detail, wp_wpjb_resume_search, wp_wpjb_shortlist, wp_wpjb_tag, wp_wpjb_tagged, wp_wsluserscontacts, wp_wslusersprofiles
# Table Prefix: wp_
# Post Types: revision, acf, bp-email, company, job, nav_menu_item, page, post
# Protocol: http
# --------------------------------------------------------

/*!40101 SET NAMES utf8 */;

SET sql_mode='NO_AUTO_VALUE_ON_ZERO';



#
# Delete any existing table `wp_bp_activity`
#

DROP TABLE IF EXISTS `wp_bp_activity`;


#
# Table structure of table `wp_bp_activity`
#

CREATE TABLE `wp_bp_activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `component` varchar(75) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `type` varchar(75) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `action` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `primary_link` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `secondary_item_id` bigint(20) DEFAULT NULL,
  `date_recorded` datetime NOT NULL,
  `hide_sitewide` tinyint(1) DEFAULT '0',
  `mptt_left` int(11) NOT NULL DEFAULT '0',
  `mptt_right` int(11) NOT NULL DEFAULT '0',
  `is_spam` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `date_recorded` (`date_recorded`),
  KEY `user_id` (`user_id`),
  KEY `item_id` (`item_id`),
  KEY `secondary_item_id` (`secondary_item_id`),
  KEY `component` (`component`),
  KEY `type` (`type`),
  KEY `mptt_left` (`mptt_left`),
  KEY `mptt_right` (`mptt_right`),
  KEY `hide_sitewide` (`hide_sitewide`),
  KEY `is_spam` (`is_spam`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_bp_activity`
#
INSERT INTO `wp_bp_activity` ( `id`, `user_id`, `component`, `type`, `action`, `content`, `primary_link`, `item_id`, `secondary_item_id`, `date_recorded`, `hide_sitewide`, `mptt_left`, `mptt_right`, `is_spam`) VALUES
(1, 1, 'members', 'last_activity', '', '', '', 0, NULL, '2017-10-11 14:14:28', 0, 0, 0, 0),
(3, 8, 'members', 'last_activity', '', '', '', 0, NULL, '2017-10-02 12:41:44', 0, 0, 0, 0),
(4, 5, 'members', 'last_activity', '', '', '', 0, NULL, '2017-09-27 11:08:46', 0, 0, 0, 0) ;

#
# End of data contents of table `wp_bp_activity`
# --------------------------------------------------------



#
# Delete any existing table `wp_bp_activity_meta`
#

DROP TABLE IF EXISTS `wp_bp_activity_meta`;


#
# Table structure of table `wp_bp_activity_meta`
#

CREATE TABLE `wp_bp_activity_meta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activity_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`id`),
  KEY `activity_id` (`activity_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_bp_activity_meta`
#

#
# End of data contents of table `wp_bp_activity_meta`
# --------------------------------------------------------



#
# Delete any existing table `wp_bp_friends`
#

DROP TABLE IF EXISTS `wp_bp_friends`;


#
# Table structure of table `wp_bp_friends`
#

CREATE TABLE `wp_bp_friends` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `initiator_user_id` bigint(20) NOT NULL,
  `friend_user_id` bigint(20) NOT NULL,
  `is_confirmed` tinyint(1) DEFAULT '0',
  `is_limited` tinyint(1) DEFAULT '0',
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `initiator_user_id` (`initiator_user_id`),
  KEY `friend_user_id` (`friend_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_bp_friends`
#

#
# End of data contents of table `wp_bp_friends`
# --------------------------------------------------------



#
# Delete any existing table `wp_bp_groups`
#

DROP TABLE IF EXISTS `wp_bp_groups`;


#
# Table structure of table `wp_bp_groups`
#

CREATE TABLE `wp_bp_groups` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `creator_id` bigint(20) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'public',
  `parent_id` bigint(20) NOT NULL DEFAULT '0',
  `enable_forum` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `creator_id` (`creator_id`),
  KEY `status` (`status`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_bp_groups`
#
INSERT INTO `wp_bp_groups` ( `id`, `creator_id`, `name`, `slug`, `description`, `status`, `parent_id`, `enable_forum`, `date_created`) VALUES
(3, 1, 'admin’s Peer Group', 'admins-peer-group', 'admin\\\'s Peer Group', 'public', 0, 0, '2017-09-19 01:13:25') ;

#
# End of data contents of table `wp_bp_groups`
# --------------------------------------------------------



#
# Delete any existing table `wp_bp_groups_groupmeta`
#

DROP TABLE IF EXISTS `wp_bp_groups_groupmeta`;


#
# Table structure of table `wp_bp_groups_groupmeta`
#

CREATE TABLE `wp_bp_groups_groupmeta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_bp_groups_groupmeta`
#
INSERT INTO `wp_bp_groups_groupmeta` ( `id`, `group_id`, `meta_key`, `meta_value`) VALUES
(7, 3, 'total_member_count', '2'),
(8, 3, 'last_activity', '2017-10-02 12:40:31'),
(9, 3, 'invite_status', 'members') ;

#
# End of data contents of table `wp_bp_groups_groupmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_bp_groups_members`
#

DROP TABLE IF EXISTS `wp_bp_groups_members`;


#
# Table structure of table `wp_bp_groups_members`
#

CREATE TABLE `wp_bp_groups_members` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `inviter_id` bigint(20) NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `is_mod` tinyint(1) NOT NULL DEFAULT '0',
  `user_title` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_modified` datetime NOT NULL,
  `comments` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `invite_sent` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `is_admin` (`is_admin`),
  KEY `is_mod` (`is_mod`),
  KEY `user_id` (`user_id`),
  KEY `inviter_id` (`inviter_id`),
  KEY `is_confirmed` (`is_confirmed`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_bp_groups_members`
#
INSERT INTO `wp_bp_groups_members` ( `id`, `group_id`, `user_id`, `inviter_id`, `is_admin`, `is_mod`, `user_title`, `date_modified`, `comments`, `is_confirmed`, `is_banned`, `invite_sent`) VALUES
(4, 3, 1, 0, 1, 0, 'Group Admin', '2017-09-19 01:13:25', '', 1, 0, 0),
(9, 3, 6, 1, 0, 0, '', '2017-09-25 13:31:50', '', 0, 0, 1),
(11, 3, 7, 1, 0, 0, '', '2017-10-02 12:40:03', '', 0, 0, 1),
(12, 3, 8, 0, 0, 0, '', '2017-10-02 12:40:31', '', 1, 0, 1) ;

#
# End of data contents of table `wp_bp_groups_members`
# --------------------------------------------------------



#
# Delete any existing table `wp_bp_messages_messages`
#

DROP TABLE IF EXISTS `wp_bp_messages_messages`;


#
# Table structure of table `wp_bp_messages_messages`
#

CREATE TABLE `wp_bp_messages_messages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `thread_id` bigint(20) NOT NULL,
  `sender_id` bigint(20) NOT NULL,
  `subject` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_sent` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sender_id` (`sender_id`),
  KEY `thread_id` (`thread_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_bp_messages_messages`
#
INSERT INTO `wp_bp_messages_messages` ( `id`, `thread_id`, `sender_id`, `subject`, `message`, `date_sent`) VALUES
(1, 1, 1, 'Hey, wanna join the group?', 'Hey, I\\\'m cool, who are you?', '2017-09-25 07:18:55'),
(2, 1, 4, 'Re: Hey, wanna join the group?', 'Hey, can I join your group?', '2017-09-25 07:22:08'),
(3, 1, 4, 'Re: Hey, wanna join the group?', 'Hey, can I join your group?', '2017-09-25 07:22:10') ;

#
# End of data contents of table `wp_bp_messages_messages`
# --------------------------------------------------------



#
# Delete any existing table `wp_bp_messages_meta`
#

DROP TABLE IF EXISTS `wp_bp_messages_meta`;


#
# Table structure of table `wp_bp_messages_meta`
#

CREATE TABLE `wp_bp_messages_meta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `message_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`id`),
  KEY `message_id` (`message_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_bp_messages_meta`
#

#
# End of data contents of table `wp_bp_messages_meta`
# --------------------------------------------------------



#
# Delete any existing table `wp_bp_messages_notices`
#

DROP TABLE IF EXISTS `wp_bp_messages_notices`;


#
# Table structure of table `wp_bp_messages_notices`
#

CREATE TABLE `wp_bp_messages_notices` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `subject` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_sent` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `is_active` (`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_bp_messages_notices`
#

#
# End of data contents of table `wp_bp_messages_notices`
# --------------------------------------------------------



#
# Delete any existing table `wp_bp_messages_recipients`
#

DROP TABLE IF EXISTS `wp_bp_messages_recipients`;


#
# Table structure of table `wp_bp_messages_recipients`
#

CREATE TABLE `wp_bp_messages_recipients` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `thread_id` bigint(20) NOT NULL,
  `unread_count` int(10) NOT NULL DEFAULT '0',
  `sender_only` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `thread_id` (`thread_id`),
  KEY `is_deleted` (`is_deleted`),
  KEY `sender_only` (`sender_only`),
  KEY `unread_count` (`unread_count`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_bp_messages_recipients`
#
INSERT INTO `wp_bp_messages_recipients` ( `id`, `user_id`, `thread_id`, `unread_count`, `sender_only`, `is_deleted`) VALUES
(1, 4, 1, 0, 0, 0),
(2, 1, 1, 0, 0, 0) ;

#
# End of data contents of table `wp_bp_messages_recipients`
# --------------------------------------------------------



#
# Delete any existing table `wp_bp_notifications`
#

DROP TABLE IF EXISTS `wp_bp_notifications`;


#
# Table structure of table `wp_bp_notifications`
#

CREATE TABLE `wp_bp_notifications` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `secondary_item_id` bigint(20) DEFAULT NULL,
  `component_name` varchar(75) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `component_action` varchar(75) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_notified` datetime NOT NULL,
  `is_new` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  KEY `secondary_item_id` (`secondary_item_id`),
  KEY `user_id` (`user_id`),
  KEY `is_new` (`is_new`),
  KEY `component_name` (`component_name`),
  KEY `component_action` (`component_action`),
  KEY `useritem` (`user_id`,`is_new`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_bp_notifications`
#

#
# End of data contents of table `wp_bp_notifications`
# --------------------------------------------------------



#
# Delete any existing table `wp_bp_notifications_meta`
#

DROP TABLE IF EXISTS `wp_bp_notifications_meta`;


#
# Table structure of table `wp_bp_notifications_meta`
#

CREATE TABLE `wp_bp_notifications_meta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `notification_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`id`),
  KEY `notification_id` (`notification_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_bp_notifications_meta`
#

#
# End of data contents of table `wp_bp_notifications_meta`
# --------------------------------------------------------



#
# Delete any existing table `wp_bp_xprofile_data`
#

DROP TABLE IF EXISTS `wp_bp_xprofile_data`;


#
# Table structure of table `wp_bp_xprofile_data`
#

CREATE TABLE `wp_bp_xprofile_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `field_id` (`field_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_bp_xprofile_data`
#
INSERT INTO `wp_bp_xprofile_data` ( `id`, `field_id`, `user_id`, `value`, `last_updated`) VALUES
(1, 1, 1, 'admin', '2017-09-25 03:52:13'),
(2, 1, 6, 'Luke Digby', '2017-09-13 11:36:12'),
(3, 1, 5, 'Luke-and Rohini', '2017-09-13 11:36:12'),
(5, 1, 7, 'Laksmipati Dasa', '2017-09-13 11:36:36'),
(7, 2, 1, 'laksmipati108', '2017-09-25 03:52:13'),
(8, 1, 8, 'Laksmipati Dasa', '2017-09-27 10:20:48') ;

#
# End of data contents of table `wp_bp_xprofile_data`
# --------------------------------------------------------



#
# Delete any existing table `wp_bp_xprofile_fields`
#

DROP TABLE IF EXISTS `wp_bp_xprofile_fields`;


#
# Table structure of table `wp_bp_xprofile_fields`
#

CREATE TABLE `wp_bp_xprofile_fields` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) unsigned NOT NULL,
  `parent_id` bigint(20) unsigned NOT NULL,
  `type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_required` tinyint(1) NOT NULL DEFAULT '0',
  `is_default_option` tinyint(1) NOT NULL DEFAULT '0',
  `field_order` bigint(20) NOT NULL DEFAULT '0',
  `option_order` bigint(20) NOT NULL DEFAULT '0',
  `order_by` varchar(15) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `can_delete` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `parent_id` (`parent_id`),
  KEY `field_order` (`field_order`),
  KEY `can_delete` (`can_delete`),
  KEY `is_required` (`is_required`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_bp_xprofile_fields`
#
INSERT INTO `wp_bp_xprofile_fields` ( `id`, `group_id`, `parent_id`, `type`, `name`, `description`, `is_required`, `is_default_option`, `field_order`, `option_order`, `order_by`, `can_delete`) VALUES
(1, 1, 0, 'textbox', 'Name', '', 1, 0, 0, 0, '', 0),
(2, 1, 0, 'textbox', 'Skype name', 'Your Skype username', 0, 0, 1, 0, '', 1) ;

#
# End of data contents of table `wp_bp_xprofile_fields`
# --------------------------------------------------------



#
# Delete any existing table `wp_bp_xprofile_groups`
#

DROP TABLE IF EXISTS `wp_bp_xprofile_groups`;


#
# Table structure of table `wp_bp_xprofile_groups`
#

CREATE TABLE `wp_bp_xprofile_groups` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `group_order` bigint(20) NOT NULL DEFAULT '0',
  `can_delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `can_delete` (`can_delete`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_bp_xprofile_groups`
#
INSERT INTO `wp_bp_xprofile_groups` ( `id`, `name`, `description`, `group_order`, `can_delete`) VALUES
(1, 'Base', '', 0, 0) ;

#
# End of data contents of table `wp_bp_xprofile_groups`
# --------------------------------------------------------



#
# Delete any existing table `wp_bp_xprofile_meta`
#

DROP TABLE IF EXISTS `wp_bp_xprofile_meta`;


#
# Table structure of table `wp_bp_xprofile_meta`
#

CREATE TABLE `wp_bp_xprofile_meta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `object_id` bigint(20) NOT NULL,
  `object_type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`id`),
  KEY `object_id` (`object_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_bp_xprofile_meta`
#
INSERT INTO `wp_bp_xprofile_meta` ( `id`, `object_id`, `object_type`, `meta_key`, `meta_value`) VALUES
(1, 2, 'field', 'default_visibility', 'public'),
(2, 2, 'field', 'allow_custom_visibility', 'allowed'),
(3, 2, 'field', 'do_autolink', 'off') ;

#
# End of data contents of table `wp_bp_xprofile_meta`
# --------------------------------------------------------



#
# Delete any existing table `wp_commentmeta`
#

DROP TABLE IF EXISTS `wp_commentmeta`;


#
# Table structure of table `wp_commentmeta`
#

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_commentmeta`
#

#
# End of data contents of table `wp_commentmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_comments`
#

DROP TABLE IF EXISTS `wp_comments`;


#
# Table structure of table `wp_comments`
#

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_comments`
#
INSERT INTO `wp_comments` ( `comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2017-09-07 01:15:44', '2017-09-07 01:15:44', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0) ;

#
# End of data contents of table `wp_comments`
# --------------------------------------------------------



#
# Delete any existing table `wp_links`
#

DROP TABLE IF EXISTS `wp_links`;


#
# Table structure of table `wp_links`
#

CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_links`
#

#
# End of data contents of table `wp_links`
# --------------------------------------------------------



#
# Delete any existing table `wp_nxtchptr_peer_match_descriptors`
#

DROP TABLE IF EXISTS `wp_nxtchptr_peer_match_descriptors`;


#
# Table structure of table `wp_nxtchptr_peer_match_descriptors`
#

CREATE TABLE `wp_nxtchptr_peer_match_descriptors` (
  `id` mediumint(9) NOT NULL,
  `descriptor` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_nxtchptr_peer_match_descriptors`
#

#
# End of data contents of table `wp_nxtchptr_peer_match_descriptors`
# --------------------------------------------------------



#
# Delete any existing table `wp_nxtchptr_peer_match_form_descriptors`
#

DROP TABLE IF EXISTS `wp_nxtchptr_peer_match_form_descriptors`;


#
# Table structure of table `wp_nxtchptr_peer_match_form_descriptors`
#

CREATE TABLE `wp_nxtchptr_peer_match_form_descriptors` (
  `form_id` bigint(20) NOT NULL,
  `descriptor_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_nxtchptr_peer_match_form_descriptors`
#

#
# End of data contents of table `wp_nxtchptr_peer_match_form_descriptors`
# --------------------------------------------------------



#
# Delete any existing table `wp_nxtchptr_peer_match_form_friends`
#

DROP TABLE IF EXISTS `wp_nxtchptr_peer_match_form_friends`;


#
# Table structure of table `wp_nxtchptr_peer_match_form_friends`
#

CREATE TABLE `wp_nxtchptr_peer_match_form_friends` (
  `form_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_nxtchptr_peer_match_form_friends`
#

#
# End of data contents of table `wp_nxtchptr_peer_match_form_friends`
# --------------------------------------------------------



#
# Delete any existing table `wp_nxtchptr_peer_match_forms`
#

DROP TABLE IF EXISTS `wp_nxtchptr_peer_match_forms`;


#
# Table structure of table `wp_nxtchptr_peer_match_forms`
#

CREATE TABLE `wp_nxtchptr_peer_match_forms` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gender` enum('male','female') COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `timezone` tinyint(1) NOT NULL,
  `zipcode` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `school_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `education_level` tinyint(1) NOT NULL,
  `books_mags` enum('books','magazines','neither') COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `crossword_sudoku` enum('crossword','sudoku','neither') COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `feel_look` enum('feel','look') COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `humor_important` int(1) NOT NULL,
  `most_important_ABCD` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `why_joined` varchar(1000) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `fav_websites` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `fav_tv_shows` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `fav_movie` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `child_career_dream` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `prev_career` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `gender_preference` enum('male','female') COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `peer_member_residence_preference` enum('local','anywhere') COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `language` tinyint(1) NOT NULL,
  `duration` enum('6 months','1 year','indefinite') COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `auto_match` tinyint(1) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_nxtchptr_peer_match_forms`
#
INSERT INTO `wp_nxtchptr_peer_match_forms` ( `id`, `gender`, `status`, `timezone`, `zipcode`, `school_name`, `education_level`, `books_mags`, `crossword_sudoku`, `feel_look`, `humor_important`, `most_important_ABCD`, `why_joined`, `fav_websites`, `fav_tv_shows`, `fav_movie`, `child_career_dream`, `prev_career`, `gender_preference`, `peer_member_residence_preference`, `language`, `duration`, `auto_match`, `user_id`) VALUES
(1, '', 2, 1, '741313', '0', 2, 'neither', 'neither', '', 0, '["A","B","C","D"]', 'Because it\\\'s awesome', 'google facebook', 'show1 show2', 'moviemovie', 'fireperson', 'awesomeness', '', 'local', 1, '', 0, 0),
(5, 'female', 1, 1, '94010', 'Princeton University', 4, 'books', 'crossword', '', 1, 'A, C, D, B', 'I am looking to go back to work, but I need help with ideas.', 'Soul Cycle; Pinterest', 'Big Bang Theory;  Odd Mom Out', 'Sixteen Candles', 'Firefighter', 'Marketer', 'female', 'local', 1, '6 months', 1, 0),
(6, 'female', 1, 1, '94115', 'University of Delaware', 4, 'neither', 'sudoku', '', 1, 'C, A, D, B', 'I am looking to go back to work, but I need help with ideas.', 'YouTube; Google', 'Real Housewives OC; The Wire', 'Breakfast Club', 'Jockey', 'Fashion', 'female', 'anywhere', 1, '1 year', 1, 0),
(7, 'female', 1, 1, '94118', 'UC Davis', 5, 'books', 'crossword', '', 1, 'A, B, C, D ', 'I need support in working on my drema job', 'Google; Amazon', 'The Wire; Homeland', 'Jungle Book', 'Beautician', 'Lawyer', 'female', 'local', 1, 'indefinite', 0, 0),
(8, 'female', 2, 1, '94010', 'UC Berkeley', 4, 'magazines', 'crossword', '', 0, 'D, A, B, C', 'I need support in working on my drema job', 'Amazon; Vogue', 'Homeland; Modern Family', 'Moana', 'Interior Designer', 'Teacher', 'female', 'local', 1, '6 months', 0, 0),
(9, 'female', 1, 1, '94027', 'Wharton', 5, 'magazines', 'sudoku', '', 0, 'B, A, D, C', 'I need support in working on my drema job', 'Vogue; Soul Cylce', 'Modern Family; Cosby Show', 'Frozen', 'Neurosurgeon', 'Doctor', 'female', 'local', 1, '1 year', 1, 0),
(10, 'female', 1, 1, '94010', 'Chico State', 4, 'neither', 'crossword', '', 1, 'B, D, C, A', 'I need support in working on my drema job', 'Twitter; Google', 'Cosby Show; The Wire', 'The Sure Thing', 'Policewoman', 'Executive Assistant', 'male', 'local', 1, '1 year', 1, 0),
(11, 'female', 2, 1, '94118', 'University of Chicago', 5, 'books', 'crossword', '', 1, 'A, B, C, D', 'I am looking to go back to work, but I need help with ideas.', 'Google;  The onion', 'Homeland;  Magnum PI', 'Pride & Prejudice', 'Doctor', 'Lawyer', 'male', 'local', 1, 'indefinite', 0, 0),
(12, 'female', 2, 1, '94027', 'Villanova', 5, 'magazines', 'neither', '', 0, 'A, C, D, B', 'I am looking to go back to work, but I need help with ideas.', 'Pinterest; TMZ', 'Mahnum PI; Friends', 'Weird Science', 'Metermaid', 'Investment banker', 'male', 'local', 1, '1 year', 0, 0),
(13, 'female', 1, 1, '97211', 'University of Oregon', 4, 'magazines', 'crossword', '', 0, 'A, C, D, B', 'I am looking to go back to work, but I need help with ideas.', 'TMZ; Amazon', 'Friends; Shark Tank', 'Annie Hall', 'Zookeeper', 'Accountant', 'female', 'anywhere', 1, '6 months', 0, 0),
(14, 'female', 1, 1, '97229', 'Occidental', 4, 'books', 'sudoku', '', 1, 'C, A, D, B', 'I need support in working on my drema job', 'Soul Cycle; Pinterest', 'Shark Tank; Broad City', 'Straight Outta Compton', 'Teacher', 'HR manager', 'female', 'anywhere', 1, '1 year', 1, 0),
(15, 'female', 1, 1, '97280', 'University of Vermont', 4, 'books', 'neither', '', 1, 'A, B, C, D ', 'I need support in working on my drema job', 'YouTube; Google', 'Broad City; Catastrophe', 'Moonlight', 'Doctor', 'Accountant', 'female', 'anywhere', 1, 'indefinite', 1, 0),
(16, 'female', 1, 1, '97236', 'Portland High School', 2, 'magazines', 'neither', '', 0, 'A, B, C, D', 'I need support in working on my drema job', 'Google; Amazon', 'Catatrophe; Real Housewives OC', 'Heathers', 'Librarian', 'Telemarketer', 'female', 'local', 1, '6 months', 1, 0),
(17, 'female', 1, 1, '97201', 'Teaching Graduate School', 5, 'books', 'neither', '', 1, 'A, C, D, B', 'I need support in working on my drema job', 'Amazon; Vogue', 'Real Housewives OC; Real Housewives NYC', 'Jaws', 'Lifeguard', 'CFO', 'female', 'local', 1, '1 year', 0, 0),
(18, 'female', 1, 1, '97211', 'UC Davis', 4, 'books', 'neither', '', 0, 'B, A, D, C', 'I am looking to go back to work, but I need help with ideas.', 'Vogue; Soul Cylce', 'Big Bang Theory;  Odd Mom Out', 'Blue Lagoon', 'Firefighter', 'Marketer', 'male', 'local', 1, '1 year', 1, 0),
(19, 'female', 2, 1, '97280', 'Portlandia High School', 3, 'neither', 'crossword', '', 1, 'A, B, C, D', 'I am looking to go back to work, but I need help with ideas.', 'Twitter; Google', 'Real Housewives OC; The Wire', 'Twins', 'Jockey', 'Bookkeeping', 'female', 'local', 1, '6 months', 0, 0),
(20, 'female', 1, 1, '97211', 'Reed College', 4, 'books', 'sudoku', '', 1, 'A, C, D, B', 'I am looking to go back to work, but I need help with ideas.', 'Google;  The onion', 'The Wire; Homeland', 'Big', 'Beautician', 'Marketer', 'female', 'local', 1, 'indefinite', 0, 0),
(21, 'female', 1, 1, '90012', 'Harvard Westlake', 2, 'magazines', 'sudoku', '', 1, 'C, B, A, D', 'I need support in working on my drema job', 'Pinterest; TMZ', 'Homeland; Modern Family', 'Sixteen Candles', 'Interior Designer', 'Telemarketer', 'female', 'local', 1, '1 year', 0, 0),
(22, 'female', 1, 1, '90020', 'Arizona State University', 4, 'books', 'neither', '', 0, 'B, A, D, C', 'I need support in working on my drema job', 'TMZ; Amazon', 'Modern Family; Cosby Show', 'Breakfast Club', 'Neurosurgeon', 'Accountant', 'female', 'anywhere', 1, 'indefinite', 1, 0),
(23, 'female', 2, 1, '90035', 'University of Texas', 4, 'magazines', '', '', 1, 'C, A, D, B', 'I need support in working on my drema job', 'Soul Cycle; Pinterest', 'Cosby Show; The Wire', 'Jungle Book', 'Policewoman', 'Marketer', 'female', 'local', 1, '6 months', 1, 0),
(24, 'female', 2, 1, '90041', 'Colorado College', 4, 'books', 'neither', '', 0, 'A, B, C, D', 'I need support in working on my drema job', 'YouTube; Google', 'Homeland;  Magnum PI', 'Moana', 'Doctor', 'Marketer', 'female', 'local', 1, '1 year', 0, 0),
(25, 'female', 1, 1, '90042', 'Crossroads', 2, 'magazines', 'neither', '', 1, 'C, A, D, B', 'I am looking to go back to work, but I need help with ideas.', 'Google; Amazon', 'Mahnum PI; Friends', 'Frozen', 'Metermaid', 'Telemarketer', 'female', 'anywhere', 1, 'indefinite', 0, 0),
(26, 'female', 2, 1, '90120', 'UCLA', 4, 'books', 'sudoku', '', 0, 'D, A, B, C', 'I am looking to go back to work, but I need help with ideas.', 'Amazon; Vogue', 'Friends; Shark Tank', 'The Sure Thing', 'Zookeeper', 'Researcher', 'female', 'anywhere', 1, '6 months', 1, 0),
(27, 'female', 1, 1, '90211', 'Brown University', 4, 'magazines', 'neither', '', 1, 'C, B, A, D', 'I am looking to go back to work, but I need help with ideas.', 'Vogue; Soul Cylce', 'Shark Tank; Broad City', 'Pride & Prejudice', 'Teacher', 'Researcher', 'female', 'local', 1, '1 year', 1, 0),
(28, 'female', 1, 1, '90210', 'UC Santa Cruz', 5, 'books', 'crossword', '', 0, 'D, C, B, A', 'I need support in working on my drema job', 'Twitter; Google', 'Broad City; Catastrophe', 'Weird Science', 'Doctor', 'Lawyer', 'male', 'anywhere', 1, 'indefinite', 0, 0),
(29, 'female', 1, 1, '90020', 'UCLA', 5, 'neither', 'sudoku', '', 1, 'C, B, A, D', 'I need support in working on my drema job', 'Google;  The onion', 'Catatrophe; Real Housewives OC', 'Annie Hall', 'Librarian', 'Doctor', 'female', 'local', 1, '6 months', 1, 0),
(30, 'female', 1, 1, '90120', 'Pepperdine', 4, 'books', 'crossword', '', 0, 'B, A, D, C', 'I need support in working on my drema job', 'Pinterest; TMZ', 'Real Housewives OC; Real Housewives NYC', 'Straight Outta Compton', 'Lifeguard', 'Marketer', 'female', 'local', 1, '1 year', 0, 0),
(31, 'female', 1, 2, '19004', 'Wellesley', 4, 'books', 'crossword', '', 0, 'B, A, D, C', 'I need support in working on my drema job', 'TMZ; Amazon', 'Big Bang Theory;  Odd Mom Out', 'Moonlight', 'Firefighter', 'Librarian', 'female', 'local', 1, 'indefinite', 1, 0),
(32, 'female', 1, 2, '19103', 'Colgate University', 4, 'magazines', 'neither', '', 0, 'C, A, D, B', 'I am looking to go back to work, but I need help with ideas.', 'Soul Cycle; Pinterest', 'Real Housewives OC; The Wire', 'Heathers', 'Jockey', 'Librarian', 'female', 'local', 1, '6 months', 1, 0),
(33, 'female', 1, 2, '19146', 'University of Chicago', 5, 'books', '', '', 1, 'A, B, C, D', 'I am looking to go back to work, but I need help with ideas.', 'YouTube; Google', 'The Wire; Homeland', 'Jaws', 'Beautician', 'Lawyer', 'female', 'local', 1, '1 year', 1, 0),
(34, 'female', 1, 2, '19106', 'Temple', 3, 'magazines', 'crossword', '', 1, 'C, A, D, B', 'I am looking to go back to work, but I need help with ideas.', 'Google; Amazon', 'Homeland; Modern Family', 'Blue Lagoon', 'Interior Designer', 'Trainer', 'female', 'local', 1, 'indefinite', 0, 0),
(35, 'female', 2, 2, '19333', 'Friends Select HS', 2, 'books', 'sudoku', '', 0, 'D, A, B, C', 'I need support in working on my drema job', 'Amazon; Vogue', 'Modern Family; Cosby Show', 'Twins', 'Neurosurgeon', 'Trainer', 'female', 'local', 1, '6 months', 0, 0),
(36, 'female', 1, 2, '19103', 'Boston University', 4, 'magazines', 'neither', '', 1, 'B, D, C, A', 'I need support in working on my drema job', 'Vogue; Soul Cylce', 'Cosby Show; The Wire', 'Big', 'Policewoman', 'Researcher', 'male', 'local', 1, '1 year', 1, 0),
(37, 'female', 1, 2, '19146', 'George Washington University', 5, 'books', 'sudoku', '', 1, 'B, A, D, C', 'I need support in working on my drema job', 'Twitter; Google', 'Homeland;  Magnum PI', 'Sixteen Candles', 'Doctor', 'Lawyer', 'male', 'local', 1, 'indefinite', 0, 0),
(38, 'female', 1, 2, '19333', 'Wellesley', 4, 'magazines', 'crossword', '', 1, 'C, D, A, B', 'I need support in working on my drema job', 'Google;  The onion', 'Mahnum PI; Friends', 'Breakfast Club', 'Metermaid', 'Marketer', 'male', 'anywhere', 1, 'indefinite', 0, 0),
(39, 'female', 1, 2, '53204', 'University of Wisconsin', 4, 'magazines', 'neither', '', 0, 'C, B, A, D', 'I am looking to go back to work, but I need help with ideas.', 'Pinterest; TMZ', 'Friends; Shark Tank', 'Jungle Book', 'Zookeeper', 'Operations manager', 'female', 'anywhere', 1, '1 year', 1, 0),
(40, 'female', 1, 2, '53217', 'University of Chicago', 5, 'books', 'sudoku', '', 1, 'B, A, D, C', 'I am looking to go back to work, but I need help with ideas.', 'TMZ; Amazon', 'Shark Tank; Broad City', 'Moana', 'Teacher', 'Lawyer', 'female', 'local', 1, 'indefinite', 1, 0),
(41, 'female', 1, 2, '53223', 'Rice University', 5, 'books', 'sudoku', '', 1, 'D, A, B, C', 'I am looking to go back to work, but I need help with ideas.', 'Soul Cycle; Pinterest', 'Broad City; Catastrophe', 'Frozen', 'Doctor', 'Doctor', 'female', 'local', 1, '6 months', 1, 0),
(42, 'female', 2, 2, '53210', 'Milwaukee High School', 2, 'magazines', 'crossword', '', 1, 'C, B, A, D', 'I need support in working on my drema job', 'YouTube; Google', 'Catatrophe; Real Housewives OC', 'The Sure Thing', 'Librarian', 'Telemarketer', 'female', 'local', 1, '1 year', 1, 0),
(43, 'female', 1, 2, '53216', 'Iowa State', 4, 'books', 'sudoku', '', 0, 'B, A, D, C', 'I need support in working on my drema job', 'Google; Amazon', 'Real Housewives OC; Real Housewives NYC', 'Pride & Prejudice', 'Lifeguard', 'Operations manager', 'female', 'anywhere', 1, 'indefinite', 1, 0),
(44, 'female', 1, 2, '53204', 'University of Michigan', 5, 'magazines', 'neither', '', 1, 'B, C, A, D', 'I need support in working on my drema job', 'Amazon; Vogue', 'Big Bang Theory;  Odd Mom Out', 'Weird Science', 'Firefighter', 'Lawyer', 'male', 'local', 1, '6 months', 1, 0),
(45, 'female', 1, 2, '53223', 'University of Chicago', 5, 'books', 'sudoku', '', 0, 'A, B, C, D', 'I need support in working on my drema job', 'Vogue; Soul Cylce', 'Real Housewives OC; The Wire', 'Annie Hall', 'Jockey', 'Doctor', 'female', 'local', 1, '1 year', 0, 0),
(46, 'female', 1, 2, '53201', 'Harvard University', 4, 'magazines', 'neither', '', 1, 'A, C, D, B', 'I am looking to go back to work, but I need help with ideas.', 'Twitter; Google', 'The Wire; Homeland', 'Straight Outta Compton', 'Beautician', 'Marketer', 'female', 'anywhere', 1, 'indefinite', 0, 0),
(47, 'female', 2, 2, '08540', 'Lehigh', 4, 'books', 'sudoku', '', 0, 'B, A, D, C', 'I am looking to go back to work, but I need help with ideas.', 'Google;  The onion', 'Homeland; Modern Family', 'Moonlight', 'Interior Designer', 'HR manager', 'female', 'anywhere', 1, 'indefinite', 0, 0),
(48, 'female', 1, 2, '08544', 'Burlingame High School', 2, 'neither', 'crossword', '', 1, 'C, A, D, B', 'I am looking to go back to work, but I need help with ideas.', 'Pinterest; TMZ', 'Modern Family; Cosby Show', 'Heathers', 'Neurosurgeon', 'Waitress', 'female', 'anywhere', 1, '6 months', 0, 0),
(49, 'female', 1, 2, '08541', 'Rutgers', 5, 'magazines', 'sudoku', '', 0, 'A, B, C, D', 'I need support in working on my drema job', 'TMZ; Amazon', 'Cosby Show; The Wire', 'Jaws', 'Policewoman', 'Lawyer', 'female', 'anywhere', 1, '1 year', 1, 0),
(50, 'female', 2, 2, '08543', 'University of Pennsylvania', 4, 'books', 'neither', '', 1, 'C, A, D, B', 'I need support in working on my drema job', 'Soul Cycle; Pinterest', 'Homeland;  Magnum PI', 'Blue Lagoon', 'Doctor', 'Researcher', 'female', 'anywhere', 1, 'indefinite', 1, 0),
(51, 'female', 1, 2, '08542', 'Penn State', 4, 'magazines', 'sudoku', '', 0, 'D, A, B, C', 'I need support in working on my drema job', 'YouTube; Google', 'Mahnum PI; Friends', 'Twins', 'Metermaid', 'Waitress', 'female', 'local', 1, '6 months', 1, 0),
(52, 'female', 1, 2, '08541', 'Moore College of Art', 4, 'neither', 'neither', '', 1, 'B, A, D, C', 'I need support in working on my drema job', 'Google; Amazon', 'Friends; Shark Tank', 'Big', 'Zookeeper', 'Entrepreneur', 'female', 'local', 1, '1 year', 0, 0),
(53, 'female', 2, 2, '08543', 'Rutgers', 5, 'books', 'crossword', '', 0, 'D, A, B, C', 'I am looking to go back to work, but I need help with ideas.', 'Amazon; Vogue', 'Shark Tank; Broad City', 'Sixteen Candles', 'Teacher', 'Lawyer', 'male', 'anywhere', 1, '6 months', 0, 0),
(54, 'female', 1, 2, '08542', 'Pace', 4, 'books', 'sudoku', '', 1, 'D, C, B, A', 'I am looking to go back to work, but I need help with ideas.', 'Vogue; Soul Cylce', 'Broad City; Catastrophe', 'Breakfast Club', 'Doctor', 'Researcher', 'male', 'anywhere', 1, 'indefinite', 1, 0) ;

#
# End of data contents of table `wp_nxtchptr_peer_match_forms`
# --------------------------------------------------------



#
# Delete any existing table `wp_nxtchptr_peer_matches`
#

DROP TABLE IF EXISTS `wp_nxtchptr_peer_matches`;


#
# Table structure of table `wp_nxtchptr_peer_matches`
#

CREATE TABLE `wp_nxtchptr_peer_matches` (
  `group_id` bigint(20) NOT NULL,
  `peer_id` bigint(20) NOT NULL,
  `match_percent` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`,`peer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_nxtchptr_peer_matches`
#
INSERT INTO `wp_nxtchptr_peer_matches` ( `group_id`, `peer_id`, `match_percent`) VALUES
(3, 4, 85),
(3, 5, 10),
(3, 6, 20),
(3, 7, 10),
(3, 8, 20),
(3, 9, 20),
(3, 10, 20),
(3, 11, 10),
(3, 12, 20),
(3, 13, 20),
(3, 14, 10),
(3, 15, 10),
(3, 16, 35),
(3, 17, 10),
(3, 18, 20),
(3, 19, 20),
(3, 20, 10),
(3, 21, 25),
(3, 22, 20),
(3, 23, 10),
(3, 24, 20),
(3, 25, 25),
(3, 26, 20),
(3, 27, 10),
(3, 28, 20),
(3, 29, 20),
(3, 30, 20),
(3, 31, 20),
(3, 32, 20),
(3, 33, 10),
(3, 34, 10),
(3, 35, 35),
(3, 36, 10),
(3, 37, 10),
(3, 38, 10),
(3, 39, 20),
(3, 40, 10),
(3, 41, 10),
(3, 42, 25),
(3, 43, 20),
(3, 44, 10),
(3, 45, 20),
(3, 46, 10),
(3, 47, 20),
(3, 48, 35),
(3, 49, 20),
(3, 50, 10),
(3, 51, 20),
(3, 52, 20),
(3, 53, 20),
(3, 54, 10) ;

#
# End of data contents of table `wp_nxtchptr_peer_matches`
# --------------------------------------------------------



#
# Delete any existing table `wp_options`
#

DROP TABLE IF EXISTS `wp_options`;


#
# Table structure of table `wp_options`
#

CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=917 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_options`
#
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://nxtchptr.herokuapp.com', 'yes'),
(2, 'home', 'http://nxtchptr.herokuapp.com', 'yes'),
(3, 'blogname', 'NxtChptr', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'laksmipati_dasa@hotmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:177:{s:54:"(jobs/post-a-job)/(add|preview|save|reset|complete)/?$";s:52:"index.php?pagename=$matches[1]&wpjb-step=$matches[2]";s:210:"(employer-panel)/(job-add|job-preview|job-save|job-reset|job-edit|job-delete|login|logout|edit|listings|password|delete|application|applications|membership-details|membership-purchase|membership)/([0-9]{1,})/?$";s:76:"index.php?pagename=$matches[1]&wpjb-employer=$matches[2]&wpjb-id=$matches[3]";s:198:"(employer-panel)/(job-add|job-preview|job-save|job-reset|job-edit|job-delete|login|logout|edit|listings|password|delete|application|applications|membership-details|membership-purchase|membership)/?$";s:56:"index.php?pagename=$matches[1]&wpjb-employer=$matches[2]";s:84:"(candidate-panel)/(my-resume|my-applications|my-bookmarks|logout|password|delete)/?$";s:57:"index.php?pagename=$matches[1]&wpjb-candidate=$matches[2]";s:33:"(jobs)/(category|type)/([^/]+)/?$";s:81:"index.php?pagename=$matches[1]&wpjb-tag=$matches[2]&wpjb-slug=$matches[3]&paged=1";s:51:"(jobs)/(category|type)/([^/]+)/page/?([0-9]{1,})/?$";s:91:"index.php?pagename=$matches[1]&wpjb-tag=$matches[2]&wpjb-slug=$matches[3]&paged=$matches[4]";s:16:"wpjobboard/(.*)$";s:32:"index.php?wpjobboard=$matches[1]";s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:6:"job/?$";s:23:"index.php?post_type=job";s:36:"job/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?post_type=job&feed=$matches[1]";s:31:"job/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?post_type=job&feed=$matches[1]";s:23:"job/page/([0-9]{1,})/?$";s:41:"index.php?post_type=job&paged=$matches[1]";s:9:"resume/?$";s:26:"index.php?post_type=resume";s:39:"resume/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?post_type=resume&feed=$matches[1]";s:34:"resume/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?post_type=resume&feed=$matches[1]";s:26:"resume/page/([0-9]{1,})/?$";s:44:"index.php?post_type=resume&paged=$matches[1]";s:10:"company/?$";s:27:"index.php?post_type=company";s:40:"company/feed/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=company&feed=$matches[1]";s:35:"company/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=company&feed=$matches[1]";s:27:"company/page/([0-9]{1,})/?$";s:45:"index.php?post_type=company&paged=$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:55:"bp_member_type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:53:"index.php?bp_member_type=$matches[1]&feed=$matches[2]";s:50:"bp_member_type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:53:"index.php?bp_member_type=$matches[1]&feed=$matches[2]";s:31:"bp_member_type/([^/]+)/embed/?$";s:47:"index.php?bp_member_type=$matches[1]&embed=true";s:43:"bp_member_type/([^/]+)/page/?([0-9]{1,})/?$";s:54:"index.php?bp_member_type=$matches[1]&paged=$matches[2]";s:25:"bp_member_type/([^/]+)/?$";s:36:"index.php?bp_member_type=$matches[1]";s:54:"bp_group_type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?bp_group_type=$matches[1]&feed=$matches[2]";s:49:"bp_group_type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?bp_group_type=$matches[1]&feed=$matches[2]";s:30:"bp_group_type/([^/]+)/embed/?$";s:46:"index.php?bp_group_type=$matches[1]&embed=true";s:42:"bp_group_type/([^/]+)/page/?([0-9]{1,})/?$";s:53:"index.php?bp_group_type=$matches[1]&paged=$matches[2]";s:24:"bp_group_type/([^/]+)/?$";s:35:"index.php?bp_group_type=$matches[1]";s:31:"job/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:41:"job/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:61:"job/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:56:"job/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:56:"job/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:37:"job/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:20:"job/([^/]+)/embed/?$";s:36:"index.php?job=$matches[1]&embed=true";s:24:"job/([^/]+)/trackback/?$";s:30:"index.php?job=$matches[1]&tb=1";s:44:"job/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?job=$matches[1]&feed=$matches[2]";s:39:"job/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?job=$matches[1]&feed=$matches[2]";s:32:"job/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?job=$matches[1]&paged=$matches[2]";s:39:"job/([^/]+)/comment-page-([0-9]{1,})/?$";s:43:"index.php?job=$matches[1]&cpage=$matches[2]";s:30:"job/([^/]+)/applied(/(.*))?/?$";s:45:"index.php?job=$matches[1]&applied=$matches[3]";s:28:"job/([^/]+)(?:/([0-9]+))?/?$";s:42:"index.php?job=$matches[1]&page=$matches[2]";s:20:"job/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:30:"job/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:50:"job/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:45:"job/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:45:"job/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:26:"job/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:34:"resume/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"resume/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"resume/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"resume/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"resume/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:40:"resume/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:23:"resume/([^/]+)/embed/?$";s:39:"index.php?resume=$matches[1]&embed=true";s:27:"resume/([^/]+)/trackback/?$";s:33:"index.php?resume=$matches[1]&tb=1";s:47:"resume/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?resume=$matches[1]&feed=$matches[2]";s:42:"resume/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?resume=$matches[1]&feed=$matches[2]";s:35:"resume/([^/]+)/page/?([0-9]{1,})/?$";s:46:"index.php?resume=$matches[1]&paged=$matches[2]";s:42:"resume/([^/]+)/comment-page-([0-9]{1,})/?$";s:46:"index.php?resume=$matches[1]&cpage=$matches[2]";s:33:"resume/([^/]+)/applied(/(.*))?/?$";s:48:"index.php?resume=$matches[1]&applied=$matches[3]";s:31:"resume/([^/]+)(?:/([0-9]+))?/?$";s:45:"index.php?resume=$matches[1]&page=$matches[2]";s:23:"resume/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:33:"resume/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:53:"resume/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:48:"resume/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:48:"resume/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:29:"resume/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:35:"company/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:45:"company/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:65:"company/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"company/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"company/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:41:"company/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:24:"company/([^/]+)/embed/?$";s:40:"index.php?company=$matches[1]&embed=true";s:28:"company/([^/]+)/trackback/?$";s:34:"index.php?company=$matches[1]&tb=1";s:48:"company/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?company=$matches[1]&feed=$matches[2]";s:43:"company/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?company=$matches[1]&feed=$matches[2]";s:36:"company/([^/]+)/page/?([0-9]{1,})/?$";s:47:"index.php?company=$matches[1]&paged=$matches[2]";s:43:"company/([^/]+)/comment-page-([0-9]{1,})/?$";s:47:"index.php?company=$matches[1]&cpage=$matches[2]";s:34:"company/([^/]+)/applied(/(.*))?/?$";s:49:"index.php?company=$matches[1]&applied=$matches[3]";s:32:"company/([^/]+)(?:/([0-9]+))?/?$";s:46:"index.php?company=$matches[1]&page=$matches[2]";s:24:"company/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:34:"company/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:54:"company/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"company/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"company/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:30:"company/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:12:"robots\\.txt$";s:18:"index.php?robots=1";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"([^/]+)/embed/?$";s:37:"index.php?name=$matches[1]&embed=true";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:26:"([^/]+)/applied(/(.*))?/?$";s:46:"index.php?name=$matches[1]&applied=$matches[3]";s:24:"([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:22:"[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:11:{i:0;s:30:"advanced-custom-fields/acf.php";i:1;s:24:"buddypress/bp-loader.php";i:2;s:41:"nxtchptr-buddypress-pmpro-restriction.php";i:3;s:39:"nxtchptr-membership-levels-settings.php";i:4;s:49:"nxtchptr-peer-matching/nxtchptr-peer-matching.php";i:5;s:45:"paid-memberships-pro/paid-memberships-pro.php";i:6;s:41:"pmpro-social-login/pmpro-social-login.php";i:7;s:33:"theme-my-login/theme-my-login.php";i:8;s:42:"wordpress-social-login/wp-social-login.php";i:9;s:31:"wp-migrate-db/wp-migrate-db.php";i:10;s:20:"wpjobboard/index.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'nxtchptr', 'yes'),
(41, 'stylesheet', 'nxtchptr', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:33:"theme-my-login/theme-my-login.php";a:2:{i:0;s:20:"Theme_My_Login_Admin";i:1;s:9:"uninstall";}}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'wp_user_roles', 'a:6:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:78:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:22:"pmpro_memberships_menu";b:1;s:22:"pmpro_membershiplevels";b:1;s:22:"pmpro_edit_memberships";b:1;s:18:"pmpro_pagesettings";b:1;s:21:"pmpro_paymentsettings";b:1;s:19:"pmpro_emailsettings";b:1;s:22:"pmpro_advancedsettings";b:1;s:12:"pmpro_addons";b:1;s:17:"pmpro_memberslist";b:1;s:20:"pmpro_memberslistcsv";b:1;s:13:"pmpro_reports";b:1;s:12:"pmpro_orders";b:1;s:15:"pmpro_orderscsv";b:1;s:19:"pmpro_discountcodes";b:1;s:13:"pmpro_updates";b:1;s:11:"manage_jobs";b:1;s:14:"manage_resumes";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:3:{s:4:"read";b:1;s:7:"level_0";b:1;s:14:"manage_resumes";b:1;}}s:8:"employer";a:2:{s:4:"name";s:8:"Employer";s:12:"capabilities";a:2:{s:4:"read";b:1;s:11:"manage_jobs";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:3:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes') ;
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(101, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'cron', 'a:9:{i:1504747321;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1504764241;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1504790152;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1504919583;a:1:{s:8:"do_pings";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:2:{s:8:"schedule";b:0;s:4:"args";a:0:{}}}}i:1504928126;a:1:{s:29:"pmpro_cron_expire_memberships";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1504928127;a:1:{s:30:"pmpro_cron_expiration_warnings";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1504928129;a:1:{s:40:"pmpro_cron_credit_card_expiring_warnings";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:7:"monthly";s:4:"args";a:0:{}s:8:"interval";i:2635200;}}}i:1505107763;a:3:{s:24:"wpjb_event_expiring_jobs";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}s:30:"wpjb_event_subscriptions_daily";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}s:31:"wpjb_event_subscriptions_weekly";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}}s:7:"version";i:2;}', 'yes'),
(110, 'nonce_salt', ')a*Ea~jB_g{PONA2JP1QM;=;k]b!LyAZ}ZW=_hQSD6;gAB<dDgCxe!`$w72L P&_', 'no'),
(111, 'theme_mods_twentyseventeen', 'a:2:{s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1504749676;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(124, 'can_compress_scripts', '0', 'no'),
(140, 'current_theme', 'NxtChptr', 'yes'),
(141, 'theme_mods_FoundationPress', 'a:3:{i:0;b:0;s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1504751909;s:4:"data";a:3:{s:19:"wp_inactive_widgets";a:0:{}s:15:"sidebar-widgets";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:14:"footer-widgets";a:0:{}}}}', 'yes'),
(142, 'theme_switched', '', 'yes'),
(145, 'theme_mods_nxtchptr', 'a:3:{i:0;b:0;s:18:"custom_css_post_id";i:-1;s:18:"nav_menu_locations";a:5:{s:6:"menu-1";i:2;s:6:"menu-2";i:3;s:6:"menu-3";i:4;s:6:"menu-4";i:5;s:6:"menu-5";i:6;}}', 'yes'),
(148, 'category_children', 'a:0:{}', 'yes'),
(167, 'recently_activated', 'a:0:{}', 'yes'),
(176, 'wpmdb_usage', 'a:2:{s:6:"action";s:8:"savefile";s:4:"time";i:1507731339;}', 'no'),
(193, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(200, 'pmpro_nonmembertext', 'This content is for !!levels!! members only.<br /><a href="http://nxtchptr.herokuapp.com/wp-login.php?action=register">Register</a>', 'yes'),
(201, 'pmpro_notloggedintext', 'This content is for !!levels!! members only.<br /><a href="http://nxtchptr.herokuapp.com/wp-login.php">Log In</a> <a href="http://nxtchptr.herokuapp.com/wp-login.php?action=register">Register</a>', 'yes'),
(202, 'pmpro_rsstext', 'This content is for !!levels!! members only. Visit the site and log in/register to read.', 'yes'),
(203, 'pmpro_gateway_environment', 'sandbox', 'yes'),
(204, 'pmpro_currency', 'USD', 'yes'),
(205, 'pmpro_accepted_credit_cards', 'Visa,Mastercard,American Express,Discover', 'yes'),
(206, 'pmpro_from_email', 'memberships@nxtchptr.herokuapp.com', 'yes'),
(207, 'pmpro_from_name', 'NxtChptr Memberships', 'yes'),
(208, 'pmpro_email_admin_checkout', '1', 'yes'),
(209, 'pmpro_email_admin_changes', '1', 'yes'),
(210, 'pmpro_email_admin_cancels', '1', 'yes'),
(211, 'pmpro_email_admin_billing', '1', 'yes'),
(212, 'pmpro_tospage', '', 'yes'),
(213, 'pmpro_nag_paused', '1505532916', 'no'),
(214, 'pmpro_db_version', '1.93', 'yes'),
(215, 'pmpro_filterqueries', '1', 'yes'),
(224, 'pmpro_account_page_id', '32', 'yes'),
(225, 'pmpro_billing_page_id', '33', 'yes'),
(226, 'pmpro_cancel_page_id', '34', 'yes'),
(227, 'pmpro_checkout_page_id', '35', 'yes'),
(228, 'pmpro_confirmation_page_id', '36', 'yes'),
(229, 'pmpro_invoice_page_id', '37', 'yes'),
(230, 'pmpro_levels_page_id', '38', 'yes'),
(233, 'pmpro_sslseal', '', 'yes'),
(234, 'pmpro_nuclear_HTTPS', '', 'yes'),
(235, 'pmpro_twocheckout_apiusername', '', 'yes'),
(236, 'pmpro_twocheckout_apipassword', '', 'yes'),
(237, 'pmpro_twocheckout_accountnumber', '', 'yes'),
(238, 'pmpro_twocheckout_secretword', '', 'yes'),
(239, 'pmpro_use_ssl', '0', 'yes'),
(240, 'pmpro_tax_state', '', 'yes'),
(241, 'pmpro_tax_rate', '', 'yes'),
(242, 'pmpro_stripe_secretkey', 'sk_test_K1n3NnMsMS5iw2vJ4c2cFPoP', 'yes'),
(243, 'pmpro_stripe_publishablekey', 'pk_test_URLN0Ihl6lxKXic5zRZTCaK5', 'yes'),
(244, 'pmpro_stripe_billingaddress', '0', 'yes'),
(245, 'pmpro_gateway_email', '', 'yes'),
(246, 'pmpro_apiusername', '', 'yes'),
(247, 'pmpro_apipassword', '', 'yes'),
(248, 'pmpro_apisignature', '', 'yes'),
(249, 'pmpro_paypalexpress_skip_confirmation', '0', 'yes'),
(250, 'pmpro_payflow_partner', '', 'yes'),
(251, 'pmpro_payflow_vendor', '', 'yes'),
(252, 'pmpro_payflow_user', 'admin', 'yes'),
(253, 'pmpro_payflow_pwd', 'wmnMpwrd', 'yes'),
(254, 'pmpro_cybersource_merchantid', '', 'yes'),
(255, 'pmpro_cybersource_securitykey', '', 'yes'),
(256, 'pmpro_instructions', '', 'yes'),
(257, 'pmpro_braintree_merchantid', '', 'yes'),
(258, 'pmpro_braintree_publickey', '', 'yes'),
(259, 'pmpro_braintree_privatekey', '', 'yes'),
(260, 'pmpro_braintree_encryptionkey', '', 'yes'),
(261, 'pmpro_loginname', '', 'yes'),
(262, 'pmpro_transactionkey', '', 'yes'),
(263, 'pmpro_gateway', 'stripe', 'yes'),
(266, 'pmpro_only_filter_pmpro_emails', '1', 'yes'),
(267, 'pmpro_email_member_notification', '', 'yes'),
(270, 'pmpro_visits', 'a:5:{s:5:"today";i:1;s:8:"thisdate";s:10:"2017-10-10";s:5:"month";i:5;s:9:"thismonth";s:2:"10";s:7:"alltime";i:20;}', 'yes'),
(271, 'pmpro_views', 'a:5:{s:5:"today";i:207;s:8:"thisdate";s:10:"2017-11-10";s:5:"month";i:503;s:9:"thismonth";s:2:"10";s:7:"alltime";i:1712;}', 'yes'),
(373, 'pmpro_license_check', 'a:2:{s:7:"license";s:4:"plus";s:7:"enddate";i:1536537600;}', 'no'),
(374, 'pmpro_license_key', '4bd2176da4f6ded1a3f7db32f8e02de0', 'no'),
(383, 'acf_version', '4.4.12', 'yes'),
(386, 'wpjb_config', 'a:37:{s:9:"urls_mode";s:1:"2";s:8:"urls_cpt";s:1:"1";s:9:"first_run";i:0;s:23:"front_show_related_jobs";a:1:{i:0;s:1:"1";}s:9:"show_maps";i:1;s:10:"cv_enabled";i:1;s:7:"version";s:5:"5.0.1";s:13:"urls_link_job";i:59;s:17:"urls_link_job_add";i:60;s:20:"urls_link_job_search";i:61;s:16:"urls_link_resume";i:62;s:23:"urls_link_resume_search";i:63;s:19:"urls_link_emp_panel";i:64;s:17:"urls_link_emp_reg";i:65;s:20:"urls_link_cand_panel";i:66;s:18:"urls_link_cand_reg";i:67;s:5:"count";a:2:{i:1;s:1:"3";i:5;s:1:"3";}s:10:"count_date";s:10:"2017-10-11";s:11:"license_key";s:27:"402b59-a47b74-100056-170910";s:17:"license_site_type";s:1:"1";s:18:"license_usage_data";N;s:23:"activation_message_hide";s:1:"1";s:10:"search_bar";s:7:"enabled";s:19:"front_jobs_per_page";s:2:"20";s:17:"front_hide_filled";N;s:18:"front_show_expired";N;s:19:"front_marked_as_new";s:1:"7";s:21:"front_hide_apply_link";N;s:20:"front_hide_bookmarks";N;s:24:"front_apply_members_only";N;s:20:"default_job_duration";s:2:"30";s:13:"posting_allow";s:1:"1";s:18:"posting_moderation";N;s:19:"front_allow_edition";N;s:28:"employer_login_only_approved";N;s:17:"employer_approval";s:1:"0";s:18:"employer_is_public";s:1:"1";}', 'yes'),
(387, 'widget_wpjb-widget-alerts', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(388, 'widget_wpjb-job-categories', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(389, 'widget_wpjb-custom-menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(390, 'widget_wpjb-featured-jobs', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(391, 'widget_wpjb-widget-feeds', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(392, 'widget_wpjb-job-board-menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(393, 'widget_wpjb-job-types', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(394, 'widget_wpjb-job-locations', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(395, 'widget_wpjb-recent-jobs', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(396, 'widget_wpjb-recently-viewed', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(397, 'widget_wpjb-resumes-menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(398, 'widget_wpjb-search', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(403, 'wsl_settings_welcome_panel_enabled', '2.3.3', 'yes'),
(404, 'wsl_settings_redirect_url', 'http://nxtchptr.herokuapp.com', 'yes'),
(405, 'wsl_settings_force_redirect_url', '2', 'yes'),
(406, 'wsl_settings_connect_with_label', '', 'yes'),
(407, 'wsl_settings_users_avatars', '1', 'yes'),
(408, 'wsl_settings_use_popup', '2', 'yes'),
(409, 'wsl_settings_widget_display', '1', 'yes') ;
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(410, 'wsl_settings_authentication_widget_css', '.wp-social-login-connect-with {}\r\n.wp-social-login-provider-list {}\r\n.wp-social-login-provider-list a {}\r\n.wp-social-login-provider-list img {}\r\n.wsl_connect_with_provider {}', 'yes'),
(411, 'wsl_settings_bouncer_registration_enabled', '1', 'yes'),
(412, 'wsl_settings_bouncer_authentication_enabled', '1', 'yes'),
(413, 'wsl_settings_bouncer_accounts_linking_enabled', '1', 'yes'),
(414, 'wsl_settings_bouncer_profile_completion_require_email', '2', 'yes'),
(415, 'wsl_settings_bouncer_profile_completion_change_username', '2', 'yes'),
(416, 'wsl_settings_bouncer_profile_completion_hook_extra_fields', '2', 'yes'),
(417, 'wsl_settings_bouncer_new_users_moderation_level', '1', 'yes'),
(418, 'wsl_settings_bouncer_new_users_membership_default_role', 'default', 'yes'),
(419, 'wsl_settings_bouncer_new_users_restrict_domain_enabled', '2', 'yes'),
(420, 'wsl_settings_bouncer_new_users_restrict_domain_text_bounce', '<strong>This website is restricted to invited readers only.</strong>\r\n\r\nIt doesn\'t look like you have been invited to access this site. If you think this is a mistake, you might want to contact the website owner and request an invitation.\r\n\r\n&nbsp;', 'yes'),
(421, 'wsl_settings_bouncer_new_users_restrict_email_enabled', '2', 'yes'),
(422, 'wsl_settings_bouncer_new_users_restrict_email_text_bounce', '<strong>This website is restricted to invited readers only.</strong>\r\n\r\nIt doesn\'t look like you have been invited to access this site. If you think this is a mistake, you might want to contact the website owner and request an invitation.\r\n\r\n&nbsp;', 'yes'),
(423, 'wsl_settings_bouncer_new_users_restrict_profile_enabled', '2', 'yes'),
(424, 'wsl_settings_bouncer_new_users_restrict_profile_text_bounce', '<strong>This website is restricted to invited readers only.</strong>\r\n\r\nIt doesn\'t look like you have been invited to access this site. If you think this is a mistake, you might want to contact the website owner and request an invitation.\r\n\r\n&nbsp;', 'yes'),
(425, 'wsl_settings_contacts_import_facebook', '1', 'yes'),
(426, 'wsl_settings_contacts_import_google', '', 'yes'),
(427, 'wsl_settings_contacts_import_twitter', '', 'yes'),
(428, 'wsl_settings_contacts_import_live', '', 'yes'),
(429, 'wsl_settings_contacts_import_linkedin', '1', 'yes'),
(430, 'wsl_settings_buddypress_enable_mapping', '1', 'yes'),
(431, 'wsl_settings_buddypress_xprofile_map', 'a:2:{i:1;s:11:"displayName";i:2;s:0:"";}', 'yes'),
(432, 'wsl_settings_Facebook_enabled', '1', 'yes'),
(433, 'wsl_settings_Google_enabled', '1', 'yes'),
(434, 'wsl_settings_Twitter_enabled', '0', 'yes'),
(435, 'wsl_components_core_enabled', '1', 'yes'),
(436, 'wsl_components_networks_enabled', '1', 'yes'),
(437, 'wsl_components_login-widget_enabled', '1', 'yes'),
(438, 'wsl_components_bouncer_enabled', '1', 'yes'),
(439, 'wsl_settings_Facebook_app_scope', 'email, public_profile, user_friends', 'yes'),
(440, 'wsl_settings_Google_app_scope', 'profile https://www.googleapis.com/auth/plus.profile.emails.read', 'yes'),
(441, 'wsl_settings_GitHub_app_scope', 'user:email', 'yes'),
(442, 'wsl_settings_Facebook_app_id', '868668233291291', 'yes'),
(443, 'wsl_settings_Facebook_app_secret', '146d0138ea357d980165bd4d721d462d', 'yes'),
(444, 'wsl_settings_Google_app_id', '637605021378-nnn3cmc447dtmffd7gneshgfep1kjte0.apps.googleusercontent.com', 'yes'),
(445, 'wsl_settings_Google_app_secret', 'NmGW6ggZvTRo05t_zpjnUlC7', 'yes'),
(446, 'wsl_settings_Twitter_app_key', '', 'yes'),
(447, 'wsl_settings_Twitter_app_secret', '', 'yes'),
(448, 'wsl_settings_WordPress_enabled', '', 'yes'),
(449, 'wsl_settings_WordPress_app_id', '', 'yes'),
(450, 'wsl_settings_WordPress_app_secret', '', 'yes'),
(451, 'wsl_settings_Yahoo_enabled', '', 'yes'),
(452, 'wsl_settings_LinkedIn_enabled', '1', 'yes'),
(453, 'wsl_settings_LinkedIn_app_key', '86wfvsjni5c85l', 'yes'),
(454, 'wsl_settings_LinkedIn_app_secret', 'v1HPxTflrVkuAAqR', 'yes'),
(455, 'wsl_settings_Disqus_enabled', '', 'yes'),
(456, 'wsl_settings_Disqus_app_id', '', 'yes'),
(457, 'wsl_settings_Disqus_app_secret', '', 'yes'),
(458, 'wsl_settings_Instagram_enabled', '1', 'yes'),
(459, 'wsl_settings_Instagram_app_id', '75d0e309903d419690d1c6e25b4f19cc', 'yes'),
(460, 'wsl_settings_Instagram_app_secret', 'ac4d55d6624c448f92802773b58a36b8', 'yes'),
(461, 'wsl_settings_Reddit_enabled', '', 'yes'),
(462, 'wsl_settings_Reddit_app_id', '', 'yes'),
(463, 'wsl_settings_Reddit_app_secret', '', 'yes'),
(464, 'wsl_settings_Foursquare_enabled', '', 'yes'),
(465, 'wsl_settings_Foursquare_app_id', '', 'yes'),
(466, 'wsl_settings_Foursquare_app_secret', '', 'yes'),
(467, 'wsl_settings_LastFM_enabled', '', 'yes'),
(468, 'wsl_settings_LastFM_app_key', '', 'yes'),
(469, 'wsl_settings_LastFM_app_secret', '', 'yes'),
(470, 'wsl_settings_Tumblr_enabled', '', 'yes'),
(471, 'wsl_settings_Tumblr_app_key', '', 'yes'),
(472, 'wsl_settings_Tumblr_app_secret', '', 'yes'),
(473, 'wsl_settings_Goodreads_enabled', '1', 'yes'),
(474, 'wsl_settings_Goodreads_app_key', 'G47CbD4FJlyN9DDSwXiM3Q', 'yes'),
(475, 'wsl_settings_Goodreads_app_secret', '43KjVRA3UcRUOyrddkf99w6dWavpyTrQHs4mWoezF0c', 'yes'),
(476, 'wsl_settings_Stackoverflow_enabled', '', 'yes'),
(477, 'wsl_settings_GitHub_enabled', '', 'yes'),
(478, 'wsl_settings_GitHub_app_id', '', 'yes'),
(479, 'wsl_settings_GitHub_app_secret', '', 'yes'),
(480, 'wsl_settings_Dribbble_enabled', '', 'yes'),
(481, 'wsl_settings_Dribbble_app_id', '', 'yes'),
(482, 'wsl_settings_Dribbble_app_secret', '', 'yes'),
(483, 'wsl_settings_500px_enabled', '', 'yes'),
(484, 'wsl_settings_500px_app_key', '', 'yes'),
(485, 'wsl_settings_500px_app_secret', '', 'yes'),
(486, 'wsl_settings_Skyrock_enabled', '', 'yes'),
(487, 'wsl_settings_Skyrock_app_key', '', 'yes'),
(488, 'wsl_settings_Skyrock_app_secret', '', 'yes'),
(489, 'wsl_settings_Mixi_enabled', '', 'yes'),
(490, 'wsl_settings_Steam_enabled', '', 'yes'),
(491, 'wsl_settings_Steam_app_key', '', 'yes'),
(492, 'wsl_settings_Steam_app_secret', '', 'yes'),
(493, 'wsl_settings_TwitchTV_enabled', '', 'yes'),
(494, 'wsl_settings_TwitchTV_app_id', '', 'yes'),
(495, 'wsl_settings_TwitchTV_app_secret', '', 'yes'),
(496, 'wsl_settings_Vkontakte_enabled', '', 'yes'),
(497, 'wsl_settings_Vkontakte_app_id', '', 'yes'),
(498, 'wsl_settings_Vkontakte_app_secret', '', 'yes'),
(499, 'wsl_settings_Mailru_enabled', '', 'yes'),
(500, 'wsl_settings_Mailru_app_id', '', 'yes'),
(501, 'wsl_settings_Mailru_app_secret', '', 'yes'),
(502, 'wsl_settings_Yandex_enabled', '', 'yes'),
(503, 'wsl_settings_Yandex_app_id', '', 'yes'),
(504, 'wsl_settings_Yandex_app_secret', '', 'yes'),
(505, 'wsl_settings_Odnoklassniki_enabled', '', 'yes'),
(506, 'wsl_settings_Odnoklassniki_app_id', '', 'yes'),
(507, 'wsl_settings_Odnoklassniki_app_secret', '', 'yes'),
(508, 'wsl_settings_AOL_enabled', '', 'yes'),
(509, 'wsl_settings_Live_enabled', '', 'yes') ;
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(510, 'wsl_settings_Live_app_id', '', 'yes'),
(511, 'wsl_settings_Live_app_secret', '', 'yes'),
(512, 'wsl_settings_PixelPin_enabled', '', 'yes'),
(513, 'wsl_settings_PixelPin_app_id', '', 'yes'),
(514, 'wsl_settings_PixelPin_app_secret', '', 'yes'),
(525, 'widget_theme-my-login', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(526, 'theme_my_login', 'a:4:{s:10:"enable_css";b:1;s:10:"login_type";s:7:"default";s:14:"active_modules";a:1:{i:0;s:41:"custom-redirection/custom-redirection.php";}s:7:"version";s:5:"6.4.9";}', 'yes'),
(529, 'wsl_settings_social_icon_set', 'wpzoom', 'yes'),
(530, 'wsl_settings_users_notification', '0', 'yes'),
(533, 'wsl_settings_bouncer_new_users_restrict_domain_list', '', 'yes'),
(534, 'wsl_settings_bouncer_new_users_restrict_email_list', '', 'yes'),
(535, 'wsl_settings_bouncer_new_users_restrict_profile_list', '', 'yes'),
(538, 'pmpro_logins', 'a:5:{s:5:"today";i:1;s:8:"thisdate";s:10:"2017-10-10";s:5:"month";i:7;s:9:"thismonth";s:2:"10";s:7:"alltime";i:37;}', 'yes'),
(567, 'wsl_components_users_enabled', '1', 'yes'),
(568, 'wsl_components_contacts_enabled', '1', 'yes'),
(569, 'wsl_components_buddypress_enabled', '1', 'yes'),
(570, 'wsl_settings_contacts_import_vkontakte', '', 'yes'),
(601, 'bp-deactivated-components', 'a:0:{}', 'yes'),
(602, 'bb-config-location', 'C:\\xampp\\htdocs\\nxtchptr/bb-config.php', 'yes'),
(603, 'bp-xprofile-base-group-name', 'Base', 'yes'),
(604, 'bp-xprofile-fullname-field-name', 'Name', 'yes'),
(605, 'bp-blogs-first-install', '', 'yes'),
(606, 'bp-disable-profile-sync', '0', 'yes'),
(607, 'hide-loggedout-adminbar', '0', 'yes'),
(608, 'bp-disable-avatar-uploads', '0', 'yes'),
(609, 'bp-disable-cover-image-uploads', '0', 'yes'),
(610, 'bp-disable-group-avatar-uploads', '0', 'yes'),
(611, 'bp-disable-group-cover-image-uploads', '0', 'yes'),
(612, 'bp-disable-account-deletion', '0', 'yes'),
(613, 'bp-disable-blogforum-comments', '1', 'yes'),
(614, '_bp_theme_package_id', 'legacy', 'yes'),
(615, 'bp-emails-unsubscribe-salt', 'OyVzcG9WTUd6UGJ+TXZeajsvUi9pfmBeZzsrKGElfUJEUj8oYE8/NVtDQE0yQl8yLVZKaHI5VHNuVVA0YWA7dA==', 'yes'),
(616, 'bp_restrict_group_creation', '0', 'yes'),
(617, '_bp_enable_akismet', '1', 'yes'),
(618, '_bp_enable_heartbeat_refresh', '1', 'yes'),
(619, '_bp_force_buddybar', '', 'yes'),
(620, '_bp_retain_bp_default', '', 'yes'),
(621, '_bp_ignore_deprecated_code', '1', 'yes'),
(622, 'widget_bp_core_login_widget', '', 'yes'),
(623, 'widget_bp_core_members_widget', '', 'yes'),
(624, 'widget_bp_core_whos_online_widget', '', 'yes'),
(625, 'widget_bp_core_recently_active_widget', '', 'yes'),
(626, 'widget_bp_groups_widget', '', 'yes'),
(627, 'widget_bp_messages_sitewide_notices_widget', '', 'yes'),
(632, 'bp-active-components', 'a:5:{s:8:"xprofile";s:1:"1";s:7:"friends";s:1:"1";s:8:"messages";s:1:"1";s:6:"groups";s:1:"1";s:7:"members";s:1:"1";}', 'yes'),
(633, 'bp-pages', 'a:2:{s:7:"members";i:74;s:6:"groups";i:91;}', 'yes'),
(634, '_bp_db_version', '11105', 'yes'),
(635, 'bp_disable_blogforum_comments', '1', 'yes'),
(727, 'widget_bp_core_friends_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(847, 'theme_my_login_redirection', 'a:6:{s:13:"administrator";a:4:{s:10:"login_type";s:7:"referer";s:9:"login_url";s:0:"";s:11:"logout_type";s:6:"custom";s:10:"logout_url";s:1:"/";}s:6:"editor";a:4:{s:10:"login_type";s:7:"referer";s:9:"login_url";s:0:"";s:11:"logout_type";s:6:"custom";s:10:"logout_url";s:0:"";}s:6:"author";a:4:{s:10:"login_type";s:7:"referer";s:9:"login_url";s:0:"";s:11:"logout_type";s:6:"custom";s:10:"logout_url";s:0:"";}s:11:"contributor";a:4:{s:10:"login_type";s:7:"referer";s:9:"login_url";s:0:"";s:11:"logout_type";s:6:"custom";s:10:"logout_url";s:0:"";}s:10:"subscriber";a:4:{s:10:"login_type";s:7:"referer";s:9:"login_url";s:0:"";s:11:"logout_type";s:6:"custom";s:10:"logout_url";s:0:"";}s:8:"employer";a:4:{s:10:"login_type";s:7:"referer";s:9:"login_url";s:0:"";s:11:"logout_type";s:6:"custom";s:10:"logout_url";s:0:"";}}', 'yes') ;
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(850, 'pmpro_addons', 'a:73:{i:0;a:11:{s:4:"Name";s:30:"Hide Admin Bar From Non-admins";s:5:"Title";s:30:"Hide Admin Bar From Non-admins";s:9:"PluginURI";s:68:"http://www.paidmembershipspro.com/wp/hide-admin-bar-from-non-admins/";s:4:"Slug";s:30:"hide-admin-bar-from-non-admins";s:6:"plugin";s:65:"hide-admin-bar-from-non-admins/hide-admin-bar-from-non-admins.php";s:7:"Version";s:3:"1.0";s:11:"Description";s:71:"A tweak of the code by Yoast to hide the admin bar for non-admins only.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:73:"https://downloads.wordpress.org/plugin/hide-admin-bar-from-non-admins.zip";s:7:"License";s:13:"wordpress.org";}i:1;a:11:{s:4:"Name";s:21:"Import Users from CSV";s:5:"Title";s:21:"Import Users from CSV";s:9:"PluginURI";s:58:"http://wordpress.org/extend/plugins/import-users-from-csv/";s:4:"Slug";s:21:"import-users-from-csv";s:6:"plugin";s:47:"import-users-from-csv/import-users-from-csv.php";s:7:"Version";s:5:"1.0.0";s:11:"Description";s:47:"Import Users data and metadata from a csv file.";s:6:"Author";s:13:"Ulrich Sossou";s:9:"AuthorURI";s:24:"http://ulrichsossou.com/";s:8:"Download";s:64:"https://downloads.wordpress.org/plugin/import-users-from-csv.zip";s:7:"License";s:13:"wordpress.org";}i:2;a:11:{s:4:"Name";s:39:"Paid Memberships Pro - Add Member Admin";s:5:"Title";s:39:"Paid Memberships Pro - Add Member Admin";s:9:"PluginURI";s:60:"http://www.paidmembershipspro.com/wp/pmpro-add-member-admin/";s:4:"Slug";s:22:"pmpro-add-member-admin";s:6:"plugin";s:49:"pmpro-add-member-admin/pmpro-add-member-admin.php";s:7:"Version";s:2:".2";s:11:"Description";s:48:"Allow admins to add members in the WP dashboard.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:79:"http://license.paidmembershipspro.com/downloads/plus/pmpro-add-member-admin.zip";s:7:"License";s:4:"plus";}i:3;a:11:{s:4:"Name";s:50:"Paid Memberships Pro - Add Name to Checkout Add On";s:5:"Title";s:50:"Paid Memberships Pro - Add Name to Checkout Add On";s:9:"PluginURI";s:64:"http://www.paidmembershipspro.com/wp/pmpro-add-name-to-checkout/";s:4:"Slug";s:26:"pmpro-add-name-to-checkout";s:6:"plugin";s:57:"pmpro-add-name-to-checkout/pmpro-add-name-to-checkout.php";s:7:"Version";s:4:".3.1";s:11:"Description";s:97:"Adds first and last name fields to the user account section at checkout for Paid Memberships Pro.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:83:"http://license.paidmembershipspro.com/downloads/plus/pmpro-add-name-to-checkout.zip";s:7:"License";s:4:"plus";}i:4;a:11:{s:4:"Name";s:48:"Paid Memberships Pro - Add PayPal Express Add On";s:5:"Title";s:48:"Paid Memberships Pro - Add PayPal Express Add On";s:9:"PluginURI";s:62:"http://www.paidmembershipspro.com/wp/pmpro-add-paypal-express/";s:4:"Slug";s:24:"pmpro-add-paypal-express";s:6:"plugin";s:53:"pmpro-add-paypal-express/pmpro-add-paypal-express.php";s:7:"Version";s:4:".5.1";s:11:"Description";s:49:"Add PayPal Express as a Second Option at Checkout";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:81:"http://license.paidmembershipspro.com/downloads/plus/pmpro-add-paypal-express.zip";s:7:"License";s:4:"plus";}i:5;a:11:{s:4:"Name";s:37:"Paid Memberships Pro - Addon Packages";s:5:"Title";s:37:"Paid Memberships Pro - Addon Packages";s:9:"PluginURI";s:55:"http://www.paidmembershipspro.com/pmpro-addon-packages/";s:4:"Slug";s:20:"pmpro-addon-packages";s:6:"plugin";s:45:"pmpro-addon-packages/pmpro-addon-packages.php";s:7:"Version";s:4:".7.3";s:11:"Description";s:169:"Allow PMPro members to purchase access to specific pages. This plugin is meant to be a temporary solution until support for multiple membership levels is added to PMPro.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:77:"http://license.paidmembershipspro.com/downloads/plus/pmpro-addon-packages.zip";s:7:"License";s:4:"plus";}i:6;a:11:{s:4:"Name";s:53:"Paid Memberships Pro - Address For Free Levels Add On";s:5:"Title";s:53:"Paid Memberships Pro - Address For Free Levels Add On";s:9:"PluginURI";s:67:"http://www.paidmembershipspro.com/wp/pmpro-address-for-free-levels/";s:4:"Slug";s:29:"pmpro-address-for-free-levels";s:6:"plugin";s:63:"pmpro-address-for-free-levels/pmpro-address-for-free-levels.php";s:7:"Version";s:4:".3.2";s:11:"Description";s:66:"Show address fields for free levels also with Paid Memberships Pro";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:86:"http://license.paidmembershipspro.com/downloads/plus/pmpro-address-for-free-levels.zip";s:7:"License";s:4:"plus";}i:7;a:11:{s:4:"Name";s:60:"Paid Memberships Pro - Advanced Levels Page Shortcode Add On";s:5:"Title";s:60:"Paid Memberships Pro - Advanced Levels Page Shortcode Add On";s:9:"PluginURI";s:59:"http://www.paidmembershipspro.com/wp/pmpro-advanced-levels/";s:4:"Slug";s:31:"pmpro-advanced-levels-shortcode";s:6:"plugin";s:67:"pmpro-advanced-levels-shortcode/pmpro-advanced-levels-shortcode.php";s:7:"Version";s:4:".2.4";s:11:"Description";s:105:"An enhanced shortcode for customizing the display of your Membership Levels Page for Paid Memberships Pro";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:88:"http://license.paidmembershipspro.com/downloads/plus/pmpro-advanced-levels-shortcode.zip";s:7:"License";s:4:"plus";}i:8;a:11:{s:4:"Name";s:40:"Paid Memberships Pro - Affiliates Add On";s:5:"Title";s:40:"Paid Memberships Pro - Affiliates Add On";s:9:"PluginURI";s:51:"http://www.paidmembershipspro.com/pmpro-affiliates/";s:4:"Slug";s:16:"pmpro-affiliates";s:6:"plugin";s:37:"pmpro-affiliates/pmpro-affiliates.php";s:7:"Version";s:2:".3";s:11:"Description";s:185:"Create affiliate accounts and codes. If a code is passed to a page as a parameter, a cookie is set. If a cookie is present after checkout, the order is awarded to the affiliate account.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:73:"http://license.paidmembershipspro.com/downloads/plus/pmpro-affiliates.zip";s:7:"License";s:4:"plus";}i:9;a:11:{s:4:"Name";s:39:"Paid Memberships Pro - Approvals Add On";s:5:"Title";s:39:"Paid Memberships Pro - Approvals Add On";s:9:"PluginURI";s:71:"https://www.paidmembershipspro.com/add-ons/approval-process-membership/";s:4:"Slug";s:15:"pmpro-approvals";s:6:"plugin";s:35:"pmpro-approvals/pmpro-approvals.php";s:7:"Version";s:5:"1.0.4";s:11:"Description";s:75:"Grants administrators the ability to approve/deny memberships after signup.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:34:"https://www.paidmembershipspro.com";s:8:"Download";s:72:"http://license.paidmembershipspro.com/downloads/plus/pmpro-approvals.zip";s:7:"License";s:4:"plus";}i:10;a:11:{s:4:"Name";s:44:"Paid Memberships Pro - Auto-Renewal Checkbox";s:5:"Title";s:44:"Paid Memberships Pro - Auto-Renewal Checkbox";s:9:"PluginURI";s:98:"https://www.paidmembershipspro.com/add-ons/plus-add-ons/auto-renewal-checkbox-membership-checkout/";s:4:"Slug";s:27:"pmpro-auto-renewal-checkbox";s:6:"plugin";s:59:"pmpro-auto-renewal-checkbox/pmpro-auto-renewal-checkbox.php";s:7:"Version";s:4:".2.2";s:11:"Description";s:55:"Make auto-renewal optional at checkout with a checkbox.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:84:"http://license.paidmembershipspro.com/downloads/plus/pmpro-auto-renewal-checkbox.zip";s:7:"License";s:4:"plus";}i:11;a:11:{s:4:"Name";s:36:"Paid Memberships Pro - AWeber Add On";s:5:"Title";s:36:"Paid Memberships Pro - AWeber Add On";s:9:"PluginURI";s:47:"http://www.paidmembershipspro.com/pmpro-aweber/";s:4:"Slug";s:12:"pmpro-aweber";s:6:"plugin";s:29:"pmpro-aweber/pmpro-aweber.php";s:7:"Version";s:3:"1.3";s:11:"Description";s:56:"Sync your WordPress users and members with AWeber lists.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:55:"https://downloads.wordpress.org/plugin/pmpro-aweber.zip";s:7:"License";s:13:"wordpress.org";}i:12;a:11:{s:4:"Name";s:37:"Paid Memberships Pro - bbPress Add On";s:5:"Title";s:37:"Paid Memberships Pro - bbPress Add On";s:9:"PluginURI";s:57:"https://www.paidmembershipspro.com/add-ons/pmpro-bbpress/";s:4:"Slug";s:13:"pmpro-bbpress";s:6:"plugin";s:31:"pmpro-bbpress/pmpro-bbpress.php";s:7:"Version";s:5:"1.5.4";s:11:"Description";s:54:"Allow individual forums to be locked down for members.";s:6:"Author";s:29:"Stranger Studios, Scott Sousa";s:9:"AuthorURI";s:34:"https://www.paidmembershipspro.com";s:8:"Download";s:56:"https://downloads.wordpress.org/plugin/pmpro-bbpress.zip";s:7:"License";s:13:"wordpress.org";}i:13;a:11:{s:4:"Name";s:50:"Paid Memberships Pro - Better Logins Report Add On";s:5:"Title";s:50:"Paid Memberships Pro - Better Logins Report Add On";s:9:"PluginURI";s:64:"http://www.paidmembershipspro.com/wp/pmpro-better-logins-report/";s:4:"Slug";s:26:"pmpro-better-logins-report";s:6:"plugin";s:57:"pmpro-better-logins-report/pmpro-better-logins-report.php";s:7:"Version";s:6:".2.3.2";s:11:"Description";s:66:"Adds login, view, and visit stats for "This Week" and "This Year".";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:83:"http://license.paidmembershipspro.com/downloads/plus/pmpro-better-logins-report.zip";s:7:"License";s:4:"plus";}i:14;a:11:{s:4:"Name";s:42:"Paid Memberships Pro - Check Levels Add On";s:5:"Title";s:42:"Paid Memberships Pro - Check Levels Add On";s:9:"PluginURI";s:56:"http://www.paidmembershipspro.com/wp/pmpro-check-levels/";s:4:"Slug";s:18:"pmpro-check-levels";s:6:"plugin";s:41:"pmpro-check-levels/pmpro-check-levels.php";s:7:"Version";s:2:".3";s:11:"Description";s:106:"A collection of customizations useful when allowing users to pay by check for Paid Memberships Pro levels.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:75:"http://license.paidmembershipspro.com/downloads/plus/pmpro-check-levels.zip";s:7:"License";s:4:"plus";}i:15;a:11:{s:4:"Name";s:46:"Paid Memberships Pro - Constant Contact Add On";s:5:"Title";s:46:"Paid Memberships Pro - Constant Contact Add On";s:9:"PluginURI";s:56:"http://www.paidmembershipspro.com/pmpro-constantcontact/";s:4:"Slug";s:22:"pmpro-constant-contact";s:6:"plugin";s:49:"pmpro-constant-contact/pmpro-constant-contact.php";s:7:"Version";s:5:"1.0.2";s:11:"Description";s:66:"Sync your WordPress users and members with Constant Contact lists.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:65:"https://downloads.wordpress.org/plugin/pmpro-constant-contact.zip";s:7:"License";s:13:"wordpress.org";}i:16;a:11:{s:4:"Name";s:52:"Paid Memberships Pro - Custom Level Cost Text Add On";s:5:"Title";s:52:"Paid Memberships Pro - Custom Level Cost Text Add On";s:9:"PluginURI";s:66:"http://www.paidmembershipspro.com/wp/pmpro-custom-level-cost-text/";s:4:"Slug";s:21:"pmpro-level-cost-text";s:6:"plugin";s:47:"pmpro-level-cost-text/pmpro-level-cost-text.php";s:7:"Version";s:4:".3.1";s:11:"Description";s:99:"Modify the default level cost text per level, per discount code, or globally via advanced settings.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:78:"http://license.paidmembershipspro.com/downloads/plus/pmpro-level-cost-text.zip";s:7:"License";s:4:"plus";}i:17;a:11:{s:4:"Name";s:46:"Paid Memberships Pro - Custom Post Type Add On";s:5:"Title";s:46:"Paid Memberships Pro - Custom Post Type Add On";s:9:"PluginURI";s:47:"http://www.paidmembershipspro.com/wp/pmpro-cpt/";s:4:"Slug";s:9:"pmpro-cpt";s:6:"plugin";s:23:"pmpro-cpt/pmpro-cpt.php";s:7:"Version";s:2:".2";s:11:"Description";s:76:"Add the PMPro meta box to CPTs and redirects non-members to a selected page.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:66:"http://license.paidmembershipspro.com/downloads/plus/pmpro-cpt.zip";s:7:"License";s:4:"plus";}i:18;a:11:{s:4:"Name";s:42:"Paid Memberships Pro - Developer\'s Toolkit";s:5:"Title";s:42:"Paid Memberships Pro - Developer\'s Toolkit";s:9:"PluginURI";s:0:"";s:4:"Slug";s:13:"pmpro-toolkit";s:6:"plugin";s:31:"pmpro-toolkit/pmpro-toolkit.php";s:7:"Version";s:2:".4";s:11:"Description";s:70:"Various tools to test and debug Paid Memberships Pro enabled websites.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:0:"";s:8:"Download";s:70:"http://license.paidmembershipspro.com/downloads/free/pmpro-toolkit.zip";s:7:"License";s:4:"plus";}i:19;a:11:{s:4:"Name";s:32:"Paid Memberships Pro - Donations";s:5:"Title";s:32:"Paid Memberships Pro - Donations";s:9:"PluginURI";s:58:"http://www.paidmembershipspro.com/add-ons/pmpro-donations/";s:4:"Slug";s:15:"pmpro-donations";s:6:"plugin";s:35:"pmpro-donations/pmpro-donations.php";s:7:"Version";s:2:".4";s:11:"Description";s:65:"Allow customers to set an additional donation amount at checkout.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:72:"http://license.paidmembershipspro.com/downloads/plus/pmpro-donations.zip";s:7:"License";s:4:"plus";}i:20;a:11:{s:4:"Name";s:58:"Paid Memberships Pro - Download Monitor Integration Add On";s:5:"Title";s:58:"Paid Memberships Pro - Download Monitor Integration Add On";s:9:"PluginURI";s:57:"http://www.paidmembershipspro.com/pmpro-download-monitor/";s:4:"Slug";s:22:"pmpro-download-monitor";s:6:"plugin";s:49:"pmpro-download-monitor/pmpro-download-monitor.php";s:7:"Version";s:2:".1";s:11:"Description";s:72:"Require membership for downloads when using the Download Monitor plugin.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:79:"http://license.paidmembershipspro.com/downloads/plus/pmpro-download-monitor.zip";s:7:"License";s:4:"plus";}i:21;a:11:{s:4:"Name";s:48:"Paid Memberships Pro - Email Confirmation Add On";s:5:"Title";s:48:"Paid Memberships Pro - Email Confirmation Add On";s:9:"PluginURI";s:66:"http://www.paidmembershipspro.com/addons/pmpro-email-confirmation/";s:4:"Slug";s:24:"pmpro-email-confirmation";s:6:"plugin";s:53:"pmpro-email-confirmation/pmpro-email-confirmation.php";s:7:"Version";s:2:".4";s:11:"Description";s:73:"Require email confirmation before certain levels are enabled for members.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:81:"http://license.paidmembershipspro.com/downloads/plus/pmpro-email-confirmation.zip";s:7:"License";s:4:"plus";}i:22;a:11:{s:4:"Name";s:45:"Paid Memberships Pro - Email Templates Add On";s:5:"Title";s:45:"Paid Memberships Pro - Email Templates Add On";s:9:"PluginURI";s:100:"http://www.paidmembershipspro.com/add-ons/plugins-wordpress-repository/email-templates-admin-editor/";s:4:"Slug";s:21:"pmpro-email-templates";s:6:"plugin";s:47:"pmpro-email-templates/pmpro-email-templates.php";s:7:"Version";s:5:"0.7.1";s:11:"Description";s:50:"Define your own custom PMPro HTML Email Templates.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:78:"http://license.paidmembershipspro.com/downloads/plus/pmpro-email-templates.zip";s:7:"License";s:4:"plus";}i:23;a:11:{s:4:"Name";s:61:"Paid Memberships Pro - Extra Expiration Warning Emails Add On";s:5:"Title";s:61:"Paid Memberships Pro - Extra Expiration Warning Emails Add On";s:9:"PluginURI";s:75:"http://www.paidmembershipspro.com/wp/pmpro-extra-expiration-warning-emails/";s:4:"Slug";s:37:"pmpro-extra-expiration-warning-emails";s:6:"plugin";s:79:"pmpro-extra-expiration-warning-emails/pmpro-extra-expiration-warning-emails.php";s:7:"Version";s:6:".3.7.1";s:11:"Description";s:81:"Send out more than one "membership expiration warning" email to users with PMPro.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:94:"http://license.paidmembershipspro.com/downloads/plus/pmpro-extra-expiration-warning-emails.zip";s:7:"License";s:4:"plus";}i:24;a:11:{s:4:"Name";s:41:"Paid Memberships Pro - GetResponse Add On";s:5:"Title";s:41:"Paid Memberships Pro - GetResponse Add On";s:9:"PluginURI";s:52:"http://www.paidmembershipspro.com/pmpro-getresponse/";s:4:"Slug";s:17:"pmpro-getresponse";s:6:"plugin";s:39:"pmpro-getresponse/pmpro-getresponse.php";s:7:"Version";s:4:".4.1";s:11:"Description";s:65:"Sync your WordPress users and members with GetResponse campaigns.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:74:"http://license.paidmembershipspro.com/downloads/plus/pmpro-getresponse.zip";s:7:"License";s:4:"plus";}i:25;a:11:{s:4:"Name";s:41:"Paid Memberships Pro - Gift Levels Add On";s:5:"Title";s:41:"Paid Memberships Pro - Gift Levels Add On";s:9:"PluginURI";s:60:"http://www.paidmembershipspro.com/add-ons/pmpro-gift-levels/";s:4:"Slug";s:17:"pmpro-gift-levels";s:6:"plugin";s:39:"pmpro-gift-levels/pmpro-gift-levels.php";s:7:"Version";s:4:".2.3";s:11:"Description";s:87:"Some levels will generate discount codes to give to others to use for gift memberships.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:74:"http://license.paidmembershipspro.com/downloads/plus/pmpro-gift-levels.zip";s:7:"License";s:4:"plus";}i:26;a:11:{s:4:"Name";s:50:"Paid Memberships Pro - Group Discount Codes Add On";s:5:"Title";s:50:"Paid Memberships Pro - Group Discount Codes Add On";s:9:"PluginURI";s:61:"http://www.paidmembershipspro.com/pmpro-group-discount-codes/";s:4:"Slug";s:26:"pmpro-group-discount-codes";s:6:"plugin";s:57:"pmpro-group-discount-codes/pmpro-group-discount-codes.php";s:7:"Version";s:4:".3.1";s:11:"Description";s:98:"Adds features to PMPro to better manage grouped discount codes or large numbers of discount codes.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:33:"http://www.paidmembershipspro.com";s:8:"Download";s:83:"http://license.paidmembershipspro.com/downloads/plus/pmpro-group-discount-codes.zip";s:7:"License";s:4:"plus";}i:27;a:11:{s:4:"Name";s:45:"Paid Memberships Pro - Holler Box Integration";s:5:"Title";s:45:"Paid Memberships Pro - Holler Box Integration";s:9:"PluginURI";s:66:"https://www.paidmembershipspro.com/add-ons/holler-box-integration/";s:4:"Slug";s:15:"pmpro-hollerbox";s:6:"plugin";s:35:"pmpro-hollerbox/pmpro-hollerbox.php";s:7:"Version";s:4:".1.1";s:11:"Description";s:105:"Integrates Paid Memberships Pro with the Holler Box plugin to display popups/banners by membership level.";s:6:"Author";s:20:"Paid Memberships Pro";s:9:"AuthorURI";s:34:"https://www.paidmembershipspro.com";s:8:"Download";s:72:"http://license.paidmembershipspro.com/downloads/plus/pmpro-hollerbox.zip";s:7:"License";s:4:"plus";}i:28;a:11:{s:4:"Name";s:51:"Paid Memberships Pro - Import Users from CSV Add On";s:5:"Title";s:51:"Paid Memberships Pro - Import Users from CSV Add On";s:9:"PluginURI";s:62:"http://www.paidmembershipspro.com/pmpro-import-users-from-csv/";s:4:"Slug";s:27:"pmpro-import-users-from-csv";s:6:"plugin";s:59:"pmpro-import-users-from-csv/pmpro-import-users-from-csv.php";s:7:"Version";s:4:".3.3";s:11:"Description";s:90:"Add-on for the Import Users From CSV plugin to import PMPro and membership-related fields.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:84:"http://license.paidmembershipspro.com/downloads/plus/pmpro-import-users-from-csv.zip";s:7:"License";s:4:"plus";}i:29;a:11:{s:4:"Name";s:42:"Paid Memberships Pro - Infusionsoft Add On";s:5:"Title";s:42:"Paid Memberships Pro - Infusionsoft Add On";s:9:"PluginURI";s:53:"http://www.paidmembershipspro.com/pmpro-infusionsoft/";s:4:"Slug";s:18:"pmpro-infusionsoft";s:6:"plugin";s:41:"pmpro-infusionsoft/pmpro-infusionsoft.php";s:7:"Version";s:5:"1.3.2";s:11:"Description";s:65:"Sync your WordPress users and members with Infusionsoft contacts.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:61:"https://downloads.wordpress.org/plugin/pmpro-infusionsoft.zip";s:7:"License";s:13:"wordpress.org";}i:30;a:11:{s:4:"Name";s:41:"Paid Memberships Pro - Invite Only Add On";s:5:"Title";s:41:"Paid Memberships Pro - Invite Only Add On";s:9:"PluginURI";s:60:"http://www.paidmembershipspro.com/add-ons/pmpro-invite-only/";s:4:"Slug";s:17:"pmpro-invite-only";s:6:"plugin";s:39:"pmpro-invite-only/pmpro-invite-only.php";s:7:"Version";s:4:".3.2";s:11:"Description";s:102:"Users must have an invite code to sign up for certain levels. Users are given an invite code to share.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:74:"http://license.paidmembershipspro.com/downloads/plus/pmpro-invite-only.zip";s:7:"License";s:4:"plus";}i:31;a:11:{s:4:"Name";s:41:"Paid Memberships Pro - Kissmetrics Add On";s:5:"Title";s:41:"Paid Memberships Pro - Kissmetrics Add On";s:9:"PluginURI";s:0:"";s:4:"Slug";s:17:"pmpro-kissmetrics";s:6:"plugin";s:39:"pmpro-kissmetrics/pmpro-kissmetrics.php";s:7:"Version";s:4:".3.1";s:11:"Description";s:116:"Integrates your WordPress site with Kissmetrics to track meaningful user data, with or without Paid Memberships Pro.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:26:"http://strangerstudios.com";s:8:"Download";s:60:"https://downloads.wordpress.org/plugin/pmpro-kissmetrics.zip";s:7:"License";s:13:"wordpress.org";}i:32;a:11:{s:4:"Name";s:50:"Paid Memberships Pro - Levels as DIV Layout Add On";s:5:"Title";s:50:"Paid Memberships Pro - Levels as DIV Layout Add On";s:9:"PluginURI";s:55:"https://www.paidmembershipspro.com/wp/pmpro-div-levels/";s:4:"Slug";s:16:"pmpro-div-levels";s:6:"plugin";s:37:"pmpro-div-levels/pmpro-div-levels.php";s:7:"Version";s:2:".2";s:11:"Description";s:76:"Display your Membership Levels Page in a DIV Layout for Paid Memberships Pro";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:73:"http://license.paidmembershipspro.com/downloads/plus/pmpro-div-levels.zip";s:7:"License";s:4:"plus";}i:33;a:11:{s:4:"Name";s:46:"Paid Memberships Pro - Limit Post Views Add On";s:5:"Title";s:46:"Paid Memberships Pro - Limit Post Views Add On";s:9:"PluginURI";s:60:"http://www.paidmembershipspro.com/wp/pmpro-limit-post-views/";s:4:"Slug";s:22:"pmpro-limit-post-views";s:6:"plugin";s:49:"pmpro-limit-post-views/pmpro-limit-post-views.php";s:7:"Version";s:2:".5";s:11:"Description";s:106:"Integrates with Paid Memberships Pro to limit the number of times non-members can view posts on your site.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:79:"http://license.paidmembershipspro.com/downloads/plus/pmpro-limit-post-views.zip";s:7:"License";s:4:"plus";}i:34;a:11:{s:4:"Name";s:44:"Paid Memberships Pro - Lock Membership Level";s:5:"Title";s:44:"Paid Memberships Pro - Lock Membership Level";s:9:"PluginURI";s:59:"http://www.paidmembershipspro.com/wp/lock-membership-level/";s:4:"Slug";s:27:"pmpro-lock-membership-level";s:6:"plugin";s:59:"pmpro-lock-membership-level/pmpro-lock-membership-level.php";s:7:"Version";s:2:".2";s:11:"Description";s:61:"Lock membership level changes for specific users or by level.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:84:"http://license.paidmembershipspro.com/downloads/plus/pmpro-lock-membership-level.zip";s:7:"License";s:4:"plus";}i:35;a:11:{s:4:"Name";s:39:"Paid Memberships Pro - MailChimp Add On";s:5:"Title";s:39:"Paid Memberships Pro - MailChimp Add On";s:9:"PluginURI";s:50:"http://www.paidmembershipspro.com/pmpro-mailchimp/";s:4:"Slug";s:15:"pmpro-mailchimp";s:6:"plugin";s:35:"pmpro-mailchimp/pmpro-mailchimp.php";s:7:"Version";s:5:"2.1.1";s:11:"Description";s:59:"Sync your WordPress users and members with MailChimp lists.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:58:"https://downloads.wordpress.org/plugin/pmpro-mailchimp.zip";s:7:"License";s:13:"wordpress.org";}i:36;a:11:{s:4:"Name";s:43:"Paid Memberships Pro - Member Badges Add On";s:5:"Title";s:43:"Paid Memberships Pro - Member Badges Add On";s:9:"PluginURI";s:54:"http://www.paidmembershipspro.com/pmpro-member-badges/";s:4:"Slug";s:19:"pmpro-member-badges";s:6:"plugin";s:43:"pmpro-member-badges/pmpro-member-badges.php";s:7:"Version";s:4:".3.1";s:11:"Description";s:115:"Assign unique member badges (images) to each membership level and display via a shortcode or template PHP function.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:76:"http://license.paidmembershipspro.com/downloads/plus/pmpro-member-badges.zip";s:7:"License";s:4:"plus";}i:37;a:11:{s:4:"Name";s:46:"Paid Memberships Pro - Member Directory Add On";s:5:"Title";s:46:"Paid Memberships Pro - Member Directory Add On";s:9:"PluginURI";s:60:"http://www.paidmembershipspro.com/wp/pmpro-member-directory/";s:4:"Slug";s:22:"pmpro-member-directory";s:6:"plugin";s:49:"pmpro-member-directory/pmpro-member-directory.php";s:7:"Version";s:4:".5.1";s:11:"Description";s:81:"Adds a customizable Member Directory and Member Profiles to your membership site.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:79:"http://license.paidmembershipspro.com/downloads/plus/pmpro-member-directory.zip";s:7:"License";s:4:"plus";}i:38;a:11:{s:4:"Name";s:44:"Paid Memberships Pro - Member History Add On";s:5:"Title";s:44:"Paid Memberships Pro - Member History Add On";s:9:"PluginURI";s:58:"http://www.paidmembershipspro.com/wp/pmpro-member-history/";s:4:"Slug";s:20:"pmpro-member-history";s:6:"plugin";s:45:"pmpro-member-history/pmpro-member-history.php";s:7:"Version";s:6:".2.4.2";s:11:"Description";s:77:"Display a history of a user\'s Membership on the User Profile for admins only.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:77:"http://license.paidmembershipspro.com/downloads/plus/pmpro-member-history.zip";s:7:"License";s:4:"plus";}i:39;a:11:{s:4:"Name";s:46:"Paid Memberships Pro - Member Homepages Add On";s:5:"Title";s:46:"Paid Memberships Pro - Member Homepages Add On";s:9:"PluginURI";s:57:"http://www.paidmembershipspro.com/pmpro-member-homepages/";s:4:"Slug";s:22:"pmpro-member-homepages";s:6:"plugin";s:49:"pmpro-member-homepages/pmpro-member-homepages.php";s:7:"Version";s:2:".1";s:11:"Description";s:72:"Redirect members to a unique homepage/landing page based on their level.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:79:"http://license.paidmembershipspro.com/downloads/plus/pmpro-member-homepages.zip";s:7:"License";s:4:"plus";}i:40;a:11:{s:4:"Name";s:50:"Paid Memberships Pro - Member Network Sites Add On";s:5:"Title";s:50:"Paid Memberships Pro - Member Network Sites Add On";s:9:"PluginURI";s:48:"http://www.paidmembershipspro.com/network-sites/";s:4:"Slug";s:13:"pmpro-network";s:6:"plugin";s:31:"pmpro-network/pmpro-network.php";s:7:"Version";s:2:".5";s:11:"Description";s:76:"Create a network site for the member as part of membership to the main site.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:70:"http://license.paidmembershipspro.com/downloads/plus/pmpro-network.zip";s:7:"License";s:4:"plus";}i:41;a:11:{s:4:"Name";s:40:"Paid Memberships Pro - Member RSS Add On";s:5:"Title";s:40:"Paid Memberships Pro - Member RSS Add On";s:9:"PluginURI";s:54:"http://www.paidmembershipspro.com/wp/pmpro-member-rss/";s:4:"Slug";s:16:"pmpro-member-rss";s:6:"plugin";s:37:"pmpro-member-rss/pmpro-member-rss.php";s:7:"Version";s:2:".2";s:11:"Description";s:57:"Create Member-Specific RSS Feeds for Paid Memberships Pro";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:73:"http://license.paidmembershipspro.com/downloads/plus/pmpro-member-rss.zip";s:7:"License";s:4:"plus";}i:42;a:11:{s:4:"Name";s:45:"Paid Memberships Pro - Membership Card Add On";s:5:"Title";s:45:"Paid Memberships Pro - Membership Card Add On";s:9:"PluginURI";s:59:"http://www.paidmembershipspro.com/wp/pmpro-membership-card/";s:4:"Slug";s:21:"pmpro-membership-card";s:6:"plugin";s:47:"pmpro-membership-card/pmpro-membership-card.php";s:7:"Version";s:2:".4";s:11:"Description";s:81:"Display a printable Membership Card for Paid Memberships Pro members or WP users.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:78:"http://license.paidmembershipspro.com/downloads/plus/pmpro-membership-card.zip";s:7:"License";s:4:"plus";}i:43;a:11:{s:4:"Name";s:53:"Paid Memberships Pro - Membership Manager Role Add On";s:5:"Title";s:53:"Paid Memberships Pro - Membership Manager Role Add On";s:9:"PluginURI";s:73:"https://www.paidmembershipspro.com/add-ons/pmpro-membership-manager-role/";s:4:"Slug";s:29:"pmpro-membership-manager-role";s:6:"plugin";s:63:"pmpro-membership-manager-role/pmpro-membership-manager-role.php";s:7:"Version";s:4:".3.1";s:11:"Description";s:86:"Adds a Membership Manager role to WordPress with access to PMPro settings and reports.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:34:"https://www.paidmembershipspro.com";s:8:"Download";s:86:"http://license.paidmembershipspro.com/downloads/plus/pmpro-membership-manager-role.zip";s:7:"License";s:4:"plus";}i:44;a:11:{s:4:"Name";s:50:"Paid Memberships Pro - Multisite Membership Add On";s:5:"Title";s:50:"Paid Memberships Pro - Multisite Membership Add On";s:9:"PluginURI";s:65:"http://www.paidmembershipspro.com/add-ons/pmpro-network-subsites/";s:4:"Slug";s:21:"pmpro-network-subsite";s:6:"plugin";s:47:"pmpro-network-subsite/pmpro-network-subsite.php";s:7:"Version";s:4:".4.1";s:11:"Description";s:139:"Manage memberships at the network’s main site (the primary domain of the network) and provide/restrict access on subsites in the network.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:78:"http://license.paidmembershipspro.com/downloads/plus/pmpro-network-subsite.zip";s:7:"License";s:4:"plus";}i:45;a:11:{s:4:"Name";s:39:"Paid Memberships Pro - Nav Menus Add On";s:5:"Title";s:39:"Paid Memberships Pro - Nav Menus Add On";s:9:"PluginURI";s:59:"https://www.paidmembershipspro.com/add-ons/pmpro-nav-menus/";s:4:"Slug";s:15:"pmpro-nav-menus";s:6:"plugin";s:35:"pmpro-nav-menus/pmpro-nav-menus.php";s:7:"Version";s:4:".3.2";s:11:"Description";s:100:"Creates member navigation menus and swaps your theme\'s navigation based on a user\'s Membership Level";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:34:"https://www.paidmembershipspro.com";s:8:"Download";s:72:"http://license.paidmembershipspro.com/downloads/plus/pmpro-nav-menus.zip";s:7:"License";s:4:"plus";}i:46;a:11:{s:4:"Name";s:42:"Paid Memberships Pro - Pay by Check Add On";s:5:"Title";s:42:"Paid Memberships Pro - Pay by Check Add On";s:9:"PluginURI";s:69:"https://www.paidmembershipspro.com/add-ons/pmpro-pay-by-check-add-on/";s:4:"Slug";s:18:"pmpro-pay-by-check";s:6:"plugin";s:41:"pmpro-pay-by-check/pmpro-pay-by-check.php";s:7:"Version";s:4:".7.8";s:11:"Description";s:106:"A collection of customizations useful when allowing users to pay by check for Paid Memberships Pro levels.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:34:"https://www.paidmembershipspro.com";s:8:"Download";s:75:"http://license.paidmembershipspro.com/downloads/plus/pmpro-pay-by-check.zip";s:7:"License";s:4:"plus";}i:47;a:11:{s:4:"Name";s:60:"Paid Memberships Pro - Post Affiliate Pro Integration Add On";s:5:"Title";s:60:"Paid Memberships Pro - Post Affiliate Pro Integration Add On";s:9:"PluginURI";s:62:"http://www.paidmembershipspro.com/wp/pmpro-post-affiliate-pro/";s:4:"Slug";s:24:"pmpro-post-affiliate-pro";s:6:"plugin";s:53:"pmpro-post-affiliate-pro/pmpro-post-affiliate-pro.php";s:7:"Version";s:6:".2.1.1";s:11:"Description";s:67:"Process an affiliate via Post Affiliate Pro after a PMPro checkout.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:81:"http://license.paidmembershipspro.com/downloads/plus/pmpro-post-affiliate-pro.zip";s:7:"License";s:4:"plus";}i:48;a:11:{s:4:"Name";s:39:"Paid Memberships Pro - Proration Add On";s:5:"Title";s:39:"Paid Memberships Pro - Proration Add On";s:9:"PluginURI";s:53:"http://www.paidmembershipspro.com/wp/pmpro-proration/";s:4:"Slug";s:15:"pmpro-proration";s:6:"plugin";s:35:"pmpro-proration/pmpro-proration.php";s:7:"Version";s:2:".2";s:11:"Description";s:46:"Custom Prorating Code for Paid Memberships Pro";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:72:"http://license.paidmembershipspro.com/downloads/plus/pmpro-proration.zip";s:7:"License";s:4:"plus";}i:49;a:11:{s:4:"Name";s:46:"Paid Memberships Pro - Recurring Emails Add On";s:5:"Title";s:46:"Paid Memberships Pro - Recurring Emails Add On";s:9:"PluginURI";s:60:"http://www.paidmembershipspro.com/wp/pmpro-recurring-emails/";s:4:"Slug";s:22:"pmpro-recurring-emails";s:6:"plugin";s:49:"pmpro-recurring-emails/pmpro-recurring-emails.php";s:7:"Version";s:2:".5";s:11:"Description";s:93:"Send email message(s) X days before a recurring payment is scheduled, to warn/remind members.";s:6:"Author";s:64:"Stranger Studios, Thomas Sjolshagen <thomas@eighty20results.com>";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:79:"http://license.paidmembershipspro.com/downloads/plus/pmpro-recurring-emails.zip";s:7:"License";s:4:"plus";}i:50;a:11:{s:4:"Name";s:45:"Paid Memberships Pro - Register Helper Add On";s:5:"Title";s:45:"Paid Memberships Pro - Register Helper Add On";s:9:"PluginURI";s:97:"https://www.paidmembershipspro.com/add-ons/pmpro-register-helper-add-checkout-and-profile-fields/";s:4:"Slug";s:21:"pmpro-register-helper";s:6:"plugin";s:47:"pmpro-register-helper/pmpro-register-helper.php";s:7:"Version";s:5:"1.3.6";s:11:"Description";s:106:"Capture additional member information with custom fields at Membership Checkout with Paid Memberships Pro.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:34:"https://www.paidmembershipspro.com";s:8:"Download";s:78:"http://license.paidmembershipspro.com/downloads/free/pmpro-register-helper.zip";s:7:"License";s:13:"wordpress.org";}i:51;a:11:{s:4:"Name";s:47:"Paid Memberships Pro - Reports Dashboard Add On";s:5:"Title";s:47:"Paid Memberships Pro - Reports Dashboard Add On";s:9:"PluginURI";s:61:"http://www.paidmembershipspro.com/wp/pmpro-reports-dashboard/";s:4:"Slug";s:23:"pmpro-reports-dashboard";s:6:"plugin";s:51:"pmpro-reports-dashboard/pmpro-reports-dashboard.php";s:7:"Version";s:2:".2";s:11:"Description";s:86:"Responsive Membership Reports Dashboard for Administrator and Membership Manager Role.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:80:"http://license.paidmembershipspro.com/downloads/plus/pmpro-reports-dashboard.zip";s:7:"License";s:4:"plus";}i:52;a:11:{s:4:"Name";s:50:"Paid Memberships Pro - Set Expiration Dates Add On";s:5:"Title";s:50:"Paid Memberships Pro - Set Expiration Dates Add On";s:9:"PluginURI";s:64:"http://www.paidmembershipspro.com/wp/pmpro-set-expiration-dates/";s:4:"Slug";s:26:"pmpro-set-expiration-dates";s:6:"plugin";s:57:"pmpro-set-expiration-dates/pmpro-set-expiration-dates.php";s:7:"Version";s:2:".3";s:11:"Description";s:95:"Set a specific expiration date (e.g. 2013-12-31) for a PMPro membership level or discount code.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:83:"http://license.paidmembershipspro.com/downloads/plus/pmpro-set-expiration-dates.zip";s:7:"License";s:4:"plus";}i:53;a:11:{s:4:"Name";s:38:"Paid Memberships Pro - Shipping Add On";s:5:"Title";s:38:"Paid Memberships Pro - Shipping Add On";s:9:"PluginURI";s:80:"https://www.paidmembershipspro.com/add-ons/shipping-address-membership-checkout/";s:4:"Slug";s:14:"pmpro-shipping";s:6:"plugin";s:33:"pmpro-shipping/pmpro-shipping.php";s:7:"Version";s:2:".4";s:11:"Description";s:52:"Add shipping to the checkout page and other updates.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:34:"https://www.paidmembershipspro.com";s:8:"Download";s:71:"http://license.paidmembershipspro.com/downloads/plus/pmpro-shipping.zip";s:7:"License";s:4:"plus";}i:54;a:11:{s:4:"Name";s:39:"Paid Memberships Pro - Signup Shortcode";s:5:"Title";s:39:"Paid Memberships Pro - Signup Shortcode";s:9:"PluginURI";s:60:"http://www.paidmembershipspro.com/wp/pmpro-signup-shortcode/";s:4:"Slug";s:22:"pmpro-signup-shortcode";s:6:"plugin";s:49:"pmpro-signup-shortcode/pmpro-signup-shortcode.php";s:7:"Version";s:2:".1";s:11:"Description";s:94:"Shortcode for a simplified Membership Signup Form with options for email only signup and more.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:79:"http://license.paidmembershipspro.com/downloads/plus/pmpro-signup-shortcode.zip";s:7:"License";s:4:"plus";}i:55;a:11:{s:4:"Name";s:40:"Paid Memberships Pro - Slack Integration";s:5:"Title";s:40:"Paid Memberships Pro - Slack Integration";s:9:"PluginURI";s:0:"";s:4:"Slug";s:11:"pmpro-slack";s:6:"plugin";s:27:"pmpro-slack/pmpro-slack.php";s:7:"Version";s:3:"1.0";s:11:"Description";s:53:"Slack integration for the Paid Memberships Pro plugin";s:6:"Author";s:12:"Nikhil Vimal";s:9:"AuthorURI";s:24:"http://nik.techvoltz.com";s:8:"Download";s:68:"http://license.paidmembershipspro.com/downloads/plus/pmpro-slack.zip";s:7:"License";s:4:"plus";}i:56;a:11:{s:4:"Name";s:47:"Paid Memberships Pro - Sponsored Members Add On";s:5:"Title";s:47:"Paid Memberships Pro - Sponsored Members Add On";s:9:"PluginURI";s:66:"http://www.paidmembershipspro.com/add-ons/pmpro-sponsored-members/";s:4:"Slug";s:23:"pmpro-sponsored-members";s:6:"plugin";s:51:"pmpro-sponsored-members/pmpro-sponsored-members.php";s:7:"Version";s:4:".6.2";s:11:"Description";s:84:"Generate discount code for a main account holder to distribute to sponsored members.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:80:"http://license.paidmembershipspro.com/downloads/plus/pmpro-sponsored-members.zip";s:7:"License";s:4:"plus";}i:57;a:11:{s:4:"Name";s:45:"Paid Memberships Pro - State Dropdowns Add On";s:5:"Title";s:45:"Paid Memberships Pro - State Dropdowns Add On";s:9:"PluginURI";s:0:"";s:4:"Slug";s:21:"pmpro-state-dropdowns";s:6:"plugin";s:47:"pmpro-state-dropdowns/pmpro-state-dropdowns.php";s:7:"Version";s:3:"0.1";s:11:"Description";s:85:"Creates an autopopulated field for countries and states/provinces for billing fields.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:27:"https://strangerstuidos.com";s:8:"Download";s:78:"http://license.paidmembershipspro.com/downloads/plus/pmpro-state-dropdowns.zip";s:7:"License";s:4:"plus";}i:58;a:11:{s:4:"Name";s:48:"Paid Memberships Pro - Subscription Delays Addon";s:5:"Title";s:48:"Paid Memberships Pro - Subscription Delays Addon";s:9:"PluginURI";s:63:"http://www.paidmembershipspro.com/wp/pmpro-subscription-delays/";s:4:"Slug";s:25:"pmpro-subscription-delays";s:6:"plugin";s:55:"pmpro-subscription-delays/pmpro-subscription-delays.php";s:7:"Version";s:4:".4.3";s:11:"Description";s:138:"Add a field to levels and discount codes to delay the start of a subscription by X days. (Add variable-length free trials to your levels.)";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:82:"http://license.paidmembershipspro.com/downloads/plus/pmpro-subscription-delays.zip";s:7:"License";s:4:"plus";}i:59;a:11:{s:4:"Name";s:40:"Paid Memberships Pro - User Pages Add On";s:5:"Title";s:40:"Paid Memberships Pro - User Pages Add On";s:9:"PluginURI";s:51:"http://www.paidmembershipspro.com/pmpro-user-pages/";s:4:"Slug";s:16:"pmpro-user-pages";s:6:"plugin";s:37:"pmpro-user-pages/pmpro-user-pages.php";s:7:"Version";s:4:".5.3";s:11:"Description";s:88:"When a user signs up, create a page for them that only they (and admins) have access to.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:73:"http://license.paidmembershipspro.com/downloads/plus/pmpro-user-pages.zip";s:7:"License";s:4:"plus";}i:60;a:11:{s:4:"Name";s:30:"Paid Memberships Pro - VAT Tax";s:5:"Title";s:30:"Paid Memberships Pro - VAT Tax";s:9:"PluginURI";s:51:"http://www.paidmembershipspro.com/wp/pmpro-vat-tax/";s:4:"Slug";s:13:"pmpro-vat-tax";s:6:"plugin";s:31:"pmpro-vat-tax/pmpro-vat-tax.php";s:7:"Version";s:2:".5";s:11:"Description";s:114:"Calculate VAT tax at checkout and allow customers with a VAT Number lookup for VAT tax exemptions in EU countries.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:70:"http://license.paidmembershipspro.com/downloads/plus/pmpro-vat-tax.zip";s:7:"License";s:4:"plus";}i:61;a:11:{s:4:"Name";s:41:"Paid Memberships Pro - WooCommerce Add On";s:5:"Title";s:41:"Paid Memberships Pro - WooCommerce Add On";s:9:"PluginURI";s:52:"http://www.paidmembershipspro.com/pmpro-woocommerce/";s:4:"Slug";s:17:"pmpro-woocommerce";s:6:"plugin";s:39:"pmpro-woocommerce/pmpro-woocommerce.php";s:7:"Version";s:5:"1.4.5";s:11:"Description";s:48:"Integrate WooCommerce with Paid Memberships Pro.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:60:"https://downloads.wordpress.org/plugin/pmpro-woocommerce.zip";s:7:"License";s:13:"wordpress.org";}i:62;a:11:{s:4:"Name";s:52:"Paid Memberships Pro - WordPress Social Login Add On";s:5:"Title";s:52:"Paid Memberships Pro - WordPress Social Login Add On";s:9:"PluginURI";s:56:"http://www.paidmembershipspro.com/wp/pmpro-social-login/";s:4:"Slug";s:18:"pmpro-social-login";s:6:"plugin";s:41:"pmpro-social-login/pmpro-social-login.php";s:7:"Version";s:2:".2";s:11:"Description";s:111:"Allow users to create membership account via social networks as configured via WordPress Social Login by Miled.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:75:"http://license.paidmembershipspro.com/downloads/plus/pmpro-social-login.zip";s:7:"License";s:4:"plus";}i:63;a:11:{s:4:"Name";s:63:"Paid Memberships Pro - WP Affiliate Platform Integration Add On";s:5:"Title";s:63:"Paid Memberships Pro - WP Affiliate Platform Integration Add On";s:9:"PluginURI";s:47:"http://www.paidmembershipspro.com/wp/pmpro-dev/";s:4:"Slug";s:27:"pmpro-wp-affiliate-platform";s:6:"plugin";s:59:"pmpro-wp-affiliate-platform/pmpro-wp-affiliate-platform.php";s:7:"Version";s:5:"1.7.1";s:11:"Description";s:70:"Process an affiliate via WP Affiliate Platform after a PMPro checkout.";s:6:"Author";s:36:"Stranger Studios, Tips and Tricks HQ";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:84:"http://license.paidmembershipspro.com/downloads/plus/pmpro-wp-affiliate-platform.zip";s:7:"License";s:4:"plus";}i:64;a:11:{s:4:"Name";s:14:"PMPro Gift Aid";s:5:"Title";s:14:"PMPro Gift Aid";s:9:"PluginURI";s:52:"http://www.paidmembershipspro.com/wp/pmpro-gift-aid/";s:4:"Slug";s:14:"pmpro-gift-aid";s:6:"plugin";s:33:"pmpro-gift-aid/pmpro-gift-aid.php";s:7:"Version";s:4:".1.2";s:11:"Description";s:40:"Add checkbox to opt into the UK Gift Aid";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:71:"http://license.paidmembershipspro.com/downloads/free/pmpro-gift-aid.zip";s:7:"License";s:4:"plus";}i:65;a:11:{s:4:"Name";s:25:"PMPro Levels as UL Layout";s:5:"Title";s:25:"PMPro Levels as UL Layout";s:9:"PluginURI";s:53:"http://www.paidmembershipspro.com/wp/pmpro-ul-levels/";s:4:"Slug";s:15:"pmpro-ul-levels";s:6:"plugin";s:35:"pmpro-ul-levels/pmpro-ul-levels.php";s:7:"Version";s:2:".1";s:11:"Description";s:107:"Display your Membership Levels page in a UL Layout (includes classes for Foundation by ZURB Pricing table).";s:6:"Author";s:42:"Stranger Studios / Tweaked by Lasse Larsen";s:9:"AuthorURI";s:28:"http://www.seventysixnyc.com";s:8:"Download";s:72:"http://license.paidmembershipspro.com/downloads/plus/pmpro-ul-levels.zip";s:7:"License";s:4:"plus";}i:66;a:11:{s:4:"Name";s:30:"PMPro Payflow Recurring Orders";s:5:"Title";s:30:"PMPro Payflow Recurring Orders";s:9:"PluginURI";s:68:"http://www.paidmembershipspro.com/wp/pmpro-payflow-recurring-orders/";s:4:"Slug";s:30:"pmpro-payflow-recurring-orders";s:6:"plugin";s:65:"pmpro-payflow-recurring-orders/pmpro-payflow-recurring-orders.php";s:7:"Version";s:4:".1.1";s:11:"Description";s:72:"Check daily for new recurring orders in Payflow and add as PMPro orders.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:87:"http://license.paidmembershipspro.com/downloads/plus/pmpro-payflow-recurring-orders.zip";s:7:"License";s:4:"plus";}i:67;a:11:{s:4:"Name";s:27:"PMPro Reason For Cancelling";s:5:"Title";s:27:"PMPro Reason For Cancelling";s:9:"PluginURI";s:65:"http://www.paidmembershipspro.com/wp/pmpro-reason-for-cancelling/";s:4:"Slug";s:27:"pmpro-reason-for-cancelling";s:6:"plugin";s:59:"pmpro-reason-for-cancelling/pmpro-reason-for-cancelling.php";s:7:"Version";s:4:".1.1";s:11:"Description";s:67:"Adds a field to specify a reason for cancelling to the cancel page.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:84:"http://license.paidmembershipspro.com/downloads/plus/pmpro-reason-for-cancelling.zip";s:7:"License";s:4:"plus";}i:68;a:11:{s:4:"Name";s:11:"PMPro Roles";s:5:"Title";s:11:"PMPro Roles";s:9:"PluginURI";s:22:"http://joshlevinson.me";s:4:"Slug";s:11:"pmpro-roles";s:6:"plugin";s:27:"pmpro-roles/pmpro-roles.php";s:7:"Version";s:3:"1.0";s:11:"Description";s:160:"Adds a WordPress Role for each Membership Level with Display Name = Membership Level Name and Role Name = \'pmpro_role_X\' (where X is the Membership Level\'s ID).";s:6:"Author";s:13:"Josh Levinson";s:9:"AuthorURI";s:22:"http://joshlevinson.me";s:8:"Download";s:68:"http://license.paidmembershipspro.com/downloads/plus/pmpro-roles.zip";s:7:"License";s:4:"free";}i:69;a:11:{s:4:"Name";s:12:"PMPro Series";s:5:"Title";s:12:"PMPro Series";s:9:"PluginURI";s:47:"http://www.paidmembershipspro.com/pmpro-series/";s:4:"Slug";s:12:"pmpro-series";s:6:"plugin";s:29:"pmpro-series/pmpro-series.php";s:7:"Version";s:4:".3.7";s:11:"Description";s:59:"Offer serialized (drip feed) content to your PMPro members.";s:6:"Author";s:16:"Stranger Studios";s:9:"AuthorURI";s:30:"http://www.strangerstudios.com";s:8:"Download";s:69:"http://license.paidmembershipspro.com/downloads/plus/pmpro-series.zip";s:7:"License";s:4:"plus";}i:70;a:11:{s:4:"Name";s:19:"PMPro Social Locker";s:5:"Title";s:19:"PMPro Social Locker";s:9:"PluginURI";s:0:"";s:4:"Slug";s:19:"pmpro-social-locker";s:6:"plugin";s:43:"pmpro-social-locker/pmpro-social-locker.php";s:7:"Version";s:4:".1.1";s:11:"Description";s:194:"Integrate PMPro with the Social Locker plugin from OnePress (http://wordpress.org/support/plugin/social-locker). The goal is to give a user a free membership if they interact with Social Locker.";s:6:"Author";s:45:"Scott Sousa (Slocum Studio), Stranger Studios";s:9:"AuthorURI";s:28:"http://www.slocumstudio.com/";s:8:"Download";s:76:"http://license.paidmembershipspro.com/downloads/plus/pmpro-social-locker.zip";s:7:"License";s:4:"plus";}i:71;a:11:{s:4:"Name";s:22:"PMPro Strong Passwords";s:5:"Title";s:22:"PMPro Strong Passwords";s:9:"PluginURI";s:85:"http://www.paidmembershipspro.com/add-ons/plugins-on-github/require-strong-passwords/";s:4:"Slug";s:22:"pmpro-strong-passwords";s:6:"plugin";s:49:"pmpro-strong-passwords/pmpro-strong-passwords.php";s:7:"Version";s:3:"0.1";s:11:"Description";s:39:"Force users to submit strong passwords.";s:6:"Author";s:11:"Scott Sousa";s:9:"AuthorURI";s:23:"http://slocumstudio.com";s:8:"Download";s:79:"http://license.paidmembershipspro.com/downloads/plus/pmpro-strong-passwords.zip";s:7:"License";s:4:"plus";}i:72;a:11:{s:4:"Name";s:10:"WP Bouncer";s:5:"Title";s:10:"WP Bouncer";s:9:"PluginURI";s:45:"http://andrewnorcross.com/plugins/wp-bouncer/";s:4:"Slug";s:10:"wp-bouncer";s:6:"plugin";s:25:"wp-bouncer/wp-bouncer.php";s:7:"Version";s:5:"1.3.1";s:11:"Description";s:64:"Only allow one device to be logged into WordPress for each user.";s:6:"Author";s:32:"Andrew Norcross, strangerstudios";s:9:"AuthorURI";s:25:"http://andrewnorcross.com";s:8:"Download";s:53:"https://downloads.wordpress.org/plugin/wp-bouncer.zip";s:7:"License";s:13:"wordpress.org";}}', 'no'),
(851, 'pmpro_addons_timestamp', '1507705950', 'no') ;
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(874, 'wpjb_form_job', 'a:2:{s:5:"field";a:18:{s:9:"job_title";a:13:{s:5:"title";s:5:"Title";s:4:"name";s:9:"job_title";s:4:"hint";s:0:"";s:7:"default";s:0:"";s:3:"css";s:0:"";s:11:"is_required";i:1;s:10:"is_trashed";s:1:"0";s:10:"is_builtin";i:1;s:4:"type";s:13:"ui-input-text";s:11:"placeholder";s:20:"Enter Job Title Here";s:8:"is_saved";i:1;s:5:"order";i:0;s:5:"group";s:3:"job";}s:15:"job_description";a:14:{s:5:"title";s:27:"Summary of Responsibilities";s:4:"name";s:15:"job_description";s:4:"hint";s:0:"";s:7:"default";s:0:"";s:3:"css";s:18:"wpjb-textarea-wide";s:11:"is_required";i:1;s:10:"is_trashed";s:1:"0";s:10:"is_builtin";i:1;s:4:"type";s:17:"ui-input-textarea";s:10:"visibility";s:1:"0";s:16:"textarea_wysiwyg";s:1:"1";s:8:"is_saved";i:1;s:5:"order";i:1;s:5:"group";s:3:"job";}s:26:"job_duties_responsbilities";a:14:{s:5:"title";s:37:"Essential Duties and Responsibilities";s:4:"name";s:26:"job_duties_responsbilities";s:4:"hint";s:55:"Will be shown as a list. Enter each item on a new line.";s:7:"default";s:0:"";s:3:"css";s:0:"";s:11:"is_required";i:0;s:10:"is_trashed";s:1:"1";s:10:"is_builtin";i:0;s:4:"type";s:17:"ui-input-textarea";s:10:"visibility";s:1:"0";s:16:"textarea_wysiwyg";s:1:"0";s:8:"is_saved";i:1;s:5:"order";i:-1;s:5:"group";s:0:"";}s:20:"job_skills_abilities";a:14:{s:5:"title";s:29:"Required Skills and Abilities";s:4:"name";s:20:"job_skills_abilities";s:4:"hint";s:55:"Will be shown as a list. Enter each item on a new line.";s:7:"default";s:0:"";s:3:"css";s:0:"";s:11:"is_required";i:0;s:10:"is_trashed";s:1:"0";s:10:"is_builtin";i:0;s:4:"type";s:17:"ui-input-textarea";s:10:"visibility";s:1:"0";s:16:"textarea_wysiwyg";s:1:"0";s:8:"is_saved";i:1;s:5:"order";i:3;s:5:"group";s:3:"job";}s:4:"type";a:17:{s:5:"title";s:8:"Job Type";s:4:"name";s:4:"type";s:4:"hint";s:0:"";s:7:"default";a:0:{}s:3:"css";s:0:"";s:11:"is_required";i:0;s:10:"is_trashed";s:1:"0";s:10:"is_builtin";i:1;s:4:"type";s:15:"ui-input-select";s:11:"fill_method";s:0:"";s:12:"fill_choices";s:0:"";s:13:"fill_callback";s:0:"";s:14:"select_choices";i:1;s:12:"empty_option";s:0:"";s:8:"is_saved";i:1;s:5:"order";i:4;s:5:"group";s:3:"job";}s:8:"category";a:18:{s:5:"title";s:8:"Category";s:4:"name";s:8:"category";s:4:"hint";s:0:"";s:7:"default";a:0:{}s:3:"css";s:0:"";s:11:"is_required";i:0;s:10:"is_trashed";s:1:"0";s:10:"is_builtin";i:1;s:4:"type";s:15:"ui-input-select";s:10:"visibility";s:1:"0";s:11:"fill_method";s:7:"default";s:12:"fill_choices";s:0:"";s:13:"fill_callback";s:0:"";s:14:"select_choices";i:1;s:12:"empty_option";i:0;s:8:"is_saved";i:1;s:5:"order";i:5;s:5:"group";s:3:"job";}s:12:"company_name";a:15:{s:5:"title";s:12:"Company Name";s:4:"name";s:12:"company_name";s:4:"hint";s:0:"";s:7:"default";s:0:"";s:3:"css";s:0:"";s:11:"is_required";i:1;s:10:"is_trashed";s:1:"0";s:10:"is_builtin";i:1;s:4:"type";s:13:"ui-input-text";s:11:"placeholder";s:0:"";s:8:"is_saved";i:1;s:10:"visibility";s:1:"0";s:16:"validation_rules";s:0:"";s:5:"order";i:6;s:5:"group";s:7:"company";}s:7:"field_1";a:14:{s:5:"title";s:8:"Field #1";s:4:"name";s:7:"field_1";s:4:"hint";s:0:"";s:7:"default";s:0:"";s:3:"css";s:0:"";s:11:"is_required";i:0;s:10:"is_trashed";s:1:"1";s:10:"is_builtin";i:0;s:4:"type";s:17:"ui-input-textarea";s:16:"textarea_wysiwyg";s:0:"";s:8:"is_saved";i:1;s:10:"visibility";s:1:"0";s:5:"order";i:-1;s:5:"group";s:0:"";}s:13:"company_email";a:14:{s:5:"title";s:13:"Contact Email";s:4:"name";s:13:"company_email";s:4:"hint";s:0:"";s:7:"default";s:0:"";s:3:"css";s:0:"";s:11:"is_required";i:1;s:10:"is_trashed";s:1:"0";s:10:"is_builtin";i:1;s:4:"type";s:13:"ui-input-text";s:16:"validation_rules";s:18:"Daq_Validate_Email";s:11:"placeholder";s:0:"";s:8:"is_saved";i:1;s:5:"order";i:8;s:5:"group";s:7:"company";}s:11:"company_url";a:14:{s:5:"title";s:7:"Website";s:4:"name";s:11:"company_url";s:4:"hint";s:0:"";s:7:"default";s:0:"";s:3:"css";s:0:"";s:11:"is_required";i:0;s:10:"is_trashed";s:1:"0";s:10:"is_builtin";i:1;s:4:"type";s:13:"ui-input-text";s:16:"validation_rules";s:16:"Daq_Validate_Url";s:11:"placeholder";s:0:"";s:8:"is_saved";i:1;s:5:"order";i:9;s:5:"group";s:7:"company";}s:12:"company_logo";a:16:{s:5:"title";s:4:"Logo";s:4:"name";s:12:"company_logo";s:4:"hint";s:0:"";s:7:"default";s:0:"";s:3:"css";s:0:"";s:11:"is_required";i:0;s:10:"is_trashed";s:1:"0";s:10:"is_builtin";i:1;s:4:"type";s:13:"ui-input-file";s:8:"file_ext";s:19:"jpg, jpeg, gif, png";s:9:"file_size";i:300000;s:8:"file_num";i:1;s:8:"is_saved";i:1;s:5:"order";i:10;s:5:"group";s:7:"company";s:8:"renderer";s:22:"wpjb_form_field_upload";}s:11:"job_country";a:17:{s:5:"title";s:7:"Country";s:4:"name";s:11:"job_country";s:4:"hint";s:0:"";s:7:"default";s:3:"840";s:3:"css";s:21:"wpjb-location-country";s:11:"is_required";i:0;s:10:"is_trashed";s:1:"0";s:10:"is_builtin";i:1;s:4:"type";s:15:"ui-input-select";s:11:"fill_method";s:0:"";s:12:"fill_choices";s:0:"";s:13:"fill_callback";s:0:"";s:14:"select_choices";i:1;s:12:"empty_option";s:0:"";s:8:"is_saved";i:1;s:5:"order";i:11;s:5:"group";s:8:"location";}s:9:"job_state";a:13:{s:5:"title";s:5:"State";s:4:"name";s:9:"job_state";s:4:"hint";s:0:"";s:7:"default";s:0:"";s:3:"css";s:19:"wpjb-location-state";s:11:"is_required";i:0;s:10:"is_trashed";s:1:"0";s:10:"is_builtin";i:1;s:4:"type";s:13:"ui-input-text";s:11:"placeholder";s:0:"";s:8:"is_saved";i:1;s:5:"order";i:12;s:5:"group";s:8:"location";}s:12:"job_zip_code";a:13:{s:5:"title";s:8:"Zip-Code";s:4:"name";s:12:"job_zip_code";s:4:"hint";s:0:"";s:7:"default";s:0:"";s:3:"css";s:0:"";s:11:"is_required";i:0;s:10:"is_trashed";s:1:"0";s:10:"is_builtin";i:1;s:4:"type";s:13:"ui-input-text";s:11:"placeholder";s:0:"";s:8:"is_saved";i:1;s:5:"order";i:13;s:5:"group";s:8:"location";}s:8:"job_city";a:13:{s:5:"title";s:4:"City";s:4:"name";s:8:"job_city";s:4:"hint";s:62:"For example: "Chicago", "London", "Anywhere" or "Telecommute".";s:7:"default";s:0:"";s:3:"css";s:18:"wpjb-location-city";s:11:"is_required";i:1;s:10:"is_trashed";s:1:"0";s:10:"is_builtin";i:1;s:4:"type";s:13:"ui-input-text";s:11:"placeholder";s:0:"";s:8:"is_saved";i:1;s:5:"order";i:14;s:5:"group";s:8:"location";}s:7:"listing";a:16:{s:5:"title";s:12:"Listing Type";s:4:"name";s:7:"listing";s:4:"hint";s:0:"";s:7:"default";s:0:"";s:3:"css";s:0:"";s:11:"is_required";i:1;s:10:"is_trashed";s:1:"0";s:10:"is_builtin";i:1;s:4:"type";s:14:"ui-input-radio";s:11:"fill_method";s:0:"";s:12:"fill_choices";s:0:"";s:13:"fill_callback";s:0:"";s:14:"select_choices";i:1;s:8:"is_saved";i:1;s:5:"order";i:15;s:5:"group";s:6:"coupon";}s:19:"company_description";a:12:{s:4:"name";s:19:"company_description";s:5:"title";s:19:"Company Description";s:4:"type";s:17:"ui-input-textarea";s:8:"is_saved";i:1;s:10:"is_builtin";i:0;s:4:"hint";s:0:"";s:11:"is_required";i:0;s:10:"visibility";s:1:"0";s:16:"textarea_wysiwyg";s:0:"";s:5:"order";i:7;s:5:"group";s:7:"company";s:10:"is_trashed";s:1:"0";}s:27:"job_duties_responsibilities";a:12:{s:4:"name";s:27:"job_duties_responsibilities";s:5:"title";s:37:"Essential Duties and Responsibilities";s:4:"type";s:17:"ui-input-textarea";s:8:"is_saved";i:0;s:10:"is_builtin";i:0;s:4:"hint";s:55:"Will be shown as a list. Enter each item on a new line.";s:11:"is_required";i:0;s:10:"visibility";s:1:"0";s:16:"textarea_wysiwyg";s:1:"0";s:5:"order";i:2;s:5:"group";s:3:"job";s:10:"is_trashed";s:1:"0";}}s:5:"group";a:4:{s:3:"job";a:7:{s:5:"title";s:15:"Job Information";s:4:"name";s:3:"job";s:5:"order";i:0;s:4:"type";s:14:"ui-input-group";s:10:"is_builtin";i:1;s:10:"is_trashed";s:1:"0";s:8:"is_saved";i:1;}s:7:"company";a:7:{s:5:"title";s:19:"Company Information";s:4:"name";s:7:"company";s:5:"order";i:1;s:4:"type";s:14:"ui-input-group";s:10:"is_builtin";i:1;s:10:"is_trashed";s:1:"0";s:8:"is_saved";i:1;}s:8:"location";a:7:{s:5:"title";s:8:"Location";s:4:"name";s:8:"location";s:5:"order";i:2;s:4:"type";s:14:"ui-input-group";s:10:"is_builtin";i:1;s:10:"is_trashed";s:1:"0";s:8:"is_saved";i:1;}s:6:"coupon";a:7:{s:5:"title";s:7:"Listing";s:4:"name";s:6:"coupon";s:5:"order";i:3;s:4:"type";s:14:"ui-input-group";s:10:"is_builtin";i:1;s:10:"is_trashed";s:1:"0";s:8:"is_saved";i:1;}}}', 'yes'),
(881, 'wpjb_payment_method', 'a:1:{s:6:"Stripe";a:7:{s:8:"disabled";s:1:"0";s:5:"title";s:0:"";s:5:"order";s:1:"0";s:10:"secret_key";s:32:"sk_test_K1n3NnMsMS5iw2vJ4c2cFPoP";s:15:"publishable_key";s:32:"pk_test_URLN0Ihl6lxKXic5zRZTCaK5";s:18:"stripe_description";s:26:"Nxt Chptr Job Board ID: %s";s:20:"statement_descriptor";s:19:"Nxt Chptr Job Board";}}', 'yes'),
(890, 'wpjb_form_apply', 'a:2:{s:5:"field";a:5:{s:14:"applicant_name";a:13:{s:5:"title";s:9:"Your name";s:4:"name";s:14:"applicant_name";s:4:"hint";s:0:"";s:7:"default";s:0:"";s:3:"css";s:0:"";s:11:"is_required";i:1;s:10:"is_trashed";s:1:"0";s:10:"is_builtin";i:1;s:4:"type";s:13:"ui-input-text";s:11:"placeholder";s:0:"";s:8:"is_saved";i:1;s:5:"order";i:0;s:5:"group";s:5:"apply";}s:5:"email";a:14:{s:5:"title";s:19:"Your e-mail address";s:4:"name";s:5:"email";s:4:"hint";s:0:"";s:7:"default";s:0:"";s:3:"css";s:0:"";s:11:"is_required";i:1;s:10:"is_trashed";s:1:"0";s:10:"is_builtin";i:1;s:4:"type";s:13:"ui-input-text";s:16:"validation_rules";s:18:"Daq_Validate_Email";s:11:"placeholder";s:0:"";s:8:"is_saved";i:1;s:5:"order";i:1;s:5:"group";s:5:"apply";}s:7:"message";a:13:{s:5:"title";s:7:"Message";s:4:"name";s:7:"message";s:4:"hint";s:0:"";s:7:"default";s:0:"";s:3:"css";s:0:"";s:11:"is_required";i:0;s:10:"is_trashed";s:1:"1";s:10:"is_builtin";i:1;s:4:"type";s:17:"ui-input-textarea";s:16:"textarea_wysiwyg";i:0;s:8:"is_saved";i:1;s:5:"order";i:-1;s:5:"group";s:0:"";}s:4:"file";a:17:{s:5:"title";s:11:"CV / Resume";s:4:"name";s:4:"file";s:4:"hint";s:0:"";s:7:"default";s:0:"";s:3:"css";s:0:"";s:11:"is_required";i:0;s:10:"is_trashed";s:1:"0";s:10:"is_builtin";i:1;s:4:"type";s:13:"ui-input-file";s:8:"file_ext";s:19:"pdf, doc, docx, txt";s:8:"file_num";s:2:"10";s:8:"is_saved";i:1;s:10:"visibility";s:1:"0";s:9:"file_size";s:0:"";s:5:"order";i:3;s:5:"group";s:5:"apply";s:8:"renderer";s:22:"wpjb_form_field_upload";}s:21:"application_linked_in";a:13:{s:4:"name";s:21:"application_linked_in";s:5:"title";s:21:"Linkedin Profile Link";s:4:"type";s:13:"ui-input-text";s:8:"is_saved";i:0;s:10:"is_builtin";i:0;s:4:"hint";s:0:"";s:11:"is_required";i:0;s:10:"visibility";s:1:"0";s:16:"validation_rules";s:0:"";s:11:"placeholder";s:0:"";s:5:"order";i:2;s:5:"group";s:5:"apply";s:10:"is_trashed";s:1:"0";}}s:5:"group";a:1:{s:5:"apply";a:7:{s:5:"title";s:5:"Apply";s:4:"name";s:5:"apply";s:5:"order";i:0;s:4:"type";s:14:"ui-input-group";s:10:"is_builtin";i:1;s:10:"is_trashed";s:1:"0";s:8:"is_saved";i:1;}}}', 'yes') ;

#
# End of data contents of table `wp_options`
# --------------------------------------------------------



#
# Delete any existing table `wp_pmpro_discount_codes`
#

DROP TABLE IF EXISTS `wp_pmpro_discount_codes`;


#
# Table structure of table `wp_pmpro_discount_codes`
#

CREATE TABLE `wp_pmpro_discount_codes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `starts` date NOT NULL,
  `expires` date NOT NULL,
  `uses` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `starts` (`starts`),
  KEY `expires` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_pmpro_discount_codes`
#

#
# End of data contents of table `wp_pmpro_discount_codes`
# --------------------------------------------------------



#
# Delete any existing table `wp_pmpro_discount_codes_levels`
#

DROP TABLE IF EXISTS `wp_pmpro_discount_codes_levels`;


#
# Table structure of table `wp_pmpro_discount_codes_levels`
#

CREATE TABLE `wp_pmpro_discount_codes_levels` (
  `code_id` int(11) unsigned NOT NULL,
  `level_id` int(11) unsigned NOT NULL,
  `initial_payment` decimal(10,2) NOT NULL DEFAULT '0.00',
  `billing_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `cycle_number` int(11) NOT NULL DEFAULT '0',
  `cycle_period` enum('Day','Week','Month','Year') DEFAULT 'Month',
  `billing_limit` int(11) NOT NULL COMMENT 'After how many cycles should billing stop?',
  `trial_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `trial_limit` int(11) NOT NULL DEFAULT '0',
  `expiration_number` int(10) unsigned NOT NULL,
  `expiration_period` enum('Day','Week','Month','Year') NOT NULL,
  PRIMARY KEY (`code_id`,`level_id`),
  KEY `initial_payment` (`initial_payment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_pmpro_discount_codes_levels`
#

#
# End of data contents of table `wp_pmpro_discount_codes_levels`
# --------------------------------------------------------



#
# Delete any existing table `wp_pmpro_discount_codes_uses`
#

DROP TABLE IF EXISTS `wp_pmpro_discount_codes_uses`;


#
# Table structure of table `wp_pmpro_discount_codes_uses`
#

CREATE TABLE `wp_pmpro_discount_codes_uses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_pmpro_discount_codes_uses`
#

#
# End of data contents of table `wp_pmpro_discount_codes_uses`
# --------------------------------------------------------



#
# Delete any existing table `wp_pmpro_membership_levelmeta`
#

DROP TABLE IF EXISTS `wp_pmpro_membership_levelmeta`;


#
# Table structure of table `wp_pmpro_membership_levelmeta`
#

CREATE TABLE `wp_pmpro_membership_levelmeta` (
  `meta_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pmpro_membership_level_id` int(10) unsigned NOT NULL,
  `meta_key` varchar(255) NOT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `pmpro_membership_level_id` (`pmpro_membership_level_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_pmpro_membership_levelmeta`
#

#
# End of data contents of table `wp_pmpro_membership_levelmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_pmpro_membership_levels`
#

DROP TABLE IF EXISTS `wp_pmpro_membership_levels`;


#
# Table structure of table `wp_pmpro_membership_levels`
#

CREATE TABLE `wp_pmpro_membership_levels` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `confirmation` longtext NOT NULL,
  `initial_payment` decimal(10,2) NOT NULL DEFAULT '0.00',
  `billing_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `cycle_number` int(11) NOT NULL DEFAULT '0',
  `cycle_period` enum('Day','Week','Month','Year') DEFAULT 'Month',
  `billing_limit` int(11) NOT NULL COMMENT 'After how many cycles should billing stop?',
  `trial_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `trial_limit` int(11) NOT NULL DEFAULT '0',
  `allow_signups` tinyint(4) NOT NULL DEFAULT '1',
  `expiration_number` int(10) unsigned NOT NULL,
  `expiration_period` enum('Day','Week','Month','Year') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `allow_signups` (`allow_signups`),
  KEY `initial_payment` (`initial_payment`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_pmpro_membership_levels`
#
INSERT INTO `wp_pmpro_membership_levels` ( `id`, `name`, `description`, `confirmation`, `initial_payment`, `billing_amount`, `cycle_number`, `cycle_period`, `billing_limit`, `trial_amount`, `trial_limit`, `allow_signups`, `expiration_number`, `expiration_period`) VALUES
(1, 'Explorer', '', '', '0.00', '0.00', 0, '', 0, '0.00', 0, 1, 0, ''),
(2, 'Community Member', '', '', '50.00', '4.99', 1, 'Year', 0, '0.00', 0, 1, 0, ''),
(3, 'Student', '', '', '130.00', '4.99', 1, 'Year', 0, '0.00', 0, 1, 0, ''),
(4, 'Sage', '', '', '250.00', '250.00', 1, 'Year', 0, '0.00', 0, 1, 0, '') ;

#
# End of data contents of table `wp_pmpro_membership_levels`
# --------------------------------------------------------



#
# Delete any existing table `wp_pmpro_membership_orders`
#

DROP TABLE IF EXISTS `wp_pmpro_membership_orders`;


#
# Table structure of table `wp_pmpro_membership_orders`
#

CREATE TABLE `wp_pmpro_membership_orders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `session_id` varchar(64) NOT NULL DEFAULT '',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `membership_id` int(11) unsigned NOT NULL DEFAULT '0',
  `paypal_token` varchar(64) NOT NULL DEFAULT '',
  `billing_name` varchar(128) NOT NULL DEFAULT '',
  `billing_street` varchar(128) NOT NULL DEFAULT '',
  `billing_city` varchar(128) NOT NULL DEFAULT '',
  `billing_state` varchar(32) NOT NULL DEFAULT '',
  `billing_zip` varchar(16) NOT NULL DEFAULT '',
  `billing_country` varchar(128) NOT NULL,
  `billing_phone` varchar(32) NOT NULL,
  `subtotal` varchar(16) NOT NULL DEFAULT '',
  `tax` varchar(16) NOT NULL DEFAULT '',
  `couponamount` varchar(16) NOT NULL DEFAULT '',
  `checkout_id` int(11) NOT NULL DEFAULT '0',
  `certificate_id` int(11) NOT NULL DEFAULT '0',
  `certificateamount` varchar(16) NOT NULL DEFAULT '',
  `total` varchar(16) NOT NULL DEFAULT '',
  `payment_type` varchar(64) NOT NULL DEFAULT '',
  `cardtype` varchar(32) NOT NULL DEFAULT '',
  `accountnumber` varchar(32) NOT NULL DEFAULT '',
  `expirationmonth` char(2) NOT NULL DEFAULT '',
  `expirationyear` varchar(4) NOT NULL DEFAULT '',
  `status` varchar(32) NOT NULL DEFAULT '',
  `gateway` varchar(64) NOT NULL,
  `gateway_environment` varchar(64) NOT NULL,
  `payment_transaction_id` varchar(64) NOT NULL,
  `subscription_transaction_id` varchar(32) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `affiliate_id` varchar(32) NOT NULL,
  `affiliate_subid` varchar(32) NOT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `session_id` (`session_id`),
  KEY `user_id` (`user_id`),
  KEY `membership_id` (`membership_id`),
  KEY `status` (`status`),
  KEY `timestamp` (`timestamp`),
  KEY `gateway` (`gateway`),
  KEY `gateway_environment` (`gateway_environment`),
  KEY `payment_transaction_id` (`payment_transaction_id`),
  KEY `subscription_transaction_id` (`subscription_transaction_id`),
  KEY `affiliate_id` (`affiliate_id`),
  KEY `affiliate_subid` (`affiliate_subid`),
  KEY `checkout_id` (`checkout_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_pmpro_membership_orders`
#
INSERT INTO `wp_pmpro_membership_orders` ( `id`, `code`, `session_id`, `user_id`, `membership_id`, `paypal_token`, `billing_name`, `billing_street`, `billing_city`, `billing_state`, `billing_zip`, `billing_country`, `billing_phone`, `subtotal`, `tax`, `couponamount`, `checkout_id`, `certificate_id`, `certificateamount`, `total`, `payment_type`, `cardtype`, `accountnumber`, `expirationmonth`, `expirationyear`, `status`, `gateway`, `gateway_environment`, `payment_transaction_id`, `subscription_transaction_id`, `timestamp`, `affiliate_id`, `affiliate_subid`, `notes`) VALUES
(1, '63280AB8F5', 'fo11sirb8n4ue8f6qtuigaa0l6', 1, 2, '', '', '', '', '', '', '', '', '50.00', '0', '', 1, 0, '', '50', '', 'Visa', 'XXXXXXXXXXXX4242', '04', '2022', 'cancelled', 'stripe', 'sandbox', 'ch_1B01M1C1Fm4mFmRs53Lv7WGZ', 'sub_BMkrHHpjDGd6z3', '2017-09-09 05:18:03', '', '', ''),
(2, '5632F1BF6D', 'uf7qavc5065n4g3lohvieg08l5', 1, 1, '', '', '', '', '', '', '', '', '0', '0', '', 2, 0, '', '0', '', '', '', '', '', '', 'free', 'sandbox', '', '', '2017-09-11 05:20:51', '', '', ''),
(3, '2471775A4C', 'uf7qavc5065n4g3lohvieg08l5', 1, 3, '', '', '', '', '', '', '', '', '130.00', '0', '', 3, 0, '', '130', '', 'Visa', 'XXXXXXXXXXXX4242', '01', '2021', 'success', 'stripe', 'sandbox', 'ch_1B0kOOC1Fm4mFmRsIh6ZeXiq', 'sub_BNVPdhzbwUE1jY', '2017-09-11 05:23:30', '', '', '') ;

#
# End of data contents of table `wp_pmpro_membership_orders`
# --------------------------------------------------------



#
# Delete any existing table `wp_pmpro_memberships_categories`
#

DROP TABLE IF EXISTS `wp_pmpro_memberships_categories`;


#
# Table structure of table `wp_pmpro_memberships_categories`
#

CREATE TABLE `wp_pmpro_memberships_categories` (
  `membership_id` int(11) unsigned NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `membership_category` (`membership_id`,`category_id`),
  UNIQUE KEY `category_membership` (`category_id`,`membership_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_pmpro_memberships_categories`
#

#
# End of data contents of table `wp_pmpro_memberships_categories`
# --------------------------------------------------------



#
# Delete any existing table `wp_pmpro_memberships_pages`
#

DROP TABLE IF EXISTS `wp_pmpro_memberships_pages`;


#
# Table structure of table `wp_pmpro_memberships_pages`
#

CREATE TABLE `wp_pmpro_memberships_pages` (
  `membership_id` int(11) unsigned NOT NULL,
  `page_id` int(11) unsigned NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `category_membership` (`page_id`,`membership_id`),
  UNIQUE KEY `membership_page` (`membership_id`,`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_pmpro_memberships_pages`
#
INSERT INTO `wp_pmpro_memberships_pages` ( `membership_id`, `page_id`, `modified`) VALUES
(1, 96, '2017-09-23 14:42:12'),
(4, 74, '2017-09-23 14:43:41') ;

#
# End of data contents of table `wp_pmpro_memberships_pages`
# --------------------------------------------------------



#
# Delete any existing table `wp_pmpro_memberships_users`
#

DROP TABLE IF EXISTS `wp_pmpro_memberships_users`;


#
# Table structure of table `wp_pmpro_memberships_users`
#

CREATE TABLE `wp_pmpro_memberships_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `membership_id` int(11) unsigned NOT NULL,
  `code_id` int(11) unsigned NOT NULL,
  `initial_payment` decimal(10,2) NOT NULL,
  `billing_amount` decimal(10,2) NOT NULL,
  `cycle_number` int(11) NOT NULL,
  `cycle_period` enum('Day','Week','Month','Year') NOT NULL DEFAULT 'Month',
  `billing_limit` int(11) NOT NULL,
  `trial_amount` decimal(10,2) NOT NULL,
  `trial_limit` int(11) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'active',
  `startdate` datetime NOT NULL,
  `enddate` datetime DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `membership_id` (`membership_id`),
  KEY `modified` (`modified`),
  KEY `code_id` (`code_id`),
  KEY `enddate` (`enddate`),
  KEY `user_id` (`user_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_pmpro_memberships_users`
#
INSERT INTO `wp_pmpro_memberships_users` ( `id`, `user_id`, `membership_id`, `code_id`, `initial_payment`, `billing_amount`, `cycle_number`, `cycle_period`, `billing_limit`, `trial_amount`, `trial_limit`, `status`, `startdate`, `enddate`, `modified`) VALUES
(1, 1, 2, 0, '50.00', '4.99', 1, 'Year', 0, '0.00', 0, 'changed', '2017-09-09 05:18:10', '2017-09-11 05:20:41', '2017-09-11 10:50:41'),
(2, 1, 1, 0, '0.00', '0.00', 0, '', 0, '0.00', 0, 'changed', '2017-09-11 05:20:41', '2017-09-11 05:23:36', '2017-09-11 10:53:36'),
(3, 1, 3, 0, '130.00', '4.99', 1, 'Year', 0, '0.00', 0, 'active', '2017-09-11 05:23:36', '0000-00-00 00:00:00', '2017-09-11 10:53:36'),
(4, 5, 1, 0, '0.00', '0.00', 0, '', 0, '0.00', 0, 'active', '2017-09-11 11:40:34', '0000-00-00 00:00:00', '2017-09-11 17:10:34'),
(5, 4, 1, 0, '0.00', '0.00', 0, '', 0, '0.00', 0, 'inactive', '2017-09-11 11:40:34', '2017-09-27 10:19:54', '2017-09-27 15:49:54'),
(6, 6, 1, 0, '0.00', '0.00', 0, '', 0, '0.00', 0, 'active', '2017-09-12 05:22:02', '0000-00-00 00:00:00', '2017-09-12 10:52:02'),
(7, 7, 1, 0, '0.00', '0.00', 0, '', 0, '0.00', 0, 'active', '2017-09-12 05:24:53', '0000-00-00 00:00:00', '2017-09-12 10:54:53'),
(8, 8, 1, 0, '0.00', '0.00', 0, '', 0, '0.00', 0, 'active', '2017-09-27 10:20:48', '0000-00-00 00:00:00', '2017-09-27 15:50:48') ;

#
# End of data contents of table `wp_pmpro_memberships_users`
# --------------------------------------------------------



#
# Delete any existing table `wp_postmeta`
#

DROP TABLE IF EXISTS `wp_postmeta`;


#
# Table structure of table `wp_postmeta`
#

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=345 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_postmeta`
#
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 4, '_edit_last', '1'),
(3, 4, '_edit_lock', '1504764277:1'),
(4, 5, '_edit_last', '1'),
(5, 5, '_edit_lock', '1505106928:1'),
(6, 7, '_edit_last', '1'),
(7, 7, '_edit_lock', '1504917222:1'),
(8, 9, '_edit_last', '1'),
(9, 9, '_edit_lock', '1504917355:1'),
(10, 11, '_edit_last', '1'),
(11, 11, '_edit_lock', '1504918796:1'),
(19, 15, '_edit_last', '1'),
(20, 15, '_edit_lock', '1504920732:1'),
(21, 17, '_edit_last', '1'),
(22, 17, '_edit_lock', '1504923623:1'),
(23, 19, '_menu_item_type', 'custom'),
(24, 19, '_menu_item_menu_item_parent', '0'),
(25, 19, '_menu_item_object_id', '19'),
(26, 19, '_menu_item_object', 'custom'),
(27, 19, '_menu_item_target', ''),
(28, 19, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(29, 19, '_menu_item_xfn', ''),
(30, 19, '_menu_item_url', 'http://nxtchptr.herokuapp.com/'),
(31, 19, '_menu_item_orphaned', '1504926525'),
(32, 20, '_menu_item_type', 'post_type'),
(33, 20, '_menu_item_menu_item_parent', '0'),
(34, 20, '_menu_item_object_id', '15'),
(35, 20, '_menu_item_object', 'page'),
(36, 20, '_menu_item_target', ''),
(37, 20, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(38, 20, '_menu_item_xfn', ''),
(39, 20, '_menu_item_url', ''),
(41, 21, '_menu_item_type', 'post_type'),
(42, 21, '_menu_item_menu_item_parent', '0'),
(43, 21, '_menu_item_object_id', '11'),
(44, 21, '_menu_item_object', 'page'),
(45, 21, '_menu_item_target', ''),
(46, 21, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(47, 21, '_menu_item_xfn', ''),
(48, 21, '_menu_item_url', ''),
(50, 22, '_menu_item_type', 'post_type'),
(51, 22, '_menu_item_menu_item_parent', '0'),
(52, 22, '_menu_item_object_id', '17'),
(53, 22, '_menu_item_object', 'page'),
(54, 22, '_menu_item_target', ''),
(55, 22, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(56, 22, '_menu_item_xfn', ''),
(57, 22, '_menu_item_url', ''),
(59, 23, '_menu_item_type', 'post_type'),
(60, 23, '_menu_item_menu_item_parent', '0'),
(61, 23, '_menu_item_object_id', '5'),
(62, 23, '_menu_item_object', 'page'),
(63, 23, '_menu_item_target', ''),
(64, 23, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(65, 23, '_menu_item_xfn', ''),
(66, 23, '_menu_item_url', ''),
(68, 24, '_menu_item_type', 'post_type'),
(69, 24, '_menu_item_menu_item_parent', '0'),
(70, 24, '_menu_item_object_id', '9'),
(71, 24, '_menu_item_object', 'page'),
(72, 24, '_menu_item_target', ''),
(73, 24, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(74, 24, '_menu_item_xfn', ''),
(75, 24, '_menu_item_url', ''),
(77, 25, '_menu_item_type', 'post_type'),
(78, 25, '_menu_item_menu_item_parent', '0'),
(79, 25, '_menu_item_object_id', '2'),
(80, 25, '_menu_item_object', 'page'),
(81, 25, '_menu_item_target', ''),
(82, 25, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(83, 25, '_menu_item_xfn', ''),
(84, 25, '_menu_item_url', ''),
(85, 25, '_menu_item_orphaned', '1504926529'),
(86, 26, '_menu_item_type', 'post_type'),
(87, 26, '_menu_item_menu_item_parent', '0'),
(88, 26, '_menu_item_object_id', '7'),
(89, 26, '_menu_item_object', 'page'),
(90, 26, '_menu_item_target', ''),
(91, 26, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(92, 26, '_menu_item_xfn', ''),
(93, 26, '_menu_item_url', ''),
(95, 27, '_menu_item_type', 'post_type'),
(96, 27, '_menu_item_menu_item_parent', '0'),
(97, 27, '_menu_item_object_id', '17'),
(98, 27, '_menu_item_object', 'page'),
(99, 27, '_menu_item_target', ''),
(100, 27, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(101, 27, '_menu_item_xfn', ''),
(102, 27, '_menu_item_url', ''),
(104, 28, '_menu_item_type', 'post_type'),
(105, 28, '_menu_item_menu_item_parent', '0'),
(106, 28, '_menu_item_object_id', '15'),
(107, 28, '_menu_item_object', 'page'),
(108, 28, '_menu_item_target', ''),
(109, 28, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(110, 28, '_menu_item_xfn', ''),
(111, 28, '_menu_item_url', ''),
(113, 29, '_menu_item_type', 'post_type'),
(114, 29, '_menu_item_menu_item_parent', '0'),
(115, 29, '_menu_item_object_id', '9') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(116, 29, '_menu_item_object', 'page'),
(117, 29, '_menu_item_target', ''),
(118, 29, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(119, 29, '_menu_item_xfn', ''),
(120, 29, '_menu_item_url', ''),
(122, 30, '_menu_item_type', 'post_type'),
(123, 30, '_menu_item_menu_item_parent', '0'),
(124, 30, '_menu_item_object_id', '7'),
(125, 30, '_menu_item_object', 'page'),
(126, 30, '_menu_item_target', ''),
(127, 30, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(128, 30, '_menu_item_xfn', ''),
(129, 30, '_menu_item_url', ''),
(131, 31, '_menu_item_type', 'post_type'),
(132, 31, '_menu_item_menu_item_parent', '0'),
(133, 31, '_menu_item_object_id', '5'),
(134, 31, '_menu_item_object', 'page'),
(135, 31, '_menu_item_target', ''),
(136, 31, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(137, 31, '_menu_item_xfn', ''),
(138, 31, '_menu_item_url', ''),
(140, 32, '_edit_lock', '1505005728:1'),
(141, 39, '_menu_item_type', 'post_type'),
(142, 39, '_menu_item_menu_item_parent', '0'),
(143, 39, '_menu_item_object_id', '11'),
(144, 39, '_menu_item_object', 'page'),
(145, 39, '_menu_item_target', ''),
(146, 39, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(147, 39, '_menu_item_xfn', ''),
(148, 39, '_menu_item_url', ''),
(159, 41, '_menu_item_type', 'post_type'),
(160, 41, '_menu_item_menu_item_parent', '0'),
(161, 41, '_menu_item_object_id', '9'),
(162, 41, '_menu_item_object', 'page'),
(163, 41, '_menu_item_target', ''),
(164, 41, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(165, 41, '_menu_item_xfn', ''),
(166, 41, '_menu_item_url', ''),
(168, 42, '_menu_item_type', 'post_type'),
(169, 42, '_menu_item_menu_item_parent', '0'),
(170, 42, '_menu_item_object_id', '7'),
(171, 42, '_menu_item_object', 'page'),
(172, 42, '_menu_item_target', ''),
(173, 42, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(174, 42, '_menu_item_xfn', ''),
(175, 42, '_menu_item_url', ''),
(177, 43, '_menu_item_type', 'post_type'),
(178, 43, '_menu_item_menu_item_parent', '0'),
(179, 43, '_menu_item_object_id', '15'),
(180, 43, '_menu_item_object', 'page'),
(181, 43, '_menu_item_target', ''),
(182, 43, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(183, 43, '_menu_item_xfn', ''),
(184, 43, '_menu_item_url', ''),
(186, 44, '_menu_item_type', 'post_type'),
(187, 44, '_menu_item_menu_item_parent', '0'),
(188, 44, '_menu_item_object_id', '17'),
(189, 44, '_menu_item_object', 'page'),
(190, 44, '_menu_item_target', ''),
(191, 44, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(192, 44, '_menu_item_xfn', ''),
(193, 44, '_menu_item_url', ''),
(195, 32, '_edit_last', '1'),
(196, 46, '_edit_last', '1'),
(197, 46, '_edit_lock', '1505005752:1'),
(198, 48, '_edit_last', '1'),
(199, 48, '_edit_lock', '1505005758:1'),
(200, 50, '_menu_item_type', 'post_type'),
(201, 50, '_menu_item_menu_item_parent', '0'),
(202, 50, '_menu_item_object_id', '48'),
(203, 50, '_menu_item_object', 'page'),
(204, 50, '_menu_item_target', ''),
(205, 50, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(206, 50, '_menu_item_xfn', ''),
(207, 50, '_menu_item_url', ''),
(209, 51, '_menu_item_type', 'post_type'),
(210, 51, '_menu_item_menu_item_parent', '0'),
(211, 51, '_menu_item_object_id', '46'),
(212, 51, '_menu_item_object', 'page'),
(213, 51, '_menu_item_target', ''),
(214, 51, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(215, 51, '_menu_item_xfn', ''),
(216, 51, '_menu_item_url', ''),
(218, 52, '_menu_item_type', 'post_type'),
(219, 52, '_menu_item_menu_item_parent', '0'),
(220, 52, '_menu_item_object_id', '32'),
(221, 52, '_menu_item_object', 'page'),
(222, 52, '_menu_item_target', ''),
(223, 52, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(224, 52, '_menu_item_xfn', ''),
(225, 52, '_menu_item_url', ''),
(227, 54, '_edit_last', '1'),
(228, 54, '_edit_lock', '1505005569:1'),
(229, 57, '_edit_last', '1'),
(230, 57, 'field_59b5f6641c4aa', 'a:14:{s:3:"key";s:19:"field_59b5f6641c4aa";s:5:"label";s:11:"Access Type";s:4:"name";s:11:"access_type";s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";s:1:"1";s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:10:"formatting";s:4:"html";s:9:"maxlength";s:0:"";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:3:{s:5:"field";s:4:"null";s:8:"operator";s:2:"==";s:5:"value";s:0:"";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:1;}'),
(231, 57, 'field_59b5f68e1c4ab', 'a:10:{s:3:"key";s:19:"field_59b5f68e1c4ab";s:5:"label";s:29:""Reflectionnaire" & Resources";s:4:"name";s:27:"reflectionnaire_&_resources";s:4:"type";s:10:"true_false";s:12:"instructions";s:0:"";s:8:"required";s:1:"0";s:7:"message";s:0:"";s:13:"default_value";s:1:"0";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:3:{s:5:"field";s:19:"field_59b5f6df1c4ac";s:8:"operator";s:2:"==";s:5:"value";s:1:"1";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:2;}'),
(232, 57, 'field_59b5f6df1c4ac', 'a:10:{s:3:"key";s:19:"field_59b5f6df1c4ac";s:5:"label";s:27:"Peer Discovery and Matching";s:4:"name";s:27:"peer_discovery_and_matching";s:4:"type";s:10:"true_false";s:12:"instructions";s:0:"";s:8:"required";s:1:"0";s:7:"message";s:0:"";s:13:"default_value";s:1:"0";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:3:{s:5:"field";s:19:"field_59b5f68e1c4ab";s:8:"operator";s:2:"==";s:5:"value";s:1:"1";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:3;}'),
(233, 57, 'field_59b5f6f41c4ad', 'a:13:{s:3:"key";s:19:"field_59b5f6f41c4ad";s:5:"label";s:23:"Why choose this option?";s:4:"name";s:22:"why_choose_this_option";s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";s:1:"1";s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:10:"formatting";s:2:"br";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:3:{s:5:"field";s:19:"field_59b5f68e1c4ab";s:8:"operator";s:2:"==";s:5:"value";s:1:"1";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:4;}'),
(234, 57, 'field_59b5f7061c4ae', 'a:10:{s:3:"key";s:19:"field_59b5f7061c4ae";s:5:"label";s:71:"Ability to "earn" suite of personality tests with 30 referral purchases";s:4:"name";s:42:"ability_to_earn_suite_of_personality_tests";s:4:"type";s:10:"true_false";s:12:"instructions";s:0:"";s:8:"required";s:1:"0";s:7:"message";s:0:"";s:13:"default_value";s:1:"0";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:3:{s:5:"field";s:19:"field_59b5f68e1c4ab";s:8:"operator";s:2:"==";s:5:"value";s:1:"1";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:5;}'),
(236, 57, 'position', 'normal') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(237, 57, 'layout', 'no_box'),
(238, 57, 'hide_on_screen', 'a:14:{i:0;s:9:"permalink";i:1;s:11:"the_content";i:2;s:7:"excerpt";i:3;s:13:"custom_fields";i:4;s:10:"discussion";i:5;s:8:"comments";i:6;s:9:"revisions";i:7;s:4:"slug";i:8;s:6:"author";i:9;s:6:"format";i:10;s:14:"featured_image";i:11;s:10:"categories";i:12;s:4:"tags";i:13;s:15:"send-trackbacks";}'),
(239, 57, '_edit_lock', '1505100542:1'),
(242, 57, 'field_59b5fbbae80d8', 'a:8:{s:3:"key";s:19:"field_59b5fbbae80d8";s:5:"label";s:8:"Explorer";s:4:"name";s:0:"";s:4:"type";s:3:"tab";s:12:"instructions";s:0:"";s:8:"required";s:1:"0";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:3:{s:5:"field";s:19:"field_59b5f68e1c4ab";s:8:"operator";s:2:"==";s:5:"value";s:1:"1";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:0;}'),
(243, 57, 'rule', 'a:5:{s:5:"param";s:4:"page";s:8:"operator";s:2:"==";s:5:"value";s:1:"5";s:8:"order_no";i:0;s:8:"group_no";i:0;}'),
(244, 57, '_wp_trash_meta_status', 'publish'),
(245, 57, '_wp_trash_meta_time', '1505100690'),
(246, 57, '_wp_desired_post_slug', 'acf_membership-levels'),
(247, 58, 'access_type_1', 'Upon Facebook Login'),
(248, 58, '_access_type_1', 'field_59b5f6641c4aa1'),
(249, 58, 'reflectionnaire_&_resources_1', '0'),
(250, 58, '_reflectionnaire_&_resources_1', 'field_59b5f68e1c4ab1'),
(251, 58, 'peer_discovery_and_matching_1', '0'),
(252, 58, '_peer_discovery_and_matching_1', 'field_59b5f6df1c4ac1'),
(253, 58, 'why_choose_this_option_1', 'Exploring & Weekly Mom Profiles Email'),
(254, 58, '_why_choose_this_option_1', 'field_59b5f6f41c4ad1'),
(255, 58, 'ability_to_earn_suite_of_personality_tests_1', '0'),
(256, 58, '_ability_to_earn_suite_of_personality_tests_1', 'field_59b5f7061c4ae1'),
(257, 58, 'access_type_2', 'Basic Access'),
(258, 58, '_access_type_2', 'field_59b5f6641c4aa2'),
(259, 58, 'reflectionnaire_&_resources_2', '1'),
(260, 58, '_reflectionnaire_&_resources_2', 'field_59b5f68e1c4ab2'),
(261, 58, 'peer_discovery_and_matching_2', '1'),
(262, 58, '_peer_discovery_and_matching_2', 'field_59b5f6df1c4ac2'),
(263, 58, 'why_choose_this_option_2', 'Full access to website'),
(264, 58, '_why_choose_this_option_2', 'field_59b5f6f41c4ad2'),
(265, 58, 'ability_to_earn_suite_of_personality_tests_2', '1'),
(266, 58, '_ability_to_earn_suite_of_personality_tests_2', 'field_59b5f7061c4ae2'),
(267, 58, 'access_type_3', 'Access + Test'),
(268, 58, '_access_type_3', 'field_59b5f6641c4aa3'),
(269, 58, 'reflectionnaire_&_resources_3', '1'),
(270, 58, '_reflectionnaire_&_resources_3', 'field_59b5f68e1c4ab3'),
(271, 58, 'peer_discovery_and_matching_3', '1'),
(272, 58, '_peer_discovery_and_matching_3', 'field_59b5f6df1c4ac3'),
(273, 58, 'why_choose_this_option_3', 'Skills testing along with community access'),
(274, 58, '_why_choose_this_option_3', 'field_59b5f6f41c4ad3'),
(275, 58, 'ability_to_earn_suite_of_personality_tests_3', '1'),
(276, 58, '_ability_to_earn_suite_of_personality_tests_3', 'field_59b5f7061c4ae3'),
(277, 58, 'access_type_4', 'Lead Board Access'),
(278, 58, '_access_type_4', 'field_59b5f6641c4aa4'),
(279, 58, 'reflectionnaire_&_resources_4', '0'),
(280, 58, '_reflectionnaire_&_resources_4', 'field_59b5f68e1c4ab4'),
(281, 58, 'peer_discovery_and_matching_4', '0'),
(282, 58, '_peer_discovery_and_matching_4', 'field_59b5f6df1c4ac4'),
(283, 58, 'why_choose_this_option_4', 'To post opportunities & search for job leads'),
(284, 58, '_why_choose_this_option_4', 'field_59b5f6f41c4ad4'),
(285, 58, 'ability_to_earn_suite_of_personality_tests_4', '0'),
(286, 58, '_ability_to_earn_suite_of_personality_tests_4', 'field_59b5f7061c4ae4'),
(287, 5, 'access_type_1', 'Upon Facebook Login'),
(288, 5, '_access_type_1', 'field_59b5f6641c4aa1'),
(289, 5, 'reflectionnaire_&_resources_1', '0'),
(290, 5, '_reflectionnaire_&_resources_1', 'field_59b5f68e1c4ab1'),
(291, 5, 'peer_discovery_and_matching_1', '0'),
(292, 5, '_peer_discovery_and_matching_1', 'field_59b5f6df1c4ac1'),
(293, 5, 'why_choose_this_option_1', 'Exploring & Weekly Mom Profiles Email'),
(294, 5, '_why_choose_this_option_1', 'field_59b5f6f41c4ad1'),
(295, 5, 'ability_to_earn_suite_of_personality_tests_1', '0'),
(296, 5, '_ability_to_earn_suite_of_personality_tests_1', 'field_59b5f7061c4ae1'),
(297, 5, 'access_type_2', 'Basic Access'),
(298, 5, '_access_type_2', 'field_59b5f6641c4aa2'),
(299, 5, 'reflectionnaire_&_resources_2', '1'),
(300, 5, '_reflectionnaire_&_resources_2', 'field_59b5f68e1c4ab2'),
(301, 5, 'peer_discovery_and_matching_2', '1'),
(302, 5, '_peer_discovery_and_matching_2', 'field_59b5f6df1c4ac2'),
(303, 5, 'why_choose_this_option_2', 'Full access to website'),
(304, 5, '_why_choose_this_option_2', 'field_59b5f6f41c4ad2'),
(305, 5, 'ability_to_earn_suite_of_personality_tests_2', '1'),
(306, 5, '_ability_to_earn_suite_of_personality_tests_2', 'field_59b5f7061c4ae2'),
(307, 5, 'access_type_3', 'Access + Test'),
(308, 5, '_access_type_3', 'field_59b5f6641c4aa3'),
(309, 5, 'reflectionnaire_&_resources_3', '1'),
(310, 5, '_reflectionnaire_&_resources_3', 'field_59b5f68e1c4ab3'),
(311, 5, 'peer_discovery_and_matching_3', '1'),
(312, 5, '_peer_discovery_and_matching_3', 'field_59b5f6df1c4ac3'),
(313, 5, 'why_choose_this_option_3', 'Skills testing along with community access'),
(314, 5, '_why_choose_this_option_3', 'field_59b5f6f41c4ad3'),
(315, 5, 'ability_to_earn_suite_of_personality_tests_3', '1'),
(316, 5, '_ability_to_earn_suite_of_personality_tests_3', 'field_59b5f7061c4ae3'),
(317, 5, 'access_type_4', 'Lead Board Access'),
(318, 5, '_access_type_4', 'field_59b5f6641c4aa4'),
(319, 5, 'reflectionnaire_&_resources_4', '0'),
(320, 5, '_reflectionnaire_&_resources_4', 'field_59b5f68e1c4ab4'),
(321, 5, 'peer_discovery_and_matching_4', '0'),
(322, 5, '_peer_discovery_and_matching_4', 'field_59b5f6df1c4ac4'),
(323, 5, 'why_choose_this_option_4', 'To post opportunities & search for job leads'),
(324, 5, '_why_choose_this_option_4', 'field_59b5f6f41c4ad4'),
(325, 5, 'ability_to_earn_suite_of_personality_tests_4', '0'),
(326, 5, '_ability_to_earn_suite_of_personality_tests_4', 'field_59b5f7061c4ae4'),
(327, 68, '_tml_action', 'login'),
(328, 69, '_tml_action', 'logout'),
(329, 70, '_tml_action', 'register'),
(330, 71, '_tml_action', 'lostpassword'),
(331, 72, '_tml_action', 'resetpass'),
(332, 68, '_edit_lock', '1507338853:1'),
(333, 91, '_edit_lock', '1505786068:1'),
(334, 92, '_edit_lock', '1505449897:1'),
(335, 92, '_edit_last', '1'),
(336, 94, '_edit_lock', '1506139149:1'),
(337, 94, '_edit_last', '1'),
(338, 74, '_edit_lock', '1506158537:1') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(339, 74, '_edit_last', '1'),
(340, 59, '_edit_lock', '1506499279:1'),
(341, 59, '_edit_last', '1'),
(342, 97, '_edit_lock', '1506431951:1'),
(343, 60, '_edit_lock', '1506511413:1'),
(344, 100, '_edit_lock', '1507731178:1') ;

#
# End of data contents of table `wp_postmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_posts`
#

DROP TABLE IF EXISTS `wp_posts`;


#
# Table structure of table `wp_posts`
#

CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_posts`
#
INSERT INTO `wp_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2017-09-07 01:15:44', '2017-09-07 01:15:44', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2017-09-07 01:15:44', '2017-09-07 01:15:44', '', 0, 'http://nxtchptr.herokuapp.com/?p=1', 0, 'post', '', 1),
(2, 1, '2017-09-07 01:15:44', '2017-09-07 01:15:44', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://nxtchptr.herokuapp.com/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2017-09-07 01:15:44', '2017-09-07 01:15:44', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=2', 0, 'page', '', 0),
(3, 1, '2017-09-07 01:22:02', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2017-09-07 01:22:02', '0000-00-00 00:00:00', '', 0, 'http://nxtchptr.herokuapp.com/?p=3', 0, 'post', '', 0),
(4, 1, '2017-09-07 06:04:37', '0000-00-00 00:00:00', '', 'Home', '', 'draft', 'closed', 'closed', '', '', '', '', '2017-09-07 06:04:37', '2017-09-07 06:04:37', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=4', 0, 'page', '', 0),
(5, 1, '2017-09-08 12:22:19', '2017-09-08 12:22:19', '', 'Membership', '', 'publish', 'closed', 'closed', '', 'membership', '', '', '2017-09-11 03:40:40', '2017-09-11 03:40:40', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=5', 0, 'page', '', 0),
(6, 1, '2017-09-08 12:22:19', '2017-09-08 12:22:19', '', 'Membership', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2017-09-08 12:22:19', '2017-09-08 12:22:19', '', 5, 'http://nxtchptr.herokuapp.com/5-revision-v1/', 0, 'revision', '', 0),
(7, 1, '2017-09-08 23:59:20', '2017-09-08 23:59:20', '', 'Self Discovery', '', 'publish', 'closed', 'closed', '', 'self-discovery', '', '', '2017-09-08 23:59:20', '2017-09-08 23:59:20', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=7', 0, 'page', '', 0),
(8, 1, '2017-09-08 23:59:20', '2017-09-08 23:59:20', '', 'Self Discovery', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2017-09-08 23:59:20', '2017-09-08 23:59:20', '', 7, 'http://nxtchptr.herokuapp.com/7-revision-v1/', 0, 'revision', '', 0),
(9, 1, '2017-09-09 00:36:32', '2017-09-09 00:36:32', '', 'Peer Discovery', '', 'publish', 'closed', 'closed', '', 'peer-discovery', '', '', '2017-09-09 00:36:32', '2017-09-09 00:36:32', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=9', 0, 'page', '', 0),
(10, 1, '2017-09-09 00:36:32', '2017-09-09 00:36:32', '', 'Peer Discovery', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2017-09-09 00:36:32', '2017-09-09 00:36:32', '', 9, 'http://nxtchptr.herokuapp.com/9-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2017-09-09 01:02:06', '2017-09-09 01:02:06', '', 'Career Discovery', '', 'publish', 'closed', 'closed', '', 'career-discovery', '', '', '2017-09-09 01:02:06', '2017-09-09 01:02:06', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=11', 0, 'page', '', 0),
(12, 1, '2017-09-09 01:02:06', '2017-09-09 01:02:06', '', 'Career Discovery', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2017-09-09 01:02:06', '2017-09-09 01:02:06', '', 11, 'http://nxtchptr.herokuapp.com/11-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2017-09-09 01:34:25', '2017-09-09 01:34:25', '', 'About Us', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2017-09-09 01:34:25', '2017-09-09 01:34:25', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=15', 0, 'page', '', 0),
(16, 1, '2017-09-09 01:34:25', '2017-09-09 01:34:25', '', 'About Us', '', 'inherit', 'closed', 'closed', '', '15-revision-v1', '', '', '2017-09-09 01:34:25', '2017-09-09 01:34:25', '', 15, 'http://nxtchptr.herokuapp.com/15-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2017-09-09 02:21:33', '2017-09-09 02:21:33', '', 'Contact Us', '', 'publish', 'closed', 'closed', '', 'contact-us', '', '', '2017-09-09 02:21:33', '2017-09-09 02:21:33', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=17', 0, 'page', '', 0),
(18, 1, '2017-09-09 02:21:33', '2017-09-09 02:21:33', '', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2017-09-09 02:21:33', '2017-09-09 02:21:33', '', 17, 'http://nxtchptr.herokuapp.com/17-revision-v1/', 0, 'revision', '', 0),
(19, 1, '2017-09-09 03:08:44', '0000-00-00 00:00:00', '', 'Home', '', 'draft', 'closed', 'closed', '', '', '', '', '2017-09-09 03:08:44', '0000-00-00 00:00:00', '', 0, 'http://nxtchptr.herokuapp.com/?p=19', 1, 'nav_menu_item', '', 0),
(20, 1, '2017-09-09 03:09:49', '2017-09-09 03:09:49', ' ', '', '', 'publish', 'closed', 'closed', '', '20', '', '', '2017-09-09 03:10:50', '2017-09-09 03:10:50', '', 0, 'http://nxtchptr.herokuapp.com/?p=20', 5, 'nav_menu_item', '', 0),
(21, 1, '2017-09-09 03:09:48', '2017-09-09 03:09:48', ' ', '', '', 'publish', 'closed', 'closed', '', '21', '', '', '2017-09-09 03:10:50', '2017-09-09 03:10:50', '', 0, 'http://nxtchptr.herokuapp.com/?p=21', 4, 'nav_menu_item', '', 0),
(22, 1, '2017-09-09 03:09:50', '2017-09-09 03:09:50', ' ', '', '', 'publish', 'closed', 'closed', '', '22', '', '', '2017-09-09 03:10:50', '2017-09-09 03:10:50', '', 0, 'http://nxtchptr.herokuapp.com/?p=22', 6, 'nav_menu_item', '', 0),
(23, 1, '2017-09-09 03:09:47', '2017-09-09 03:09:47', ' ', '', '', 'publish', 'closed', 'closed', '', '23', '', '', '2017-09-09 03:10:49', '2017-09-09 03:10:49', '', 0, 'http://nxtchptr.herokuapp.com/?p=23', 1, 'nav_menu_item', '', 0),
(24, 1, '2017-09-09 03:09:48', '2017-09-09 03:09:48', ' ', '', '', 'publish', 'closed', 'closed', '', '24', '', '', '2017-09-09 03:10:50', '2017-09-09 03:10:50', '', 0, 'http://nxtchptr.herokuapp.com/?p=24', 3, 'nav_menu_item', '', 0),
(25, 1, '2017-09-09 03:08:49', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2017-09-09 03:08:49', '0000-00-00 00:00:00', '', 0, 'http://nxtchptr.herokuapp.com/?p=25', 1, 'nav_menu_item', '', 0),
(26, 1, '2017-09-09 03:09:48', '2017-09-09 03:09:48', ' ', '', '', 'publish', 'closed', 'closed', '', '26', '', '', '2017-09-09 03:10:50', '2017-09-09 03:10:50', '', 0, 'http://nxtchptr.herokuapp.com/?p=26', 2, 'nav_menu_item', '', 0),
(27, 1, '2017-09-09 03:27:01', '2017-09-09 03:27:01', ' ', '', '', 'publish', 'closed', 'closed', '', '27', '', '', '2017-09-09 03:27:01', '2017-09-09 03:27:01', '', 0, 'http://nxtchptr.herokuapp.com/?p=27', 5, 'nav_menu_item', '', 0),
(28, 1, '2017-09-09 03:27:00', '2017-09-09 03:27:00', ' ', '', '', 'publish', 'closed', 'closed', '', '28', '', '', '2017-09-09 03:27:00', '2017-09-09 03:27:00', '', 0, 'http://nxtchptr.herokuapp.com/?p=28', 4, 'nav_menu_item', '', 0),
(29, 1, '2017-09-09 03:27:00', '2017-09-09 03:27:00', ' ', '', '', 'publish', 'closed', 'closed', '', '29', '', '', '2017-09-09 03:27:00', '2017-09-09 03:27:00', '', 0, 'http://nxtchptr.herokuapp.com/?p=29', 3, 'nav_menu_item', '', 0),
(30, 1, '2017-09-09 03:26:59', '2017-09-09 03:26:59', ' ', '', '', 'publish', 'closed', 'closed', '', '30', '', '', '2017-09-09 03:26:59', '2017-09-09 03:26:59', '', 0, 'http://nxtchptr.herokuapp.com/?p=30', 2, 'nav_menu_item', '', 0),
(31, 1, '2017-09-09 03:26:58', '2017-09-09 03:26:58', ' ', '', '', 'publish', 'closed', 'closed', '', '31', '', '', '2017-09-09 03:26:58', '2017-09-09 03:26:58', '', 0, 'http://nxtchptr.herokuapp.com/?p=31', 1, 'nav_menu_item', '', 0),
(32, 1, '2017-09-09 03:41:22', '2017-09-09 03:41:22', '[pmpro_account]', 'My Account', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2017-09-10 01:08:48', '2017-09-10 01:08:48', '', 54, 'http://nxtchptr.herokuapp.com/membership-account/', 0, 'page', '', 0),
(33, 1, '2017-09-09 03:41:22', '2017-09-09 03:41:22', '[pmpro_billing]', 'Membership Billing', '', 'publish', 'closed', 'closed', '', 'membership-billing', '', '', '2017-09-09 03:41:22', '2017-09-09 03:41:22', '', 32, 'http://nxtchptr.herokuapp.com/membership-account/membership-billing/', 0, 'page', '', 0),
(34, 1, '2017-09-09 03:41:22', '2017-09-09 03:41:22', '[pmpro_cancel]', 'Membership Cancel', '', 'publish', 'closed', 'closed', '', 'membership-cancel', '', '', '2017-09-09 03:41:22', '2017-09-09 03:41:22', '', 32, 'http://nxtchptr.herokuapp.com/membership-account/membership-cancel/', 0, 'page', '', 0),
(35, 1, '2017-09-09 03:41:23', '2017-09-09 03:41:23', '[pmpro_checkout]', 'Membership Checkout', '', 'publish', 'closed', 'closed', '', 'membership-checkout', '', '', '2017-09-09 03:41:23', '2017-09-09 03:41:23', '', 32, 'http://nxtchptr.herokuapp.com/membership-account/membership-checkout/', 0, 'page', '', 0),
(36, 1, '2017-09-09 03:41:23', '2017-09-09 03:41:23', '[pmpro_confirmation]', 'Membership Confirmation', '', 'publish', 'closed', 'closed', '', 'membership-confirmation', '', '', '2017-09-09 03:41:23', '2017-09-09 03:41:23', '', 32, 'http://nxtchptr.herokuapp.com/membership-account/membership-confirmation/', 0, 'page', '', 0),
(37, 1, '2017-09-09 03:41:23', '2017-09-09 03:41:23', '[pmpro_invoice]', 'Membership Invoice', '', 'publish', 'closed', 'closed', '', 'membership-invoice', '', '', '2017-09-09 03:41:23', '2017-09-09 03:41:23', '', 32, 'http://nxtchptr.herokuapp.com/membership-account/membership-invoice/', 0, 'page', '', 0),
(38, 1, '2017-09-09 03:41:24', '2017-09-09 03:41:24', '[pmpro_levels]', 'Membership Levels', '', 'publish', 'closed', 'closed', '', 'membership-levels', '', '', '2017-09-09 03:41:24', '2017-09-09 03:41:24', '', 32, 'http://nxtchptr.herokuapp.com/membership-account/membership-levels/', 0, 'page', '', 0),
(39, 1, '2017-09-09 05:51:41', '2017-09-09 05:51:41', ' ', '', '', 'publish', 'closed', 'closed', '', '39', '', '', '2017-09-25 03:08:35', '2017-09-25 03:08:35', '', 0, 'http://nxtchptr.herokuapp.com/?p=39', 3, 'nav_menu_item', '', 0),
(41, 1, '2017-09-09 05:51:41', '2017-09-09 05:51:41', ' ', '', '', 'publish', 'closed', 'closed', '', '41', '', '', '2017-09-25 03:08:35', '2017-09-25 03:08:35', '', 0, 'http://nxtchptr.herokuapp.com/?p=41', 2, 'nav_menu_item', '', 0),
(42, 1, '2017-09-09 05:51:40', '2017-09-09 05:51:40', ' ', '', '', 'publish', 'closed', 'closed', '', '42', '', '', '2017-09-25 03:08:35', '2017-09-25 03:08:35', '', 0, 'http://nxtchptr.herokuapp.com/?p=42', 1, 'nav_menu_item', '', 0),
(43, 1, '2017-09-09 05:52:36', '2017-09-09 05:52:36', ' ', '', '', 'publish', 'closed', 'closed', '', '43', '', '', '2017-09-09 05:52:36', '2017-09-09 05:52:36', '', 0, 'http://nxtchptr.herokuapp.com/?p=43', 1, 'nav_menu_item', '', 0),
(44, 1, '2017-09-09 05:52:37', '2017-09-09 05:52:37', ' ', '', '', 'publish', 'closed', 'closed', '', '44', '', '', '2017-09-09 05:52:37', '2017-09-09 05:52:37', '', 0, 'http://nxtchptr.herokuapp.com/?p=44', 2, 'nav_menu_item', '', 0),
(45, 1, '2017-09-09 06:39:57', '2017-09-09 06:39:57', '[pmpro_account]', 'Personal Area', '', 'inherit', 'closed', 'closed', '', '32-revision-v1', '', '', '2017-09-09 06:39:57', '2017-09-09 06:39:57', '', 32, 'http://nxtchptr.herokuapp.com/32-revision-v1/', 0, 'revision', '', 0),
(46, 1, '2017-09-09 10:25:57', '2017-09-09 10:25:57', '', 'Peer Matching', '', 'publish', 'closed', 'closed', '', 'peer-matching', '', '', '2017-09-10 01:09:12', '2017-09-10 01:09:12', '', 54, 'http://nxtchptr.herokuapp.com/?page_id=46', 0, 'page', '', 0),
(47, 1, '2017-09-09 10:25:57', '2017-09-09 10:25:57', '', 'Peer Matching', '', 'inherit', 'closed', 'closed', '', '46-revision-v1', '', '', '2017-09-09 10:25:57', '2017-09-09 10:25:57', '', 46, 'http://nxtchptr.herokuapp.com/46-revision-v1/', 0, 'revision', '', 0),
(48, 1, '2017-09-09 10:26:15', '2017-09-09 10:26:15', '', 'Test Results', '', 'publish', 'closed', 'closed', '', 'test-results', '', '', '2017-09-10 01:09:18', '2017-09-10 01:09:18', '', 54, 'http://nxtchptr.herokuapp.com/?page_id=48', 0, 'page', '', 0),
(49, 1, '2017-09-09 10:26:15', '2017-09-09 10:26:15', '', 'Test Results', '', 'inherit', 'closed', 'closed', '', '48-revision-v1', '', '', '2017-09-09 10:26:15', '2017-09-09 10:26:15', '', 48, 'http://nxtchptr.herokuapp.com/48-revision-v1/', 0, 'revision', '', 0),
(50, 1, '2017-09-09 10:27:42', '2017-09-09 10:27:42', ' ', '', '', 'publish', 'closed', 'closed', '', '50', '', '', '2017-09-09 10:27:42', '2017-09-09 10:27:42', '', 0, 'http://nxtchptr.herokuapp.com/?p=50', 1, 'nav_menu_item', '', 0),
(51, 1, '2017-09-09 10:27:43', '2017-09-09 10:27:43', ' ', '', '', 'publish', 'closed', 'closed', '', '51', '', '', '2017-09-09 10:27:43', '2017-09-09 10:27:43', '', 0, 'http://nxtchptr.herokuapp.com/?p=51', 2, 'nav_menu_item', '', 0),
(52, 1, '2017-09-09 10:27:43', '2017-09-09 10:27:43', ' ', '', '', 'publish', 'closed', 'closed', '', '52', '', '', '2017-09-09 10:27:43', '2017-09-09 10:27:43', '', 0, 'http://nxtchptr.herokuapp.com/?p=52', 3, 'nav_menu_item', '', 0),
(53, 1, '2017-09-09 10:28:11', '2017-09-09 10:28:11', '[pmpro_account]', 'My Account', '', 'inherit', 'closed', 'closed', '', '32-revision-v1', '', '', '2017-09-09 10:28:11', '2017-09-09 10:28:11', '', 32, 'http://nxtchptr.herokuapp.com/32-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2017-09-10 01:08:21', '2017-09-10 01:08:21', '', 'Personal Area', '', 'publish', 'closed', 'closed', '', 'personal-area', '', '', '2017-09-10 01:08:21', '2017-09-10 01:08:21', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=54', 0, 'page', '', 0),
(55, 1, '2017-09-10 01:08:21', '2017-09-10 01:08:21', '', 'Personal Area', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2017-09-10 01:08:21', '2017-09-10 01:08:21', '', 54, 'http://nxtchptr.herokuapp.com/54-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2017-09-10 02:19:27', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-09-10 02:19:27', '0000-00-00 00:00:00', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=56', 0, 'page', '', 0),
(57, 1, '2017-09-11 02:39:11', '2017-09-11 02:39:11', '', 'Membership Levels', '', 'trash', 'closed', 'closed', '', 'acf_membership-levels__trashed', '', '', '2017-09-11 03:31:30', '2017-09-11 03:31:30', '', 0, 'http://nxtchptr.herokuapp.com/?post_type=acf&#038;p=57', 0, 'acf', '', 0),
(58, 1, '2017-09-11 03:40:40', '2017-09-11 03:40:40', '', 'Membership', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2017-09-11 03:40:40', '2017-09-11 03:40:40', '', 5, 'http://nxtchptr.herokuapp.com/5-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2017-09-11 05:30:03', '2017-09-11 05:30:03', '[wpjb_jobs_list]', 'Jobs', '', 'publish', 'closed', 'closed', '', 'jobs', '', '', '2017-09-27 08:01:19', '2017-09-27 08:01:19', '', 0, 'http://nxtchptr.herokuapp.com/jobs/', 0, 'page', '', 0),
(60, 1, '2017-09-11 05:30:03', '2017-09-11 05:30:03', '[wpjb_jobs_add]', 'Post a Job', '', 'publish', 'closed', 'closed', '', 'post-a-job', '', '', '2017-09-11 05:30:03', '2017-09-11 05:30:03', '', 59, 'http://nxtchptr.herokuapp.com/jobs/post-a-job/', 0, 'page', '', 0),
(61, 1, '2017-09-11 05:30:04', '2017-09-11 05:30:04', '[wpjb_jobs_search]', 'Advanced Search', '', 'publish', 'closed', 'closed', '', 'advanced-search', '', '', '2017-09-11 05:30:04', '2017-09-11 05:30:04', '', 59, 'http://nxtchptr.herokuapp.com/jobs/advanced-search/', 0, 'page', '', 0),
(62, 1, '2017-09-11 05:30:04', '2017-09-11 05:30:04', '[wpjb_resumes_list]', 'Resumes', '', 'publish', 'closed', 'closed', '', 'resumes', '', '', '2017-09-11 05:30:04', '2017-09-11 05:30:04', '', 0, 'http://nxtchptr.herokuapp.com/resumes/', 0, 'page', '', 0),
(63, 1, '2017-09-11 05:30:04', '2017-09-11 05:30:04', '[wpjb_resumes_search]', 'Advanced Search', '', 'publish', 'closed', 'closed', '', 'advanced-search', '', '', '2017-09-11 05:30:04', '2017-09-11 05:30:04', '', 62, 'http://nxtchptr.herokuapp.com/resumes/advanced-search/', 0, 'page', '', 0),
(64, 1, '2017-09-11 05:30:05', '2017-09-11 05:30:05', '[wpjb_employer_panel]', 'Employer Panel', '', 'publish', 'closed', 'closed', '', 'employer-panel', '', '', '2017-09-11 05:30:05', '2017-09-11 05:30:05', '', 0, 'http://nxtchptr.herokuapp.com/employer-panel/', 0, 'page', '', 0),
(65, 1, '2017-09-11 05:30:05', '2017-09-11 05:30:05', '[wpjb_employer_register]', 'Employer Registration', '', 'publish', 'closed', 'closed', '', 'employer-registration', '', '', '2017-09-11 05:30:05', '2017-09-11 05:30:05', '', 64, 'http://nxtchptr.herokuapp.com/employer-panel/employer-registration/', 0, 'page', '', 0),
(66, 1, '2017-09-11 05:30:06', '2017-09-11 05:30:06', '[wpjb_candidate_panel]', 'Candidate Panel', '', 'publish', 'closed', 'closed', '', 'candidate-panel', '', '', '2017-09-11 05:30:06', '2017-09-11 05:30:06', '', 0, 'http://nxtchptr.herokuapp.com/candidate-panel/', 0, 'page', '', 0),
(67, 1, '2017-09-11 05:30:06', '2017-09-11 05:30:06', '[wpjb_candidate_register]', 'Candidate Registration', '', 'publish', 'closed', 'closed', '', 'candidate-registration', '', '', '2017-09-11 05:30:06', '2017-09-11 05:30:06', '', 66, 'http://nxtchptr.herokuapp.com/candidate-panel/candidate-registration/', 0, 'page', '', 0),
(68, 1, '2017-09-11 06:02:14', '2017-09-11 06:02:14', '[theme-my-login]', 'Log In', '', 'publish', 'closed', 'closed', '', 'login', '', '', '2017-09-11 06:02:14', '2017-09-11 06:02:14', '', 0, 'http://nxtchptr.herokuapp.com/login/', 0, 'page', '', 0),
(69, 1, '2017-09-11 06:02:14', '2017-09-11 06:02:14', '[theme-my-login]', 'Log Out', '', 'publish', 'closed', 'closed', '', 'logout', '', '', '2017-09-11 06:02:14', '2017-09-11 06:02:14', '', 0, 'http://nxtchptr.herokuapp.com/logout/', 0, 'page', '', 0),
(70, 1, '2017-09-11 06:02:15', '2017-09-11 06:02:15', '[theme-my-login]', 'Register', '', 'publish', 'closed', 'closed', '', 'register', '', '', '2017-09-11 06:02:15', '2017-09-11 06:02:15', '', 0, 'http://nxtchptr.herokuapp.com/register/', 0, 'page', '', 0),
(71, 1, '2017-09-11 06:02:15', '2017-09-11 06:02:15', '[theme-my-login]', 'Lost Password', '', 'publish', 'closed', 'closed', '', 'lostpassword', '', '', '2017-09-11 06:02:15', '2017-09-11 06:02:15', '', 0, 'http://nxtchptr.herokuapp.com/lostpassword/', 0, 'page', '', 0),
(72, 1, '2017-09-11 06:02:15', '2017-09-11 06:02:15', '[theme-my-login]', 'Reset Password', '', 'publish', 'closed', 'closed', '', 'resetpass', '', '', '2017-09-11 06:02:15', '2017-09-11 06:02:15', '', 0, 'http://nxtchptr.herokuapp.com/resetpass/', 0, 'page', '', 0),
(73, 1, '2017-09-13 10:46:16', '2017-09-13 10:46:16', '', 'Activity', '', 'publish', 'closed', 'closed', '', 'activity', '', '', '2017-09-13 10:46:16', '2017-09-13 10:46:16', '', 0, 'http://nxtchptr.herokuapp.com/activity/', 0, 'page', '', 0),
(74, 1, '2017-09-13 10:46:17', '2017-09-13 10:46:17', '', 'Members', '', 'publish', 'closed', 'closed', '', 'members', '', '', '2017-09-23 09:13:41', '2017-09-23 09:13:41', '', 0, 'http://nxtchptr.herokuapp.com/members/', 0, 'page', '', 0),
(75, 1, '2017-09-13 10:46:17', '2017-09-13 10:46:17', '{{poster.name}} replied to one of your updates:\n\n<blockquote>&quot;{{usermessage}}&quot;</blockquote>\n\n<a href="{{{thread.url}}}">Go to the discussion</a> to reply or catch up on the conversation.', '[{{{site.name}}}] {{poster.name}} replied to one of your updates', '{{poster.name}} replied to one of your updates:\n\n"{{usermessage}}"\n\nGo to the discussion to reply or catch up on the conversation: {{{thread.url}}}', 'publish', 'closed', 'closed', '', 'site-name-poster-name-replied-to-one-of-your-updates', '', '', '2017-09-13 10:46:17', '2017-09-13 10:46:17', '', 0, 'http://nxtchptr.herokuapp.com/?post_type=bp-email&p=75', 0, 'bp-email', '', 0),
(76, 1, '2017-09-13 10:46:18', '2017-09-13 10:46:18', '{{poster.name}} replied to one of your comments:\n\n<blockquote>&quot;{{usermessage}}&quot;</blockquote>\n\n<a href="{{{thread.url}}}">Go to the discussion</a> to reply or catch up on the conversation.', '[{{{site.name}}}] {{poster.name}} replied to one of your comments', '{{poster.name}} replied to one of your comments:\n\n"{{usermessage}}"\n\nGo to the discussion to reply or catch up on the conversation: {{{thread.url}}}', 'publish', 'closed', 'closed', '', 'site-name-poster-name-replied-to-one-of-your-comments', '', '', '2017-09-13 10:46:18', '2017-09-13 10:46:18', '', 0, 'http://nxtchptr.herokuapp.com/?post_type=bp-email&p=76', 0, 'bp-email', '', 0),
(77, 1, '2017-09-13 10:46:18', '2017-09-13 10:46:18', '{{poster.name}} mentioned you in a status update:\n\n<blockquote>&quot;{{usermessage}}&quot;</blockquote>\n\n<a href="{{{mentioned.url}}}">Go to the discussion</a> to reply or catch up on the conversation.', '[{{{site.name}}}] {{poster.name}} mentioned you in a status update', '{{poster.name}} mentioned you in a status update:\n\n"{{usermessage}}"\n\nGo to the discussion to reply or catch up on the conversation: {{{mentioned.url}}}', 'publish', 'closed', 'closed', '', 'site-name-poster-name-mentioned-you-in-a-status-update', '', '', '2017-09-13 10:46:18', '2017-09-13 10:46:18', '', 0, 'http://nxtchptr.herokuapp.com/?post_type=bp-email&p=77', 0, 'bp-email', '', 0),
(78, 1, '2017-09-13 10:46:19', '2017-09-13 10:46:19', '{{poster.name}} mentioned you in the group "{{group.name}}":\n\n<blockquote>&quot;{{usermessage}}&quot;</blockquote>\n\n<a href="{{{mentioned.url}}}">Go to the discussion</a> to reply or catch up on the conversation.', '[{{{site.name}}}] {{poster.name}} mentioned you in an update', '{{poster.name}} mentioned you in the group "{{group.name}}":\n\n"{{usermessage}}"\n\nGo to the discussion to reply or catch up on the conversation: {{{mentioned.url}}}', 'publish', 'closed', 'closed', '', 'site-name-poster-name-mentioned-you-in-an-update', '', '', '2017-09-13 10:46:19', '2017-09-13 10:46:19', '', 0, 'http://nxtchptr.herokuapp.com/?post_type=bp-email&p=78', 0, 'bp-email', '', 0),
(79, 1, '2017-09-13 10:46:19', '2017-09-13 10:46:19', 'Thanks for registering!\n\nTo complete the activation of your account, go to the following link: <a href="{{{activate.url}}}">{{{activate.url}}}</a>', '[{{{site.name}}}] Activate your account', 'Thanks for registering!\n\nTo complete the activation of your account, go to the following link: {{{activate.url}}}', 'publish', 'closed', 'closed', '', 'site-name-activate-your-account', '', '', '2017-09-13 10:46:19', '2017-09-13 10:46:19', '', 0, 'http://nxtchptr.herokuapp.com/?post_type=bp-email&p=79', 0, 'bp-email', '', 0),
(80, 1, '2017-09-13 10:46:20', '2017-09-13 10:46:20', 'Thanks for registering!\n\nTo complete the activation of your account and site, go to the following link: <a href="{{{activate-site.url}}}">{{{activate-site.url}}}</a>.\n\nAfter you activate, you can visit your site at <a href="{{{user-site.url}}}">{{{user-site.url}}}</a>.', '[{{{site.name}}}] Activate {{{user-site.url}}}', 'Thanks for registering!\n\nTo complete the activation of your account and site, go to the following link: {{{activate-site.url}}}\n\nAfter you activate, you can visit your site at {{{user-site.url}}}.', 'publish', 'closed', 'closed', '', 'site-name-activate-user-site-url', '', '', '2017-09-13 10:46:20', '2017-09-13 10:46:20', '', 0, 'http://nxtchptr.herokuapp.com/?post_type=bp-email&p=80', 0, 'bp-email', '', 0),
(81, 1, '2017-09-13 10:46:21', '2017-09-13 10:46:21', '<a href="{{{initiator.url}}}">{{initiator.name}}</a> wants to add you as a friend.\n\nTo accept this request and manage all of your pending requests, visit: <a href="{{{friend-requests.url}}}">{{{friend-requests.url}}}</a>', '[{{{site.name}}}] New friendship request from {{initiator.name}}', '{{initiator.name}} wants to add you as a friend.\n\nTo accept this request and manage all of your pending requests, visit: {{{friend-requests.url}}}\n\nTo view {{initiator.name}}\'s profile, visit: {{{initiator.url}}}', 'publish', 'closed', 'closed', '', 'site-name-new-friendship-request-from-initiator-name', '', '', '2017-09-13 10:46:21', '2017-09-13 10:46:21', '', 0, 'http://nxtchptr.herokuapp.com/?post_type=bp-email&p=81', 0, 'bp-email', '', 0),
(82, 1, '2017-09-13 10:46:22', '2017-09-13 10:46:22', '<a href="{{{friendship.url}}}">{{friend.name}}</a> accepted your friend request.', '[{{{site.name}}}] {{friend.name}} accepted your friendship request', '{{friend.name}} accepted your friend request.\n\nTo learn more about them, visit their profile: {{{friendship.url}}}', 'publish', 'closed', 'closed', '', 'site-name-friend-name-accepted-your-friendship-request', '', '', '2017-09-13 10:46:22', '2017-09-13 10:46:22', '', 0, 'http://nxtchptr.herokuapp.com/?post_type=bp-email&p=82', 0, 'bp-email', '', 0),
(83, 1, '2017-09-13 10:46:23', '2017-09-13 10:46:23', 'Group details for the group &quot;<a href="{{{group.url}}}">{{group.name}}</a>&quot; were updated:\n<blockquote>{{changed_text}}</blockquote>', '[{{{site.name}}}] Group details updated', 'Group details for the group "{{group.name}}" were updated:\n\n{{changed_text}}\n\nTo view the group, visit: {{{group.url}}}', 'publish', 'closed', 'closed', '', 'site-name-group-details-updated', '', '', '2017-09-13 10:46:23', '2017-09-13 10:46:23', '', 0, 'http://nxtchptr.herokuapp.com/?post_type=bp-email&p=83', 0, 'bp-email', '', 0),
(84, 1, '2017-09-13 10:46:24', '2017-09-13 10:46:24', '<a href="{{{inviter.url}}}">{{inviter.name}}</a> has invited you to join the group: &quot;{{group.name}}&quot;.\n<a href="{{{invites.url}}}">Go here to accept your invitation</a> or <a href="{{{group.url}}}">visit the group</a> to learn more.', '[{{{site.name}}}] You have an invitation to the group: "{{group.name}}"', '{{inviter.name}} has invited you to join the group: "{{group.name}}".\n\nTo accept your invitation, visit: {{{invites.url}}}\n\nTo learn more about the group, visit: {{{group.url}}}.\nTo view {{inviter.name}}\'s profile, visit: {{{inviter.url}}}', 'publish', 'closed', 'closed', '', 'site-name-you-have-an-invitation-to-the-group-group-name', '', '', '2017-09-13 10:46:24', '2017-09-13 10:46:24', '', 0, 'http://nxtchptr.herokuapp.com/?post_type=bp-email&p=84', 0, 'bp-email', '', 0),
(85, 1, '2017-09-13 10:46:24', '2017-09-13 10:46:24', 'You have been promoted to <b>{{promoted_to}}</b> in the group &quot;<a href="{{{group.url}}}">{{group.name}}</a>&quot;.', '[{{{site.name}}}] You have been promoted in the group: "{{group.name}}"', 'You have been promoted to {{promoted_to}} in the group: "{{group.name}}".\n\nTo visit the group, go to: {{{group.url}}}', 'publish', 'closed', 'closed', '', 'site-name-you-have-been-promoted-in-the-group-group-name', '', '', '2017-09-13 10:46:24', '2017-09-13 10:46:24', '', 0, 'http://nxtchptr.herokuapp.com/?post_type=bp-email&p=85', 0, 'bp-email', '', 0),
(86, 1, '2017-09-13 10:46:25', '2017-09-13 10:46:25', '<a href="{{{profile.url}}}">{{requesting-user.name}}</a> wants to join the group &quot;{{group.name}}&quot;. As you are an administrator of this group, you must either accept or reject the membership request.\n\n<a href="{{{group-requests.url}}}">Go here to manage this</a> and all other pending requests.', '[{{{site.name}}}] Membership request for group: {{group.name}}', '{{requesting-user.name}} wants to join the group "{{group.name}}". As you are the administrator of this group, you must either accept or reject the membership request.\n\nTo manage this and all other pending requests, visit: {{{group-requests.url}}}\n\nTo view {{requesting-user.name}}\'s profile, visit: {{{profile.url}}}', 'publish', 'closed', 'closed', '', 'site-name-membership-request-for-group-group-name', '', '', '2017-09-13 10:46:25', '2017-09-13 10:46:25', '', 0, 'http://nxtchptr.herokuapp.com/?post_type=bp-email&p=86', 0, 'bp-email', '', 0),
(87, 1, '2017-09-13 10:46:25', '2017-09-13 10:46:25', '{{sender.name}} sent you a new message: &quot;{{usersubject}}&quot;\n\n<blockquote>&quot;{{usermessage}}&quot;</blockquote>\n\n<a href="{{{message.url}}}">Go to the discussion</a> to reply or catch up on the conversation.', '[{{{site.name}}}] New message from {{sender.name}}', '{{sender.name}} sent you a new message: "{{usersubject}}"\n\n"{{usermessage}}"\n\nGo to the discussion to reply or catch up on the conversation: {{{message.url}}}', 'publish', 'closed', 'closed', '', 'site-name-new-message-from-sender-name', '', '', '2017-09-13 10:46:25', '2017-09-13 10:46:25', '', 0, 'http://nxtchptr.herokuapp.com/?post_type=bp-email&p=87', 0, 'bp-email', '', 0),
(88, 1, '2017-09-13 10:46:26', '2017-09-13 10:46:26', 'You recently changed the email address associated with your account on {{site.name}} to {{user.email}}. If this is correct, <a href="{{{verify.url}}}">go here to confirm the change</a>.\n\nOtherwise, you can safely ignore and delete this email if you have changed your mind, or if you think you have received this email in error.', '[{{{site.name}}}] Verify your new email address', 'You recently changed the email address associated with your account on {{site.name}} to {{user.email}}. If this is correct, go to the following link to confirm the change: {{{verify.url}}}\n\nOtherwise, you can safely ignore and delete this email if you have changed your mind, or if you think you have received this email in error.', 'publish', 'closed', 'closed', '', 'site-name-verify-your-new-email-address', '', '', '2017-09-13 10:46:26', '2017-09-13 10:46:26', '', 0, 'http://nxtchptr.herokuapp.com/?post_type=bp-email&p=88', 0, 'bp-email', '', 0),
(89, 1, '2017-09-13 10:46:27', '2017-09-13 10:46:27', 'Your membership request for the group &quot;<a href="{{{group.url}}}">{{group.name}}</a>&quot; has been accepted.', '[{{{site.name}}}] Membership request for group "{{group.name}}" accepted', 'Your membership request for the group "{{group.name}}" has been accepted.\n\nTo view the group, visit: {{{group.url}}}', 'publish', 'closed', 'closed', '', 'site-name-membership-request-for-group-group-name-accepted', '', '', '2017-09-13 10:46:27', '2017-09-13 10:46:27', '', 0, 'http://nxtchptr.herokuapp.com/?post_type=bp-email&p=89', 0, 'bp-email', '', 0),
(90, 1, '2017-09-13 10:46:27', '2017-09-13 10:46:27', 'Your membership request for the group &quot;<a href="{{{group.url}}}">{{group.name}}</a>&quot; has been rejected.', '[{{{site.name}}}] Membership request for group "{{group.name}}" rejected', 'Your membership request for the group "{{group.name}}" has been rejected.\n\nTo request membership again, visit: {{{group.url}}}', 'publish', 'closed', 'closed', '', 'site-name-membership-request-for-group-group-name-rejected', '', '', '2017-09-13 10:46:27', '2017-09-13 10:46:27', '', 0, 'http://nxtchptr.herokuapp.com/?post_type=bp-email&p=90', 0, 'bp-email', '', 0),
(91, 1, '2017-09-13 10:48:19', '2017-09-13 10:48:19', '', 'Groups', '', 'publish', 'closed', 'closed', '', 'groups', '', '', '2017-09-13 10:48:19', '2017-09-13 10:48:19', '', 0, 'http://nxtchptr.herokuapp.com/groups/', 0, 'page', '', 0),
(92, 1, '2017-09-15 03:08:38', '2017-09-15 03:08:38', '', 'Peer matching form', '', 'publish', 'closed', 'closed', '', 'peer-matching-form', '', '', '2017-09-15 03:08:38', '2017-09-15 03:08:38', '', 0, 'http://nxtchptr.herokuapp.com/?page_id=92', 0, 'page', '', 0),
(93, 1, '2017-09-15 03:08:38', '2017-09-15 03:08:38', '', 'Peer matching form', '', 'inherit', 'closed', 'closed', '', '92-revision-v1', '', '', '2017-09-15 03:08:38', '2017-09-15 03:08:38', '', 92, 'http://nxtchptr.herokuapp.com/92-revision-v1/', 0, 'revision', '', 0),
(94, 1, '2017-09-23 03:58:35', '0000-00-00 00:00:00', '<ol>\n 	<li><span style="font-weight: 400;">     </span><b>What is your business?</b></li>\n</ol>\n<span style="font-weight: 400;">KLIMB is a consultancy that provides dyslexia assessment and helps dyslexic children and their families overcome the educational and emotional obstacles that go hand-in-hand with the disability.  I conduct [Hyperlink: Slingerland Assessments] and, then once I know exactly what the problem is, I help families put together plans that cater to the way kids learn.  These plans can be a big help to teachers, tutors and parents in gearing a child’s lessons to his or her learning style. </span>\n\n<span style="font-weight: 400;">Dyslexia is a hereditary learning disability among people of normal intelligence who have difficulty acquiring and processing language.  When children learn to read, they first figure out the sound that each letter makes, for example, "D" makes a "duh" sound.  Dyslexic people have a hard time connecting letters to the sounds they make and then blending them into words.  As a result, people with dyslexia often mix up the reading of words, for example reading “was” as “saw” or “of” as “for”, consequently reading becomes extremely slow and inaccurate.  Dyslexia can affect a person’s ability to read, write, spell, and even speak, which clearly makes school an extraordinary challenge.  It is said that 20% of the earth’s population is dyslexic and many are undiagnosed. </span>\n\n<span style="font-weight: 400;">Research shows that dyslexic children’s academic trajectories are dramatically improved with professionally guided reading tutorials.  As a result, there is a lot of demand for trained reading tutors, and in Northern California the tutoring cost is about $100 an hour.  Having spoken to many families about a tutoring plan for their children, I grew concerned about how many families couldn’t afford tutoring for the recommended four to five times a week.  </span>\n\n<span style="font-weight: 400;">In response to that concern I just launched the pilot program for Keep KLIMBing.  Its purpose is to level the playing field so that all children, regardless of economic advantage, can experience the beneficial effects of reading intervention.  Effectively, it is a buddy reading program that we provide at no or low cost to families so that intervention can happen as soon as possible.  The buddies are trained volunteers, and the reading locations take place in donated spaces like libraries and schools.  </span>\n\n<span style="font-weight: 400;">One of my favorite quotes is by the dyslexic, Albert Einstein, and it reads: </span><i><span style="font-weight: 400;">“Everybody is a genius. But if you judge a fish by its ability to climb a tree, it will live it\'s whole life believing that it is stupid." </span></i><span style="font-weight: 400;"> Inspired by this quote I named my corporation, KLIMB, which is meant to be a misspelled version of the word “climb” as someone with dyslexia might spell it.  </span>\n\n&nbsp;\n<ol start="2">\n 	<li><span style="font-weight: 400;">     </span><b>What made you decide to start your business?</b></li>\n</ol>\n<span style="font-weight: 400;">When my oldest daughter was diagnosed with dyslexia 5 years ago, it was really hard for me.  I remember feeling incredibly lonely as I was navigating everything for the first time.  Then a year later, my second daughter was also diagnosed as dyslexic.  My oldest daughter became extremely anxious at school each day and especially around the weekly spelling tests. We were worried about her stress level at such a young age and so we moved her to [Hyperlink: Charles Armstrong School], a local school that was specifically set up for kids with dyslexia.  My younger daughter continued to go to her regular school, as she seemed to be managing her stress and getting the support she needed.  </span>\n\n<span style="font-weight: 400;">As a result, I had experience with dyslexic kids at regular and targeted schools, and I learned how to advocate for my kids at each place.  Then, my older daughter went back to the regular school after three years, and so I became familiar with re-entry.  At the same time that my oldest daughter returned to her mainstream school, my son, the third child in our family, was diagnosed with dyslexia. </span>\n\n<span style="font-weight: 400;">I am passionate about improving the lives of kids with dyslexia, and the time also seemed right once my kids were in school fulltime. I wanted to do something of my own and to be contributing.  </span>\n\n<span style="font-weight: 400;">  </span>\n<ol start="3">\n 	<li><span style="font-weight: 400;">     </span><b>Was there one moment that gave you the confidence that this was a good idea?</b></li>\n</ol>\n<span style="font-weight: 400;">Having experienced the roller coaster ride of dyslexia for the past five years, at one point I looked around and realized that I knew a lot about all aspects of the disability.  I felt strongly that all children with dyslexia should have the opportunity to reach their full potential, and I felt like I could make that happen given my educational background in psychology (undergraduate and masters).  </span>\n\n<span style="font-weight: 400;">As part of my research into dyslexia, I spent some time learning about Richard Branson and other famous dyslexics.  Richard Branson, the well-known founder of Virgin Records and Virgin Airways, was told by his former headmaster that, “He would either end up in prison or become a millionaire.” </span>\n\n<span style="font-weight: 400;">What is interesting to me about this quote is that it quite accurately summarizes the outcomes for kids with dyslexia.  Some of the most innovative people of our time are dyslexic, Steven Spielberg, Charles Schwab, Thomas Edison to name a few, and there are estimates that 48% of prison inmates are also dyslexic.  A lot of this bifurcation has to do with how these people, as children, ended up feeling about their reading problems.  Some felt they had talents to contribute even though reading was hard for them and others felt they had nothing of value to give.   </span>\n\n&nbsp;\n<ol start="4">\n 	<li><span style="font-weight: 400;">     </span><b>Was there ever a particularly tough time that in retrospect was a priceless learning moment?</b></li>\n</ol>\n<span style="font-weight: 400;">Yes, two examples particularly stand out in my mind as heart-breaking times for me that drive me to advocate for these kids.   </span>\n\n<span style="font-weight: 400;">The first example of a really hard experience was when I volunteered to give twenty Slingerland Assessments at a Bay Area school.  Of the 20 kids I screened, every single one of them needed help.  Until I got there, I hadn’t realized the overwhelming need.  They may not all end up being dyslexic, but they all needed reading support.  Because of feeling so helpless with such great numbers, I was motivated to create an intervention arm of KLIMB, what eventually became Keep KLIMBing.</span>\n\n<span style="font-weight: 400;">The second example comes from my consulting practice.  I was working with a kid from an affluent family, who ended up being dyslexic, who had been told by his teachers over and over again that he just wasn’t trying.  It was heart-breaking to see how down on himself he was. He was still so young and clearly bright but he was giving up. This is a tragedy but unfortunately it isn’t uncommon.  I often recall this student’s particular story when I’m tired or feeling  stretched, and it pushes me on.</span>\n\n&nbsp;\n<ol start="5">\n 	<li><span style="font-weight: 400;">     </span><b>Were your family and friends helpful or obstacles in launching your business?  How so?</b></li>\n</ol>\n<span style="font-weight: 400;">My kids were the reason for this.  My children are my daily inspiration. Seeing the effects dyslexia has on a child at such a young age is profound.  Without the belief that you have anything worthwhile to contribute, a child will give up and this is what happens to so many bright dyslexic children. I never wanted my kids to lose confidence or think that they couldn’t achieve anything they wanted in life.  Watching my children work and persevere each day motivates me to do the same, to persist in my goals for KLIMB, regardless of obstacles along the way. </span>\n\n&nbsp;\n<ol start="6">\n 	<li><span style="font-weight: 400;">     </span><b>Were there any partnerships or advice that were particularly helpful?</b></li>\n</ol>\n<span style="font-weight: 400;">Lynne Ruppel and I initially met for business referral reasons, but in that first meeting we ended up conceiving of the idea of Keep KLIMBing.  Lynne and I were both concerned about all of the families who can’t afford the tutoring but who need it just as badly as the families who can afford it.  Lynne is an educational therapist who is very educated on reading intervention and her involvement is integral in making the idea of Keep KLIMBing work.  She trains the volunteers on various phonemic awareness activities, games, and literacy instruction and creates daily lesson plans. These trainings and lesson plans help set Keep KLIMBing apart from other reading buddy programs. Through Lynne’s explicit instruction and our early intervention, one-to-one model, we aim to provide quantifiable reading improvement for every participating child. </span>\n\n&nbsp;\n<ol start="7">\n 	<li><span style="font-weight: 400;">     </span><b>Who did you speak to for support as you were working on the idea/launch?</b></li>\n</ol>\n<span style="font-weight: 400;">Two people have been incredibly supportive to me.</span>\n\n<span style="font-weight: 400;">The first is Tuck Geerds, an educational therapist.  She gave the Slingerland Assessment to both of my daughters, and I was always inspired by her and appreciated her advice.  When I started this business she gave me great advice.  One of the truly wonderful gifts was that she allowed me to use her proprietary tool that helps with deciphering the results of the assessment.  This has been a big help in giving parents really clear feedback on their children’s needs. </span>\n\n<span style="font-weight: 400;">The second person is my husband.  He also has dyslexia, so I think he supports me both because he understands so clearly what it is like to live with the hurdles of the disability and also just because he believes in me.  He supported KLIMB from the start and has helped spread the word of my business through his wide network on LinkedIn.    </span>\n\n<span style="font-weight: 400;">  </span>\n<ol start="8">\n 	<li><span style="font-weight: 400;">  </span><b>What are some successes you have had with your business that make you proud?</b></li>\n</ol>\n<span style="font-weight: 400;">I was nervous that my consulting wouldn’t provide something of value.  The consulting side is hard to quantify, and I wanted clients to feel like I was making a difference in their lives.  I knew that I was helping out when the mothers were relieved by my advice.  Sometimes it is something so simple. In one instance, a mother was asking about audiobooks and whether it was OK for her son to listen to them since he loved them so much.  It turns out that audio books are great for kids with dyslexia, so she was thrilled that she could preserve that pleasure for her son.   </span>\n\n<span style="font-weight: 400;">The role that parents play is also very interesting to me.  I really believe that whatever feelings the parents have get subtly communicated to the kids.  So, I also spend a lot of time giving parents motivational information so that they realize that their child really can do anything she wants to do.  Dyslexia doesn’t go away, but giving parents hope prevents them from unintentionally sending negative feelings to their kids.  All of these amazing people like Branson, Einstein, Spielberg, Picasso, had this.  Of course, they had to work hard to get what they wanted, but that is an important lesson too.  Whenever I can help remove the worries of a parent or give them hope that makes me really proud. </span>\n\n&nbsp;\n<ol start="9">\n 	<li><span style="font-weight: 400;">  </span><b>What are some of your current challenges?</b></li>\n</ol>\n<span style="font-weight: 400;">Keep KLIMBING, the reading buddy program, will need funding to become self-sustaining.  Right now proceeds from KLIMB Consulting support it, but I need to fundraise to grow it.  We have started but we need to create a more seamless and scalable system to recruit volunteers, and we need to get schools and libraries to provide space.  Just a lot of logistics surrounding the buddy reading, but I think that once I figure it out in one location we can replicate it easily.  My dream is for it to become a national program. </span>\n\n&nbsp;\n<ol start="10">\n 	<li><span style="font-weight: 400;">  </span><b>What are some of the biggest positive or negative surprises in your business?</b></li>\n</ol>\n<span style="font-weight: 400;">The most wonderful surprise has been the kindness of people.  The response from mentors and volunteers is heart-warming.  I feel like there is so much negativity in the world that it is so nice to see all of the goodness in people. </span>', 'Lindsay Terry-Lloyd', '', 'draft', 'open', 'open', '', '', '', '', '2017-09-23 03:58:35', '2017-09-23 03:58:35', '', 0, 'http://nxtchptr.herokuapp.com/?p=94', 0, 'post', '', 0) ;
INSERT INTO `wp_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(95, 1, '2017-09-23 03:56:15', '2017-09-23 03:56:15', '<ol>\r\n 	<li><span style="font-weight: 400;">     </span><b>What is your business?</b></li>\r\n</ol>\r\n<span style="font-weight: 400;">KLIMB is a consultancy that provides dyslexia assessment and helps dyslexic children and their families overcome the educational and emotional obstacles that go hand-in-hand with the disability.  I conduct [Hyperlink: Slingerland Assessments] and, then once I know exactly what the problem is, I help families put together plans that cater to the way kids learn.  These plans can be a big help to teachers, tutors and parents in gearing a child’s lessons to his or her learning style. </span>\r\n\r\n<span style="font-weight: 400;">Dyslexia is a hereditary learning disability among people of normal intelligence who have difficulty acquiring and processing language.  When children learn to read, they first figure out the sound that each letter makes, for example, "D" makes a "duh" sound.  Dyslexic people have a hard time connecting letters to the sounds they make and then blending them into words.  As a result, people with dyslexia often mix up the reading of words, for example reading “was” as “saw” or “of” as “for”, consequently reading becomes extremely slow and inaccurate.  Dyslexia can affect a person’s ability to read, write, spell, and even speak, which clearly makes school an extraordinary challenge.  It is said that 20% of the earth’s population is dyslexic and many are undiagnosed. </span>\r\n\r\n<span style="font-weight: 400;">Research shows that dyslexic children’s academic trajectories are dramatically improved with professionally guided reading tutorials.  As a result, there is a lot of demand for trained reading tutors, and in Northern California the tutoring cost is about $100 an hour.  Having spoken to many families about a tutoring plan for their children, I grew concerned about how many families couldn’t afford tutoring for the recommended four to five times a week.  </span>\r\n\r\n<span style="font-weight: 400;">In response to that concern I just launched the pilot program for Keep KLIMBing.  Its purpose is to level the playing field so that all children, regardless of economic advantage, can experience the beneficial effects of reading intervention.  Effectively, it is a buddy reading program that we provide at no or low cost to families so that intervention can happen as soon as possible.  The buddies are trained volunteers, and the reading locations take place in donated spaces like libraries and schools.  </span>\r\n\r\n<span style="font-weight: 400;">One of my favorite quotes is by the dyslexic, Albert Einstein, and it reads: </span><i><span style="font-weight: 400;">“Everybody is a genius. But if you judge a fish by its ability to climb a tree, it will live it\'s whole life believing that it is stupid." </span></i><span style="font-weight: 400;"> Inspired by this quote I named my corporation, KLIMB, which is meant to be a misspelled version of the word “climb” as someone with dyslexia might spell it.  </span>\r\n\r\n&nbsp;\r\n<ol start="2">\r\n 	<li><span style="font-weight: 400;">     </span><b>What made you decide to start your business?</b></li>\r\n</ol>\r\n<span style="font-weight: 400;">When my oldest daughter was diagnosed with dyslexia 5 years ago, it was really hard for me.  I remember feeling incredibly lonely as I was navigating everything for the first time.  Then a year later, my second daughter was also diagnosed as dyslexic.  My oldest daughter became extremely anxious at school each day and especially around the weekly spelling tests. We were worried about her stress level at such a young age and so we moved her to [Hyperlink: Charles Armstrong School], a local school that was specifically set up for kids with dyslexia.  My younger daughter continued to go to her regular school, as she seemed to be managing her stress and getting the support she needed.  </span>\r\n\r\n<span style="font-weight: 400;">As a result, I had experience with dyslexic kids at regular and targeted schools, and I learned how to advocate for my kids at each place.  Then, my older daughter went back to the regular school after three years, and so I became familiar with re-entry.  At the same time that my oldest daughter returned to her mainstream school, my son, the third child in our family, was diagnosed with dyslexia. </span>\r\n\r\n<span style="font-weight: 400;">I am passionate about improving the lives of kids with dyslexia, and the time also seemed right once my kids were in school fulltime. I wanted to do something of my own and to be contributing.  </span>\r\n\r\n<span style="font-weight: 400;">  </span>\r\n<ol start="3">\r\n 	<li><span style="font-weight: 400;">     </span><b>Was there one moment that gave you the confidence that this was a good idea?</b></li>\r\n</ol>\r\n<span style="font-weight: 400;">Having experienced the roller coaster ride of dyslexia for the past five years, at one point I looked around and realized that I knew a lot about all aspects of the disability.  I felt strongly that all children with dyslexia should have the opportunity to reach their full potential, and I felt like I could make that happen given my educational background in psychology (undergraduate and masters).  </span>\r\n\r\n<span style="font-weight: 400;">As part of my research into dyslexia, I spent some time learning about Richard Branson and other famous dyslexics.  Richard Branson, the well-known founder of Virgin Records and Virgin Airways, was told by his former headmaster that, “He would either end up in prison or become a millionaire.” </span>\r\n\r\n<span style="font-weight: 400;">What is interesting to me about this quote is that it quite accurately summarizes the outcomes for kids with dyslexia.  Some of the most innovative people of our time are dyslexic, Steven Spielberg, Charles Schwab, Thomas Edison to name a few, and there are estimates that 48% of prison inmates are also dyslexic.  A lot of this bifurcation has to do with how these people, as children, ended up feeling about their reading problems.  Some felt they had talents to contribute even though reading was hard for them and others felt they had nothing of value to give.   </span>\r\n\r\n&nbsp;\r\n<ol start="4">\r\n 	<li><span style="font-weight: 400;">     </span><b>Was there ever a particularly tough time that in retrospect was a priceless learning moment?</b></li>\r\n</ol>\r\n<span style="font-weight: 400;">Yes, two examples particularly stand out in my mind as heart-breaking times for me that drive me to advocate for these kids.   </span>\r\n\r\n<span style="font-weight: 400;">The first example of a really hard experience was when I volunteered to give twenty Slingerland Assessments at a Bay Area school.  Of the 20 kids I screened, every single one of them needed help.  Until I got there, I hadn’t realized the overwhelming need.  They may not all end up being dyslexic, but they all needed reading support.  Because of feeling so helpless with such great numbers, I was motivated to create an intervention arm of KLIMB, what eventually became Keep KLIMBing.</span>\r\n\r\n<span style="font-weight: 400;">The second example comes from my consulting practice.  I was working with a kid from an affluent family, who ended up being dyslexic, who had been told by his teachers over and over again that he just wasn’t trying.  It was heart-breaking to see how down on himself he was. He was still so young and clearly bright but he was giving up. This is a tragedy but unfortunately it isn’t uncommon.  I often recall this student’s particular story when I’m tired or feeling  stretched, and it pushes me on.</span>\r\n\r\n&nbsp;\r\n<ol start="5">\r\n 	<li><span style="font-weight: 400;">     </span><b>Were your family and friends helpful or obstacles in launching your business?  How so?</b></li>\r\n</ol>\r\n<span style="font-weight: 400;">My kids were the reason for this.  My children are my daily inspiration. Seeing the effects dyslexia has on a child at such a young age is profound.  Without the belief that you have anything worthwhile to contribute, a child will give up and this is what happens to so many bright dyslexic children. I never wanted my kids to lose confidence or think that they couldn’t achieve anything they wanted in life.  Watching my children work and persevere each day motivates me to do the same, to persist in my goals for KLIMB, regardless of obstacles along the way. </span>\r\n\r\n&nbsp;\r\n<ol start="6">\r\n 	<li><span style="font-weight: 400;">     </span><b>Were there any partnerships or advice that were particularly helpful?</b></li>\r\n</ol>\r\n<span style="font-weight: 400;">Lynne Ruppel and I initially met for business referral reasons, but in that first meeting we ended up conceiving of the idea of Keep KLIMBing.  Lynne and I were both concerned about all of the families who can’t afford the tutoring but who need it just as badly as the families who can afford it.  Lynne is an educational therapist who is very educated on reading intervention and her involvement is integral in making the idea of Keep KLIMBing work.  She trains the volunteers on various phonemic awareness activities, games, and literacy instruction and creates daily lesson plans. These trainings and lesson plans help set Keep KLIMBing apart from other reading buddy programs. Through Lynne’s explicit instruction and our early intervention, one-to-one model, we aim to provide quantifiable reading improvement for every participating child. </span>\r\n\r\n&nbsp;\r\n<ol start="7">\r\n 	<li><span style="font-weight: 400;">     </span><b>Who did you speak to for support as you were working on the idea/launch?</b></li>\r\n</ol>\r\n<span style="font-weight: 400;">Two people have been incredibly supportive to me.</span>\r\n\r\n<span style="font-weight: 400;">The first is Tuck Geerds, an educational therapist.  She gave the Slingerland Assessment to both of my daughters, and I was always inspired by her and appreciated her advice.  When I started this business she gave me great advice.  One of the truly wonderful gifts was that she allowed me to use her proprietary tool that helps with deciphering the results of the assessment.  This has been a big help in giving parents really clear feedback on their children’s needs. </span>\r\n\r\n<span style="font-weight: 400;">The second person is my husband.  He also has dyslexia, so I think he supports me both because he understands so clearly what it is like to live with the hurdles of the disability and also just because he believes in me.  He supported KLIMB from the start and has helped spread the word of my business through his wide network on LinkedIn.    </span>\r\n\r\n<span style="font-weight: 400;">  </span>\r\n<ol start="8">\r\n 	<li><span style="font-weight: 400;">  </span><b>What are some successes you have had with your business that make you proud?</b></li>\r\n</ol>\r\n<span style="font-weight: 400;">I was nervous that my consulting wouldn’t provide something of value.  The consulting side is hard to quantify, and I wanted clients to feel like I was making a difference in their lives.  I knew that I was helping out when the mothers were relieved by my advice.  Sometimes it is something so simple. In one instance, a mother was asking about audiobooks and whether it was OK for her son to listen to them since he loved them so much.  It turns out that audio books are great for kids with dyslexia, so she was thrilled that she could preserve that pleasure for her son.   </span>\r\n\r\n<span style="font-weight: 400;">The role that parents play is also very interesting to me.  I really believe that whatever feelings the parents have get subtly communicated to the kids.  So, I also spend a lot of time giving parents motivational information so that they realize that their child really can do anything she wants to do.  Dyslexia doesn’t go away, but giving parents hope prevents them from unintentionally sending negative feelings to their kids.  All of these amazing people like Branson, Einstein, Spielberg, Picasso, had this.  Of course, they had to work hard to get what they wanted, but that is an important lesson too.  Whenever I can help remove the worries of a parent or give them hope that makes me really proud. </span>\r\n\r\n&nbsp;\r\n<ol start="9">\r\n 	<li><span style="font-weight: 400;">  </span><b>What are some of your current challenges?</b></li>\r\n</ol>\r\n<span style="font-weight: 400;">Keep KLIMBING, the reading buddy program, will need funding to become self-sustaining.  Right now proceeds from KLIMB Consulting support it, but I need to fundraise to grow it.  We have started but we need to create a more seamless and scalable system to recruit volunteers, and we need to get schools and libraries to provide space.  Just a lot of logistics surrounding the buddy reading, but I think that once I figure it out in one location we can replicate it easily.  My dream is for it to become a national program. </span>\r\n\r\n&nbsp;\r\n<ol start="10">\r\n 	<li><span style="font-weight: 400;">  </span><b>What are some of the biggest positive or negative surprises in your business?</b></li>\r\n</ol>\r\n<span style="font-weight: 400;">The most wonderful surprise has been the kindness of people.  The response from mentors and volunteers is heart-warming.  I feel like there is so much negativity in the world that it is so nice to see all of the goodness in people. </span>', 'Lindsay Terry-Lloyd', '', 'inherit', 'closed', 'closed', '', '94-revision-v1', '', '', '2017-09-23 03:56:15', '2017-09-23 03:56:15', '', 94, 'http://nxtchptr.herokuapp.com/94-revision-v1/', 0, 'revision', '', 0),
(96, 1, '2017-09-23 09:12:11', '2017-09-23 09:12:11', '', 'Members', '', 'inherit', 'closed', 'closed', '', '74-revision-v1', '', '', '2017-09-23 09:12:11', '2017-09-23 09:12:11', '', 74, 'http://nxtchptr.herokuapp.com/74-revision-v1/', 0, 'revision', '', 0),
(97, 1, '2017-09-26 12:39:43', '2017-09-26 12:39:43', 'The Sales Representative position is responsible for attaining direct sales goals and objectives through the creation and execution of a sales plan in the assigned territory. This would include the training, sales and related support services of P28 products. This position will drive sales growth through communication and in-person interactions with current and prospective customers regarding P28 products. Earning potential of $200k+/year.', 'Director of Communications', '', 'publish', 'closed', 'closed', '', 'director-of-communications', '', '', '2017-09-26 12:39:44', '2017-09-26 12:39:44', '', 0, 'http://nxtchptr.herokuapp.com/job/director-of-communications/', 0, 'job', '', 0),
(98, 1, '2017-09-26 12:46:37', '2017-09-26 12:46:37', '[wpjb_jobs_list]', 'Jobs', '', 'inherit', 'closed', 'closed', '', '59-autosave-v1', '', '', '2017-09-26 12:46:37', '2017-09-26 12:46:37', '', 59, 'http://nxtchptr.herokuapp.com/59-autosave-v1/', 0, 'revision', '', 0),
(99, 1, '2017-09-26 12:53:35', '2017-09-26 12:53:35', '[wpjb_jobs_list]', 'Jobs', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2017-09-26 12:53:35', '2017-09-26 12:53:35', '', 59, 'http://nxtchptr.herokuapp.com/59-revision-v1/', 0, 'revision', '', 0),
(100, 1, '2017-10-11 12:06:27', '2017-10-11 12:06:27', 'This is a cool job', 'Sample Job', '', 'publish', 'closed', 'closed', '', 'sample-job', '', '', '2017-10-11 13:30:10', '2017-10-11 13:30:10', '', 0, 'http://nxtchptr.herokuapp.com/job/sample-job/', 0, 'job', '', 0),
(101, 1, '2017-10-11 12:22:43', '2017-10-11 12:22:43', 'This is stuff', 'Test Single Posting', '', 'wpjb-disabled', 'closed', 'closed', '', 'test-single-posting', '', '', '2017-10-11 12:22:44', '2017-10-11 12:22:44', '', 0, 'http://nxtchptr.herokuapp.com/job/test-single-posting/', 0, 'job', '', 0),
(102, 1, '2017-10-11 12:41:14', '2017-10-11 12:41:14', '', 'Test Co.', '', 'wpjb-disabled', 'closed', 'closed', '', 'test-co', '', '', '2017-10-11 12:41:14', '2017-10-11 12:41:14', '', 0, 'http://nxtchptr.herokuapp.com/company/test-co/', 0, 'company', '', 0),
(103, 1, '2017-10-11 13:32:57', '2017-10-11 13:32:57', 'This is a sample', 'The Best Job Ever!!!', '', 'publish', 'closed', 'closed', '', 'the-best-job-ever', '', '', '2017-10-11 13:32:59', '2017-10-11 13:32:59', '', 0, 'http://nxtchptr.herokuapp.com/job/the-best-job-ever/', 0, 'job', '', 0) ;

#
# End of data contents of table `wp_posts`
# --------------------------------------------------------



#
# Delete any existing table `wp_signups`
#

DROP TABLE IF EXISTS `wp_signups`;


#
# Table structure of table `wp_signups`
#

CREATE TABLE `wp_signups` (
  `signup_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `domain` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `path` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `title` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `activation_key` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `meta` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`signup_id`),
  KEY `activation_key` (`activation_key`),
  KEY `user_email` (`user_email`),
  KEY `user_login_email` (`user_login`,`user_email`),
  KEY `domain_path` (`domain`(140),`path`(51))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_signups`
#

#
# End of data contents of table `wp_signups`
# --------------------------------------------------------



#
# Delete any existing table `wp_term_relationships`
#

DROP TABLE IF EXISTS `wp_term_relationships`;


#
# Table structure of table `wp_term_relationships`
#

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_term_relationships`
#
INSERT INTO `wp_term_relationships` ( `object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(20, 2, 0),
(21, 2, 0),
(22, 2, 0),
(23, 2, 0),
(24, 2, 0),
(26, 2, 0),
(27, 3, 0),
(28, 3, 0),
(29, 3, 0),
(30, 3, 0),
(31, 3, 0),
(39, 4, 0),
(41, 4, 0),
(42, 4, 0),
(43, 5, 0),
(44, 5, 0),
(50, 6, 0),
(51, 6, 0),
(52, 6, 0),
(75, 7, 0),
(76, 8, 0),
(77, 9, 0),
(78, 10, 0),
(79, 11, 0),
(80, 12, 0),
(81, 13, 0),
(82, 14, 0),
(83, 15, 0),
(84, 16, 0),
(85, 17, 0),
(86, 18, 0),
(87, 19, 0),
(88, 20, 0),
(89, 21, 0),
(90, 22, 0),
(94, 1, 0) ;

#
# End of data contents of table `wp_term_relationships`
# --------------------------------------------------------



#
# Delete any existing table `wp_term_taxonomy`
#

DROP TABLE IF EXISTS `wp_term_taxonomy`;


#
# Table structure of table `wp_term_taxonomy`
#

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_term_taxonomy`
#
INSERT INTO `wp_term_taxonomy` ( `term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 6),
(3, 3, 'nav_menu', '', 0, 5),
(4, 4, 'nav_menu', '', 0, 3),
(5, 5, 'nav_menu', '', 0, 2),
(6, 6, 'nav_menu', '', 0, 3),
(7, 7, 'bp-email-type', 'A member has replied to an activity update that the recipient posted.', 0, 1),
(8, 8, 'bp-email-type', 'A member has replied to a comment on an activity update that the recipient posted.', 0, 1),
(9, 9, 'bp-email-type', 'Recipient was mentioned in an activity update.', 0, 1),
(10, 10, 'bp-email-type', 'Recipient was mentioned in a group activity update.', 0, 1),
(11, 11, 'bp-email-type', 'Recipient has registered for an account.', 0, 1),
(12, 12, 'bp-email-type', 'Recipient has registered for an account and site.', 0, 1),
(13, 13, 'bp-email-type', 'A member has sent a friend request to the recipient.', 0, 1),
(14, 14, 'bp-email-type', 'Recipient has had a friend request accepted by a member.', 0, 1),
(15, 15, 'bp-email-type', 'A group\'s details were updated.', 0, 1),
(16, 16, 'bp-email-type', 'A member has sent a group invitation to the recipient.', 0, 1),
(17, 17, 'bp-email-type', 'Recipient\'s status within a group has changed.', 0, 1),
(18, 18, 'bp-email-type', 'A member has requested permission to join a group.', 0, 1),
(19, 19, 'bp-email-type', 'Recipient has received a private message.', 0, 1),
(20, 20, 'bp-email-type', 'Recipient has changed their email address.', 0, 1),
(21, 21, 'bp-email-type', 'Recipient had requested to join a group, which was accepted.', 0, 1),
(22, 22, 'bp-email-type', 'Recipient had requested to join a group, which was rejected.', 0, 1) ;

#
# End of data contents of table `wp_term_taxonomy`
# --------------------------------------------------------



#
# Delete any existing table `wp_termmeta`
#

DROP TABLE IF EXISTS `wp_termmeta`;


#
# Table structure of table `wp_termmeta`
#

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_termmeta`
#

#
# End of data contents of table `wp_termmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_terms`
#

DROP TABLE IF EXISTS `wp_terms`;


#
# Table structure of table `wp_terms`
#

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_terms`
#
INSERT INTO `wp_terms` ( `term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Main Navigation', 'main-navigation', 0),
(3, 'Footer Navigation', 'footer-navigation', 0),
(4, 'Main Navigation - Member', 'main-navigation-member', 0),
(5, 'Footer - Members', 'footer-members', 0),
(6, 'Personal Area', 'personal-area', 0),
(7, 'activity-comment', 'activity-comment', 0),
(8, 'activity-comment-author', 'activity-comment-author', 0),
(9, 'activity-at-message', 'activity-at-message', 0),
(10, 'groups-at-message', 'groups-at-message', 0),
(11, 'core-user-registration', 'core-user-registration', 0),
(12, 'core-user-registration-with-blog', 'core-user-registration-with-blog', 0),
(13, 'friends-request', 'friends-request', 0),
(14, 'friends-request-accepted', 'friends-request-accepted', 0),
(15, 'groups-details-updated', 'groups-details-updated', 0),
(16, 'groups-invitation', 'groups-invitation', 0),
(17, 'groups-member-promoted', 'groups-member-promoted', 0),
(18, 'groups-membership-request', 'groups-membership-request', 0),
(19, 'messages-unread', 'messages-unread', 0),
(20, 'settings-verify-email-change', 'settings-verify-email-change', 0),
(21, 'groups-membership-request-accepted', 'groups-membership-request-accepted', 0),
(22, 'groups-membership-request-rejected', 'groups-membership-request-rejected', 0) ;

#
# End of data contents of table `wp_terms`
# --------------------------------------------------------



#
# Delete any existing table `wp_usermeta`
#

DROP TABLE IF EXISTS `wp_usermeta`;


#
# Table structure of table `wp_usermeta`
#

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_usermeta`
#
INSERT INTO `wp_usermeta` ( `umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', 'admin'),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'locale', ''),
(11, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(12, 1, 'wp_user_level', '10'),
(13, 1, 'dismissed_wp_pointers', ''),
(14, 1, 'show_welcome_panel', '1'),
(16, 1, 'wp_dashboard_quick_press_last_post_id', '3'),
(17, 1, 'community-events-location', 'a:1:{s:2:"ip";s:9:"127.0.0.0";}'),
(18, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";i:4;s:15:"title-attribute";}'),
(19, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:"add-post_tag";}'),
(20, 1, 'nav_menu_recently_edited', '4'),
(21, 1, 'pmpro_visits', 'a:5:{s:4:"last";s:16:"October 10, 2017";s:8:"thisdate";N;s:5:"month";i:4;s:9:"thismonth";s:2:"10";s:7:"alltime";i:14;}'),
(22, 1, 'pmpro_views', 'a:4:{s:4:"last";s:16:"October 11, 2017";s:5:"month";i:330;s:7:"alltime";i:1268;s:9:"thismonth";s:2:"10";}'),
(23, 1, 'pmpro_stripe_customerid', 'cus_BMkrkKBbhp0Oz2'),
(24, 1, 'pmpro_stripe_updates', 'a:0:{}'),
(25, 1, 'pmpro_bfirstname', ''),
(26, 1, 'pmpro_blastname', ''),
(27, 1, 'pmpro_baddress1', ''),
(28, 1, 'pmpro_baddress2', ''),
(29, 1, 'pmpro_bcity', ''),
(30, 1, 'pmpro_bstate', ''),
(31, 1, 'pmpro_bzipcode', ''),
(32, 1, 'pmpro_bcountry', ''),
(33, 1, 'pmpro_bphone', ''),
(34, 1, 'pmpro_bemail', 'laksmipati_dasa@hotmail.com'),
(35, 1, 'pmpro_CardType', 'Visa'),
(36, 1, 'pmpro_AccountNumber', 'XXXX-XXXX-XXXX-4242'),
(37, 1, 'pmpro_ExpirationMonth', '01'),
(38, 1, 'pmpro_ExpirationYear', '2021'),
(39, 1, 'closedpostboxes_page', 'a:1:{i:0;s:6:"acf_57";}'),
(40, 1, 'metaboxhidden_page', 'a:6:{i:0;s:12:"postimagediv";i:1;s:10:"postcustom";i:2;s:16:"commentstatusdiv";i:3;s:11:"commentsdiv";i:4;s:7:"slugdiv";i:5;s:9:"authordiv";}'),
(42, 1, 'pmpro_stripe_next_on_date_update', ''),
(80, 5, 'nickname', 'Luke-and_Rohini'),
(82, 5, 'first_name', 'Luke-and'),
(84, 5, 'last_name', 'Rohini'),
(86, 5, 'description', ''),
(88, 5, 'rich_editing', 'true'),
(90, 5, 'comment_shortcuts', 'false'),
(92, 5, 'admin_color', 'fresh'),
(94, 5, 'use_ssl', '0'),
(96, 5, 'show_admin_bar_front', 'true'),
(98, 5, 'locale', ''),
(100, 5, 'wp_capabilities', 'a:1:{s:10:"subscriber";b:1;}'),
(102, 5, 'wp_user_level', '0'),
(103, 5, 'wsl_current_provider', 'Google'),
(105, 5, 'wsl_current_user_image', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=200'),
(107, 5, 'session_tokens', 'a:1:{s:64:"f56524a71e53090be0c0b4fe09aa4a4de87102ac8702002495c0d7c125474c18";a:4:{s:10:"expiration";i:1507720125;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36";s:5:"login";i:1506510525;}}'),
(109, 5, 'pmpro_logins', 'a:5:{s:4:"last";s:18:"September 27, 2017";s:8:"thisdate";N;s:5:"month";i:2;s:9:"thismonth";s:1:"9";s:7:"alltime";i:2;}'),
(111, 5, 'pmpro_views', 'a:4:{s:4:"last";s:18:"September 27, 2017";s:5:"month";i:7;s:7:"alltime";i:7;s:9:"thismonth";s:1:"9";}'),
(114, 1, 'pmpro_logins', 'a:5:{s:4:"last";s:16:"October 10, 2017";s:8:"thisdate";N;s:5:"month";i:6;s:9:"thismonth";s:2:"10";s:7:"alltime";i:19;}'),
(116, 6, 'nickname', 'Luke_Digby'),
(117, 6, 'first_name', ''),
(118, 6, 'last_name', ''),
(119, 6, 'description', ''),
(120, 6, 'rich_editing', 'true'),
(121, 6, 'comment_shortcuts', 'false'),
(122, 6, 'admin_color', 'fresh'),
(123, 6, 'use_ssl', '0'),
(124, 6, 'show_admin_bar_front', 'true'),
(125, 6, 'locale', ''),
(126, 6, 'wp_capabilities', 'a:1:{s:10:"subscriber";b:1;}'),
(127, 6, 'wp_user_level', '0'),
(128, 6, 'wsl_current_provider', 'Instagram'),
(129, 6, 'wsl_current_user_image', 'https://scontent-ams3-1.cdninstagram.com/t51.2885-19/11906329_960233084022564_1448528159_a.jpg'),
(131, 6, 'pmpro_logins', 'a:5:{s:4:"last";s:18:"September 12, 2017";s:8:"thisdate";N;s:5:"month";i:1;s:9:"thismonth";s:1:"9";s:7:"alltime";i:1;}'),
(132, 6, 'pmpro_views', 'a:4:{s:4:"last";s:18:"September 12, 2017";s:5:"month";i:2;s:7:"alltime";i:2;s:9:"thismonth";s:1:"9";}'),
(133, 7, 'nickname', 'Laksmipati_Dasa_1'),
(134, 7, 'first_name', ''),
(135, 7, 'last_name', ''),
(136, 7, 'description', ''),
(137, 7, 'rich_editing', 'true'),
(138, 7, 'comment_shortcuts', 'false'),
(139, 7, 'admin_color', 'fresh'),
(140, 7, 'use_ssl', '0'),
(141, 7, 'show_admin_bar_front', 'true'),
(142, 7, 'locale', ''),
(143, 7, 'wp_capabilities', 'a:1:{s:10:"subscriber";b:1;}'),
(144, 7, 'wp_user_level', '0'),
(145, 7, 'wsl_current_provider', 'Goodreads'),
(146, 7, 'wsl_current_user_image', 'http://images.gr-assets.com/users/1390738686p3/28064580.jpg'),
(148, 7, 'pmpro_logins', 'a:5:{s:4:"last";s:18:"September 12, 2017";s:8:"thisdate";N;s:5:"month";i:1;s:9:"thismonth";s:1:"9";s:7:"alltime";i:1;}'),
(149, 7, 'pmpro_views', 'a:4:{s:4:"last";s:18:"September 12, 2017";s:5:"month";i:1;s:7:"alltime";i:1;s:9:"thismonth";s:1:"9";}'),
(151, 1, 'wsl_current_provider', 'LinkedIn'),
(152, 1, 'wsl_current_user_image', 'https://media.licdn.com/mpr/mprx/0_i7SM9jPR3qyn_Rp1GsDBvyGcF--9CEK1GY1qrpuzEQPz6wGg8x2vts7BxFc'),
(156, 1, 'last_activity', '2017-10-11 14:14:28'),
(157, 1, 'total_group_count', '1'),
(161, 1, 'bp_xprofile_visibility_levels', 'a:2:{i:1;s:6:"public";i:2;s:6:"public";}'),
(164, 6, 'total_group_count', '0'),
(168, 8, 'nickname', 'Laksmipati_Dasa'),
(169, 8, 'first_name', 'Laksmipati'),
(170, 8, 'last_name', 'Dasa'),
(171, 8, 'description', '') ;
INSERT INTO `wp_usermeta` ( `umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(172, 8, 'rich_editing', 'true'),
(173, 8, 'comment_shortcuts', 'false'),
(174, 8, 'admin_color', 'fresh'),
(175, 8, 'use_ssl', '0'),
(176, 8, 'show_admin_bar_front', 'true'),
(177, 8, 'locale', ''),
(178, 8, 'wp_capabilities', 'a:1:{s:10:"subscriber";b:1;}'),
(179, 8, 'wp_user_level', '0'),
(180, 8, 'wsl_current_provider', 'Facebook'),
(181, 8, 'wsl_current_user_image', 'https://graph.facebook.com/10159295816385704/picture?width=150&height=150'),
(183, 8, 'pmpro_logins', 'a:5:{s:4:"last";s:15:"October 2, 2017";s:8:"thisdate";N;s:5:"month";i:1;s:9:"thismonth";s:2:"10";s:7:"alltime";i:2;}'),
(184, 8, 'pmpro_visits', 'a:5:{s:4:"last";s:15:"October 2, 2017";s:8:"thisdate";N;s:5:"month";i:1;s:9:"thismonth";s:2:"10";s:7:"alltime";i:2;}'),
(185, 8, 'last_activity', '2017-10-02 12:41:44'),
(186, 8, 'pmpro_views', 'a:4:{s:4:"last";s:15:"October 2, 2017";s:5:"month";i:13;s:7:"alltime";i:15;s:9:"thismonth";s:2:"10";}'),
(187, 5, 'last_activity', '2017-09-27 11:08:46'),
(188, 8, 'session_tokens', 'a:1:{s:64:"9db9733ba126ed6aae2aa6747f6021a9e485fd169f428cafb7369cec072650ff";a:4:{s:10:"expiration";i:1508157338;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36";s:5:"login";i:1506947738;}}'),
(189, 7, 'total_group_count', '0'),
(190, 8, 'total_group_count', '1'),
(193, 1, 'session_tokens', 'a:1:{s:64:"b499853dfd7727bfd29349d070a6126eae4b7a47f19880099a02cea0d086db94";a:4:{s:10:"expiration";i:1507795915;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36";s:5:"login";i:1507623115;}}'),
(199, 1, '_wpjb_stripe_customer_id', 'cus_BYrFOhgvNyNF5I') ;

#
# End of data contents of table `wp_usermeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_users`
#

DROP TABLE IF EXISTS `wp_users`;


#
# Table structure of table `wp_users`
#

CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_users`
#
INSERT INTO `wp_users` ( `ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$Bf6NwQKf/mctdJTEayk.pHgRglbiRL1', 'admin', 'laksmipati_dasa@hotmail.com', '', '2017-09-07 01:15:43', '', 0, 'admin'),
(5, 'Luke-and_Rohini', '$P$BGAuiFToFMTlO1B8pnKsbfIvdNhwN.1', 'luke-and_rohini', 'laksmipati108@gmail.com', 'https://www.facebook.com/app_scoped_user_id/124649058265019/', '2017-09-11 11:40:32', '', 0, 'Luke-and Rohini'),
(6, 'Luke_Digby', '$P$BbyKdVi99vZOEMHTQ6ptpShEtTtx0V0', 'luke_digby', 'instagram_user_luke_digby@example.com', 'https://instagram.com/lukedigby108', '2017-09-12 05:22:01', '', 0, 'Luke Digby'),
(7, 'Laksmipati_Dasa_1', '$P$BVJkCVhIzG6vayEa06q4xNK32b5g0Y1', 'laksmipati_dasa_1', 'goodreads_user_laksmipati_dasa_1@example.com', 'http://www.goodreads.com/user/show/28064580-laksmipati-dasa?utm_medium=api', '2017-09-12 05:24:52', '', 0, 'Laksmipati Dasa'),
(8, 'Laksmipati_Dasa', '$P$BWnySoAvoNdcIAXgk6y21wT0QRraez.', 'laksmipati_dasa', 'lukedigby@hotmail.com', 'https://www.facebook.com/app_scoped_user_id/10159295816385704/', '2017-09-27 10:20:47', '', 0, 'Laksmipati Dasa') ;

#
# End of data contents of table `wp_users`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpjb_alert`
#

DROP TABLE IF EXISTS `wp_wpjb_alert`;


#
# Table structure of table `wp_wpjb_alert`
#

CREATE TABLE `wp_wpjb_alert` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `keyword` varchar(80) NOT NULL,
  `email` varchar(80) NOT NULL,
  `created_at` datetime NOT NULL,
  `last_run` datetime NOT NULL,
  `frequency` tinyint(1) unsigned NOT NULL COMMENT '1: daily, 2: weekly',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `last_run` (`last_run`),
  KEY `last_run_freq` (`last_run`,`frequency`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wpjb_alert`
#

#
# End of data contents of table `wp_wpjb_alert`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpjb_application`
#

DROP TABLE IF EXISTS `wp_wpjb_application`;


#
# Table structure of table `wp_wpjb_application`
#

CREATE TABLE `wp_wpjb_application` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `applied_at` datetime NOT NULL,
  `applicant_name` varchar(120) NOT NULL,
  `message` text NOT NULL,
  `email` varchar(120) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `job_id` (`job_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wpjb_application`
#
INSERT INTO `wp_wpjb_application` ( `id`, `job_id`, `user_id`, `applied_at`, `applicant_name`, `message`, `email`, `status`) VALUES
(1, 2, 1, '2017-10-11 13:16:14', 'Luke', '', 'test@me.com', 1),
(2, 2, 1, '2017-10-11 13:28:34', 'Luke', '', 'me@test.com', 1),
(3, 4, 1, '2017-10-11 13:34:43', 'Dude', '', 'perfect@me.co', 1),
(4, 4, 1, '2017-10-11 13:35:33', 'Man', '', 'me2@hello.co', 1) ;

#
# End of data contents of table `wp_wpjb_application`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpjb_company`
#

DROP TABLE IF EXISTS `wp_wpjb_company`;


#
# Table structure of table `wp_wpjb_company`
#

CREATE TABLE `wp_wpjb_company` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned NOT NULL COMMENT 'foreign key wp_users.id',
  `company_name` varchar(120) NOT NULL DEFAULT '',
  `company_slogan` varchar(250) NOT NULL DEFAULT '',
  `company_slug` varchar(80) DEFAULT NULL,
  `company_website` varchar(120) NOT NULL DEFAULT '',
  `company_info` text NOT NULL,
  `company_country` smallint(5) unsigned NOT NULL,
  `company_state` varchar(40) NOT NULL,
  `company_zip_code` varchar(20) NOT NULL,
  `company_location` varchar(250) NOT NULL DEFAULT '',
  `jobs_posted` int(11) unsigned NOT NULL DEFAULT '0',
  `is_public` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `is_verified` tinyint(1) NOT NULL DEFAULT '0' COMMENT '-2: declined, -1: pending, 0: unset, 1: verified',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  UNIQUE KEY `post_id` (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wpjb_company`
#
INSERT INTO `wp_wpjb_company` ( `id`, `post_id`, `user_id`, `company_name`, `company_slogan`, `company_slug`, `company_website`, `company_info`, `company_country`, `company_state`, `company_zip_code`, `company_location`, `jobs_posted`, `is_public`, `is_active`, `is_verified`) VALUES
(1, 102, 1, 'Test Co.', '', NULL, '', '', 840, '', '', '', 1, 0, 1, 0) ;

#
# End of data contents of table `wp_wpjb_company`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpjb_discount`
#

DROP TABLE IF EXISTS `wp_wpjb_discount`;


#
# Table structure of table `wp_wpjb_discount`
#

CREATE TABLE `wp_wpjb_discount` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `discount_for` tinyint(3) unsigned NOT NULL DEFAULT '101',
  `title` varchar(120) NOT NULL,
  `code` varchar(20) NOT NULL,
  `discount` decimal(10,2) unsigned NOT NULL,
  `type` tinyint(1) unsigned NOT NULL COMMENT '1=%; 2=$',
  `currency` varchar(3) NOT NULL,
  `expires_at` date NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL,
  `used` int(11) NOT NULL,
  `max_uses` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wpjb_discount`
#

#
# End of data contents of table `wp_wpjb_discount`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpjb_import`
#

DROP TABLE IF EXISTS `wp_wpjb_import`;


#
# Table structure of table `wp_wpjb_import`
#

CREATE TABLE `wp_wpjb_import` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `engine` varchar(20) NOT NULL,
  `keyword` varchar(80) NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `country` varchar(10) NOT NULL,
  `location` varchar(80) NOT NULL,
  `posted` tinyint(3) unsigned NOT NULL,
  `add_max` tinyint(3) unsigned NOT NULL,
  `last_run` datetime NOT NULL,
  `success` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `last_run` (`last_run`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wpjb_import`
#

#
# End of data contents of table `wp_wpjb_import`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpjb_job`
#

DROP TABLE IF EXISTS `wp_wpjb_job`;


#
# Table structure of table `wp_wpjb_job`
#

CREATE TABLE `wp_wpjb_job` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned DEFAULT NULL,
  `employer_id` int(11) unsigned DEFAULT NULL,
  `job_title` varchar(120) NOT NULL DEFAULT '',
  `job_slug` varchar(120) NOT NULL DEFAULT '',
  `job_description` text NOT NULL,
  `job_created_at` date NOT NULL,
  `job_modified_at` date NOT NULL,
  `job_expires_at` date NOT NULL,
  `job_country` smallint(5) unsigned NOT NULL,
  `job_state` varchar(40) NOT NULL,
  `job_zip_code` varchar(20) NOT NULL,
  `job_city` varchar(120) NOT NULL,
  `company_name` varchar(120) NOT NULL DEFAULT '',
  `company_url` varchar(120) NOT NULL DEFAULT '',
  `company_email` varchar(120) NOT NULL DEFAULT '',
  `is_approved` tinyint(1) unsigned NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL,
  `is_filled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_featured` tinyint(1) unsigned NOT NULL,
  `read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `membership_id` int(11) unsigned NOT NULL DEFAULT '0',
  `pricing_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `job_slug` (`job_slug`),
  UNIQUE KEY `post_id` (`post_id`),
  KEY `employer_id` (`employer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wpjb_job`
#
INSERT INTO `wp_wpjb_job` ( `id`, `post_id`, `employer_id`, `job_title`, `job_slug`, `job_description`, `job_created_at`, `job_modified_at`, `job_expires_at`, `job_country`, `job_state`, `job_zip_code`, `job_city`, `company_name`, `company_url`, `company_email`, `is_approved`, `is_active`, `is_filled`, `is_featured`, `read`, `membership_id`, `pricing_id`) VALUES
(1, 97, NULL, 'Director of Communications', 'director-of-communications', 'The Sales Representative position is responsible for attaining direct sales goals and objectives through the creation and execution of a sales plan in the assigned territory. This would include the training, sales and related support services of P28 products. This position will drive sales growth through communication and in-person interactions with current and prospective customers regarding P28 products. Earning potential of $200k+/year.', '2017-09-26', '2017-09-26', '2017-10-26', 840, '', '', 'Anywhere', 'Paragon 28', '', 'test@gtest.com', 1, 1, 0, 0, 0, 0, 0),
(2, 100, NULL, 'Sample Job', 'sample-job', 'This is a cool job', '2017-10-11', '2017-10-11', '2017-11-10', 840, '', '', 'Chicago', 'Awesomeness', '', 'me@mail.com', 1, 1, 0, 0, 0, 0, 1),
(3, 101, NULL, 'Test Single Posting', 'test-single-posting', 'This is stuff', '2017-10-11', '2017-10-11', '2017-11-10', 840, '', '', 'Anywhere', 'name here', '', 'contact@us.com', 0, 0, 0, 0, 0, 0, 2),
(4, 103, 1, 'The Best Job Ever!!!', 'the-best-job-ever', 'This is a sample', '2017-10-11', '2017-10-11', '2017-11-10', 840, '', '', 'Telecommute', 'Test Co.', '', 'laksmipati_dasa@hotmail.com', 1, 1, 0, 0, 0, 1, 2) ;

#
# End of data contents of table `wp_wpjb_job`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpjb_job_search`
#

DROP TABLE IF EXISTS `wp_wpjb_job_search`;


#
# Table structure of table `wp_wpjb_job_search`
#

CREATE TABLE `wp_wpjb_job_search` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` int(11) unsigned NOT NULL,
  `title` varchar(120) NOT NULL,
  `description` text NOT NULL,
  `company` varchar(120) NOT NULL,
  `location` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `job_id` (`job_id`),
  FULLTEXT KEY `search` (`title`,`description`,`company`,`location`),
  FULLTEXT KEY `search_location` (`location`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wpjb_job_search`
#
INSERT INTO `wp_wpjb_job_search` ( `id`, `job_id`, `title`, `description`, `company`, `location`) VALUES
(1, 1, 'Director of Communications', 'The Sales Representative position is responsible for attaining direct sales goals and objectives through the creation and execution of a sales plan in the assigned territory. This would include the training, sales and related support services of P28 products. This position will drive sales growth through communication and in-person interactions with current and prospective customers regarding P28 products. Earning potential of $200k+/year.', 'Paragon 28', 'US USA United States  Anywhere '),
(2, 2, 'Sample Job', 'This is a cool job', 'Awesomeness', 'US USA United States  Chicago '),
(3, 3, 'Test Single Posting', 'This is stuff', 'name here', 'US USA United States  Anywhere '),
(4, 4, 'The Best Job Ever!!!', 'This is a sample', 'Test Co.', 'US USA United States  Telecommute ') ;

#
# End of data contents of table `wp_wpjb_job_search`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpjb_mail`
#

DROP TABLE IF EXISTS `wp_wpjb_mail`;


#
# Table structure of table `wp_wpjb_mail`
#

CREATE TABLE `wp_wpjb_mail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sent_to` tinyint(1) unsigned NOT NULL COMMENT '1:admin; 2:job poster; 3: other',
  `format` varchar(20) NOT NULL DEFAULT 'text/plain',
  `mail_title` varchar(120) NOT NULL,
  `mail_body_text` text NOT NULL,
  `mail_body_html` text NOT NULL,
  `mail_from` varchar(120) NOT NULL,
  `mail_from_name` varchar(120) DEFAULT NULL,
  `mail_bcc` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wpjb_mail`
#
INSERT INTO `wp_wpjb_mail` ( `id`, `name`, `is_active`, `sent_to`, `format`, `mail_title`, `mail_body_text`, `mail_body_html`, `mail_from`, `mail_from_name`, `mail_bcc`) VALUES
(1, 'notify_admin_new_job', 1, 1, 'text/plain', 'New Job has been posted.', 'Greetings!\r\n\r\nThis is an automated email letting you know a new job has been\r\nposted to the job board. You can read the details below:\r\n\r\nThe job "{$job.job_title}" ({$job.id}) has been posted in {$category} {loop="job.tag.category"}{$value.title}{/loop} by ({$job.company_name} {$job.company_email}). \r\n\r\n{if condition="$payment.id>0"} Listing cost: {function="wpjb_price($payment.payment_sum, $payment.payment_currency)"} \r\n{else}\r\nThe listing was posted as a free listing. \r\n{/if}\r\n\r\nRead the original job listing here: {$job.url}\r\nSee this listing as an administrator here: {$job.admin_url}\r\n\r\nBest regards,\r\nJob Board Support', '', 'test@example.com', 'Admin', ''),
(2, 'notify_admin_payment_received', 1, 1, 'text/plain', 'Payment Received.', 'Hi there!\r\n\r\nThis is an automated email notifying you that a client has sent you\r\na payment. Read the details below.\r\n\r\nYou have received {$payment.payment_sum} from {$payment.user_id}\r\n({$payment.email}) for the listing {$payment.id}.\r\n\r\nBest regards,\r\nJob Board Support', '', 'test@example.com', 'Admin', ''),
(3, 'notify_employer_new_job', 1, 2, 'text/plain', 'Your job listing has been saved.', 'Hello!\r\n\r\nThis is an automated message from the WordPress JobBoard letting you know your job listing has been saved. You can read the details below:\r\n\r\nTitle: {$job.job_title} \r\nURL: {$job.url}\r\n\r\n{if="$payment.payment_sum==0 && $job.is_approved==0"}\r\nYour job listing is awaiting moderation. We will notify you be email when admin will approve it.\r\n{/if}\r\n{if="$payment.payment_sum>0"}\r\nIf you haven\'t made payment yet, you can do it using URL below:\r\n{$payment.url}\r\n{/if}\r\n\r\nBest regards,\r\nJob Board Support', '', 'test@example.com', 'Admin', ''),
(5, 'notify_employer_job_expires', 1, 2, 'text/plain', 'Listing will expire soon.', 'Hi there,\r\n\r\nThis is a friendly automated reminder from the WordPress JobBoard. We just wanted to let you know that your job listing for the {$job.job_title} position will expire soon --- on {$job.job_expires_at}, to be exact.\r\n\r\nNeed to see the listing? Click here: {$job.url}\r\n\r\nBest regards,\r\nJob Board Support', '', 'test@example.com', 'Admin', ''),
(6, 'notify_admin_new_application', 1, 1, 'text/plain', 'Application for: {$job.job_title}', 'Hello!\r\n\r\nThis is an automated email from the WordPress JobBoard Support team. We just wanted to let you know that an application for the {$job.job_title} position with the company {$job.company_name} has been submitted. Read the details below:\r\n\r\nOriginal job listing: {$job.url}\r\nApplication ID: {$application.id}\r\nApplicant email address: {$application.email}\r\nApplicant message: \r\n{$application.message}\r\n\r\nBest regards,\r\nJob Board Support', '', 'test@example.com', 'Admin', ''),
(8, 'notify_applicant_applied', 1, 3, 'text/plain', 'Your application has been sent', 'Hey there,\r\n\r\nThis is an automated email from the WordPress JobBoard Support team letting you know your job application for the {$job.job_title} position with {$job.company_name} has been sent successfully.\r\n\r\nIf the employer is interested in your qualifications or would like to set up an interview, he or she will contact you shortly.\r\n\r\nYou can view the original job listing here: {$job.url}\r\n\r\nThank you for using the WordPress Job Board. Good luck!\r\nJob Board Support', '', 'test@example.com', 'Admin', ''),
(9, 'notify_employer_register', 1, 2, 'text/plain', 'Your login and password', 'Hello!\r\n\r\nWelcome to the WordPress JobBoard. We\'re happy to have you. Below you\'ll find your username and password --- just in case you forget.\r\n\r\nUsername: {$username}\r\nPassword: {$password}\r\n\r\nReady to get started? Click here to log in: {$login_url}\r\n\r\nHappy hunting,\r\nJob Board Support', '', 'test@example.com', 'Admin', ''),
(10, 'notify_canditate_register', 1, 3, 'text/plain', 'Your login and password', 'Hello!\r\n\r\nWelcome to the WordPress JobBoard. We\'re happy to have you. Below you\'ll find your username and password --- just in case you forget.\r\n\r\nUsername: {$username}\r\nPassword: {$password}\r\n\r\nReady to get started? Click here to log in: {$login_url}\r\n\r\nHappy hunting,\r\nJob Board Support', '', 'test@example.com', 'Admin', ''),
(13, 'notify_admin_grant_access', 1, 1, 'text/plain', 'Employer requesting verification', 'Hi there,\r\n\r\nThis is an automated email from the WordPress JobBoard Support Team letting you know that an employer has requested access to resumes. Read the details below:\r\n\r\nEmployer requesting verification: {$company.company_name} (Company ID: {$company.id})\r\n\r\nEmployer email: {$company.email}\r\n\r\nYou can view (and approve) the company profile here:\r\n{$company_edit_url}.\r\n\r\nBest regards,\r\nJob Board Support', '', 'test@example.com', 'Admin', ''),
(14, 'notify_employer_verify', 1, 2, 'text/plain', 'Verification request', 'Hello,\r\n\r\nYou are receiving this email because you requested manual verification for a company profile. \r\n{if condition="$company.is_verified==1"} Your account was verified successfully. You now have full access to resumes. {else} \r\nThe administrator has declined you access to resumes. Try updating your profile and requesting verification again.{/if}\r\n\r\nBest regards,\r\nJob Board Support', '', 'test@example.com', 'Admin', ''),
(15, 'notify_employer_new_application', 1, 2, 'text/plain', 'Application for position {$job.job_title}', 'Hi there,\r\n\r\nThis is an automated email from the WordPress JobBoard Support team letting you know that an application (Applicant ID: {$application.id}) for the {$job.job_title} position you posted on {$job.job_created_at} has been submitted.\r\n\r\nSee the original listing here: {$job.url}\r\n\r\nBest regards,\r\nJob Board Support', '', 'test@example.com', 'Admin', ''),
(16, 'notify_job_alerts', 1, 4, 'text/plain', 'Your email alert.', 'Greetings from the WordPress JobBoard!\r\n\r\nHere are some new job listings for you to check out.\r\n\r\n{loop="jobs"}\r\n {$value.job_title} at {$value.company_name}\r\n {$value.url}\r\n --------------------------------------------------------------\r\n{/loop}\r\n\r\nNo longer wish to receive email notifications about new postings on the WordPress JobBoard? Click here to unsubscribe from this list: \r\n{$unsubscribe_url}\r\n\r\nBest regards,\r\nJob Board Support', '', 'test@example.com', 'Admin', ''),
(17, 'notify_employer_job_paid', 1, 2, 'text/plain', 'Your job has been activated', 'Hi there,\r\n\r\nThis is an automated email from the WordPress JobBoard Support team letting you know your job "{$job.job_title}" has been activated.\r\n\r\nYou can see it live at: {$job.url}\r\n\r\nBest regards,\r\nJob Board Support', '', 'test@example.com', 'Admin', ''),
(18, 'notify_employer_resume_paid', 1, 2, 'text/plain', 'Your resume access has been granted.', 'Hi there,\r\n\r\nThis is an automated email from the WordPress JobBoard Support team letting you know you have been granted access to "{$resume.headline}" resume.\r\n\r\nYou can see it live at:\r\n{$resume_unique_url}\r\n\r\nBest regards,\r\nJob Board Support', '', 'test@example.com', 'Admin', ''),
(19, 'notify_applicant_status_change', 1, 3, 'text/plain', 'Application status changed', 'Hi there!\r\n\r\nThis is an automated email notifying you that your application for job "{$job.job_title}" has been reviewed. Your application status has been changed to {$status}.\r\n\r\nBest regards,\r\nJob Board Support', '', 'test@example.com', 'Admin', ''),
(20, 'notify_admin_general_application', 1, 1, 'text/plain', 'New general application.', 'Hello!\r\n\r\nThis is an automated email from the WordPress JobBoard Support team. We just wanted to let you know that new general application has been submitted. Read the details below:\r\n\r\nApplication ID: {$application.id}\r\nApplicant email address: {$application.email}\r\nApplicant message: \r\n{$application.message}\r\n\r\nBest regards,\r\nJob Board Support', '', 'test@example.com', 'Admin', ''),
(21, 'notify_account_approved', 1, 4, 'text/plain', 'Your account has been approved.', 'Hello!\r\n\r\nThis is an automated email from the WordPress JobBoard Support team. We just wanted to let you know that your account has been approved and you can now login\r\n\r\n{$login_url}\r\n\r\nBest regards,\r\nJob Board Support', '', 'test@example.com', 'Admin', ''),
(22, 'notify_admin_new_employer', 1, 1, 'text/plain', 'New Employer: {$company.company_name}', 'Hi there!\r\n\r\nThis is an automated email letting you know a new company {$company.company_name} has been registered on the job board.\r\n\r\nView the company profile: \r\n{$company.url}\r\n\r\nSee this listing as an administrator here: \r\n{$company.admin_url}\r\n\r\nBest regards,\r\nJob Board Support', '', 'test@example.com', 'Admin', ''),
(23, 'notify_admin_new_candidate', 1, 1, 'text/plain', 'New Candidate: {$resume.full_name}', 'Hi there!\r\n\r\nThis is an automated email letting you {$resume.full_name} registered on the job board.\r\n\r\nView the company profile: \r\n{$resume.url}\r\n\r\nSee this listing as an administrator here: \r\n{$resume.admin_url}\r\n\r\nBest regards,\r\nJob Board Support', '', 'test@example.com', 'Admin', ''),
(24, 'notify_candidate_message', 1, 3, 'text/plain', 'Message From {$company.company_name}', '{$contact_form.message}\r\n\r\n--------------------\r\n{$contact_form.fullname} / {$contact_form.email}', '', 'test@example.com', 'Admin', '') ;

#
# End of data contents of table `wp_wpjb_mail`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpjb_membership`
#

DROP TABLE IF EXISTS `wp_wpjb_membership`;


#
# Table structure of table `wp_wpjb_membership`
#

CREATE TABLE `wp_wpjb_membership` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `package_id` int(11) unsigned NOT NULL,
  `started_at` date NOT NULL,
  `expires_at` date NOT NULL,
  `package` text,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wpjb_membership`
#
INSERT INTO `wp_wpjb_membership` ( `id`, `user_id`, `package_id`, `started_at`, `expires_at`, `package`) VALUES
(1, 1, 3, '2017-10-11', '2018-10-11', 'a:3:{i:101;a:1:{i:2;a:4:{s:2:"id";i:2;s:6:"status";s:9:"unlimited";s:5:"usage";s:0:"";s:4:"used";i:0;}}i:201;a:0:{}s:5:"valid";s:3:"365";}') ;

#
# End of data contents of table `wp_wpjb_membership`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpjb_meta`
#

DROP TABLE IF EXISTS `wp_wpjb_meta`;


#
# Table structure of table `wp_wpjb_meta`
#

CREATE TABLE `wp_wpjb_meta` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `meta_object` varchar(20) NOT NULL COMMENT 'job, resume, company',
  `meta_type` tinyint(1) unsigned NOT NULL COMMENT '1:builtin; 2:registered; 3:visual',
  `meta_value` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `object_name` (`meta_object`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wpjb_meta`
#
INSERT INTO `wp_wpjb_meta` ( `id`, `name`, `meta_object`, `meta_type`, `meta_value`) VALUES
(1, 'color', 'tag', 2, ''),
(2, 'is_featured', 'pricing', 1, ''),
(3, 'visible', 'pricing', 1, ''),
(4, 'geo_status', 'job', 1, ''),
(5, 'geo_latitude', 'job', 1, ''),
(6, 'geo_longitude', 'job', 1, ''),
(8, 'job_description_format', 'job', 1, ''),
(9, 'geo_status', 'company', 1, ''),
(10, 'geo_latitude', 'company', 1, ''),
(11, 'geo_longitude', 'company', 1, ''),
(12, 'geo_status', 'resume', 1, ''),
(13, 'geo_latitude', 'resume', 1, ''),
(14, 'geo_longitude', 'resume', 1, ''),
(15, 'company_info_format', 'company', 1, ''),
(16, 'job_source', 'job', 1, ''),
(17, 'package', 'pricing', 1, ''),
(18, 'linkedin_profile_url', 'apply', 1, ''),
(19, 'access_keys', 'resume', 1, ''),
(20, 'company_logo', 'job', 1, ''),
(21, 'file', 'apply', 1, ''),
(22, 'image', 'resume', 1, ''),
(23, 'company_logo', 'company', 1, ''),
(24, 'features', 'pricing', 1, ''),
(25, 'is_trial', 'pricing', 1, ''),
(26, 'rating', 'apply', 1, ''),
(27, 'job_duties_responsbilities', 'job', 3, 'a:14:{s:5:"title";s:37:"Essential Duties and Responsibilities";s:4:"name";s:26:"job_duties_responsbilities";s:4:"hint";s:55:"Will be shown as a list. Enter each item on a new line.";s:7:"default";s:0:"";s:3:"css";s:0:"";s:11:"is_required";i:0;s:10:"is_trashed";s:1:"1";s:10:"is_builtin";i:0;s:4:"type";s:17:"ui-input-textarea";s:10:"visibility";s:1:"0";s:16:"textarea_wysiwyg";s:1:"0";s:8:"is_saved";i:1;s:5:"order";i:-1;s:5:"group";s:0:"";}'),
(28, 'job_skills_abilities', 'job', 3, 'a:14:{s:5:"title";s:29:"Required Skills and Abilities";s:4:"name";s:20:"job_skills_abilities";s:4:"hint";s:55:"Will be shown as a list. Enter each item on a new line.";s:7:"default";s:0:"";s:3:"css";s:0:"";s:11:"is_required";i:0;s:10:"is_trashed";s:1:"0";s:10:"is_builtin";i:0;s:4:"type";s:17:"ui-input-textarea";s:10:"visibility";s:1:"0";s:16:"textarea_wysiwyg";s:1:"0";s:8:"is_saved";i:1;s:5:"order";i:3;s:5:"group";s:3:"job";}'),
(29, 'field_1', 'job', 3, 'a:14:{s:5:"title";s:8:"Field #1";s:4:"name";s:7:"field_1";s:4:"hint";s:0:"";s:7:"default";s:0:"";s:3:"css";s:0:"";s:11:"is_required";i:0;s:10:"is_trashed";s:1:"1";s:10:"is_builtin";i:0;s:4:"type";s:17:"ui-input-textarea";s:16:"textarea_wysiwyg";s:0:"";s:8:"is_saved";i:1;s:10:"visibility";s:1:"0";s:5:"order";i:-1;s:5:"group";s:0:"";}'),
(30, 'company_description', 'job', 3, 'a:12:{s:4:"name";s:19:"company_description";s:5:"title";s:19:"Company Description";s:4:"type";s:17:"ui-input-textarea";s:8:"is_saved";i:1;s:10:"is_builtin";i:0;s:4:"hint";s:0:"";s:11:"is_required";i:0;s:10:"visibility";s:1:"0";s:16:"textarea_wysiwyg";s:0:"";s:5:"order";i:7;s:5:"group";s:7:"company";s:10:"is_trashed";s:1:"0";}'),
(31, 'job_duties_responsibilities', 'job', 3, 'a:12:{s:4:"name";s:27:"job_duties_responsibilities";s:5:"title";s:37:"Essential Duties and Responsibilities";s:4:"type";s:17:"ui-input-textarea";s:8:"is_saved";i:0;s:10:"is_builtin";i:0;s:4:"hint";s:55:"Will be shown as a list. Enter each item on a new line.";s:11:"is_required";i:0;s:10:"visibility";s:1:"0";s:16:"textarea_wysiwyg";s:1:"0";s:5:"order";i:2;s:5:"group";s:3:"job";s:10:"is_trashed";s:1:"0";}'),
(32, 'application_linked_in', 'apply', 3, 'a:13:{s:4:"name";s:21:"application_linked_in";s:5:"title";s:21:"Linkedin Profile Link";s:4:"type";s:13:"ui-input-text";s:8:"is_saved";i:0;s:10:"is_builtin";i:0;s:4:"hint";s:0:"";s:11:"is_required";i:0;s:10:"visibility";s:1:"0";s:16:"validation_rules";s:0:"";s:11:"placeholder";s:0:"";s:5:"order";i:2;s:5:"group";s:5:"apply";s:10:"is_trashed";s:1:"0";}') ;

#
# End of data contents of table `wp_wpjb_meta`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpjb_meta_value`
#

DROP TABLE IF EXISTS `wp_wpjb_meta_value`;


#
# Table structure of table `wp_wpjb_meta_value`
#

CREATE TABLE `wp_wpjb_meta_value` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `meta_id` int(11) unsigned NOT NULL,
  `object_id` int(11) unsigned NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `object_id` (`object_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wpjb_meta_value`
#
INSERT INTO `wp_wpjb_meta_value` ( `id`, `meta_id`, `object_id`, `value`) VALUES
(4, 3, 2, '30'),
(5, 4, 1, '2'),
(6, 5, 1, '33.526849'),
(7, 6, 1, '-84.2399604'),
(8, 8, 1, 'html'),
(9, 3, 3, '365'),
(10, 17, 3, 'a:2:{i:101;a:1:{i:2;a:2:{s:6:"status";s:9:"unlimited";s:5:"usage";s:0:"";}}i:201;a:0:{}}'),
(11, 25, 3, '0'),
(12, 4, 2, '1'),
(13, 5, 2, '0'),
(14, 6, 2, '0'),
(15, 31, 2, 'Duty here'),
(16, 28, 2, 'This skill\r\nBeing awesome\r\nas\r\ndas\r\nasd\r\nasd'),
(17, 30, 2, 'This is the best company ever!'),
(18, 8, 2, 'html'),
(19, 24, 2, 'N;'),
(20, 4, 3, '2'),
(21, 5, 3, '33.526849'),
(22, 6, 3, '-84.2399604'),
(23, 31, 3, ''),
(24, 28, 3, ''),
(25, 30, 3, ''),
(26, 8, 3, 'html'),
(27, 9, 1, '2'),
(28, 10, 1, '37.09024'),
(29, 11, 1, '-95.712891'),
(30, 15, 1, 'html'),
(31, 32, 1, ''),
(32, 32, 2, ''),
(33, 4, 4, '1'),
(34, 5, 4, '0'),
(35, 6, 4, '0'),
(36, 31, 4, 'asdas\r\nasdasd'),
(37, 28, 4, ''),
(38, 30, 4, ''),
(39, 8, 4, 'html'),
(40, 32, 3, ''),
(41, 32, 4, '') ;

#
# End of data contents of table `wp_wpjb_meta_value`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpjb_payment`
#

DROP TABLE IF EXISTS `wp_wpjb_payment`;


#
# Table structure of table `wp_wpjb_payment`
#

CREATE TABLE `wp_wpjb_payment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL COMMENT 'foreign key: wp_users.ID',
  `email` varchar(120) NOT NULL,
  `fullname` varchar(250) NOT NULL DEFAULT '',
  `user_ip` varchar(120) NOT NULL DEFAULT '',
  `object_id` int(11) unsigned NOT NULL,
  `object_type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1:job; 2:resume',
  `engine` varchar(40) NOT NULL,
  `pricing_id` int(11) unsigned DEFAULT NULL,
  `external_id` varchar(80) NOT NULL,
  `is_valid` tinyint(1) NOT NULL DEFAULT '0' COMMENT '-1:failed; 0:new; 1:success',
  `status` tinyint(1) unsigned NOT NULL,
  `message` text NOT NULL,
  `created_at` datetime NOT NULL,
  `paid_at` datetime DEFAULT NULL,
  `payment_sum` float(10,2) unsigned NOT NULL,
  `payment_paid` float(10,2) unsigned NOT NULL,
  `payment_discount` float(10,2) unsigned NOT NULL DEFAULT '0.00',
  `payment_currency` varchar(3) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `object` (`object_type`,`object_id`),
  KEY `object_id` (`object_id`),
  KEY `pricing_id` (`pricing_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wpjb_payment`
#
INSERT INTO `wp_wpjb_payment` ( `id`, `user_id`, `email`, `fullname`, `user_ip`, `object_id`, `object_type`, `engine`, `pricing_id`, `external_id`, `is_valid`, `status`, `message`, `created_at`, `paid_at`, `payment_sum`, `payment_paid`, `payment_discount`, `payment_currency`, `params`) VALUES
(1, 1, 'laksmipati_dasa@hotmail.com', 'admin', '127.0.0.1', 3, 1, '', 2, '', 0, 1, '', '2017-10-11 12:22:44', '0000-00-00 00:00:00', '60.00', '0.00', '0.00', 'USD', ''),
(2, 1, 'laksmipati_dasa@hotmail.com', 'Test Co.', '127.0.0.1', 1, 3, 'Stripe', 3, 'ch_1BBjXQC1Fm4mFmRseNIf37Xw', 0, 2, '2017-10-11 12:42:16 — Payment verified by remote server.\r\n', '2017-10-11 12:42:07', '2017-10-11 12:42:16', '200.00', '200.00', '0.00', 'USD', '') ;

#
# End of data contents of table `wp_wpjb_payment`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpjb_pricing`
#

DROP TABLE IF EXISTS `wp_wpjb_pricing`;


#
# Table structure of table `wp_wpjb_pricing`
#

CREATE TABLE `wp_wpjb_pricing` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(80) NOT NULL,
  `price_for` tinyint(3) unsigned NOT NULL COMMENT '101:single-job; 201:single-resume',
  `price` float(8,2) unsigned NOT NULL,
  `currency` varchar(3) NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wpjb_pricing`
#
INSERT INTO `wp_wpjb_pricing` ( `id`, `title`, `price_for`, `price`, `currency`, `is_active`) VALUES
(2, 'Single Listing', 101, '60.00', 'USD', 1),
(3, 'Annual Lead Board Access', 250, '200.00', 'USD', 1) ;

#
# End of data contents of table `wp_wpjb_pricing`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpjb_resume`
#

DROP TABLE IF EXISTS `wp_wpjb_resume`;


#
# Table structure of table `wp_wpjb_resume`
#

CREATE TABLE `wp_wpjb_resume` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned NOT NULL COMMENT 'foreign key wp_users.id',
  `candidate_slug` varchar(80) NOT NULL,
  `phone` varchar(80) NOT NULL,
  `headline` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `created_at` date NOT NULL,
  `modified_at` date NOT NULL,
  `candidate_country` smallint(5) unsigned NOT NULL,
  `candidate_state` varchar(40) NOT NULL,
  `candidate_zip_code` varchar(20) NOT NULL,
  `candidate_location` varchar(250) NOT NULL DEFAULT '',
  `is_public` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `candidate_slug` (`candidate_slug`),
  UNIQUE KEY `user_id` (`user_id`),
  UNIQUE KEY `post_id` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wpjb_resume`
#

#
# End of data contents of table `wp_wpjb_resume`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpjb_resume_detail`
#

DROP TABLE IF EXISTS `wp_wpjb_resume_detail`;


#
# Table structure of table `wp_wpjb_resume_detail`
#

CREATE TABLE `wp_wpjb_resume_detail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `resume_id` int(11) unsigned NOT NULL,
  `type` tinyint(1) unsigned NOT NULL COMMENT '1:experience; 2:education',
  `started_at` date NOT NULL,
  `completed_at` date DEFAULT NULL,
  `is_current` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `grantor` varchar(120) NOT NULL,
  `detail_title` varchar(120) NOT NULL,
  `detail_description` text,
  PRIMARY KEY (`id`),
  KEY `resume_id` (`resume_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wpjb_resume_detail`
#

#
# End of data contents of table `wp_wpjb_resume_detail`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpjb_resume_search`
#

DROP TABLE IF EXISTS `wp_wpjb_resume_search`;


#
# Table structure of table `wp_wpjb_resume_search`
#

CREATE TABLE `wp_wpjb_resume_search` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `resume_id` int(11) unsigned NOT NULL,
  `fullname` varchar(120) NOT NULL,
  `location` varchar(180) NOT NULL,
  `details` text NOT NULL,
  `details_all` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `resume_id` (`resume_id`),
  FULLTEXT KEY `search_narrow` (`details`),
  FULLTEXT KEY `search_broad` (`details_all`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wpjb_resume_search`
#

#
# End of data contents of table `wp_wpjb_resume_search`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpjb_shortlist`
#

DROP TABLE IF EXISTS `wp_wpjb_shortlist`;


#
# Table structure of table `wp_wpjb_shortlist`
#

CREATE TABLE `wp_wpjb_shortlist` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `object` varchar(20) NOT NULL,
  `object_id` int(11) NOT NULL,
  `shortlisted_at` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_list` (`user_id`,`object`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wpjb_shortlist`
#

#
# End of data contents of table `wp_wpjb_shortlist`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpjb_tag`
#

DROP TABLE IF EXISTS `wp_wpjb_tag`;


#
# Table structure of table `wp_wpjb_tag`
#

CREATE TABLE `wp_wpjb_tag` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL COMMENT 'category; type',
  `slug` varchar(120) NOT NULL,
  `title` varchar(120) NOT NULL,
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `order` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type_slug` (`type`,`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wpjb_tag`
#
INSERT INTO `wp_wpjb_tag` ( `id`, `type`, `slug`, `title`, `parent_id`, `order`) VALUES
(1, 'category', 'default', 'Default', 0, 0),
(3, 'type', 'full-time', 'Full-time', 0, 0),
(4, 'type', 'part-time', 'Part-time', 0, 0),
(5, 'type', 'freelance', 'Freelance', 0, 0),
(6, 'type', 'internship', 'Internship', 0, 0) ;

#
# End of data contents of table `wp_wpjb_tag`
# --------------------------------------------------------



#
# Delete any existing table `wp_wpjb_tagged`
#

DROP TABLE IF EXISTS `wp_wpjb_tagged`;


#
# Table structure of table `wp_wpjb_tagged`
#

CREATE TABLE `wp_wpjb_tagged` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) unsigned NOT NULL,
  `object` varchar(20) NOT NULL,
  `object_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `search` (`tag_id`,`object`,`object_id`),
  KEY `quick_load` (`object`,`object_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wpjb_tagged`
#
INSERT INTO `wp_wpjb_tagged` ( `id`, `tag_id`, `object`, `object_id`) VALUES
(1, 5, 'job', 1),
(2, 1, 'job', 1),
(3, 5, 'job', 2),
(4, 1, 'job', 2),
(5, 5, 'job', 3),
(6, 1, 'job', 3),
(7, 5, 'job', 4),
(8, 1, 'job', 4) ;

#
# End of data contents of table `wp_wpjb_tagged`
# --------------------------------------------------------



#
# Delete any existing table `wp_wsluserscontacts`
#

DROP TABLE IF EXISTS `wp_wsluserscontacts`;


#
# Table structure of table `wp_wsluserscontacts`
#

CREATE TABLE `wp_wsluserscontacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `provider` varchar(50) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `full_name` varchar(150) NOT NULL,
  `email` varchar(255) NOT NULL,
  `profile_url` varchar(255) NOT NULL,
  `photo_url` varchar(255) NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wsluserscontacts`
#

#
# End of data contents of table `wp_wsluserscontacts`
# --------------------------------------------------------



#
# Delete any existing table `wp_wslusersprofiles`
#

DROP TABLE IF EXISTS `wp_wslusersprofiles`;


#
# Table structure of table `wp_wslusersprofiles`
#

CREATE TABLE `wp_wslusersprofiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `provider` varchar(50) NOT NULL,
  `object_sha` varchar(45) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `profileurl` varchar(255) NOT NULL,
  `websiteurl` varchar(255) NOT NULL,
  `photourl` varchar(255) NOT NULL,
  `displayname` varchar(150) NOT NULL,
  `description` varchar(255) NOT NULL,
  `firstname` varchar(150) NOT NULL,
  `lastname` varchar(150) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `language` varchar(20) NOT NULL,
  `age` varchar(10) NOT NULL,
  `birthday` int(11) NOT NULL,
  `birthmonth` int(11) NOT NULL,
  `birthyear` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `emailverified` varchar(255) NOT NULL,
  `phone` varchar(75) NOT NULL,
  `address` varchar(255) NOT NULL,
  `country` varchar(75) NOT NULL,
  `region` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `zip` varchar(25) NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `user_id` (`user_id`),
  KEY `provider` (`provider`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_wslusersprofiles`
#
INSERT INTO `wp_wslusersprofiles` ( `id`, `user_id`, `provider`, `object_sha`, `identifier`, `profileurl`, `websiteurl`, `photourl`, `displayname`, `description`, `firstname`, `lastname`, `gender`, `language`, `age`, `birthday`, `birthmonth`, `birthyear`, `email`, `emailverified`, `phone`, `address`, `country`, `region`, `city`, `zip`) VALUES
(3, 5, 'Facebook', 'd4463bb2b006d2cbda62cca0eb91813104a576ed', '124649058265019', 'https://www.facebook.com/app_scoped_user_id/124649058265019/', '', 'https://graph.facebook.com/124649058265019/picture?width=150&height=150', 'Luke-and Rohini', '', 'Luke-and', 'Rohini', 'male', 'en_GB', '', 0, 0, 0, 'laksmipati108@gmail.com', 'laksmipati108@gmail.com', '', '', '', '', '', ''),
(5, 6, 'Instagram', '5fab36cf74e5091f85c644c840b6589c073d8f9d', '6029319425', 'https://instagram.com/lukedigby108', '', 'https://scontent-ams3-1.cdninstagram.com/t51.2885-19/11906329_960233084022564_1448528159_a.jpg', 'Luke Digby', '', '', '', '', '', '', 0, 0, 0, '', '', '', '', '', '', '', ''),
(6, 7, 'Goodreads', '4f071e24800581f9dee8835fc5c3c339d367b63c', '28064580', 'http://www.goodreads.com/user/show/28064580-laksmipati-dasa?utm_medium=api', '', 'http://images.gr-assets.com/users/1390738686p3/28064580.jpg', 'Laksmipati Dasa', '', '', '', '', '', '', 0, 0, 0, '', '', '', '', 'Kolkata, 28, India', '', '', ''),
(7, 1, 'LinkedIn', '821838e4eb5bf2e81b0a380793e5fd51fca73910', 'kjY_uXPrUF', 'https://www.linkedin.com/in/luke-digby-148508a8', '', 'https://media.licdn.com/mpr/mprx/0_i7SM9jPR3qyn_Rp1GsDBvyGcF--9CEK1GY1qrpuzEQPz6wGg8x2vts7BxFc', 'Luke Digby', 'Web Developer at Self Employed (Freelancer)', 'Luke', 'Digby', '', '', '', 0, 0, 0, 'laksmipati_dasa@hotmail.com', 'laksmipati_dasa@hotmail.com', '', '', '', '', '', ''),
(8, 8, 'Facebook', '3a1506842cb845bcc308f9ebec49de779093cde3', '10159295816385704', 'https://www.facebook.com/app_scoped_user_id/10159295816385704/', '', 'https://graph.facebook.com/10159295816385704/picture?width=150&height=150', 'Laksmipati Dasa', '', 'Laksmipati', 'Dasa', 'male', 'en_US', '', 0, 0, 0, 'lukedigby@hotmail.com', 'lukedigby@hotmail.com', '', '', '', '', '', ''),
(9, 5, 'Google', '5a1f25b937821e8b051fc966046599ab78360376', '110194529107299926501', 'https://plus.google.com/110194529107299926501', '', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=200', 'Luke Digby', '', 'Luke', 'Digby', 'male', '', '', 0, 0, 0, 'laksmipati108@gmail.com', 'laksmipati108@gmail.com', '', '', '', '', '', '') ;

#
# End of data contents of table `wp_wslusersprofiles`
# --------------------------------------------------------

#
# Add constraints back in and apply any alter data queries.
#

