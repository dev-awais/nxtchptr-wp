<?php 
/*
Plugin Name: NxtChptr Courses Post Type
Author: Luke Digby
Version: 1.0.0
*/

function create_post_type() {
    register_post_type( 'courses',
        array(
            'labels' => array(
                'name' => __( 'Courses' ),
                'singular_name' => __( 'Course' ),
                'all_items' => __( 'All Courses' ),
                'view_item' => __( 'View Course' ),
                'edit_item' => __( 'Edit Course' )
            ),
            'supports' => array(
                'title',
                'editor',
                'thumbnail',
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'courses'),
        )
    );

}
// Hooking up our function to theme setup
add_action( 'init', 'create_post_type' );


function nxt_chptr_coaches_var($vars) {
    $vars[] = 'coach_id';
    return $vars;
}
add_filter('query_vars', 'nxt_chptr_coaches_var');

function nxt_chptr_coaches_rewrite() 
{
    add_rewrite_rule('^coaches/([0-9]+)/?', 'coaches?coach_id=$1', 'top');
}
add_action('init', 'nxt_chptr_coaches_rewrite');

function nxt_chptr_courses_activate() {
    remove_role('coach');
    add_role( 'coach', 'Coach', array(
        'read' => true, // True allows that capability
    ) );


    if(!get_page_by_slug('coaches')){
        wp_insert_post(array(
            'post_name' => 'coaches',
            'post_title' => 'Coaches',
            'post_content' => '',
            'post_status' => 'publish',
            'post_type' => 'page'
        ));
    }
}

register_activation_hook( __FILE__, 'nxt_chptr_courses_activate' );

function get_page_by_slug($the_slug) {
    $args = array(
      'name'        => $the_slug,
      'post_type'   => 'page',
      'post_status' => 'publish',
      'numberposts' => 1
    );
    $my_posts = get_posts($args);
    if( $my_posts ) {
      return $my_posts[0]->ID;
    }
    return false;
}

// Add custom fields

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
    'key' => 'group_5ac1ab9cdd187',
    'title' => 'Coach',
    'fields' => array (
        array (
            'key' => 'field_5ac1abb7090b4',
            'label' => 'Photo',
            'name' => 'photo',
            'type' => 'image',
            'value' => NULL,
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'return_format' => 'url',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ),
        array (
            'key' => 'field_5ac1b5513436c',
            'label' => 'Bio',
            'name' => 'bio',
            'type' => 'wysiwyg',
            'value' => NULL,
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'tabs' => 'all',
            'toolbar' => 'full',
            'media_upload' => 1,
            'default_value' => '',
            'delay' => 0,
        ),
    ),
    'location' => array (
        array (
            array (
                'param' => 'user_form',
                'operator' => '==',
                'value' => 'edit',
            ),
            array (
                'param' => 'user_role',
                'operator' => '==',
                'value' => 'coach',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

acf_add_local_field_group(array (
    'key' => 'group_5ac1834da3771',
    'title' => 'Course',
    'fields' => array (
        array (
            'key' => 'field_5ac1834de78d8',
            'label' => 'Coach',
            'name' => 'coach',
            'type' => 'user',
            'value' => NULL,
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'role' => array (
                0 => 'coach',
            ),
            'allow_null' => 0,
            'multiple' => 0,
        ),
        array (
            'key' => 'field_5ac1834de7e96',
            'label' => 'Date and time',
            'name' => 'date_time',
            'type' => 'date_time_picker',
            'value' => NULL,
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'display_format' => 'd/m/Y g:i a',
            'return_format' => 'F j, Y g:i a',
            'first_day' => 1,
        ),
        array (
            'key' => 'field_5ac1a7e806bcb',
            'label' => 'Description',
            'name' => 'description',
            'type' => 'wysiwyg',
            'value' => NULL,
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'tabs' => 'all',
            'toolbar' => 'full',
            'media_upload' => 1,
            'default_value' => '',
            'delay' => 0,
        ),
    ),
    'location' => array (
        array (
            array (
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'courses',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'acf_after_title',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

endif;