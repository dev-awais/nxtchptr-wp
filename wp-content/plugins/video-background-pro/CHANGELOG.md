# Changelog:

**v1.1.1**
* Add localized text for backend
* Added preventDefault to advanced button
* Replaced spaces with tabs
* Added new [vidbg] shortcode. The [vidbgpro] shortcode is now deprecated

**v1.1.0**
* Fixed an issue with the looping option when using the YouTube integration.

**v1.0.0**
* Initial Release
