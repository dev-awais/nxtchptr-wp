<?php
/*
Plugin Name: NxtChptr Membership Levels Settings
Author: Luke Digby
Version: 1.0.0

Requires Paid Memberships Pro and Advanced Custom Fields plugins
*/

function additional_levels_settings_acf_admin_head() {
	?>
	<style type="text/css">
		.acf_postbox > .inside > .field {
			padding-top: 5px;
			padding-bottom: 5px;
		}
		.acf_postbox p.label {
			display: inline-block;
	    width: 295px;
	    padding-right: 15px;
	    vertical-align: top;
	  }
	  ul.acf-radio-list, ul.acf-checkbox-list {
	  	display: inline-block;
	  }
	  .acf_postbox .field input[type="text"], 
	  .acf_postbox .field input[type="number"], 
	  .acf_postbox .field input[type="password"], 
	  .acf_postbox .field input[type="email"], 
	  .acf_postbox .field textarea {
	  	width: 25em;
	  }

	</style>
	<?php
}
add_action('acf/input/admin_head', 'additional_levels_settings_acf_admin_head');

function additional_levels_settings_init() {
	if(function_exists("register_field_group") && function_exists("pmpro_getAllLevels"))
	{
		$pmpro_levels = pmpro_getAllLevels(1,1);
		$fields = [];
		foreach ($pmpro_levels as $level){
			$fields = array_merge($fields, array (
						array (
							'key' => 'field_59b5fbbae80d8'.$level->id,
							'label' => $level->name,
							'name' => '',
							'type' => 'tab',
						),
						array (
							'key' => 'field_59b5f6641c4aa'.$level->id,
							'label' => 'Access Type',
							'name' => 'access_type_'.$level->id,
							'type' => 'text',
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'formatting' => 'html',
							'maxlength' => '',
						),
						array (
							'key' => 'field_59b5f68e1c4ab'.$level->id,
							'label' => '"Reflectionnaire" & Resources',
							'name' => 'reflectionnaire_&_resources_'.$level->id,
							'type' => 'true_false',
							'message' => '',
							'default_value' => 0,
						),
						array (
							'key' => 'field_59b5f6df1c4ac'.$level->id,
							'label' => 'Peer Discovery and Matching',
							'name' => 'peer_discovery_and_matching_'.$level->id,
							'type' => 'true_false',
							'message' => '',
							'default_value' => 0,
						),
						array (
							'key' => 'field_59b5f6f41c4ad'.$level->id,
							'label' => 'Why choose this option?',
							'name' => 'why_choose_this_option_'.$level->id,
							'type' => 'textarea',
							'default_value' => '',
							'placeholder' => '',
							'maxlength' => '',
							'rows' => '',
							'formatting' => 'br',
						),
						array (
							'key' => 'field_59b5f7061c4ae'.$level->id,
							'label' => 'Ability to "earn" suite of personality tests with 30 referral purchases',
							'name' => 'ability_to_earn_suite_of_personality_tests_'.$level->id,
							'type' => 'true_false',
							'message' => '',
							'default_value' => 0,
						),
						array (
							'key' => 'field_59b5f7061c4af'.$level->id,
							'label' => 'CareerLeader Test included',
							'name' => 'careerleader_test_included'.$level->id,
							'type' => 'true_false',
							'message' => '',
							'default_value' => 0,
						)
					));
		}
		register_field_group(array (
			'id' => 'acf_membership-levels',
			'title' => 'Membership Levels',
			'fields' => $fields,
			'location' => array (
				array (
					array (
						'param' => 'page',
						'operator' => '==',
						'value' => '5',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'no_box',
				'hide_on_screen' => array (
					0 => 'permalink',
					1 => 'the_content',
					2 => 'excerpt',
					3 => 'custom_fields',
					4 => 'discussion',
					5 => 'comments',
					6 => 'revisions',
					7 => 'slug',
					8 => 'author',
					9 => 'format',
					10 => 'featured_image',
					11 => 'categories',
					12 => 'tags',
					13 => 'send-trackbacks',
				),
			),
			'menu_order' => 0,
		));
	}
}
add_action( 'plugins_loaded', 'additional_levels_settings_init' );

function the_level_field($name, $level) {
	$page = get_page_by_path( 'membership' );
	the_field($name.'_'.$level->id, $page->ID);
}

function the_level_toggle_field($name, $level) {
	$page = get_page_by_path( 'membership' );
	echo get_field($name.'_'.$level->id, $page->ID) ? '<i class="sp-check"></i>' : '';
}