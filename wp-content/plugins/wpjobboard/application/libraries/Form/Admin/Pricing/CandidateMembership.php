<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmployerMembership
 *
 * @author Grzegorz
 */
class Wpjb_Form_Admin_Pricing_CandidateMembership extends Wpjb_Form_Admin_Pricing
{

    public function init()
    {
        $e = $this->create("price_for", "hidden");
        $e->setValue(Wpjb_Model_Pricing::PRICE_CANDIDATE_MEMBERSHIP);
        $this->addElement($e);
        
        $e = $this->create("is_trial", "checkbox");
        $e->setBoolean(true);
        $e->setLabel(__("Trial", "wpjobboard"));
        $e->addOption("1", "1", __("Automatically apply this membership to every newly registered Employer.", "wpjobboard"));
        $e->setOrder(101);
        $e->setValue($this->getObject()->meta->is_trial->value());
        $this->addElement($e);
        
        $e = $this->create("is_featured", "checkbox");
        $e->setBoolean(true);
        $e->setLabel(__("Featured", "wpjobboard"));
        $e->addOption("1", "1", __("Featured membership has additional class on membership pricing page, that allows you to distinguish most popular membership.", "wpjobboard"));
        $e->setOrder(102);
        $e->setValue($this->getObject()->meta->is_featured->value());
        $this->addElement($e);
        
        $e = $this->create("is_recurring", "checkbox");
        $e->setBoolean(true);
        $e->setLabel(__("Recurring", "wpjobboard"));
        $e->addOption("1", "1", __("Recurring membership will charge user every month untill he resign. ", "wpjobboard"));
        $e->setOrder(103);
        $e->setValue($this->getObject()->meta->is_recurring->value());
        $this->addElement($e);
        
        $e = $this->create("visible");
        /* @var $e Daq_Form_Element */
        //$e->setRequired(true);
        $e->setValue($this->_object->meta->visible);
        $e->setLabel(__("Recurrence", "wpjobboard"));
        $e->addFilter(new Daq_Filter_NotNegativeInt());
        $e->setHint(__("How many days the membership will be valid (0 for Never Expire).", "wpjobboard"));
        $e->setBuiltin(false);
        $e->setOrder(104);
        $this->addElement($e);
        
        //$data = unserialize($this->getObject()->meta->package->value());
        
        $e = $this->create("have_access", "checkbox");
        $e->setBoolean(true);
        $e->setLabel(__("Have Access", "wpjobboard"));
        $e->addOption("1", "1", __("If this option is selected, Candidate with this membership will have access to WPJobBoard features. ", "wpjobboard"));
        $e->setOrder(105);
        $e->setValue($this->getObject()->meta->have_access->value());
        $this->addElement($e);
        
        $e = $this->create("is_searchable", "checkbox");
        $e->setBoolean(true);
        $e->setLabel(__("Is Searchable", "wpjobboard"));
        $e->addOption("1", "1", __("If this option is selected, Candidate with this membership will be visible in search results. ", "wpjobboard"));
        $e->setOrder(106);
        $e->setValue($this->getObject()->meta->is_searchable->value());
        $this->addElement($e);
        
        $e = $this->create("featured_level", "text");
        $e->setLabel(__("Featured Level", "wpjobboard"));
        $e->setHint(__("Candidates with higher featured level will be higher in search results. ", "wpjobboard"));
        $e->setOrder(107);
        $e->setValue($this->getObject()->meta->featured_level->value());
        $this->addElement($e);
        
        $e = $this->create("alert_slots", "text");
        $e->setLabel(__("Alert Slots", "wpjobboard"));
        $e->setHint(__("Set max number of alerts for Candidate with this membership. ", "wpjobboard"));
        $e->setOrder(108);
        $e->setValue($this->getObject()->meta->alert_slots->value());
        $this->addElement($e);
        
        
        parent::init();
        
    }
    
    public function save($append = array())
    {
        parent::save($append);
        
        $object = $this->getObject();

        $meta_fields = array(
            "is_trial", "is_featured", "is_recurring", "have_access",
            "is_searchable", "featured_level", "alert_slots"
        );
                
        foreach($meta_fields as $meta_field) {
            $meta = $object->meta->{$meta_field}->getFirst();
            $meta->object_id = $object->getId();
            $meta->value = absint($this->value($meta_field));
            $meta->save();
        }

    }
    
}

?>
