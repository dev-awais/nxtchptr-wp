<?php
/*
Plugin Name: Zoom Meetings api integration
Author: Luke Digby
Version: 1.0.0
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

require_once dirname(__FILE__) . '/vendor/autoload.php';

use Firebase\JWT\JWT;

class ZoomMeetings {

  public static function generateJWT () {
    //Zoom API credentials from https://developer.zoom.us/me/
    $key = 'bh2IVjXuRP2K69bMaRAuZw';
    $secret = 'hy4rRmBA9VmeSKqfWFb4SdzTCTvYyoLtlz7Y';
    $token = array(
      "iss" => $key,
          // The benefit of JWT is expiry tokens, we'll set this one to expire in 1 minute
      "exp" => time() + 60000000
    );
    return JWT::encode($token, $secret);
  }

  public static function post ($ch, $fields) {
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
      'Authorization: Bearer ' . self::generateJWT(),
      'Content-Type: application/json',                                                                                
      'Content-Length: ' . strlen($fields))                                                                       
    ); 
    $response = curl_exec($ch);
    $response = json_decode($response);
    return $response;
  }

  public static function createUser () {
    if ( !isset($_POST['_create-user-nonce']) || !wp_verify_nonce($_POST['_create-user-nonce'], 'create-zoom-account')) {
      wp_redirect(wp_get_referer());
      die();
    }
    $user = get_userdata($_POST['userId']);

    $ch = curl_init('https://api.zoom.us/v2/users');
    $fields = json_encode([
      'action' => 'create',
      'user_info' => [
        'email' => $user->user_email, 
        'type' => 1 
      ]
    ]);
    $response = self::post($ch, $fields);

    if(isset($response->id)){
      xprofile_set_field_data(3, $user->ID, $response->id);
      wp_redirect(wp_get_referer().'?zoom_created');
      die();
    }

    if(strpos($response->message, "User already in the account") != -1){
      $response = self::getUser($user->user_email);

      if(isset($response->id)){
        xprofile_set_field_data(3, $user->ID, $response->id);
        wp_redirect(wp_get_referer().'?zoom_found');
        die();
      }
    }

    wp_redirect(wp_get_referer());
    die();
  }

  public static function getUsers () {
      //list users endpoint GET https://api.zoom.us/v2/users
    $ch = curl_init('https://api.zoom.us/v2/users');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
      // add token to the authorization header
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Authorization: Bearer ' . self::generateJWT()
    ));
    $response = curl_exec($ch);
    $response = json_decode($response);
    return $response;
  }

  public static function getUser ($id) {
      //list users endpoint GET https://api.zoom.us/v2/users
    $ch = curl_init('https://api.zoom.us/v2/users/'.$id);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
      // add token to the authorization header
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Authorization: Bearer ' . self::generateJWT()
    ));
    $response = curl_exec($ch);
    $response = json_decode($response);
    return $response;
  }

  public static function updateUser ($id) {
      //list users endpoint GET https://api.zoom.us/v2/users
    $ch = curl_init('https://api.zoom.us/v2/users/'.$id);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $fields = http_build_query([
      'first_name' => 'Lukie'
    ]);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    
      // add token to the authorization header
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Authorization: Bearer ' . self::generateJWT()
    ));
    $response = curl_exec($ch);
    $response = json_decode($response);
    return $response;
  }

  public static function createMeeting () {
    if ( !isset($_POST['_schedule-meeting-nonce']) || !wp_verify_nonce($_POST['_schedule-meeting-nonce'], 'schedule-meeting')) {
      wp_redirect(wp_get_referer());
      die();
    }

    //create users endpoint GET https://api.zoom.us/v2/users
    $ch = curl_init('https://api.zoom.us/v2/users/'.$_POST['zoomId'].'/meetings');
    $fields = json_encode([
      'topic' => $_POST['meeting_name'],
      'type' => 2,
      'start_time' => $_POST['meeting_date_time'],
      'timezone' => $_COOKIE['NxtchptrUserTimeZone']
    ]);
    $response = self::post($ch, $fields);
    if(isset($response->id)){
      // Get all peer group members
      $members = new BP_Groups_Group_Members_Template([
        'group_id'            => $_POST['groupId'],
        'page'                => 1,
        'per_page'            => 20,
        'max'                 => false,
        'exclude'             => $_POST['userId'],
        'exclude_admins_mods' => 0,
        'exclude_banned'      => 1,
        'group_role'          => false,
        'search_terms'        => false,
        'type'                => 'last_joined',
      ]);

      $user = get_userdata($_POST['userId']);
      $date = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $response->start_time)->format('M j, Y g:i a T');
      $split_id = implode(' ', str_split($response->id, 3));
      $subject = "{$user->display_name} is inviting you to a meeting";
      $message = "Hi there, 

{$user->display_name} is inviting you to a scheduled Zoom meeting. 

Topic: {$response->topic}
Time: $date

Join from PC, Mac, Linux, iOS or Android: {$response->join_url}

Or iPhone one-tap :
    US: +16699006833,,{$response->id}#  or +16468769923,,{$response->id}# 
Or Telephone:
    Dial(for higher quality, dial a number based on your current location)：
        US: +1 669 900 6833  or +1 646 876 9923 
    Meeting ID: $split_id
    International numbers available: https://zoom.us/zoomconference

";
      // Send email to each peer group member inviting them to meeting
      while($members->members()){
        $member = $members->next_member();
        wp_mail($member->user_email, $subject, $message);
      }
    }
    wp_redirect(wp_get_referer());
    die();
  }

  public static function cancelMeeting () {
    if ( !isset($_POST['_cancel-meeting-nonce'.$_POST['meetingId']]) || !wp_verify_nonce($_POST['_cancel-meeting-nonce'.$_POST['meetingId']], 'cancel-meeting')) {
      wp_redirect(wp_get_referer());
      die();
    }
    // Get the meeting so we can use the datils in the emails to members
    $meeting = self::getMeeting($_POST['meetingId']);

    // Delete the meeting
    $ch = curl_init('https://api.zoom.us/v2/meetings/'.$_POST['meetingId']);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
      'Authorization: Bearer ' . self::generateJWT()
    )); 
    curl_exec($ch);

    // Get all peer group members
    $members = new BP_Groups_Group_Members_Template([
      'group_id'            => $_POST['groupId'],
      'page'                => 1,
      'per_page'            => 20,
      'max'                 => false,
      'exclude'             => $_POST['userId'],
      'exclude_admins_mods' => 0,
      'exclude_banned'      => 1,
      'group_role'          => false,
      'search_terms'        => false,
      'type'                => 'last_joined',
    ]);

    $user = get_userdata($_POST['userId']);
    $date = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $meeting->start_time)->format('M j, Y g:i a T');
    $subject = "Your Meeting - {$meeting->topic} Is Cancelled";
    $message = "Hi there, 

The below zoom meeting has been cancelled. 

Topic: {$meeting->topic}
Time: $date
";
    // Send email to each peer group member inviting them to meeting
    while($members->members()){
      $member = $members->next_member();
      wp_mail($member->user_email, $subject, $message);
    }
    wp_redirect(wp_get_referer());
    die();
  }

  public static function getMeeting ($meeting_id) {
      //list meetings endpoint GET https://api.zoom.us/v2/users
    $ch = curl_init('https://api.zoom.us/v2/meetings/'.$meeting_id);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      // add token to the authorization header
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Authorization: Bearer ' . self::generateJWT()
    ));
    $response = curl_exec($ch);
    $response = json_decode($response);
    return $response;
  }

  public static function getMeetings ($zoom_id) {
      //list meetings endpoint GET https://api.zoom.us/v2/users
    $ch = curl_init('https://api.zoom.us/v2/users/'.$zoom_id.'/meetings?type=scheduled');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
      // add token to the authorization header
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Authorization: Bearer ' . self::generateJWT()
    ));
    $response = curl_exec($ch);
    $response = json_decode($response);
    return $response;
  }
}

add_action('admin_post_zoom_create_meeting', ['ZoomMeetings', 'createMeeting']);
add_action('admin_post_zoom_cancel_meeting', ['ZoomMeetings', 'cancelMeeting']);
add_action('admin_post_zoom_create_user', ['ZoomMeetings', 'createUser']);

/*var_dump(ZoomMeetings::getMeetings(
  ['userId' => "vWhpU1PzQIC60xoDSRVhdQ"]
));*/
//var_dump(getUsers());
//var_dump(updateUser("vWhpU1PzQIC60xoDSRVhdQ"));
//var_dump(getUser("vWhpU1PzQIC60xoDSRVhdQ"));