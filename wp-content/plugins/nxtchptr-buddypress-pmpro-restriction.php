<?php
/*
Plugin Name: NxtChptr Buddypress PMPro Restriction
Author: Luke Digby
Version: 1.0.0

Requires Paid Memberships Pro and Buddypress plugins
*/

function pmpro_hide_buddypress_pages() {
	$uri = $_SERVER['REQUEST_URI'];	
	$not_allowed = array(
		"/members/",
		"/members/*/friends/",
	);

	foreach($not_allowed as $check)
	{
		//prep for regex
		$check = str_replace("/", "[\/]+", $check);					//replace /
		$check = str_replace("*\/", "[^\/]*\/", $check);			//replace * in middle of URIs
		$check = str_replace("+*", "+.*", $check);					//replace * at end of URIs
		$check = "/^" . $check . "\/?$/";							//add start/end /
				
		if(preg_match($check, $uri))
		{			
				wp_redirect(home_url());
			exit;
		}
	}

	$members_only = array(
		"/members/*",
		"/jobs/",
		"/jobs/*",
		"/job/*",
	);
	foreach($members_only as $check)
	{
		//prep for regex
		$check = str_replace("/", "[\/]+", $check);				//replace /
		$check = str_replace("*\/", "[^\/]*\/", $check);		//replace * in middle of URIs
		$check = str_replace("+*", "+.*", $check);				//replace * at end of URIs
		$check = "/^" . $check . "\/?/";						//add start/end /
				
		//make sure they are a member
		if(preg_match($check, $uri) && !pmpro_hasMembershipLevel())
		{			
			wp_redirect(home_url());
			exit;
		}
	}	
}

add_action("init", "pmpro_hide_buddypress_pages");