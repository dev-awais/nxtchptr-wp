<?php
/*
Plugin Name: CareerLeader
Author: Luke Digby
Version: 1.0.0
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

require_once dirname(__FILE__) . '/vendor/autoload.php';
require_once( dirname(__FILE__) . '/../../../wp-admin/includes/file.php' );

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Smalot\PdfParser\Parser;

class CareerLeader {
  static function install () {
    global $wpdb;
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

    $table_name = $wpdb->prefix . "careerleader_reports"; 
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE $table_name (
      id bigint(20) unsigned NOT NULL auto_increment,
      user_id bigint(20) unsigned NOT NULL,
      creation_date varchar(100) NOT NULL,
      filename varchar(300) NOT NULL,
      PRIMARY KEY (id)
    ) $charset_collate;";

    dbDelta( $sql );
  }

  static function downloadCareerLeaderReport($user_id = false) {
    $user_id = $user_id ?: get_current_user_id();

    if(!$user_id) {
      return false;
    }
    $user_email = get_userdata($user_id)->user_email;
    try {
      $client = new Client([
        'verify' => false,
        'cookies' => true
      ]);

      //First, log in to set cookie
      $res = $client->post('https://www.careerleader.com/cgi-bin/adminsite.pl5', [
        'form_params' => [
          'fromFormMode' => 'newAdminLogin',
          'clusername' => 'Nxt',
          'email' => 'francie@nxt-chptr.com',
          'personalpassword' => 'CLNXT2017'
        ]
      ]);
      // Then get list of all users
      $res = $client->get('https://www.careerleader.com/cgi-bin/admin/popup_report2.pl5?rptScript=new_user_search.pl5&inLine=RiskReportdiv&univid=100518&username=nxt&password=chptr&usertype=corp&Runmode=LIVE&version=2.0&divHeight=500px&studentfirstname=&studentlastname=&studentlastyear=&noPrinting=noPrinting&this_account_manager_email_address=Kimball@careerleader.com');

      // Search HTML for passed Email
      $dom = new DOMDocument();
      libxml_use_internal_errors(true); // Hide warnings about invalid (non xhtml) html
      $dom->loadHTML($res->getBody());
      $finder = new DomXPath($dom);
      $nodes = $finder->query("//td[contains(@id, 'email')]");
      foreach($nodes as $n) {
        if($n->textContent == $user_email){
          // Find ID corresponding to email address
          $user_id = str_replace('user_', '', $n->parentNode->getAttribute('id'));
        }
      }
      if(!$user_id) {
         return false;
      }

      $nodes = $finder->query("//td[contains(@id, 'rem$user_id')]");
      if(count($nodes) > 0) {
        return false;
      }

      // Download report using ID
      $filename = '/wp-content/plugins/careerleader/reports/CareerLeader-'.time().'.pdf';
      $res = $client->post('https://www.careerleader.com/cgi-bin/careerleader.pl5', [
        'form_params' => [
          'userid' => $user_id,
          'xculturecase' => '',
          'usertype' => 'corp',
          'fromAdminSite' => 'FROMADMINSITE',
          'Adminaction' => ' User View ',
          'ReportType' => 'PDF'
        ],
        'sink' => get_home_path() . $filename
      ]);

      // Save the record to the DB
      global $wpdb;
      $wpdb->insert(
        $wpdb->prefix . "careerleader_reports",
        [
          'user_id' => $user_id,
          'creation_date' => date('F j, Y'),
          'filename' => $filename
        ],
        [
          '%d',
          '%s',
          '%s'
        ]
      );

    }catch (\Exception $e) {
      //echo $e->getMessage();
    }
  }

  static function getCareerLeaderReports($user_id = false) {
    $user_id = $user_id ?: get_current_user_id();

    if(!$user_id) {
      return false;
    }

    global $wpdb;
    $wpdb->show_errors( true );    
    return $wpdb->get_results("SELECT * 
      FROM {$wpdb->prefix}careerleader_reports
      WHERE user_id = $user_id", OBJECT );

  }

    
  static function authorizeUser($user_id = false) {
    $user_id = $user_id ?: get_current_user_id();

    if(!$user_id) {
      return false;
    }

    try {
      $client = new Client([
        'verify' => false,
        'cookies' => true
      ]);

      //First, log in to set cookie
      $res = $client->post('https://www.careerleader.com/cgi-bin/adminsite.pl5', [
        'form_params' => [
          'fromFormMode' => 'newAdminLogin',
          'clusername' => 'Nxt',
          'email' => 'francie@nxt-chptr.com',
          'personalpassword' => 'CLNXT2017'
        ]
      ]);

      // Then authorize the user we want
      $current_user = get_userdata($user_id);
      $user_email = $current_user->user_email;
      $first_name = $current_user->first_name ?: array_shift(explode($current_user->display_name));
      $last_name = $current_user->last_name ?: array_pop(explode($current_user->display_name));
      $unique_id = xprofile_get_field_data(2, $current_user->ID);
      $res = $client->get('https://www.careerleader.com/cgi-bin/admin/popup_report2.pl5?rptScript=new-authorize.pl5&inLine=RiskReportdiv&univid=100518&username=nxt&password=chptr&usertype=corp&Runmode=LIVE&version=2.0&divHeight=300px&studentfirstname='.$first_name.'&studentlastname='.$last_name.'&newuseremail='.$user_email.'&accesstime=2018&authgroup=&newusercode='.$unique_id.'&noPrinting=noPrinting');

      echo $res->getBody();

    }catch (\Exception $e) {
      //echo $e->getMessage();
    }
  }
}

register_activation_hook( __FILE__, ['CareerLeader', 'install'] );

function download_career_leader_report($user_id = false) {
  return CareerLeader::downloadCareerLeaderReport($user_id);
}

function get_career_leader_reports($user_id = false) {
  return CareerLeader::getCareerLeaderReports($user_id);
}

function careerleader_authorize_user($user_id = false) {
  return CareerLeader::authorizeUser($user_id);
}
