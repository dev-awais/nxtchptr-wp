<?php
/*
Plugin Name: NxtChptr Peer Matching Feature
Author: Luke Digby
Version: 1.0.0
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

include 'match.php';

function npm_install () {
  global $wpdb;
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

  $table_name = $wpdb->prefix . "nxtchptr_peer_matches"; 
	$charset_collate = $wpdb->get_charset_collate();
	$sql = "CREATE TABLE $table_name (
	  group_id bigint(20) NOT NULL,
	  peer_id bigint(20) NOT NULL,
	  match_percent int(4) NOT NULL default 0,
	  PRIMARY KEY  (group_id, peer_id)
	) $charset_collate;";

	dbDelta( $sql );

  $table_name = $wpdb->prefix . "nxtchptr_peer_match_forms"; 
	$charset_collate = $wpdb->get_charset_collate();
	$sql = "CREATE TABLE $table_name (
	  id bigint(20) NOT NULL,
	  gender enum('male', 'female') NOT NULL,
	  status tinyint(1) NOT NULL,
	  timezone tinyint(1) NOT NULL,
	  zipcode varchar(10) NOT NULL,
	  school_name varchar(255) NOT NULL,
	  education_level tinyint(1) NOT NULL,
		books_mags enum('books', 'magazines', 'neither') NOT NULL,
		crossword_sudoku enum('crossword', 'sudoku', 'neither') NOT NULL,
		feel_look enum('feel', 'look') NOT NULL,
		humor_important int(1) NOT NULL,
		most_important_ABCD varchar(255) NOT NULL,
		why_joined varchar(1000) NOT NULL,
		fav_websites varchar(255) NOT NULL,
		fav_tv_shows varchar(255) NOT NULL,
		fav_movie varchar(100) NOT NULL,
		child_career_dream varchar(100) NOT NULL,
		prev_career varchar(100) NOT NULL,
		gender_preference enum('male', 'female') NOT NULL,
		peer_member_residence_preference enum('local', 'anywhere')  NOT NULL,
		language tinyint(1) NOT NULL,
		duration enum('6 months', '1 year', 'indefinite') NOT NULL,
		auto_match tinyint(1) NOT NULL,
	  PRIMARY KEY  (id)
	) $charset_collate;";
	dbDelta( $sql );

	
  $table_name = $wpdb->prefix . "nxtchptr_peer_match_descriptors"; 
	$charset_collate = $wpdb->get_charset_collate();
	$sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL,
		descriptor varchar(100) NOT NULL
	)";
	dbDelta( $sql );


  $table_name = $wpdb->prefix . "nxtchptr_peer_match_form_descriptors"; 
	$charset_collate = $wpdb->get_charset_collate();
	$sql = "CREATE TABLE $table_name (
		form_id bigint(20) NOT NULL,
		descriptor_id bigint(20) NOT NULL
	)";
	dbDelta( $sql );


  $table_name = $wpdb->prefix . "nxtchptr_peer_match_form_friends"; 
	$charset_collate = $wpdb->get_charset_collate();
	$sql = "CREATE TABLE $table_name (
		form_id bigint(20) NOT NULL,
		user_id bigint(20) NOT NULL
	)";
	dbDelta( $sql );
	//top_descriptors varchar(1000),
}
register_activation_hook( __FILE__, 'npm_install' );

function save_peer_matching_form() {
  global $wpdb;

	if ( !isset($_POST['_mynonce']) || !wp_verify_nonce($_POST['_mynonce'], 'register-user')) {
		wp_redirect($_POST['redirect']);
		die();
	}

	$most_important_ABCD['A'] = intval($_POST['most_important_ABCD']['A']);
	$most_important_ABCD['B'] = intval($_POST['most_important_ABCD']['B']);
	$most_important_ABCD['C'] = intval($_POST['most_important_ABCD']['C']);
	$most_important_ABCD['D'] = intval($_POST['most_important_ABCD']['D']);
	asort($most_important_ABCD);

	for ($i=0; $i < 5; $i++) { 
		$descriptors[$i] = sanitize_text_field($_POST['descriptors'][$i]);
	}

	$friends; // Make sure passed user ids correlate to members

	$insert_data = array( 
  	'id' => bp_loggedin_user_id(),
  	'gender' => sanitize_text_field($_POST['gender']),
  	'status' => intval($_POST['status']),
		'timezone' => intval($_POST['timezone']),
		'zipcode' => sanitize_text_field($_POST['zipcode']),
		'school_name' => sanitize_text_field($_POST['school_name']),
		'education_level' => intval($_POST['education_level']),
		'books_mags' => sanitize_text_field($_POST['books_mags']),
		'crossword_sudoku' => sanitize_text_field($_POST['crossword_sudoku']),
		'feel_look' => sanitize_text_field($_POST['feel_look']),
		'humor_important' => intval($_POST['humor_important']),
		'most_important_ABCD' => json_encode(array_keys($most_important_ABCD)),
		'why_joined' => sanitize_text_field($_POST['why_joined']),
		'fav_websites' => sanitize_text_field($_POST['fav_websites']),
		'fav_tv_shows' => sanitize_text_field($_POST['fav_tv_shows']),
		'fav_movie' => sanitize_text_field($_POST['fav_movie']),
		'child_career_dream' => sanitize_text_field($_POST['child_career_dream']),
		'prev_career' => sanitize_text_field($_POST['prev_career']),
		'gender_preference' => sanitize_text_field($_POST['gender_preference']),
		'language' => intval($_POST['language']),
		'duration' => sanitize_text_field($_POST['duration']),
		'auto_match' => intval($_POST['auto_match'])
  );

	if(empty($errors)){
		$wpdb->delete($wpdb->prefix . 'nxtchptr_peer_match_forms', ['id' => bp_loggedin_user_id()]);
		$wpdb->insert(
	    $wpdb->prefix . 'nxtchptr_peer_match_forms',
	    $insert_data,
	    array( 
	    	'%s', //gender
		    '%d', //status
		    '%d', //timezone
		    '%s', //zipcode
		    '%s', //school_name
		    '%d', //education_level
				'%s', //books_mags
				'%s', //crossword_sudoku
				'%s', //feel_look
				'%d', //humor_important
				'%s', //most_important_ABCD
				'%s', //why_joined
				'%s', //fav_websites
				'%s', //fav_tv_shows
				'%s', //fav_movie
				'%s', //child_career_dream
				'%s', //prev_career
				'%s', //gender_preference
				'%d', //language
				'%s', //duration
				'%d' //auto_match
		  )
		);

		// Only create group if not existing
		$existing_groups = groups_get_user_groups(bp_loggedin_user_id(), 1);
		
		if($existing_groups['total'] == 0){
			$group_name = bp_get_loggedin_user_username() . "'s peer group";
			$create_group_data = [
				'group_id' => $existing_group_id,
				'name' => $group_name,
				'description' => $group_name,
				'status' => 'hidden'
			];
			$group_id = groups_create_group($create_group_data);
		}else{
			$group_id = $existing_groups['groups'][0];
		}

		nxtchptr_calculate_matches(bp_loggedin_user_id(), $group_id);
	}

	wp_redirect('/personal-area/peer-matching');
	die();
}
add_action( 'admin_post_peer_matching_form', 'save_peer_matching_form' );

function nxtchptr_calculate_matches($id, $group_id) {
	global $wpdb;
	$matches = nxtchptr_peer_match::get_matches($id);
	foreach($matches as $peer_id => $match_percent) {
		$insert_data = [
			'group_id' => $group_id,
			'peer_id' => $peer_id,
		];
		$wpdb->delete($wpdb->prefix . 'nxtchptr_peer_matches', $insert_data);
		
		$insert_data['match_percent'] = $match_percent;
		$wpdb->insert($wpdb->prefix . 'nxtchptr_peer_matches', $insert_data);
	}
}


function update_peer_matches($group_id) {
	$members = groups_get_group_members(['group_id' => $group_id])['members'];
	nxtchptr_calculate_matches(implode(',', wp_list_pluck($members, 'ID')), $group_id);	
}
add_action( 'groups_join_group', 'update_peer_matches' );


function nxtchptr_order_by_match_percent($a, $b) {
	return $a->match_percent < $b->match_percent;
}

function bp_group_get_peer_answers($ids) {
	global $wpdb;
	$answer_results = $wpdb->get_results("SELECT id, why_joined, fav_websites, fav_tv_shows, fav_movie, child_career_dream, prev_career FROM {$wpdb->prefix}nxtchptr_peer_match_forms WHERE id IN(" . implode(',',$ids) . ")");
	// Index the answers
	foreach($answer_results as $row){
		$answers[$row->id] = $row;
	}
	return $answers;
}
function bp_group_get_members_matches($ids) {
	global $wpdb;
	$match_results = $wpdb->get_results("SELECT peer_id, match_percent FROM {$wpdb->prefix}nxtchptr_peer_matches WHERE peer_id IN(".implode(',', $ids).")");
	foreach($match_results as $row){
		$matches[$row->peer_id] = $row;
	}
	return $matches;
}

function bp_group_has_suggested_members() {
	global $nxtchptr_suggested_members;
	global $wpdb;
	$nxtchptr_suggested_members = new stdClass();
	$match_results = $wpdb->get_results("SELECT peer_id, match_percent FROM {$wpdb->prefix}nxtchptr_peer_matches WHERE group_id = ".bp_get_current_group_id());
	foreach($match_results as $row){
		$matches[$row->peer_id] = $row;
	}
	
	$ids = wp_list_pluck($matches, 'peer_id');
	
	// Remove invited members
	if(bp_group_has_invites()){
		global $invites_template;
		$ids = array_diff($ids, $invites_template->invites);
	}

	// Remove members who have already joined the group
	$members = new BP_Group_Member_Query( ['group_id' => bp_get_group_id()] );
	if($members->total_users > 0){
		$ids = array_diff($ids, $members->user_ids);
	}

	$r = [ 'user_ids' => $ids	];
	$members = new BP_User_Query($r);

	// Get Form answers for members
	$answers = bp_group_get_peer_answers($ids);
	
	foreach($members->results as $member){
		$member->match_percent = $matches[$member->ID]->match_percent;
		$member->answers = $answers[$member->ID];
	}

	usort($members->results, 'nxtchptr_order_by_match_percent');

	$nxtchptr_suggested_members->members = $members->results;

	return count($nxtchptr_suggested_members->members) > 0;
}
function bp_group_get_suggested_members() {
	global $nxtchptr_suggested_members;
	return $nxtchptr_suggested_members->members;
}
function get_initial(&$name) {
	$name = strtoupper($name[0]);
}

function get_initials($name) {
	$words = explode(' ', $name);
	array_walk($words, 'get_initial');
	return implode('', $words);
}

function get_user_initials($user) {
	return get_initials($user->fullname);
}

/**
 * Output the markup for the message recipient tabs.
 */
function nxtchptr_bp_message_get_recipient_tabs() {
	$usernames = isset( $_GET['r'] ) ? $_GET['r'] : '';
	$recipients = explode( ' ', $usernames );
	$users = bp_core_get_users(['user_ids' => $recipients])['users'];

	foreach ( $users as $user ) {
		if ( ! empty( $user ) ) : ?>
			<li id="un-<?php echo esc_attr( $user->ID ); ?>" class="friend-tab">
				<span><?php echo bp_core_get_userlink( $user->ID ) ?></span>
			</li>
		<?php endif;
	}
}

function nxtchptr_get_user_group_members() {
	$groupid = BP_Groups_Member::get_group_ids( get_current_user_id());
	$this_id = current($groupid['groups']);  //extracts the first group ID from the array 
	$member_ids = [];
	$members_template = new BP_Groups_Group_Members_Template( ['group_id' => $this_id] );
	foreach($members_template->members as $member){
		$member_ids[] = $member->ID;
	}
	return $member_ids;
}

function nxtchptr_is_user_group_member($user_id) {
	if(bp_loggedin_user_id() == $user_id) return true;

	$members = nxtchptr_get_user_group_members();
	return in_array($user_id, $members);
}
function nxtchptr_bp_get_display_name($fullname, $user_id) {
	if(!$fullname || nxtchptr_is_user_group_member($user_id)){
		return $fullname;
	}else{
		return get_initials($fullname);
	}
}

add_filter( 'bp_core_get_user_displayname', 'nxtchptr_bp_get_display_name', 10, 2);


function nxtchptr_bp_get_userlink($link, $user_id) {
	if(!nxtchptr_is_user_group_member($user_id)){
		// Remove link
		return preg_replace('#<a.*?>(.*?)</a>#i', '\1', $link);
	}
	return $link;
}

add_filter( 'bp_core_get_userlink', 'nxtchptr_bp_get_userlink', 10, 2);

function nxtchptr_bp_get_thread_message_sender_link($link) {
	global $thread_template;	
	if(!nxtchptr_is_user_group_member($thread_template->message->sender_id)){
		return '';
	}
	return $link;
}
add_filter( 'bp_get_the_thread_message_sender_link', 'nxtchptr_bp_get_thread_message_sender_link');

function nxtchptr_bp_filter_the_thread_message_sender_link($link) {
	global $thread_template;

	if(!nxtchptr_is_user_group_member($thread_template->message->sender_id)) {
		return false;
	}
	return $link;
}
//add_filter( 'bp_get_the_thread_message_sender_link', 'nxtchptr_bp_filter_the_thread_message_sender_link');

function nxtchptr_show_avatar($params) {
	if(!in_array($params['object'], ['group', 'blog'])){
		if(!nxtchptr_is_user_group_member($params['item_id'])){
			return false;
		}
	}
	return true;
}

function nxtchptr_bp_filter_avatar_url($avatar, $params) {
	return nxtchptr_show_avatar($params) ? $avatar : false;
}

function nxtchptr_bp_filter_avatar($avatar, $params) {
	if (nxtchptr_show_avatar($params)) {
		return $avatar;
	}
	$dom = new DOMDocument();
	$dom->loadHTML($avatar);
	$dom->getElementsByTagName('img')[0]->setAttribute('src', bp_core_avatar_default_thumb());
	return $dom->saveHTML();
}

add_filter( 'bp_core_fetch_avatar_url', 'nxtchptr_bp_filter_avatar_url', 10, 2);
add_filter( 'bp_core_fetch_avatar', 'nxtchptr_bp_filter_avatar', 10, 2);

function nxtchptr_peer_matching_scripts() {
	wp_enqueue_script('nxtchptr-js', plugins_url('nxtchptr-peer-matching/nxtchptr-peer-matching.js'), ['jquery']);
}
add_action( 'wp_enqueue_scripts', 'nxtchptr_peer_matching_scripts' );
function nxtchptr_filter_bp_group_invites($has_invites, $invites_template){
	if($has_invites){
		$ids = wp_list_pluck($invites_template->invite_data, 'id');
		$matches = bp_group_get_members_matches($ids);
		$answers = bp_group_get_peer_answers($ids);
		foreach($invites_template->invite_data as $invite){
			$invite->match_percent = $matches[$invite->ID]->match_percent;
			$invite->answers = $answers[$invite->ID];
		}
		usort($matches, 'nxtchptr_order_by_match_percent'); 
	}
	return $has_invites; 
}

add_filter( 'bp_group_has_invites', 'nxtchptr_filter_bp_group_invites', 10, 2);

function nxtchptr_bp_get_group_inviter() {
	global $groups_template;
	//TODO
	// find out who invited the current user
}

function nxtchptr_filter_bp_members_suggestions_query_args($query) {
	$query['user_id'] = null;
	$query['user_ids'] = nxtchptr_get_user_group_members();

	return $query;
}

add_filter( 'bp_members_suggestions_query_args', 'nxtchptr_filter_bp_members_suggestions_query_args');

function nxtchptr_wsl_get_bp_user_custom_avatar_url($avatar, $args) {
	//Buddypress component should be enabled
	if( ! wsl_is_component_enabled( 'buddypress' ) )
	{
		return $avatar;
	}

	//Check if avatars display is enabled
	if( ! get_option( 'wsl_settings_users_avatars' ) )
	{
		return $avatar;
	}

	//Check arguments
	if( is_array( $args ) )
	{
		//User Object
		if( ! empty( $args['object'] ) AND strtolower( $args['object'] ) == 'user' )
		{
			//User Identifier
			if( ! empty( $args['item_id'] ) AND is_numeric( $args['item_id'] ) )
			{
				$user_id = $args['item_id'];

				// Overwrite all avatars
					
					/*
					//Only Overwrite gravatars
					# https://wordpress.org/support/topic/buddypress-avatar-overwriting-problem?replies=1
					if( bp_get_user_has_avatar( $user_id ) )
					{
						return $avatar;
					}*/

				$wsl_avatar = wsl_get_user_custom_avatar( $user_id );

				//Retrieve Avatar
				if( $wsl_avatar )
				{
					return $wsl_avatar;
				}
			}
		}
	}

	return $avatar;
}
add_filter( 'bp_core_fetch_avatar_url', 'nxtchptr_wsl_get_bp_user_custom_avatar_url', 8, 2 );
