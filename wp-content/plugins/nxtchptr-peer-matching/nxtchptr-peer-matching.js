(function($){
	$(function(){
		$('.peer-group__list .fa-plus-circle').click(function(){
			$inviteForm = $('#send-invite-form')
			$inviteForm.append($('<input/>').attr({
				type: 'hidden',
				name: 'friends[]',
				value: $(this).data('user-id')
			}));
			$inviteForm.submit();
		});

		$('.peer-group__list .show-peer-details').click(function(){
			$details = $(this).parent('tr').next('.peer-details');
			if(!$details.hasClass('open')){
				$details.toggleClass('open').find('table').fadeToggle();
			}	else {
				$details.find('table').fadeToggle({
					complete: function(){
						$details.toggleClass('open');
					}
				});
			}
		})
	});
})(jQuery)