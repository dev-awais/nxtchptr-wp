<?php

class nxtchptr_peer_match {
	private static $servername = DB_HOST;
	private static $username = DB_USER;
	private static $password = DB_PASSWORD;
	private static $dbname = DB_NAME;

	private static $WEIGHTS = array(15, 10, 10, 10, 10);

	private static function education_match_6($d1, $d2) {
		$match_val = 0;
		$match_weight = self::$WEIGHTS[0];
		if ($d1 == $d2)
			$match_val = $match_weight;
		elseif ((($d1 == 2) || ($d1 == 1)) && (($d2 == 1) || ($d2 == 2)))
			$match_val = $match_weight;
		elseif ((($d1 == 3) || ($d1 == 4)) && (($d2 == 3) || ($d2 == 4)))
			$match_val = $match_weight;
		return $match_val;
	}

	private static function print_match_7($d1, $d2) {
		$match_val = 0;
		if ($d1 == $d2)
			$match_val = self::$WEIGHTS[1];
		return $match_val;
	}

	private static function puzzle_match_8($d1, $d2) {
		$match_val = 0;
		if (($d1 == 'neither') || ($d2 == 'neither'))
			$match_val = self::$WEIGHTS[2];
		else if ($d1 != $d2)
			$match_val = self::$WEIGHTS[2];
		return $match_val;
	}

	private static function look_feel_match_9($d1, $d2) {
		$match_val = 0;
		if ($d1 != $d2)
			$match_val = self::$WEIGHTS[3];
		return $match_val;
	}

	private static function humor_match_10($d1, $d2) {
		$match_val = 0;
		if ($d1 == $d2)
			$match_val = self::$WEIGHTS[4];
		return $match_val;
	}

	private static function most_impt_match_11($d1, $d2) {
		$d1 = str_replace(' ', '', $d1);
		$d2 = str_replace(' ', '', $d2);
		$d1 = explode(',', $d1);
		$d2 = explode(',', $d2);
		$match_val = 0;
		if ($d1[0] == $d2[0])
			$match_val += 10;
		if ($d1[1] == $d2[1])
			$match_val += 5;
		return $match_val;
	}

	private static function two_favorite_websites_13($d1, $d2) {
		$d1 = explode(';', $d1);
		$d2 = explode(';', $d2);
		$d1 = array_map('strtolower', $d1);
		$d2 = array_map('strtolower', $d2);
		$match_val = 0;
		if (count(array_intersect($d1,$d2)) > 0) 
			$match_val = 5;
		return $match_val;
	}

	private static function two_favorite_tv_shows_14($d1, $d2) {
		$d1 = explode(';', $d1);
		$d2 = explode(';', $d2);
		$d1 = array_map('strtolower', $d1);
		$d2 = array_map('strtolower', $d2);
		$match_val = 0;
		if (count(array_intersect($d1,$d2)) > 0) 
			$match_val = 5;
		return $match_val;
	}

	private static function favorite_movie_15($d1, $d2) {
		$match_val = 0;
		if (strtolower($d1) == strtolower($d2)) 
			$match_val = 5;
		return $match_val;
	}

	private static function location_21($d1, $d2) {
		$match_val = 0;
		if ($d1 == $d2) 
			$match_val = 10;
		return $match_val;
	}

	private static function peer_group_duration_24($d1, $d2) {
		$match_val = 0;
		if ($d1 == $d2) 
			$match_val = 5;
		return $match_val;
	}

	private static function match_all($d1, $d2) {
		return self::education_match_6($d1["education_level"], $d2["education_level"]) + 
			   self::print_match_7($d1["books_mags"], $d2["books_mags"]) + 
			   self::puzzle_match_8($d1["crossword_sudoku"], $d2["crossword_sudoku"]) + 
			   self::look_feel_match_9($d1["feel_look"], $d2["feel_look"]) + 
			   self::humor_match_10($d1["humor_important"], $d2["humor_important"]) + 
			   self::most_impt_match_11($d1["most_important_ABCD"], $d2["most_important_ABCD"]);
			   self::two_favorite_websites_13($d1["fav_websites"], $d2["fav_websites"]);
			   self::two_favorite_tv_shows_14($d1["fav_tv_shows"], $d2["fav_tv_shows"]);
			   self::favorite_movie_15($d1["fav_movie"], $d2["fav_movie"]);
			   self::location_21($d1["peer_member_residence_preference"], $d2["peer_member_residence_preference"]);
			   self::peer_group_duration_24($d1["duration"], $d2["duration"]);
	}

	public static function get_matches($id){
	  $input_id = explode(",", $id);
	  $input_count = count($input_id);
		// Create connection
		$conn = new mysqli(self::$servername, self::$username, self::$password, self::$dbname);
		// Check connection
		if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error . "\n");
		} else {
			//echo("Connection successful\n");
			$sql = "SELECT * FROM wp_nxtchptr_peer_match_forms;";
			$result = mysqli_query($conn, $sql);
			$rows = array();
			if ($result->num_rows > 0) {
				while($row = $result->fetch_assoc()) {
					$rows[$row["id"]] = $row;
				}
				
				$output = array();
				$matches = array();
				foreach ($input_id as $iid) {
					foreach ($rows as $match_id => $data) {
			//			echo(json_encode($data)."\n");
						if ($match_id != $input_id) {
							if (isset($matches[$match_id])) {
								$matches[$match_id] += self::match_all($rows[$iid], $data) / $input_count;
							} else {
								$matches[$match_id] = self::match_all($rows[$iid], $data) / $input_count;
							}
						}
					}
				}
				foreach ($input_id as $iid) {
					if ($matches[$iid]) {
						unset($matches[$iid]);
					}
				}
				foreach($matches as &$match){
					$match = round($match);
				}
				arsort($matches);
				return $matches;
			}
			mysqli_close($conn);
		}
	}
}
?>
