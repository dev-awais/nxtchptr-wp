<?php phpinfo(); exit; ?>
<DOCTYPE html>
<html lang="en">
<head>
  <title>Linkedin Share Link With Image, Choose Picture for Hyperlink Thumbnail, JSON Post Developer, Web Tool, Without Meta Property og: tag Online</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- add jQuery -->
  <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
  <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <!-- add bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

  <!-- user typed js for form -->
  <script>
    $(document).ready(function(){

$("#submit_button").click(function organizeinput(){
  if (IN.User.isAuthorized() == true){
var values = new Array();
//comment, title, description, image-content, image-url
// Get the parameters as an array
values = $(":input").serializeArray();
// Find and replace `content` if there
var countinput=0;
for (index = 0; index < values.length; ++index) 
  {
      if (values[index].name == "comment" && values[index].value != "") 
      {
          var comment;
          comment = values[index].value;
          countinput=countinput+1;
      }
      if (values[index].name == "title" && values[index].value != "") 
      {
          var title;
          title = values[index].value;
          countinput=countinput+1;
      }
      if (values[index].name == "description" && values[index].value != "") 
      {
         var description;
         description = values[index].value;
         countinput=countinput+1;
      }
      if (values[index].name == "image-content" && values[index].value != "") 
      {
          var imagecontent;
          imagecontent = values[index].value;
          countinput=countinput+1;
      }
      if (values[index].name == "image-url" && values[index].value != "") 
      {
          var imageurl;
          imageurl = values[index].value;
          countinput=countinput+1;
      }
  } 

if (title && description && imagecontent && imageurl){
var postcontent = new Array();
postcontent = {"content": {"title": title,"description": description,"submitted-url": imagecontent,"submitted-image_url": imageurl},"visibility": {"code": "anyone"} };
postcontent = JSON.stringify(postcontent);

shareContent(postcontent);
}
else alert("All the fields are required.");
}
else alert("You have to login to linkedin before you can post content.");
});


function onLinkedInLoad() {
    IN.Event.on(IN, "auth", organizeinput);
  }

  // Handle the successful return from the API call
  function onSuccess(data) {
    console.log(data);
    alert("Post Successful!");
  }

  // Handle an error response from the API call
  function onError(error) {
    console.log(error);
    alert("Oh no, something went wrong. Check the console for an error log.");
  }

  // Use the API call wrapper to share content on LinkedIn
  function shareContent(pcontent) {

    IN.API.Raw("/people/~/shares?format=json")
  .method("POST")
  .body(pcontent)
  .result(onSuccess)
  .error(onError);
}

});

  </script>
  
   <!-- initialize LinkedIn JS SDK -->
  <script type="text/javascript" src="//platform.linkedin.com/in.js">
  api_key: 81ow10zv9mnank
  authorize: false
  //onLoad: onLinkedInLoad
  </script>
</head>
<body>
  
<div class="wrap"> 
<h1 align="center">Create An Advanced LinkedIn Post</h1>
<p align="center">Configure a share post for Linkedin. First, authorize through LinkedIn by logging in.</br> Then, fill out all of the fields below and click submit to share the content.</br></br><script type="in/Login"></script></p> <br><br>

<div class="col-md-4"><!--quick spacer :)--></div>
  <div class="col-md-5">
    <form name="post_content" action="" method="post">
      <!--<label for="comment">Comment: </label>
      <input type="text" class="form-control" name="comment" placeholder="Comment"></input><br>-->
      <label for="title">Title: </label>
      <input type="text" class="form-control" name="title" placeholder="Title" required value="On Being & Finding True Pioneers"></input><br>
      <label for="description">Description: </label>
      <input type="text" class="form-control" name="description" placeholder="Description" required value="1. What is your business?

NPX is a for-profit company that seeks to transform how impact is funded in the nonprofit sector and beyond. We invented a financial product called the Impact Security that brings together donors, investors and non-profits to fund impact in a new way.

Today, nonprofit"></input><br>
      <label for="image-content">Link to Content: </label>
      <input type="text" class="form-control" name="image-content" placeholder="http://example.com/content" required value="https://www.nxt-chptr.com/catarina-schwab/"></input><br>
      <label for="image-location">Image Location: </label>
      <input type="text" class="form-control" name="image-url" placeholder="http://example.com/images/example.jpg" required value="https://www.nxt-chptr.com/wp-content/uploads/2018/01/schwab-social.jpg"></input><br><br>
      <input type="button" id="submit_button" value="Submit" class="btn btn-default"></input>
      
    </form>
  </div>
</div>
</div>

<div id="VINoutput"></div>
</body>
</html>
